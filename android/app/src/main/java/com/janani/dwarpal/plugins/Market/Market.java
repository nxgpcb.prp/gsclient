package com.janani.dwarpal.plugins.Market;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import static com.janani.dwarpal.plugins.CustomPluginRequestCodes.MARKET;

@NativePlugin(requestCodes = MARKET)
public class Market extends Plugin {

    public Market() {
    }

    @Override
    public void load() {
        super.load();
    }

    @PluginMethod()
    public void open(PluginCall call) {
        String appId = call.getString("appId");

        if(!appId.equals("")){
            Context context = getActivity().getApplicationContext();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appId));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }else{
            Log.d("Market", "Invalid application id");
        }        

        call.success();
    }

}
