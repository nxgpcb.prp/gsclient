package com.janani.dwarpal.plugins.DwarPushNotifications;

import android.app.AppOpsManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.getcapacitor.Bridge;
import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;
import com.getcapacitor.LogUtils;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginHandle;
import com.getcapacitor.PluginMethod;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import java.lang.reflect.Method;
import java.util.List;

import static com.janani.dwarpal.plugins.CustomPluginRequestCodes.DWAR_PUSH_NOTIFICATIONS;

@NativePlugin(requestCodes = DWAR_PUSH_NOTIFICATIONS)
public class DwarPushNotifications extends Plugin {

  private static final int REQUEST_OVERLAY_PERMISSION = 123;
  public static String CHANNEL_ID = "id";
  public static String CHANNEL_NAME = "name";
  public static String CHANNEL_DESCRIPTION = "description";
  public static String CHANNEL_IMPORTANCE = "importance";
  public static String CHANNEL_VISIBILITY = "visibility";

  public static Bridge staticBridge = null;
  public static RemoteMessage lastMessage = null;
  public NotificationManager notificationManager;
  private static final String DEFAULT_PRESS_ACTION = "tap";


  private static final String EVENT_TOKEN_CHANGE = "registration";
  private static final String EVENT_TOKEN_ERROR = "registrationError";

  public static Context context;

  public void load() {
    notificationManager = (NotificationManager)getActivity()
            .getSystemService(Context.NOTIFICATION_SERVICE);
    staticBridge = this.bridge;
    if (lastMessage != null) {
      fireNotification(lastMessage);
      lastMessage = null;
    }
  }

  @Override
  protected void handleOnNewIntent(Intent data) {
    super.handleOnNewIntent(data);
    context = getContext();
    RemoteMessage remoteMessage = data.getParcelableExtra(Constants.INCOMING_CALL_NOTIFICATION);
    if(remoteMessage!=null && remoteMessage.getData().get("type").equals("Full")){
      if (!Intent.ACTION_MAIN.equals(data.getAction())) {
        return;
      }

      JSObject dataJson = handleFullScreenNotificationActionPerformed(data);
      if (dataJson != null) {
        notifyListeners("fullScreenNotificationActionPerformed", dataJson, true);
      }
    }else{
      Bundle bundle = data.getExtras();
      if(bundle != null && bundle.containsKey("google.message_id")) {
        JSObject notificationJson = new JSObject();
        JSObject dataObject = new JSObject();
        for (String key : bundle.keySet()) {
          if (key.equals("google.message_id")) {
            notificationJson.put("id", bundle.get(key));
          } else {
            Object value = bundle.get(key);
            String valueStr = (value != null) ? value.toString() : null;
            dataObject.put(key, valueStr);
          }
        }
        notificationJson.put("data", dataObject);
        JSObject actionJson = new JSObject();
        actionJson.put("actionId", "tap");
        actionJson.put("notification", notificationJson);
        notifyListeners("dwarPushNotificationsActionPerformed", actionJson, true);
    }
    }
  }

  @Override
  protected void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
    super.handleOnActivityResult(requestCode, resultCode, data);
    this.handleOnNewIntent(data);
  }

  @PluginMethod()
  public void register(PluginCall call) {
    FirebaseMessaging.getInstance().setAutoInitEnabled(true);
    getShowPopupPermission(call);
    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(),  new OnSuccessListener<InstanceIdResult>() {
      @Override
      public void onSuccess(InstanceIdResult instanceIdResult) {
        sendToken(instanceIdResult.getToken());
      }
    });
    FirebaseInstanceId.getInstance().getInstanceId().addOnFailureListener(new OnFailureListener() {
      public void onFailure(Exception e) {
        sendError(e.getLocalizedMessage());
      }
    });
    call.success();
  }

  public void getShowPopupPermission(PluginCall call) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if(!Settings.canDrawOverlays(getContext())){
        Intent intent = new Intent(
                "android.settings.action.MANAGE_OVERLAY_PERMISSION",
                Uri.parse("package:" +getActivity().getPackageName()));
        startActivityForResult(call, intent, REQUEST_OVERLAY_PERMISSION );
      }
    }
  }

  public static boolean canDrawOverlaysUsingReflection(Context context) {

    try {

      AppOpsManager manager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
      Class clazz = AppOpsManager.class;
      Method dispatchMethod = clazz.getMethod("checkOp", new Class[]{int.class, int.class, String.class});
      //AppOpsManager.OP_SYSTEM_ALERT_WINDOW = 24
      int mode = (Integer) dispatchMethod.invoke(manager, new Object[]{24, Binder.getCallingUid(), context.getApplicationContext().getPackageName()});

      return AppOpsManager.MODE_ALLOWED == mode;

    } catch (Exception e) {
      return false;
    }
  }

  @PluginMethod()
  public void getDeliveredNotifications(PluginCall call) {
    call.unimplemented();
  }

  @PluginMethod()
  public void removeDeliveredNotifications(PluginCall call) {
    call.unimplemented();
  }

  @PluginMethod()
  public void removeAllDeliveredNotifications(PluginCall call) {
    notificationManager.cancelAll();
    call.success();
  }

  @PluginMethod()
  public void createChannel(PluginCall call) {
    if (android.os.Build.VERSION.SDK_INT  >= android.os.Build.VERSION_CODES.O) {
      JSObject channel = new JSObject();
      channel.put(CHANNEL_ID, call.getString(CHANNEL_ID));
      channel.put(CHANNEL_NAME, call.getString(CHANNEL_NAME));
      channel.put(CHANNEL_DESCRIPTION, call.getString(CHANNEL_DESCRIPTION, ""));
      channel.put(CHANNEL_VISIBILITY,  call.getInt(CHANNEL_VISIBILITY, NotificationCompat.VISIBILITY_PUBLIC));
      channel.put(CHANNEL_IMPORTANCE, call.getInt(CHANNEL_IMPORTANCE));
      createChannel(channel);
      call.success();
    } else {
      call.unavailable();
    }
  }

  @PluginMethod()
  public void deleteChannel(PluginCall call) {
    if (android.os.Build.VERSION.SDK_INT  >= android.os.Build.VERSION_CODES.O) {
      String channelId = call.getString("id");
      notificationManager.deleteNotificationChannel(channelId);
      call.success();
    } else {
      call.unavailable();
    }
  }

  @PluginMethod()
  public void listChannels(PluginCall call) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      List<NotificationChannel> notificationChannels = notificationManager.getNotificationChannels();
      JSArray channels = new JSArray();
      for (NotificationChannel notificationChannel : notificationChannels) {
        JSObject channel = new JSObject();
        channel.put(CHANNEL_ID, notificationChannel.getId());
        channel.put(CHANNEL_NAME, notificationChannel.getName());
        channel.put(CHANNEL_DESCRIPTION, notificationChannel.getDescription());
        channel.put(CHANNEL_IMPORTANCE, notificationChannel.getImportance());
        channel.put(CHANNEL_VISIBILITY, notificationChannel.getLockscreenVisibility());
        Log.d(getLogTag(), "visibility " + notificationChannel.getLockscreenVisibility());
        Log.d(getLogTag(), "importance " + notificationChannel.getImportance());
        channels.put(channel);
      }
      JSObject result = new JSObject();
      result.put("channels", channels);
      call.success(result);
    } else {
      call.unavailable();
    }
  }

  private void createChannel(JSObject channel) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      NotificationChannel notificationChannelChannel = new NotificationChannel(channel.getString(CHANNEL_ID), channel.getString(CHANNEL_NAME), channel.getInteger(CHANNEL_IMPORTANCE));
      notificationChannelChannel.setDescription(channel.getString(CHANNEL_DESCRIPTION, ""));
      notificationChannelChannel.setLockscreenVisibility(channel.getInteger(CHANNEL_VISIBILITY, 0));
      notificationManager.createNotificationChannel(notificationChannelChannel);
    }
  }

  public void sendToken(String token) {
    JSObject data = new JSObject();
    data.put("value", token);
    notifyListeners(EVENT_TOKEN_CHANGE, data, true);
  }

  public void sendError(String error) {
    JSObject data = new JSObject();
    data.put("error", error);
    notifyListeners(EVENT_TOKEN_ERROR, data, true);
  }

  public static void onNewToken(String newToken) {
    DwarPushNotifications dwarPushPlugin = DwarPushNotifications.getPushNotificationsInstance();
    if (dwarPushPlugin != null) {
      dwarPushPlugin.sendToken(newToken);
    }
  }

  public static void sendRemoteMessage(RemoteMessage remoteMessage) {
    DwarPushNotifications dwarPushPlugin = DwarPushNotifications.getPushNotificationsInstance();
    if (dwarPushPlugin != null) {
      Log.d("dwarPushPlugin", "onMessageReceived: " + remoteMessage);
      dwarPushPlugin.fireNotification(remoteMessage);
    } else {
      Log.d("dwarPushPluginNull", "onMessageReceived: " + remoteMessage);
      lastMessage = remoteMessage;
    }
  }

  public void fireNotification(RemoteMessage remoteMessage) {
    JSObject remoteMessageData = new JSObject();

    JSObject data = new JSObject();
    remoteMessageData.put("id", remoteMessage.getMessageId());
    for (String key : remoteMessage.getData().keySet()) {
      Object value = remoteMessage.getData().get(key);
      data.put(key, value);
    }
    remoteMessageData.put("data", data);

    RemoteMessage.Notification notification = remoteMessage.getNotification();
    if (notification != null) {
      remoteMessageData.put("title", notification.getTitle());
      remoteMessageData.put("body", notification.getBody());
      remoteMessageData.put("click_action", notification.getClickAction());

      Uri link = notification.getLink();
      if (link != null) {
        remoteMessageData.put("link", link.toString());
      }
    }

    notifyListeners("dwarPushNotificationReceived", remoteMessageData, true);
  }

  public static DwarPushNotifications getPushNotificationsInstance() {
    if (staticBridge != null && staticBridge.getWebView() != null) {
      PluginHandle handle = staticBridge.getPlugin("DwarPushNotifications");
      if (handle == null) {
        return null;
      }
      return (DwarPushNotifications) handle.getInstance();
    }
    return null;
  }

  /**
   * Method extecuted when notification is launched by user from the notification bar.
   */
  public JSObject handleFullScreenNotificationActionPerformed(Intent data) {
    Log.d(LogUtils.getPluginTag("LN"), "FullScreenNotification received: " + data.getDataString());
    SoundPoolManager.getInstance(getContext()).playRinging();

    int notificationId = data.getIntExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, Integer.MIN_VALUE);
    if (notificationId == Integer.MIN_VALUE) {
      Log.d(LogUtils.getPluginTag("LN"), "Activity started without notification attached");
      return null;
    }
    JSObject dataJson = new JSObject();
    String menuAction = data.getStringExtra(Constants.ACTION_INTENT_KEY);
    if (menuAction != DEFAULT_PRESS_ACTION) {
      SoundPoolManager.getInstance(getContext()).stopRinging();
      dismissVisibleFullScreenNotification(notificationId);
    }

    RemoteMessage remoteMessage = data.getParcelableExtra(Constants.INCOMING_CALL_NOTIFICATION);
      JSObject notificationJson = new JSObject();
      JSObject dataObject = new JSObject();
      dataObject.put("title", remoteMessage.getData().get("title"));
      dataObject.put("body", remoteMessage.getData().get("body"));
      dataObject.put("id", remoteMessage.getData().get("id"));
      dataObject.put("image", remoteMessage.getData().get("image"));
      dataObject.put("objectType", remoteMessage.getData().get("objectType"));
      dataObject.put("type", remoteMessage.getData().get("type"));
      notificationJson.put("data", dataObject);
      dataJson.put("actionId", menuAction);
      dataJson.put("notification", notificationJson);

    return dataJson;
  }

  private void dismissVisibleFullScreenNotification(int notificationId) {
    getActivity().stopService(new Intent(getContext(), DwarFullNotificationService.class));
    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
    notificationManager.cancel(notificationId);
  }

  public boolean areNotificationsEnabled(){
    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
    return notificationManager.areNotificationsEnabled();
  }

//  public String toString() {
//    return "FullScreenNotification{" +
//            "title='" + title + '\'' +
//            ", body='" + body + '\'' +
//            ", id=" + id +
//            ", sound='" + sound + '\'' +
//            ", smallIcon='" + smallIcon + '\'' +
//            ", image='" + smallIcon + '\'' +
//            ", extra=" + extra +
//            ", fullScreenNotificationActions=" + fullScreenNotificationActions +
//            '}';
//  }
}
