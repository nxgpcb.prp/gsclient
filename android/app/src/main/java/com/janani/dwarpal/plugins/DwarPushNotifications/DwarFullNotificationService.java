package com.janani.dwarpal.plugins.DwarPushNotifications;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.janani.dwarpal.MainActivity;
import com.janani.dwarpal.R;


public class DwarFullNotificationService extends Service {
    private static final String TAG = DwarFullNotificationService.class.getSimpleName();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();

        if (action != null) {
            int notificationId = intent.getIntExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, 0);
            RemoteMessage remoteMessage = intent.getParcelableExtra(Constants.INCOMING_CALL_NOTIFICATION);

            switch (action) {
                case Constants.ACTION_INCOMING_CALL:
                    handleIncomingCall(notificationId, remoteMessage);
                    break;
                case "APPROVE":
                    //approve(notificationId, remoteMessage);
                    break;
                case "REJECT":
                    //reject(notificationId, remoteMessage);
                    break;
                case "HOLD":
                    //hold(notificationId, remoteMessage);
                    break;
                default:
                    break;
            }
        }
        return START_NOT_STICKY;
    }

    private void approve(int notificationId, RemoteMessage remoteMessage){
        SoundPoolManager.getInstance(this).stopRinging();
        dismissVisibleFullScreenNotification(notificationId);
    }

    private void reject(int notificationId, RemoteMessage remoteMessage){
        SoundPoolManager.getInstance(this).stopRinging();
        dismissVisibleFullScreenNotification(notificationId);
    }

    private void hold(int notificationId, RemoteMessage remoteMessage){
        SoundPoolManager.getInstance(this).stopRinging();
        dismissVisibleFullScreenNotification(notificationId);
    }

    private void dismissVisibleFullScreenNotification(int notificationId) {
        stopService(new Intent(this, this.getClass()));
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.cancel(notificationId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Notification createNotification(int notificationId, RemoteMessage remoteMessage, int channelImportance) {
        Intent intent = new Intent(this, VoiceActivity.class);
        intent.setAction(Constants.ACTION_INCOMING_CALL_NOTIFICATION);
        intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, notificationId);
        intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        /*
         * Pass the notification id and call sid to use as an identifier to cancel the
         * notification later
         */
        Bundle extras = new Bundle();
        //extras.putString(Constants.CALL_SID_KEY, callInvite.getCallSid());

        if (Build.VERSION.SDK_INT >= VERSION_CODES.O) {
            return buildNotification(remoteMessage,
                    pendingIntent,
                    extras,
                    notificationId,
                    createChannel(channelImportance));
        } else {
            //noinspection deprecation
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_action_name)
                    .setContentTitle(remoteMessage.getData().get("title"))
                    .setContentText(remoteMessage.getData().get("body"))
                    .setAutoCancel(true)
                    .setExtras(extras)
                    .setContentIntent(pendingIntent)
                    .setGroup("dwarpal_app_notification")
                    .setColor(Color.rgb(214, 10, 37));

            createActionIntents(remoteMessage, notificationId, builder);
            return builder.build();
        }
    }

    @TargetApi(VERSION_CODES.O)
    private Notification buildNotification(RemoteMessage remoteMessage, PendingIntent pendingIntent, Bundle extras,
                                           int notificationId,
                                           String channelId) {
        Notification.Builder builder =
                new Notification.Builder(getApplicationContext(), channelId)
                        .setSmallIcon(R.drawable.ic_action_name)
                        .setContentTitle(remoteMessage.getData().get("title"))
                        .setContentText(remoteMessage.getData().get("body"))
                        .setCategory(Notification.CATEGORY_CALL)
                        .setFullScreenIntent(pendingIntent, true)
                        .setExtras(extras)
                        .setAutoCancel(true)
                        .setFullScreenIntent(pendingIntent, true);
        createActionIntents(remoteMessage, notificationId, builder);
        return builder.build();
    }

    private Notification.Builder createActionIntents(RemoteMessage remoteMessage, int notificationId, Notification.Builder builder){
        String[] actions = new String[]{"APPROVE","HOLD","REJECT"};
        for(int i=0; i<actions.length; i++){
            Intent intent = null;
            if(DwarPushNotifications.getPushNotificationsInstance() == null) {
                intent = new Intent(getApplicationContext(), MainActivity.class);
            }else {
                intent = new Intent(DwarPushNotifications.context, DwarPushNotifications.context.getClass());
            }
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, notificationId);
            intent.putExtra(Constants.ACTION_INTENT_KEY, actions[i]);
            intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
            PendingIntent actionPendingIntent = PendingIntent.getActivity(this, i+"".hashCode(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
            Notification.Action.Builder actionBuilder = new Notification.Action.Builder(com.getcapacitor.android.R.drawable.ic_transparent, actions[i], actionPendingIntent);
            builder.addAction(actionBuilder.build());
        }
        return builder;
    }

    private NotificationCompat.Builder createActionIntents(RemoteMessage remoteMessage, int notificationId, NotificationCompat.Builder builder){
        String[] actions = new String[]{"APPROVE","HOLD","REJECT"};
        for(int i=0; i<actions.length; i++) {
            Intent intent = null;
            if(DwarPushNotifications.getPushNotificationsInstance() == null) {
                intent = new Intent(getApplicationContext(), MainActivity.class);
            }else {
                intent = new Intent(DwarPushNotifications.context, DwarPushNotifications.context.getClass());
            }
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, notificationId);
            intent.putExtra(Constants.ACTION_INTENT_KEY, actions[i]);
            intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
            PendingIntent actionPendingIntent = PendingIntent.getActivity(this, i + "".hashCode(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
            NotificationCompat.Action.Builder actionBuilder = new NotificationCompat.Action.Builder(com.getcapacitor.android.R.drawable.ic_transparent, actions[i], actionPendingIntent);
            builder.addAction(actionBuilder.build());
        }
        return builder;
    }

    @TargetApi(VERSION_CODES.O)
    private String createChannel(int channelImportance) {
        NotificationChannel callInviteChannel = new NotificationChannel(Constants.VOICE_CHANNEL_HIGH_IMPORTANCE,
                "Primary Voice Channel", NotificationManager.IMPORTANCE_HIGH);
        String channelId = Constants.VOICE_CHANNEL_HIGH_IMPORTANCE;

        if (channelImportance == NotificationManager.IMPORTANCE_LOW) {
            callInviteChannel = new NotificationChannel(Constants.VOICE_CHANNEL_LOW_IMPORTANCE,
                    "Primary Voice Channel", NotificationManager.IMPORTANCE_LOW);
            channelId = Constants.VOICE_CHANNEL_LOW_IMPORTANCE;
        }
        callInviteChannel.setLightColor(Color.GREEN);
        callInviteChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(callInviteChannel);

        return channelId;
    }

    private void handleIncomingCall(int notificationId, RemoteMessage remoteMessage) {
        if (Build.VERSION.SDK_INT >= VERSION_CODES.O) {
            setCallInProgressNotification(notificationId, remoteMessage);
        }
        sendCallInviteToActivity(notificationId, remoteMessage);
    }

    private void endForeground() {
        stopForeground(true);
    }

    @TargetApi(VERSION_CODES.O)
    private void setCallInProgressNotification(int notificationId, RemoteMessage remoteMessage) {
        if (isAppVisible()) {
            Log.i(TAG, "setCallInProgressNotification - app is visible.");
            startForeground(notificationId, createNotification(notificationId, remoteMessage, NotificationManager.IMPORTANCE_LOW));
        } else {
            Log.i(TAG, "setCallInProgressNotification - app is NOT visible.");
            startForeground(notificationId, createNotification(notificationId, remoteMessage, NotificationManager.IMPORTANCE_HIGH));
        }
    }

    /*
     * Send the CallInvite to the VoiceActivity. Start the activity if it is not running already.
     */
    private void sendCallInviteToActivity(int notificationId, RemoteMessage remoteMessage) {
        if (Build.VERSION.SDK_INT > 29 && !isAppVisible()) {
            return;
        }
        Intent intent = new Intent(this, VoiceActivity.class);
        intent.setAction(Constants.ACTION_INCOMING_CALL);
        intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, notificationId);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
        this.startActivity(intent);
    }

    private boolean isAppVisible() {
        return ProcessLifecycleOwner
                .get()
                .getLifecycle()
                .getCurrentState()
                .isAtLeast(Lifecycle.State.STARTED);
    }
}