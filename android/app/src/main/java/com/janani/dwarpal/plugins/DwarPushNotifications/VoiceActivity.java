package com.janani.dwarpal.plugins.DwarPushNotifications;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.messaging.RemoteMessage;
import com.janani.dwarpal.MainActivity;
import com.janani.dwarpal.R;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class VoiceActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private Button mAcceptButton;
    private Button mHoldButton;
    private Button mRejectButton;
    private TextView mTitle;
    private TextView mBody;
    private ImageView mImageView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
//            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
//                    | View.SYSTEM_UI_FLAG_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Draw over lock screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        Log.d("VoiceActivity", "onCreate: VoiceActivity");
        setContentView(R.layout.activity_voice);

        SoundPoolManager.getInstance(VoiceActivity.this).playRinging();
        RemoteMessage remoteMessage = getIntent().getParcelableExtra(com.janani.dwarpal.plugins.DwarPushNotifications.Constants.INCOMING_CALL_NOTIFICATION);

        mVisible = true;

        mTitle = findViewById(R.id.textView);
        mTitle.setText(remoteMessage.getData().get("title"));
        mBody = findViewById(R.id.textView3);
        mBody.setText(remoteMessage.getData().get("body"));
        mImageView = findViewById(R.id.imageView);
        String base64Image = remoteMessage.getData().get("image");
        if(base64Image!=null){
            byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            mImageView.setImageBitmap(decodedByte);
        }

        mAcceptButton = findViewById(R.id.accept_activity_voice_button);
        mAcceptButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                toggle();
                //Need to get activity and context correctly
                Intent intent = null;
                if(DwarPushNotifications.getPushNotificationsInstance() == null) {
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                }else {
                    intent = new Intent(DwarPushNotifications.context, DwarPushNotifications.context.getClass());
                }
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, Integer.valueOf(remoteMessage.getData().get("id")));
                intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
                intent.putExtra(Constants.ACTION_INTENT_KEY, "APPROVE");
                startActivity(intent);
                SoundPoolManager.getInstance(VoiceActivity.this).stopRinging();
                finish();
            }
        });

        mHoldButton = findViewById(R.id.hold_activity_voice_button);
        mHoldButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                toggle();
                Intent intent = null;
                if(DwarPushNotifications.getPushNotificationsInstance() == null) {
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                }else {
                    intent = new Intent(DwarPushNotifications.context, DwarPushNotifications.context.getClass());
                }
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, Integer.valueOf(remoteMessage.getData().get("id")));
                intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
                intent.putExtra(Constants.ACTION_INTENT_KEY, "HOLD");
                startActivity(intent);
                SoundPoolManager.getInstance(VoiceActivity.this).stopRinging();
                finish();
            }
        });

        mRejectButton = findViewById(R.id.reject_activity_voice_button);
        mRejectButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                toggle();
                Intent intent = null;
                if(DwarPushNotifications.getPushNotificationsInstance() == null) {
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                }else {
                    intent = new Intent(DwarPushNotifications.context, DwarPushNotifications.context.getClass());
                }
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, Integer.valueOf(remoteMessage.getData().get("id")));
                intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
                intent.putExtra(Constants.ACTION_INTENT_KEY, "REJECT");
                startActivity(intent);
                SoundPoolManager.getInstance(VoiceActivity.this).stopRinging();
                finish();
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                finish();
            }
        }, 30000);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        //mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
//        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //registerReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver();
    }

    @Override
    public void onDestroy() {
        /*
         * Tear down audio device management and restore previous volume stream
         */
        SoundPoolManager.getInstance(this).release();
        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
                || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP
                || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN
                || event.getKeyCode() == KeyEvent.KEYCODE_CAMERA
                || event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
            Log.i("Key", "keycode " + event.getKeyCode());
            finish();
        }
        return super.dispatchKeyEvent(event);
    }
}
