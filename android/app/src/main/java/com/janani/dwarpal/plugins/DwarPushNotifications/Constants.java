package com.janani.dwarpal.plugins.DwarPushNotifications;

public class Constants {
    public static final String VOICE_CHANNEL_LOW_IMPORTANCE = "notification-channel-low-importance";
    public static final String VOICE_CHANNEL_HIGH_IMPORTANCE = "notification-channel-high-importance";
    public static final String INCOMING_CALL_NOTIFICATION_ID = "INCOMING_CALL_NOTIFICATION_ID";
    public static final String ACTION_ACCEPT = "ACTION_ACCEPT";
    public static final String ACTION_REJECT = "ACTION_REJECT";
    public static final String ACTION_INCOMING_CALL_NOTIFICATION = "ACTION_INCOMING_CALL_NOTIFICATION";
    public static final String ACTION_INCOMING_CALL = "ACTION_INCOMING_CALL";
    public static final String ACTION_CANCEL_CALL = "ACTION_CANCEL_CALL";
    public static final String INCOMING_CALL_NOTIFICATION = "INCOMING_CALL_NOTIFICATION";
    public static final String ACTION_INTENT_KEY = "ACTION_INTENT_KEY";
    public static final String FULLSCREEN_NOTIFICATION_OBJ_INTENT_KEY = "FULLSCREEN_NOTIFICATION_OBJ_INTENT_KEY";
}