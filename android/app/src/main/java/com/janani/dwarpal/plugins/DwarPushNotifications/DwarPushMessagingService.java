package com.janani.dwarpal.plugins.DwarPushNotifications;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class DwarPushMessagingService extends FirebaseMessagingService {

  @Override
  public void onNewToken(String newToken) {
    super.onNewToken(newToken);
    DwarPushNotifications.onNewToken(newToken);
  }

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {
    super.onMessageReceived(remoteMessage);
    if(remoteMessage.getData().get("type").equals("Full")){
      final int notificationId = (int) System.currentTimeMillis();
      handleInvite(notificationId, remoteMessage);

    }else {
      DwarPushNotifications.sendRemoteMessage(remoteMessage);
    }
  }

  private void handleInvite(int notificationId, RemoteMessage remoteMessage) {
    Intent intent = new Intent(this, DwarFullNotificationService.class);
    intent.setAction(Constants.ACTION_INCOMING_CALL);
    intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, notificationId);
    intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
    SoundPoolManager.getInstance(this).playRinging();
    startService(intent);
  }

  private void handleCanceledCallInvite() {
    Intent intent = new Intent(this, DwarFullNotificationService.class);
    intent.setAction(Constants.ACTION_CANCEL_CALL);
    startService(intent);
  }
}