package com.janani.dwarpal.plugins.UPI;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.janani.dwarpal.plugins.CustomPluginRequestCodes.UPI_PAYMENT;

@NativePlugin(requestCodes = UPI_PAYMENT)
public class UPI extends Plugin {

    public UPI() {
    }

    @Override
    public void load() {
        super.load();
    }

    @PluginMethod()
    public void pay(PluginCall call) {
        String amount = call.getString("amount");
        String upiId = call.getString("upiId");
        String name = call.getString("name");
        String note = call.getString("note");

        // More code here...
        Uri uri = Uri.parse("upi://pay").buildUpon()
                .appendQueryParameter("pa", upiId)
                .appendQueryParameter("pn", name)
                .appendQueryParameter("tn", note)
                .appendQueryParameter("am", amount)
                .appendQueryParameter("cu", "INR")
                .build();


        Intent upiPayIntent = new Intent(Intent.ACTION_VIEW);
        upiPayIntent.setData(uri);

        // will always show a dialog to user to choose an app
        Intent chooser = Intent.createChooser(upiPayIntent, "Pay with");

        // check if intent resolves
        if(null != chooser.resolveActivity(this.getActivity().getPackageManager())) {
            this.getActivity().startActivityForResult(chooser, UPI_PAYMENT);
        } else {
            Toast.makeText(this.getActivity(),"No UPI app found, please install one to continue",Toast.LENGTH_SHORT).show();
        }
        call.success();
    }

    // in order to handle the intents result, you have to @Override handleOnActivityResult
    @Override
    protected void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        super.handleOnActivityResult(requestCode, resultCode, data);

        // Get the previously saved call
        PluginCall savedCall = getSavedCall();

        Log.d("UPI", "requestCode: " + requestCode);
        switch (requestCode) {
            case UPI_PAYMENT:
                if ((RESULT_OK == resultCode) || (resultCode == 11)) {
                    if (data != null) {
                        String trxt = data.getStringExtra("response");
                        Log.d("UPI", "onActivityResult: " + trxt);
                        ArrayList<String> dataList = new ArrayList<>();
                        dataList.add(trxt);
                        upiPaymentDataOperation(dataList);
                    } else {
                        Log.d("UPI", "onActivityResult: " + "Return data is null");
                        ArrayList<String> dataList = new ArrayList<>();
                        dataList.add("nothing");
                        upiPaymentDataOperation(dataList);
                    }
                } else {
                    Log.d("UPI", "onActivityResult: " + "Return data is null"); //when user simply back without payment
                    ArrayList<String> dataList = new ArrayList<>();
                    dataList.add("nothing");
                    upiPaymentDataOperation(dataList);
                }
                break;
        }
    }

    private void upiPaymentDataOperation(ArrayList<String> data) {
        String str = data.get(0);
        Log.d("UPIPAY", "upiPaymentDataOperation: "+str);
        String paymentCancel = "";
        if(str == null) str = "discard";
        String status = "";
        String approvalRefNo = "";
        String response[] = str.split("&");
        for (int i = 0; i < response.length; i++) {
            String equalStr[] = response[i].split("=");
            if(equalStr.length >= 2) {
                if (equalStr[0].toLowerCase().equals("Status".toLowerCase())) {
                    status = equalStr[1].toLowerCase();
                }
                else if (equalStr[0].toLowerCase().equals("ApprovalRefNo".toLowerCase()) || equalStr[0].toLowerCase().equals("txnRef".toLowerCase())) {
                    approvalRefNo = equalStr[1];
                }
            }
            else {
                paymentCancel = "Payment cancelled by user.";
            }
        }

        if (status.equals("success")) {
            //Code to handle successful transaction here.
            //Toast.makeText(getActivity(), "Transaction successful.", Toast.LENGTH_SHORT).show();
            JSObject ret = new JSObject();
            ret.put("response", approvalRefNo);
            notifyListeners("onTransactionSuccess", ret, true);
            Log.d("FullScreenNotifications", "responseStr: "+approvalRefNo);
        }
        else if("Payment cancelled by user.".equals(paymentCancel)) {
            //Toast.makeText(getActivity(), "Payment cancelled by user.", Toast.LENGTH_SHORT).show();
            JSObject ret = new JSObject();
            ret.put("error", "Payment cancelled by user");
            notifyListeners("onTransactionCancelled", ret, true);
        }
        else {
            //Toast.makeText(getActivity(), "Transaction failed.Please try again", Toast.LENGTH_SHORT).show();
            JSObject ret = new JSObject();
            ret.put("error", "Transaction failed. Please try again");
            notifyListeners("onTransactionFailed", ret, true);
        }

    }

    public static boolean isConnectionAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()
                    && netInfo.isConnectedOrConnecting()
                    && netInfo.isAvailable()) {
                return true;
            }
        }
        return false;
    }

    @PluginMethod()
    public void customFunction(PluginCall call) {
        // More code here...
        call.resolve();
    }
}
