package com.janani.dwarpal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.getcapacitor.BridgeActivity;
import com.getcapacitor.JSObject;
import com.getcapacitor.LogUtils;
import com.getcapacitor.Plugin;
import com.google.firebase.messaging.RemoteMessage;
import com.janani.dwarpal.plugins.DwarPushNotifications.Constants;
import com.janani.dwarpal.plugins.DwarPushNotifications.DwarPushNotifications;
import com.janani.dwarpal.plugins.DwarPushNotifications.SoundPoolManager;
import com.janani.dwarpal.plugins.UPI.UPI;
import com.janani.dwarpal.plugins.Market.Market;

import java.util.ArrayList;

import app.xplatform.capacitor.plugins.AdMob;


public class MainActivity extends BridgeActivity {
    private static final String DEFAULT_PRESS_ACTION = "tap";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initializes the Bridge
        this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
            // Additional plugins you've installed go here
            // Ex: add(TotallyAwesomePlugin.class);
            add(AdMob.class);
            add(UPI.class);
            add(DwarPushNotifications.class);
            add(Market.class);
        }});

        handleFullScreenNotificationActionPerformed(getIntent());

    }

    private void dismissVisibleFullScreenNotification(int notificationId) {
        stopService(new Intent(this, this.getClass()));
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.cancel(notificationId);
    }

    public JSObject handleFullScreenNotificationActionPerformed(Intent data) {
        int notificationId = data.getIntExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, Integer.MIN_VALUE);
        if (notificationId == Integer.MIN_VALUE) {
            Log.d(LogUtils.getPluginTag("LN"), "Activity started without notification attached");
            return null;
        }
        JSObject dataJson = new JSObject();
        String menuAction = data.getStringExtra(Constants.ACTION_INTENT_KEY);
        if (menuAction != DEFAULT_PRESS_ACTION) {
            SoundPoolManager.getInstance(this).stopRinging();
            dismissVisibleFullScreenNotification(notificationId);
        }

        RemoteMessage remoteMessage = data.getParcelableExtra(Constants.INCOMING_CALL_NOTIFICATION);
        JSObject notificationJson = new JSObject();
        JSObject dataObject = new JSObject();
        dataObject.put("title", remoteMessage.getData().get("title"));
        dataObject.put("body", remoteMessage.getData().get("body"));
        dataObject.put("id", remoteMessage.getData().get("id"));
        dataObject.put("image", remoteMessage.getData().get("image"));
        dataObject.put("objectType", remoteMessage.getData().get("objectType"));
        dataObject.put("type", remoteMessage.getData().get("type"));
        notificationJson.put("extra", dataObject);
        dataJson.put("actionId", menuAction);
        dataJson.put("notification", notificationJson);

        return dataJson;
    }

}
