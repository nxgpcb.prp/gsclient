package com.janani.dwarpal.plugins.DwarPushNotifications;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        DwarPushNotifications dwarPushNotifications = DwarPushNotifications.getPushNotificationsInstance();

        // Explicitly specify that GcmIntentService will handle the intent.
        ComponentName comp = new ComponentName(context.getPackageName(),
                DwarFullNotificationService.class.getName());
        Bundle extras = intent.getExtras();
        Log.d("Dwar", "onReceive: " + extras.get("title"));
        Log.d("Dwar", "onReceive: " + extras.get("body"));
        Log.d("Dwar", "onReceive: " + extras.get("id"));
        Log.d("Dwar", "onReceive: " + extras.get("image"));
        Log.d("Dwar", "onReceive: " + extras.get("objectType"));

        RemoteMessage remoteMessage = new RemoteMessage(extras);

        if (dwarPushNotifications != null) {
            Log.d("dwarPushPlugin", "onMessageReceived: " + remoteMessage);
            dwarPushNotifications.fireNotification(remoteMessage);
        } else {
            Log.d("dwarPushPluginNull", "onMessageReceived: " + remoteMessage);
            dwarPushNotifications.lastMessage = remoteMessage;
        }

        if(extras.get("type").equals("Full")) {
            intent.setComponent(comp);
            intent.setAction(Constants.ACTION_INCOMING_CALL);
            intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, Integer.valueOf(extras.get("id").toString()));
            intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION, remoteMessage);
            SoundPoolManager.getInstance(context).playRinging();
            startWakefulService(context, intent);
        }
        setResultCode(Activity.RESULT_OK);
    }
}
