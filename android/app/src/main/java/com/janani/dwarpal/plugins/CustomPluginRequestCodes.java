package com.janani.dwarpal.plugins;

public class CustomPluginRequestCodes {
    public static final int UPI_PAYMENT = 10001;
    public static final int FULLSCREEN_NOTIFICATION=10002;
    public static final int DWAR_PUSH_NOTIFICATIONS=10003;
    public static final int MARKET = 10004;
}
