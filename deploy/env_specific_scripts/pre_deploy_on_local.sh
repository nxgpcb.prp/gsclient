RETURN_DIR=$PWD
cd ./www/
zip -r ../ROOT.war ./* 
cd $RETURN_DIR
mkdir -p $DEPLOY_ENVIRONMENT/webroot/
cp -fr ./ROOT.war $DEPLOY_ENVIRONMENT/webroot/
mkdir -p $DEPLOY_ENVIRONMENT/webroot/downloads/
cp -fr $(find ./android/app/build/outputs/apk/ -name "*.apk" ) $DEPLOY_ENVIRONMENT/webroot/downloads/
mv $DEPLOY_ENVIRONMENT/webroot/downloads/app-debug.apk $DEPLOY_ENVIRONMENT/webroot/downloads/app-debug-$CI_PIPELINE_IID-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA.apk
# app-release-unsigned-$CI_COMMIT_TAG-106-staging-4cdaseed