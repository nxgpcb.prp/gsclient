#!/bin/bash
set -e
set +x

DEPLOY_ENVIRONMENT=$1
DEPLOY_ENVIRONMENT_private_key="$(eval "echo \"\$${DEPLOY_ENVIRONMENT}_private_key"\")"
if [  -z "$DEPLOY_ENVIRONMENT_private_key" ]
then
	echo "    not defined : ${DEPLOY_ENVIRONMENT}_private_key"
	exit 1
fi

# ** Alternative approach

#ls /
#
#echo $USER
#ls /home
#ls /root/
#mkdir /root/.ssh/
#echo -e "$DEPLOY_ENVIRONMENT_private_key" > /root/.ssh/id_rsa
#chmod 600 -R /root/.ssh
# ** End of alternative approach

eval $(ssh-agent -s)
echo  "$DEPLOY_ENVIRONMENT_private_key" | tr -d '\r' | ssh-add - > /dev/null

mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

if [  -z "$DEPLOY_ENVIRONMENT" ]
then
	echo "    not defined : please specify environment name for deploy/deploy.sh as first arg"
	exit 1
fi
echo "GS> preparing deploy package" 
source $(dirname $(readlink "$0" -f))/init.sh $DEPLOY_ENVIRONMENT ||  exit 1
source $(dirname $(readlink "$0" -f))/../env_specific_scripts/pre_deploy_on_local.sh $DEPLOY_ENVIRONMENT ||  exit 1

echo "GS> deploying package ./$DEPLOY_ENVIRONMENT " 
DEPLOY_ENVIRONMENT_servers="$(eval "echo \$${DEPLOY_ENVIRONMENT}_servers")"
if [  -z "$DEPLOY_ENVIRONMENT_servers" ]
then
	echo "not defined : ${DEPLOY_ENVIRONMENT}_servers"
	exit 1
fi


chmod 700 "$(dirname $(readlink "$0" -f))/../env_specific_scripts/post_deploy_on_remote.sh"

for server in $DEPLOY_ENVIRONMENT_servers
do
    echo "    deploying to $server "
    scp -o StrictHostKeyChecking=no  -pr ./$DEPLOY_ENVIRONMENT/* $server:~/
	scp -o StrictHostKeyChecking=no -p "$(dirname $(readlink "$0" -f))/../env_specific_scripts/post_deploy_on_remote.sh" $server:~/
	ssh -o StrictHostKeyChecking=no $server "~/post_deploy_on_remote.sh"
done



