#!/bin/bash
set -e
set +x

DEPLOY_ENVIRONMENT=$1

if [  -z "$DEPLOY_ENVIRONMENT" ]
then
	echo "    not defined : please specify environment name for deploy/common_scripts/prebuild.sh as first arg"
	exit 1
fi
echo "GS> preparing deploy package" 
source $(dirname $(readlink "$0" -f))/init.sh $DEPLOY_ENVIRONMENT ||  exit 1
source $(dirname $(readlink "$0" -f))/../env_specific_scripts/pre_build_on_local.sh $DEPLOY_ENVIRONMENT ||  exit 1