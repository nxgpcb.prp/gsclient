set -e
set +x
DEPLOY_ENVIRONMENT=$1

# functions
detemplate()
{
	return_value=-1
	new_file_name="$(echo "$1" | sed "s|/$||" | grep -o "[^/]*$")"
	for variable in $( if [ -f "$1" ]; then grep -o "{{[a-zA-Z0-9_][a-zA-Z0-9_]*}}" $1 |  sed "s|{{||g;s|}}||g"; fi; echo "$new_file_name" | grep -o "{{[a-zA-Z0-9_][a-zA-Z0-9_]*}}" |  sed "s|{{||g;s|}}||g" )
	do
		return_value=0
		variable_value="$(eval "echo \$${DEPLOY_ENVIRONMENT}_$variable" | sed "s|\\\|\\\\\\\|g")"
		
			
			if [ ! -z "$variable_value" ]
			then
				if [ -f "$1" ]
				then
					sed -i  "s|{{$variable}}|$variable_value|g" $1
				fi
				new_file_name="$(echo $new_file_name | sed "s|{{$variable}}|$variable_value|g" )"
			else
					echo "    not defined : ${DEPLOY_ENVIRONMENT}_$variable"
					return_value=2
			fi
	done
	
	if [ "$return_value" -eq 0 ]
	then
		#echo mv "$1" "$(echo "$1" | sed "s|/$||" | sed  "s|/[^/]*$||")/$new_file_name"
		old_path="$1" 
		new_path="$(echo "$1" | sed "s|/$||" | sed  "s|/[^/]*$||")/$new_file_name"
		
		#echo  if [ "old_path"  != "$new_path" ]
		if [ "$old_path"  != "$new_path" ]
		then
			mv  "$old_path" "$new_path"
		fi
	fi
	
	if [ $return_value -eq -1 ]
	then
		return_value=0
	fi
    return $return_value
}

detempatefiles()
{
	local return_dir="$PWD";
	cd $1
		local is_fail=0
		for file in $(find | tac)
		do
			detemplate $file || local is_fail=1
		done
	cd $return_dir
	[ $is_fail -eq 0 ] || return $is_fail
}

mergefiles()
{
	local return_dir="$PWD";
	local full_path="$(readlink "$1" -f)"
	cd $full_path
		for folder in $(find -type d| tac | grep ".unzip$");
		do
			local zip_file="$(echo $folder | sed "s|/$||"  | sed "s|.unzip$||" )";
			cd $folder;
			zip -r $full_path/$zip_file ./* 1>/dev/null
			cd $full_path
			rm -fr $folder
		done
	cd $return_dir
}




