importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');

firebase.initializeApp({
    'apiKey': "AIzaSyCUr8FnsCKspzk3-Xr2_leA9VmInoRt-Ck",
    'authDomain': "dwarpal-26012020.firebaseapp.com",
    'databaseURL': "https://dwarpal-26012020.firebaseio.com",
    'projectId': "dwarpal-26012020",
    'storageBucket': "dwarpal-26012020.appspot.com",
    'messagingSenderId': "194774147946",
    'appId': "1:194774147946:web:bc6b0d7746387d3151bb13",
    measurementId: "G-470RX820GB"
});

const messaging = firebase.messaging();
