export const environment = {
  production: true,
  debug: false,
  domain: "https://dwarpal.co.in",
  backend: {
    baseURL:"https://dwarpal.co.in"
  },
  firebase: {
    apiKey: "AIzaSyCUr8FnsCKspzk3-Xr2_leA9VmInoRt-Ck",
    authDomain: "dwarpal-26012020.firebaseapp.com",
    databaseURL: "https://dwarpal-26012020.firebaseio.com",
    projectId: "dwarpal-26012020",
    storageBucket: "dwarpal-26012020.appspot.com",
    messagingSenderId: "194774147946",
    appId: "1:194774147946:web:bc6b0d7746387d3151bb13",
    measurementId: "G-470RX820GB"
  },
  webClientId : '194774147946-kp11fcamv00qucsjea38aq6b5tkt121t.apps.googleusercontent.com',
  admob: {
    android: {
      appId: "ca-app-pub-3869986147080543~1797045987",
      bannerAddId: "ca-app-pub-3869986147080543/6421605446"
    },
    ios: {
      appId: "ca-app-pub-3869986147080543~1600761282",
      bannerAddId: "ca-app-pub-3869986147080543/4965291225"
    }
  }
};
