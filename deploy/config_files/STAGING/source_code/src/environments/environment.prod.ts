export const environment = {
  production: true,
  debug: true,
  domain: "https://staging.dwarpal.co.in",
  backend: {
    baseURL:"https://staging.dwarpal.co.in"
  },
  firebase: {
    apiKey: "AIzaSyC9EVbbOLnPRZWnpXDsbFz9mYXUXkagUWI",
    authDomain: "staging-dwarpal.firebaseapp.com",
    databaseURL: "https://staging-dwarpal.firebaseio.com",
    projectId: "staging-dwarpal",
    storageBucket: "staging-dwarpal.appspot.com",
    messagingSenderId: "93105934779",
    appId: "1:93105934779:web:dce023a659aac70ce7749e",
    measurementId: "G-N48KWV3T3J"
  },
  webClientId : '93105934779-ifa3581dilnfmh2nloh24i5ndffvbkm9.apps.googleusercontent.com',
  admob: {
    android: {
      appId: "ca-app-pub-3869986147080543~1797045987",
      bannerAddId: "ca-app-pub-3869986147080543/6421605446"
    },
    ios: {
      appId: "ca-app-pub-3869986147080543~1600761282",
      bannerAddId: "ca-app-pub-3869986147080543/4965291225"
    }
  }
};
