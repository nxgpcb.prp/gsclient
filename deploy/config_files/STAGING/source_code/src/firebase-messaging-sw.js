importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');

firebase.initializeApp({
    'apiKey': "AIzaSyC9EVbbOLnPRZWnpXDsbFz9mYXUXkagUWI",
    'authDomain': "staging-dwarpal.firebaseapp.com",
    'databaseURL': "https://staging-dwarpal.firebaseio.com",
    'projectId': "staging-dwarpal",
    'storageBucket': "staging-dwarpal.appspot.com",
    'messagingSenderId': "93105934779",
    'appId': "1:93105934779:web:dce023a659aac70ce7749e",
    "measurementId": "G-N48KWV3T3J"
});

const messaging = firebase.messaging();
