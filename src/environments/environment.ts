// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  debug: true,
  domain: "http://localhost:8100",
  backend: {
    baseURL:"http://localhost:8181"
  },
  firebase: {
    apiKey: "AIzaSyAsMIJNFIzJd48Qx3ODn-DZOm5JfF_1duw",
    authDomain: "messit-745b6.firebaseapp.com",
    databaseURL: "https://messit-745b6.firebaseio.com",
    projectId: "messit-745b6",
    storageBucket: "messit-745b6.appspot.com",
    messagingSenderId: "121983096204",
    appId: "1:121983096204:web:0d93d16fdc5911c9c0b2a9"
  },
  webClientId : '121983096204-pbrebqe6ohul66dljtajhbgfral94j59.apps.googleusercontent.com',
  admob: {
    android: {
      appId: "ca-app-pub-3869986147080543~1797045987",
      bannerAddId: "ca-app-pub-3869986147080543/6421605446"
    },
    ios: {
      appId: "ca-app-pub-3869986147080543~1600761282",
      bannerAddId: "ca-app-pub-3869986147080543/4965291225"
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
