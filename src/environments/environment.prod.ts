export const environment = {
  production: true,
  debug: true,
  domain: "http://localhost:8100",
  backend: {
    baseURL:"http://localhost:8181"
  },
  firebase: {
    apiKey: "AIzaSyAsMIJNFIzJd48Qx3ODn-DZOm5JfF_1duw",
    authDomain: "messit-745b6.firebaseapp.com",
    databaseURL: "https://messit-745b6.firebaseio.com",
    projectId: "messit-745b6",
    storageBucket: "messit-745b6.appspot.com",
    messagingSenderId: "121983096204",
    appId: "1:121983096204:web:0d93d16fdc5911c9c0b2a9"
  },
  webClientId : '121983096204-pbrebqe6ohul66dljtajhbgfral94j59.apps.googleusercontent.com',
  admob: {
    android: {
      appId: "ca-app-pub-3869986147080543~1797045987",
      bannerAddId: "ca-app-pub-3869986147080543/6421605446"
    },
    ios: {
      appId: "ca-app-pub-3869986147080543~1600761282",
      bannerAddId: "ca-app-pub-3869986147080543/4965291225"
    }
  }
};
