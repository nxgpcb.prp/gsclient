importScripts('https://www.gstatic.com/firebasejs/5.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.2.0/firebase-messaging.js');

firebase.initializeApp({
    'messagingSenderId': '121983096204'
});

const messaging = firebase.messaging();
var notificationCount = 0;

messaging.setBackgroundMessageHandler(payload => {
    notificationCount++;
    console.log("Custom background message count " + notificationCount)
    const title = payload.data.title;
    const options = {
      body: payload.data.score,
      icon: "./assets/icon/dwarpal.logo.png",
      sound: "./assets/sound/notification.mpeg"
    };
    return self.registration.showNotification(title, options);
  });
