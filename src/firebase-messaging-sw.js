importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');

firebase.initializeApp({
    'apiKey': "AIzaSyAsMIJNFIzJd48Qx3ODn-DZOm5JfF_1duw",
    'authDomain': "messit-745b6.firebaseapp.com",
    'databaseURL': "https://messit-745b6.firebaseio.com",
    'projectId': "messit-745b6",
    'storageBucket': "messit-745b6.appspot.com",
    'messagingSenderId': "121983096204",
    'appId': "1:121983096204:web:0d93d16fdc5911c9c0b2a9"
});

const messaging = firebase.messaging();