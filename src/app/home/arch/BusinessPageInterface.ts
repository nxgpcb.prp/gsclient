export declare interface BusinessPageInterface {
    init(): void;
    initCache(): void;
    initForm(): void;
    refresh(event): void;
    onInAppMessage(data): void;
    registerToEvents(): void;
    unRegisterToEvents(): void;

}
