import { NavigationEnd } from "@angular/router";
import { Subscription } from "rxjs";

export function BusinessPageTrack(): ClassDecorator {


    return function (constructor: any) {

        let subscription1:Subscription; 
        let subscription2:Subscription; 

        const ngOnInit = constructor.prototype.ngOnInit;

        const init = constructor.prototype.init;

        const initForm = constructor.prototype.initForm;

        const initCache = constructor.prototype.initCache;

        const registerToEvents = constructor.prototype.registerToEvents;

        const unRegisterToEvents = constructor.prototype.unRegisterToEvents;

        const refresh = constructor.prototype.refresh;

        const onInAppMessage = constructor.prototype.onInAppMessage;

        constructor.prototype.ngOnInit = function (...args) {
            console.log("PageTrack ngOnInit")
            ngOnInit && ngOnInit.apply(this, args);
            init && init.apply(this, args);
            initForm && initForm.apply(this, args);
            initCache && initCache.apply(this, args);
            registerToEvents && registerToEvents.apply(this, args);

            subscription1 = this.router.events.subscribe((value) => {
                if (value instanceof NavigationEnd && this.router.url == this.pageUrl) {
                    refresh && refresh.apply(this, args);
                }
            });
            
            subscription2 = this.fcm.currentMessage.subscribe((data) => {
                if (this.router.url == this.pageUrl && data != null) {
                    this.cache.objectType
                    console.log('this.fcm.currentMessage.subscribe((data)')
                    console.log(data)
                    console.log('args')
                    console.log(args)
                    args.push(data)
                    onInAppMessage && onInAppMessage.apply(this,args);
                }
            });
        }

        const ngOnDestroy = constructor.prototype.ngOnDestroy;

        constructor.prototype.ngOnDestroy = function (...args) {
            console.log("PageTrack ngOnDestroy")
            subscription1.unsubscribe();
            subscription2.unsubscribe();
            ngOnDestroy && ngOnDestroy.apply(this, args);
            unRegisterToEvents && unRegisterToEvents.apply(this, args);
        }
    }

}