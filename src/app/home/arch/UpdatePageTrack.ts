import { NavigationEnd } from "@angular/router";
import { Subscription } from "rxjs";
import { CacheService } from "../services/cache.service";

export function UpdatePageTrack(pageUrl,objectType,RESTUrl,isReferred): ClassDecorator {


    return function (constructor: any) {
        let subscription1:Subscription;

        const ngOnInit = constructor.prototype.ngOnInit;

        const onNavigationEnd = constructor.prototype.onNavigationEnd;

        constructor.prototype.ngOnInit = function (...args) {
            console.log('------------------------------')
            this.cacheService.initializeCache(objectType,RESTUrl,isReferred).then(newCache=>{
                this.cache=newCache
                this.activatedRoute.params.subscribe(params => {
                    this.cache.getObject(Number(params['id'])).then(
                        object=>{
                            this.cacheObject=object
                            console.log(this.cacheObject)
                            ngOnInit && ngOnInit.apply(this, args);
                        }
                    )
                });
            });

            subscription1 = this.router.events.subscribe((value) => {
                if (value instanceof NavigationEnd && this.router.url.indexOf(pageUrl) >= 0) {
                    onNavigationEnd && onNavigationEnd.apply(this, args);
                }
            });
        }

        const ngOnDestroy = constructor.prototype.ngOnDestroy;

        constructor.prototype.ngOnDestroy = function (...args) {
            subscription1.unsubscribe();
            ngOnDestroy && ngOnDestroy.apply(this, args);
        }
    }

}