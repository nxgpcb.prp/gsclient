import { Cache } from "../services/cache";

export declare interface ListPageInterface {
    cache: Cache
    onRefresh(event): void;
    onInAppMessage(data): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    onNavigationEnd(): void;
}
