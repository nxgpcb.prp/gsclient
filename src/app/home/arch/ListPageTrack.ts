import { NavigationEnd } from "@angular/router";
import { Subscription } from "rxjs";
//import { NavigationEnd } from "@angular/router";
//import { Subscription } from "rxjs";
import { CacheService } from "../services/cache.service";

export function ListPageTrack(pageUrl,objectType,RESTUrl,isReferred): ClassDecorator {


    return function (constructor: any) {
        let subscription1:Subscription; 
        //let subscription2:Subscription; 

        const ngOnInit = constructor.prototype.ngOnInit;

        //const registerToEvents = constructor.prototype.registerToEvents;

        //const unRegisterToEvents = constructor.prototype.unRegisterToEvents;

        const onRefresh = constructor.prototype.onRefresh;

        const onInAppMessage = constructor.prototype.onInAppMessage;

        const onNavigationEnd = constructor.prototype.onNavigationEnd;

        constructor.prototype.ngOnInit = function (...args) {
            this.cacheService.initializeCache(objectType,RESTUrl,isReferred).then(object=>{
                this.cache=object
                this.cache.load(CacheService.DEFAULT_LOAD_COUNT);
                if (this.search != undefined){
                    this.cache.filter.search = this.search;
                }
                ngOnInit && ngOnInit.apply(this, args);
            });

            subscription1 = this.router.events.subscribe((value) => {
                if (value instanceof NavigationEnd && this.router.url == pageUrl) {
                    onNavigationEnd && onNavigationEnd.apply(this, args);
                }
            });
            
            //registerToEvents && registerToEvents.apply(this, args);
            /*if(subscription1==undefined || subscription1.closed)
            {
                subscription1 = this.router.events.subscribe((value) => {
                    if (value instanceof NavigationEnd && this.router.url == pageUrl) {
                        onRefresh && onRefresh.apply(this, args);
                    }
                });
            }*/
        }

        constructor.prototype.onRefresh = function (event) {   
            this.cache.load(CacheService.DEFAULT_LOAD_COUNT).then(res => {
                if (event != undefined) {
                  event.target.complete();
                }
            });
            onRefresh && onRefresh.apply(this, event);
        }

        constructor.prototype.loadNext = function (event) {
            this.cache.loadNext(CacheService.DEFAULT_LOAD_NEXT_COUNT).then(res => {
              event.target.complete();
            });
        }
        /*constructor.prototype.registerToEvents= function() {
            this.onlineOfflineSubscription = this.onlineOfflineService.connectionChanged.subscribe(online => {
              if (online) {
                this.cache.sync();
              }
            });
          }*/

        const ngOnDestroy = constructor.prototype.ngOnDestroy;

        constructor.prototype.ngOnDestroy = function (...args) {
            //console.log("PageTrack ngOnDestroy")
            subscription1.unsubscribe();
            //subscription2.unsubscribe();
            ngOnDestroy && ngOnDestroy.apply(this, args);
            //unRegisterToEvents && unRegisterToEvents.apply(this, args);
        }
    }

}