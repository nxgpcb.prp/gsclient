import { Cache } from "../services/cache";

export declare interface UpdatePageInterface {
    cache: Cache;
    cacheObject;
    ngOnInit(): void;
    ngOnDestroy(): void;
    onNavigationEnd(): void;
}
