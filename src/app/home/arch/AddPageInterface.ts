import { Cache } from "../services/cache";

export declare interface AddPageInterface {
    cache: Cache;
    cacheObject;
    ngOnInit(): void;
    ngOnDestroy(): void;
    onNavigationEnd(): void;
}
