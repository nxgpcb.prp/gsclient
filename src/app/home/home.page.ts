import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
import { LoginService } from '../login/login.service';
import { HomeService } from './home.service';
import { LoaderService } from '../misc/loader.service';
import { Meta, Title } from '@angular/platform-browser';
import { BusinessPageTrack } from './arch/BusinessPageTrack';
import { Plugins } from '@capacitor/core';

const { Market } = Plugins;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 5
  };
  authenticated: boolean = false;
  type: String;
  selectedPath = '';
  href: string = "";
  loggedInUserName: String;
  user: any;

  constructor(
    private router: Router,
    private menu: MenuController,
    private loginService: LoginService,
    private homeService: HomeService,
    private loader: LoaderService,
    private meta: Meta,
    private title: Title,
    private platform: Platform
  ) {
    //For Seo
    title.setTitle('Dwarpal - Society Security and Management App');

    meta.addTag({
      name: 'keywords',
      content: 'Dwarpal, My Gate, MyGate Security App, Apartment Security, Society Security, Society Management'
        + 'Android App , Security platform, dwarpal.co.in, Made in India, Dwarpal is a security management platform' +
        ' Resident, Owner, Family Member, Safe, Visitor, Visitor Log, Delivery Management, Daily Visitors' +
        ' Tenant, Society Notices, Society Collabration, Amenities Management, Amenities Booking, About, Web, WebApp' +
        ' Gated Security App, Pune, Startup, Free App, Ios app, Door Keeper, Protector, Dwarpal Technologies Pvt Ltd.,' +
        ' Society Management App, Society Management Application, iOs App, Society Management Platform, Accounting, Billing, Notices'
    });

    meta.addTag({
      name: 'description', content: 'Dwarpal is a Society, Security and Management Platform.' +
        'Dwarpal is the startup created by 3 idiots that wanted to start something on their own.' +
        ' Dwarpal was the idea generated from the need to manage own Society and protect the family from' +
        ' unwanted visitors and lazy security guards. Dwarpal is the platform created by Dwarpal Technologies Pvt Ltd.' +
        ' which was first designed to manage society\'s visitors and their logs but in the period of development' +
        ' and design, we decided to make Dwarpal as a platform to manage the entire society.' +
        ' In the Dwarpal, we believe in the world everything is open source and everyone can learn,' +
        ' contribute and gain from it but at the same time, we want to build our company and business.' +
        ' So the Dwarpal core server will be released as open-source and the Dwarpal dashboard is released' +
        ' with a paid subscription model.'
    });


    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });

  }
  initCache(): void {
    console.log("Called child initCache");
  }
  initDataService(): void {
    console.log("Called child initDataService");
  }
  navigate(): void {
    console.log("Called child navigate");
  }
  refresh(): void {
    console.log("Called child refresh");
  }
  onInAppMessage(): void {
    console.log("Called child onInAppMessage");
  }
  registerToEvents(): void {
    console.log("Called child registerToEvents");
  }
  unRegisterToEvents(): void {
    console.log("Called child unRegisterToEvents");
  }

  ngOnInit() {
    console.log("Called child ngOnInit");
  }

  init() {
    console.log("Called child init");
  }

  openMenu() {
    this.menu.enable(true, 'HomeMenu');
    this.menu.open('HomeMenu');
  }

  public childCloseEventHandler(): void {
    // this will trigger the unload of the child, since you have `*ngIf="clicked"` for the child component
    console.log("closed");
  }

  async logOut() {
    this.loginService.logOut();
  }

  signUp() {
    this.router.navigate(['/register']);
  }

  contactSales() {
    this.router.navigate(['/sales']);
  }

  downloadAndroid() {
    if ((this.platform.is('android') && !this.platform.is('mobileweb'))) {
        Market.open(
          {
            appId: "com.janani.dwarpal"
          }
        );
    } else {
        location.href = "https://play.google.com/store/apps/details?id=com.janani.dwarpal&hl=en";
      }
  }

  downloadIos() {
    if ((this.platform.is('ios')) && !this.platform.is('mobileweb')) {
        Market.open(
          {
            appId: "1528705828"
          }
        );
    } else {
        location.href = "https://apps.apple.com/in/app/dwarpal/id1528705828";
    }
  }
}
