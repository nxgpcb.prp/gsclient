import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { HomeToolbarModule } from './home-toolbar/home-toolbar.module';
import { HomePopoverMenuComponent } from './home-popover-menu/home-popover-menu.component';
import { HomeFooterModule } from './home-footer/home-footer.module';
import { LanguagePopoverPage } from '../language/language-popover/language-popover.page';
import { LanguagePopoverPageModule } from '../language/language-popover/language-popover.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeToolbarModule,
    HomeFooterModule,
    LanguagePopoverPageModule,
    TranslateModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage,
      }   
    ])
  ],
  declarations: [HomePage],
  entryComponents: [
    HomePopoverMenuComponent,
    LanguagePopoverPage
  ],
})
export class HomePageModule {}
