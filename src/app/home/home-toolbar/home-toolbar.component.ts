import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { HomePopoverMenuComponent } from '../home-popover-menu/home-popover-menu.component';
import { LanguagePopoverPage } from 'src/app/language/language-popover/language-popover.page';
import { LanguageService } from 'src/app/language/language.service';
import { DebugService } from 'src/app/for-developer/debug.service';

@Component({
  selector: 'app-home-toolbar',
  templateUrl: './home-toolbar.component.html',
  styleUrls: ['./home-toolbar.component.scss']
})
export class HomeToolbarComponent implements OnInit {

  constructor(private router: Router, private popoverController: PopoverController,
    public readonly debugService:DebugService) { }

  ngOnInit() {
  }

  async presentPopover(ev: any) {
    //Style defined in global.scss
    const popover = await this.popoverController.create({
      component: HomePopoverMenuComponent,
      componentProps: {
        "popoverController" : this.popoverController,
      },
      event: ev,
      translucent: true,
      cssClass: 'custom-popover'
    });

    return await popover.present();
  }

  forDeveloper(){
    this.popoverController.dismiss();
    this.router.navigate(['/for-developer']);
  }
  
  about(){
    this.popoverController.dismiss();
    this.router.navigate(['/about']);
  }

  pricing(){
    this.popoverController.dismiss();
    this.router.navigate(['/pricing']);
  }

  support(){
    this.popoverController.dismiss();
    this.router.navigate(['/support']);
  }

  login(){
    this.popoverController.dismiss();
    this.router.navigate(['/login']);
  }

  signUp(){
    this.popoverController.dismiss();
    this.router.navigate(['/register']);
  }

  language(ev: any){
    this.popoverController.dismiss();
    this.presentLanguagePopover(ev);
  }

  async presentLanguagePopover(ev: any) {
    //Style defined in global.scss
    const popover = await this.popoverController.create({
      component: LanguagePopoverPage,
      componentProps: {
        "popoverController" : this.popoverController,
      },
      event: ev,
      translucent: true,
      cssClass: 'custom-popover'
    });

    return await popover.present();
  }
}
