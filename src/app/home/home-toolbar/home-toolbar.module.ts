import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { HomeToolbarComponent } from './home-toolbar.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HomePopoverMenuComponent } from '../home-popover-menu/home-popover-menu.component';
import { LanguagePopoverPage } from 'src/app/language/language-popover/language-popover.page';
import { LanguagePopoverPageModule } from 'src/app/language/language-popover/language-popover.module';

@NgModule({
  declarations: [HomeToolbarComponent, HomePopoverMenuComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
  ],
  exports: [
    HomePopoverMenuComponent,
    HomeToolbarComponent,
  ]
})
export class HomeToolbarModule { }
