import { Component, OnInit, Input } from '@angular/core';
import { HomeService } from '../home.service';
import { PopoverController } from '@ionic/angular';
import { SettingsService } from 'src/app/settings/settings.service';
import { WorkspaceToolbarService } from '../workspace-toolbar/workspace-toolbar.service';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-set-status',
  templateUrl: './set-status.component.html',
  styleUrls: ['./set-status.component.scss'],
})
export class SetStatusComponent implements OnInit {

  @Input()
  popoverController: PopoverController;

  user: any;
  statusInputChanged: boolean = false;
  updateStatusInProgress: boolean = false;

  constructor(private loginService: LoginService, private settingsService: SettingsService, private workspaceToolbarService: WorkspaceToolbarService) { }

  async ngOnInit() {
    this.workspaceToolbarService.getUpdateUserEmittedValue()
      .subscribe(item => {
        console.log("Emitted Value : " + item);
        this.user = item;
      }
      );

    this.user = await this.loginService.getUser()
  }

  removeStatus() {
    this.updateStatusInProgress = true;
    this.settingsService.removeStatus().
      subscribe(
        (response) => {
          this.user = response;
          this.workspaceToolbarService.updateUser(this.user)
          this.updateStatusInProgress = false;
          console.log(response);
          this.statusInputChanged = false;
          this.popoverController.dismiss();
        },
        (error) => {
          this.updateStatusInProgress = false;
          console.log(error);
          this.popoverController.dismiss();
        }
      )
  }

  statusChanged() {
    this.statusInputChanged = true;
  }

  updateStatus() {
    this.updateStatusInProgress = true;
    this.settingsService.updateStatus(this.user.status).
      subscribe(
        (response) => {
          this.user = response;
          this.workspaceToolbarService.updateUser(this.user)
          this.updateStatusInProgress = false;
          console.log(response);
          this.statusInputChanged = false;
        },
        (error) => {
          this.updateStatusInProgress = false;
          console.log(error);
        }
      )
  }

  addStatus() {
    this.updateStatusInProgress = true;
    this.settingsService.addStatus(this.user.status).
      subscribe(
        (response) => {
          this.user = response;
          this.workspaceToolbarService.updateUser(this.user)
          this.updateStatusInProgress = false;
          console.log(response);
          this.statusInputChanged = false;
        },
        (error) => {
          this.updateStatusInProgress = false;
          console.log(error);
        }
      )
  }

}
