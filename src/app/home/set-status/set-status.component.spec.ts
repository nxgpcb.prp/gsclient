import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { IonicModule } from '@ionic/angular';

import { SetStatusComponent } from './set-status.component';
import { HomeService } from '../home.service';
import { Society } from 'src/app/core/Society';

describe('SetStatusComponent', () => {
  let component: SetStatusComponent;
  let fixture: ComponentFixture<SetStatusComponent>;
  let de: DebugElement;
  let homeService: HomeService;
  let spy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SetStatusComponent],
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        HttpClientModule,
        IonicModule.forRoot(),
      ],
      providers: [
        Firebase,
        AngularFireMessaging,
        HomeService
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SetStatusComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    homeService = de.injector.get(HomeService);
    let user: Society = new Society();

    spy = spyOn(homeService, 'getUser').and.returnValue(user);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
