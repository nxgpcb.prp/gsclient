import { Component, OnInit, ViewChild, AfterViewInit, HostListener, OnChanges, DoCheck, NgZone, OnDestroy } from '@angular/core';
import { IonInfiniteScroll, PopoverController, Platform } from '@ionic/angular';
import { SecurityService } from '../../services/security.service';
import { Router, NavigationEnd } from '@angular/router';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { FcmService } from 'src/app/notifications/fcm.service';
import { LoginService } from 'src/app/login/login.service';
import { SettingsService } from 'src/app/settings/settings.service';
import { HomeService } from '../../home.service';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Society } from 'src/app/core/Society';
import { WorkspaceToolbarComponent } from '../../workspace-toolbar/workspace-toolbar.component';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { Visitor } from 'src/app/core/Visitor';
import { DailyVisitor } from 'src/app/core/DailyVisitor';
import { LoaderService } from 'src/app/misc/loader.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from '../../ad/ad.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { CacheService } from '../../services/cache.service';

@Component({
  selector: 'app-daily-visitors',
  templateUrl: './daily-visitors.page.html',
  styleUrls: ['./daily-visitors.page.scss'],
})
@ListPageTrack(
  '/security/daily-visitors',
  'DailyVisitor',
  '/GSServer/webapi/secured/Security/DailyVisitor',
  false
)
export class DailyVisitorsPage implements ListPageInterface {

  cache: Cache;
  visitorLogCache: Cache = null;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public securityService: SecurityService, 
    private router: Router, 
    private loader: LoaderService, 
    private workspaceToolbarService: WorkspaceToolbarService, 
    private toaster:ToasterService, 
    private adService: AdService, 
    public readonly loginService: LoginService,
    private cacheService: CacheService,
    ) {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
      {
        this.router.navigate(['/security/join-society']);
      } 
  }

  ngOnInit() {
    this.cacheService.initializeCache('VisitorLog', '/GSServer/webapi/secured/Security/VisitorLog', true).then(cacheObject => {
      this.visitorLogCache = cacheObject
      this.visitorLogCache.load(CacheService.DEFAULT_LOAD_COUNT)
      .then(
        res => {
        }
      )
    });
  }

  ngOnDestroy() {
  }

  
  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("TAB");
  }

  logIn(selectedVisitorLog) {
    this.loader.showLoader();
    
    console.log(selectedVisitorLog)
    let dailyVisitor: DailyVisitor = this.cache.objects[selectedVisitorLog];
    let visitorLogs:VisitorLog[] = [];
    dailyVisitor.visitingTo.forEach(facility => {
      let visitorLog = new VisitorLog();
      visitorLog.visitor = dailyVisitor;
      visitorLog.visitingTo = facility;
      visitorLog.timeIn = new Date();
      visitorLog.approvalStatus = "Approved";
      if(facility.facilityOwner != undefined){
        visitorLog.contactPerson = facility.facilityOwner;
      }else{
        visitorLog.contactPerson = null;
      }
      visitorLog.isBookedInAdvance = false;
      visitorLogs.push(visitorLog);
    });

    this.visitorLogCache.addObjects(visitorLogs)
    .then(
      (response) => {
        console.log(response)
        this.router.navigate(['/security/dashboard/visitor-logs'])
          .finally(
            () => {
              this.loader.hideLoader();
            }
          )
      },
      (error) => {
        console.log(error)
        this.loader.hideLoader()
      }
    );
  }


  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  goToAddDailyVisitorsPage() {
    this.router.navigate(['/security/add-daily-visitor']);
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }
}
