import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { DailyVisitorsPage } from './daily-visitors.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: DailyVisitorsPage
  },
  {
    path: 'update-daily-visitor/:id',
    loadChildren: './update-daily-visitor/update-daily-visitor.module#UpdateDailyVisitorPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    
  ],
  declarations: [DailyVisitorsPage]
})
export class DailyVisitorsPageModule {}
