import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UpdateDailyVisitorPage } from './update-daily-visitor.page';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicSelectableModule } from 'ionic-selectable';
import { WebcamModalModule } from 'src/app/camera/webcam-modal/webcam-modal.module';
import { WebcamModalComponent } from 'src/app/camera/webcam-modal/webcam-modal.component';

const routes: Routes = [
  {
    path: '',
    component: UpdateDailyVisitorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    IonicSelectableModule,
    WebcamModalModule
  ],
  declarations: [UpdateDailyVisitorPage],
  entryComponents: [
    WebcamModalComponent,
  ],
})
export class UpdateDailyVisitorPageModule {}
