import { Component, OnInit, ViewChild, OnDestroy, NgZone } from '@angular/core';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SecurityService } from '../../services/security.service';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Resident } from 'src/app/core/Resident';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Visitor } from 'src/app/core/Visitor';
import { Facility } from 'src/app/core/Facility';
import { LoaderService } from 'src/app/misc/loader.service';
import { Platform, ModalController } from '@ionic/angular';
import { ToasterService } from 'src/app/misc/toaster.service';
import { CameraService } from 'src/app/camera/camera.service';
import { WebcamImage } from 'ngx-webcam';
import { WebcamModalComponent } from 'src/app/camera/webcam-modal/webcam-modal.component';
import { AdService } from '../../ad/ad.service';
import { Parking } from 'src/app/core/Parking';
import { Filter } from 'src/app/filters/Filter';
import { Sort } from 'src/app/filters/Sort';
import { Subscription } from 'rxjs/internal/Subscription';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Search } from 'src/app/filters/Search';
import { DeliveryLog } from 'src/app/core/DeliveryLog';
import { DailyVisitor } from 'src/app/core/DailyVisitor';
import { throwIfEmpty } from 'rxjs/operators';
import { WorkspaceToolbarComponent } from '../../workspace-toolbar/workspace-toolbar.component';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AddPageTrack } from '../../arch/AddPageTrack';
import { Cache } from 'src/app/home/services/cache';
import { AddPageInterface } from '../../arch/AddPageInterface';
import { CacheService } from '../../services/cache.service';
import { Apartment } from 'src/app/core/Apartment';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-add-visitor-log',
  templateUrl: './add-visitor-log.page.html',
  styleUrls: ['./add-visitor-log.page.scss'],
})
@AddPageTrack(
  '/security/add-visitor-log',
  'VisitorLog',
  '/GSServer/webapi/secured/Security/VisitorLog',
  false
)
export class AddVisitorLogPage implements AddPageInterface {

  residents: Resident[] = [];
  apartments: Apartment[] = [];
  subscribers: Resident[] = [];
  parkings: Parking[] = [];

  @ViewChild('selectComponent') selectComponent: IonicSelectableComponent;

  toggle = false;
  form: FormGroup;
  public webcamImage: WebcamImage = null;
  visitorId: any;
  sub: any;
  society: String = "Society";
  logType: any = "RegularVisitorLog";
  vehicleEntry: boolean = false;
  apartmentCache: Cache = null;
  parkingCache: Cache = null;
  delveryLogCache: Cache = null;
  dailyVisitorLogCache: Cache = null;
  visitor = new Visitor();
  cacheObject: any;
  cache: Cache;
  searchVisitorSubscription: Subscription;

  constructor(public securityService: SecurityService,
    private router: Router,
    private route: ActivatedRoute,
    private platform: Platform,
    public toaster: ToasterService,
    private cameraService: CameraService,
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private ngZone: NgZone,
    private loader: LoaderService, private adService: AdService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private cacheService: CacheService,
    private loginService: LoginService
  ) {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      address: ['', [Validators.required]],
      noOfAccompanyingVisitors: ['', [Validators.required, Validators.pattern('\\d{1}')]],
      visitingTo: ['', [Validators.required]],
      contactPerson: [],
      vehicleEntry: [],
    });
  }

  ngOnInit() {
    if (this.loginService.userCache != undefined && this.loginService.userCache.objects[0].subscriptionPlan == undefined) {
      this.router.navigate(['/security/join-society']);
    }

    this.takePicture();

    this.cacheService.initializeCache('VisitorLog', '/GSServer/webapi/secured/Security/DailyVisitorLog', true).then(cacheObject => {
      this.dailyVisitorLogCache = cacheObject
      this.dailyVisitorLogCache.load(CacheService.DEFAULT_LOAD_COUNT)
        .then(
          res => {
          }
        )
    });

    this.cacheService.initializeCache('DeliveryLog', '/GSServer/webapi/secured/Security/DeliveryLog', true).then(cacheObject => {
      this.delveryLogCache = cacheObject
      this.delveryLogCache.load(CacheService.DEFAULT_LOAD_COUNT)
        .then(
          res => {
          }
        )
    });

    this.cacheService.initializeCache('Apartment', '/GSServer/webapi/secured/Security/Facilities/Apartment', true).then(cacheObject => {
      this.apartmentCache = cacheObject
      this.apartmentCache.load(CacheService.LOAD_ALL).then(
        res => {
          this.apartments = this.apartmentCache.objects
        }
      )
    });

    this.cacheService.initializeCache('Parking', '/GSServer/webapi/secured/Security/Facilities/Parking', true).then(cacheObject => {
      this.parkingCache = cacheObject
      this.parkingCache.load(CacheService.LOAD_ALL).then(
        res => {
          this.parkings = this.parkingCache.objects
        }
      )
    });
  }

  ngOnDestroy() {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addLog() {
    this.loader.showLoader();
    if (this.logType == "DailyVisitorLog") {
      this.addDailyVisitorLog(this.form.get('uniqueCode').value);
    }
    if (this.logType == "RegularVisitorLog") {
      this.addVisitorLog();
    }
    if (this.logType == "DeliveryLog") {
      this.addDeliveryLog();
    }
  }

  addVisitorLog() {
    this.visitor.name = this.form.get('name').value;
    this.visitor.phone = this.form.get('phone').value;
    this.visitor.address = this.form.get('address').value;

    let visitorLog = new VisitorLog();
    visitorLog.visitor = this.visitor;
    visitorLog.noOfAccompanyingVisitors = this.form.get('noOfAccompanyingVisitors').value;
    visitorLog.visitingTo = this.form.get('visitingTo').value;
    visitorLog.timeIn = new Date();
    visitorLog.approvalStatus = "Pending";
    visitorLog.contactPerson = this.form.get('contactPerson').value;
    if (this.form.get('vehicleEntry').value) {
      visitorLog.vehicleType = this.form.get('vehicleType').value;
      visitorLog.vehicleNo = this.form.get('vehicleNo').value;
      visitorLog.visitorParking = this.form.get('parking').value;
    }
    this.cache.addObject(visitorLog)
      .then(
        (response) => {
          this.router.navigate(['/security/dashboard/visitor-logs'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader()
        }
      );
  }

  addDeliveryLog() {
    this.visitor.name = this.form.get('name').value;
    this.visitor.phone = this.form.get('phone').value;
    this.visitor.address = this.form.get('address').value;
    let deliveryLog = new DeliveryLog();
    deliveryLog.visitor = this.visitor;
    deliveryLog.visitingTo = this.form.get('visitingTo').value;
    deliveryLog.deliveryInTime = new Date();
    deliveryLog.approvalStatus = "Pending";
    deliveryLog.contactPerson = this.form.get('contactPerson').value;
    deliveryLog.rackNumber = this.form.get('rackNumber').value;

    this.delveryLogCache.addObject(deliveryLog)
      .then(
        (response) => {
          console.log(response)
          this.router.navigate(['/security/dashboard/delivery-logs'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader()
        }
      );
  }

  addDailyVisitorLog(selectedDailyVisitorId) {

    let dailyVisitor: DailyVisitor = new DailyVisitor();
    dailyVisitor.id = selectedDailyVisitorId;
    dailyVisitor.profilePicture = this.visitor.profilePicture;
    this.securityService.addDailyVisitorLog(dailyVisitor)
      .subscribe(
        (response) => {
          console.log(response)
          this.router.navigate(['/security/dashboard/visitor-logs'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader();
        }
      );
  }

  segmentChanged(ev: any) {
    this.logType = ev.detail.value;
    if (this.logType == "DailyVisitorLog") {
      this.form = this.formBuilder.group({
        uniqueCode: ['', [Validators.required]],
      });
    }
    if (this.logType == "RegularVisitorLog") {
      if (this.form.get('vehicleEntry') != null) {
        this.form.get('vehicleEntry').setValue(false);
        this.vehicleEntryToggleChanged()
      }
      this.form = this.formBuilder.group({
        name: ['', [Validators.required]],
        phone: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
        address: ['', [Validators.required]],
        noOfAccompanyingVisitors: ['', [Validators.required, Validators.pattern('\\d{1}')]],
        visitingTo: ['', [Validators.required]],
        contactPerson: [],
        vehicleEntry: [],
      });
    }
    if (this.logType == "DeliveryLog") {
      this.form = this.formBuilder.group({
        name: ['', [Validators.required]],
        phone: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
        address: ['', [Validators.required]],
        visitingTo: ['', [Validators.required]],
        contactPerson: [],
        rackNumber: [],
      });
    }
  }

  userChanged(event: { component: IonicSelectableComponent, value: any }) {
    console.log('Selected: ', event);
    let selectedFacility: Facility = event.value;
    this.form.get('visitingTo').setValue(selectedFacility);
    this.subscribers = selectedFacility.facilitySubscribers;
    console.log('Subscribers: ', this.subscribers);
  }

  vehicleEntryToggleChanged() {
    this.vehicleEntry = this.form.get('vehicleEntry').value;
    if (this.vehicleEntry) {
      this.form.addControl('vehicleType', new FormControl('', Validators.required));
      this.form.addControl('vehicleNo', new FormControl('', [Validators.required, Validators.pattern('[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}')]));
      this.form.addControl('parking', new FormControl('', Validators.required));
    } else {
      this.form.removeControl('vehicleType');
      this.form.removeControl('vehicleNo');
      this.form.removeControl('parking');
    }
  }

  vehicleTypeChanged() {
    //Parkings are not filtered correctly
  }

  onSearch(event: { component: IonicSelectableComponent, text: any }) {
    let text = event.text.trim();
    event.component.startSearch();
    event.component.items = this.filterSearch(text);
    event.component.endSearch();
  }

  onSearchFail(event: { component: IonicSelectableComponent, text: any }) {
    if (event.text != '') {
      this.selectComponent.searchFailText = "Result not found. Apartment with this name does not exist."
    } else {
      this.selectComponent.searchFailText = "Start typing apartment name."
    }
  }

  filterSearch(text: string) {
    return this.apartments.filter(apartment => {
      return apartment.name.indexOf(text) !== -1;
    })
  }

  onOpen() {
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("TAB");
  }

  onClose() {
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("NORMAL");
  }

  openFromCode() {
    this.selectComponent.open();
  }

  clear() {
    this.selectComponent.clear();
    this.selectComponent.close();
  }

  toggleItems() {
    this.selectComponent.toggleItems(this.toggle);
    this.toggle = !this.toggle;
  }

  confirm() {
    this.selectComponent.confirm();
    this.selectComponent.close();
  }

  removePicture() {
    let temp: Visitor = new Visitor;
    this.visitor.profilePicture.content = temp.profilePicture.content;
  }

  takePicture() {
    if ((this.platform.is('android') || this.platform.is('ios'))) {
      this.cameraService.getPicture().then(
        (imageData) => {
          this.visitor.profilePicture.content = 'data:image/jpeg;base64,' + imageData.base64String;
        },
        (err) => {
          window.alert('Unable to open Camera : ' + err);
        }
      );
    } else {
      this.openWebcam();
    }
  }
  async openWebcam() {
    const modal = await this.modalController.create({
      component: WebcamModalComponent,
      componentProps: {
        "modalController": this.modalController,
      },
    });
    modal.onDidDismiss()
      .then((data) => {
        if (data['data'] != null) {
          this.visitor.profilePicture.content = data['data']; // Here's your selected user!
        }
      });
    return await modal.present();
  }

  handleImage(webcamImage: WebcamImage) {
    this.webcamImage = webcamImage;
  }

  searchByMobile() {
    this.loader.showLoader();
    this.searchVisitorSubscription = this.securityService.searchVisitor(this.form.get('phone').value)
      .subscribe(
        (response) => {
          if (this.searchVisitorSubscription.closed) {
            return;
          }
          this.visitor.id = undefined;
          this.form.get('name').setValue("")
          this.form.get('address').setValue("")
          this.visitor.id = response.id;
          this.form.get('name').setValue(response.name)
          this.form.get('phone').setValue(response.phone)
          this.form.get('address').setValue(response.address)
          this.loader.hideLoader();
        },
        (error) => {
          this.visitor.id = undefined;
          this.form.get('name').setValue("")
          this.form.get('address').setValue("")
          console.log(error)
          this.loader.hideLoader();
        }
      );
  }

}
