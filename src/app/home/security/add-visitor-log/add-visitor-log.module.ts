import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddVisitorLogPage } from './add-visitor-log.page';
import { IonicSelectableModule } from 'ionic-selectable';
import { WebcamModalModule } from 'src/app/camera/webcam-modal/webcam-modal.module';
import { WebcamModalComponent } from 'src/app/camera/webcam-modal/webcam-modal.component';

const routes: Routes = [
  {
    path: '',
    component: AddVisitorLogPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    IonicSelectableModule,
    WebcamModalModule
  ],
  declarations: [AddVisitorLogPage],
  entryComponents: [
    WebcamModalComponent,
  ],
})
export class AddVisitorLogPageModule {}
