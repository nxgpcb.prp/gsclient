import { Component, OnInit, NgZone, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';
import { BusinessPageTrack } from '../../arch/BusinessPageTrack';
import { SecurityGuard } from 'src/app/core/SecurityGuard';
import { BusinessPageInterface } from '../../arch/BusinessPageInterface';
import { Subscription } from 'rxjs';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { Platform, PopoverController } from '@ionic/angular';
import { SecurityService } from '../../services/security.service';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { HttpClient } from '@angular/common/http';
import { AdService } from '../../ad/ad.service';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { Paging } from 'src/app/filters/Paging';
import { Filter } from 'src/app/filters/Filter';
import { Sort } from 'src/app/filters/Sort';
import { SecurityGuardLog } from 'src/app/core/SecurityGuardLog';
import { Search } from 'src/app/filters/Search';

@Component({
  selector: 'app-security-guard-login',
  templateUrl: './security-guard-login.component.html',
  styleUrls: ['./security-guard-login.component.scss'],
})

export class SecurityGuardLoginComponent implements OnInit, OnDestroy {
  @Input()
  popoverController: PopoverController;
  form: FormGroup;
  securityGuard: SecurityGuard;
  securityGuardLog: SecurityGuardLog;
  securityGuards: SecurityGuard[] = [];
  securityGuardLogs: SecurityGuardLog[] = [];
  onlinseOfflineSubscription: Subscription;
  dataServiceSubscription: Subscription;
  dataServiceSubscription1: Subscription;
  navigationSubscription: Subscription;
  securityGuardCache: Cache = null;
  securityGuardLogCache: Cache = null;
  user: any;
  isLoggedIn: boolean = false;
  filter: Filter;

  constructor(private formBuilder: FormBuilder, private platform: Platform, private router: Router, private loginService: LoginService,
    public securityService: SecurityService, private ngZone: NgZone, private fcm: FcmService,
    private readonly onlineOfflineService: OnlineOfflineService, private workspaceToolbarService: WorkspaceToolbarService,
    private http: HttpClient, private adService: AdService, private loader: LoaderService) { }

  ngOnInit(): void {
    this.init();
    this.initForm();
    this.initCache();
    this.registerToEvents();
    this.refresh(null);
  }

  ngOnDestroy(): void {
    this.unRegisterToEvents();
  }

  init() {
    this.securityGuards = [];
    this.securityGuardLogs = [];
  }

  initForm(): void {
    this.form = this.formBuilder.group({
      pin: ['', [Validators.required, Validators.pattern('\\d{6}')]],
      securityGuard: ['', [Validators.required]],
    });
  }

  initCache() {
    this.securityGuardLogCache = new Cache(this.http,
      'securityGuardLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/SecurityGuardLog',
      Cache.type_REFERED);

    this.securityGuardCache = new Cache(this.http,
      'securityGuards',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/SecurityGuard',
      Cache.type_REFERED);
  }

  refresh(event) {
    console.log("Refresh called")
    console.log(event)
    this.securityService.setProgressing(true);
    setTimeout(() => {
      this.ngZone.run(async () => {
        if (event == undefined) {
          this.securityGuards = [];
          this.securityGuardLogs = [];
        }
        this.user = await this.loginService.getUser()
        this.getSecurityGuardLogs();
        this.getSecurityGuards();
        if (event != undefined) {
          event.target.complete();
        }
      })
    }, 2000);
  }

  getSecurityGuards() {
    let paging = new Paging;
    paging.start = 0;
    paging.size = 10;

    let sort = new Sort
    sort.method = "DESCENDING"
    sort.parameter = "createdOn"

    let filter = new Filter;
    filter.paging = paging;
    filter.sort = sort;

    this.filter = filter;

    this.securityGuardCache.updateMe(this.filter);
  }

  getSecurityGuardLogs() {
    let sort = new Sort
    sort.method = "DESCENDING"
    sort.parameter = "createdOn"

    let filter = new Filter;
    filter.sort = sort;

    this.filter = filter;

    this.securityGuardLogCache.updateMe(this.filter);
  }

  loadData(event) {
    setTimeout(() => {
      if (this.securityGuards.length < this.filter.paging.size) {
        this.securityService.setProgressing(false);
        event.target.complete();
      } else {
        this.filter.paging.start = this.filter.paging.start + 10;
        this.securityGuardCache.updateMe(this.filter);
        event.target.complete();
      }

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.securityGuards.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  login(form: FormGroup) {
    let pin = form.get('pin').value;
    let securityGuard = form.get('securityGuard').value;
    this.loader.showLoader();
    if (this.isLoggedIn) {
      if (securityGuard.pin == pin) {
        this.workspaceToolbarService.updateSecurityGuard(securityGuard);
        this.workspaceToolbarService.updateSecurityGuardLog(this.securityGuardLog);
        this.loader.hideLoader()
        this.popoverController.dismiss();
      } else {
        this.loader.hideLoader()
        alert("Incorrect Pin");
      }
    } else {
      if (securityGuard.pin == pin) {
        let securityGuardLog: SecurityGuardLog = new SecurityGuardLog();
        securityGuardLog.securityGuard = this.form.get("securityGuard").value;
        securityGuardLog.timeIn = new Date();
        this.securityGuardLogCache.add(securityGuardLog).then(
          (response) => {
            this.workspaceToolbarService.updateSecurityGuard(securityGuard);
            this.workspaceToolbarService.updateSecurityGuardLog(response);
            this.loader.hideLoader()
            this.popoverController.dismiss();
          },
          (error) => {
            console.log(error)
          }
        )
      } else {
        this.loader.hideLoader()
        alert("Incorrect Pin");
      }
    }
  }

  logout(){
    this.securityGuardLog.timeOut = new Date();
    this.securityGuardLogCache.update(this.securityGuardLog).then(
      (response)=>{
        console.log(response)
        this.popoverController.dismiss();
        this.workspaceToolbarService.updateSecurityGuard(undefined);
        this.workspaceToolbarService.updateSecurityGuardLog(undefined);
      },
      (error)=>{
        console.log(error)
      }
    )
  }

  registerToEvents() {
    this.onlinseOfflineSubscription = this.onlineOfflineService.connectionChanged.subscribe(
      online => {
        if (online) {
          console.log('went online');
          console.log('sending all stored items');
          this.securityGuards = [];
          this.securityGuardLogs = [];
          this.securityGuardCache.syncPendingRequests().then(
            response => {
              console.log("sync complete");
            }
          )
        } else {
          console.log('went offline, storing in indexdb');
        }
      });

    this.dataServiceSubscription = this.securityGuardCache.dataService.subscribe(
      securityGuards => {
        console.log(securityGuards);
        this.securityGuards = this.securityGuards.concat(securityGuards);
        this.securityService.setProgressing(false);
      })

    this.dataServiceSubscription1 = this.securityGuardLogCache.dataService.subscribe(
      securityGuardLogs => {
        console.log(securityGuardLogs);
        securityGuardLogs.forEach(securityGuardLog => {
          if (securityGuardLog.securityAccount.id == this.user.id) {
            this.isLoggedIn = true;
            this.securityGuardLog = securityGuardLog;
            this.securityGuard = securityGuardLog.securityGuard;
            this.form.get('securityGuard').setValue(securityGuardLog.securityGuard)
          }
        });
        this.securityService.setProgressing(false);
      })
  }

  unRegisterToEvents() {
    this.onlinseOfflineSubscription.unsubscribe();
    this.dataServiceSubscription.unsubscribe();
    this.dataServiceSubscription1.unsubscribe();
  }

}
