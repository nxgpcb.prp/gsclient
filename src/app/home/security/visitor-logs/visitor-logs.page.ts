import { Component, OnInit, ViewChild, AfterViewInit, HostListener, OnChanges, DoCheck, NgZone, OnDestroy } from '@angular/core';
import { IonInfiniteScroll} from '@ionic/angular';
import { SecurityService } from '../../services/security.service';
import { Router, NavigationEnd } from '@angular/router';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Society } from 'src/app/core/Society';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from '../../ad/ad.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { CacheService } from '../../services/cache.service';
import { LoginService } from 'src/app/login/login.service';
import { Search } from 'src/app/filters/Search';

@Component({
  selector: 'app-visitor-logs',
  templateUrl: './visitor-logs.page.html',
  styleUrls: ['./visitor-logs.page.scss'],
})
@ListPageTrack(
  '/security/dashboard/visitor-logs',
  'VisitorLog',
  '/GSServer/webapi/secured/Security/VisitorLog',
  false
)
export class VisitorLogsPage implements ListPageInterface {

  cache: Cache;
  search: Search = new Search("ISNULL", "timeOut")

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  constructor(
    private cacheService: CacheService,
    public readonly loginService: LoginService,
    private router: Router,
    private securityService: SecurityService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService, 
    private adService: AdService
  ) {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/security/join-society']);
    }        
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {

  }

  onNavigationEnd(){
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("TAB");
  }

  // ---------------------------------------------------------------------------------------

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  logOut(visitorLog) {
    visitorLog.visitorParking = null;
    visitorLog.timeOut = new Date();
    this.cache.updateObject(visitorLog);
  }

  ping(visitorLog) {
    this.securityService.pingForVisitorLog(visitorLog)
      .subscribe(
        (response) => {
          this.toaster.toast("VisitorLog has been ping successfully.")
        },
        (error) => {
          console.log(error)
        }
      );
  }

  approve(visitorLog) {
    visitorLog.approvalStatus = "Approved";
    visitorLog.timeIn = new Date();
    this.cache.updateObject(visitorLog)
  }
}
