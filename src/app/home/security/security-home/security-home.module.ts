import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { WebcamModalModule } from 'src/app/camera/webcam-modal/webcam-modal.module';
import { WebcamModalComponent } from 'src/app/camera/webcam-modal/webcam-modal.component';
import { SecurityHomePage } from './security-home.page';

const routes: Routes = [
  {
    path: '',
    component: SecurityHomePage,
    children: [
      { path: 'delivery-logs', loadChildren: '../delivery-logs/delivery-logs.module#DeliveryLogsPageModule' },
      { path: 'visitor-logs', loadChildren: '../visitor-logs/visitor-logs.module#VisitorLogsPageModule' },
      { path: 'daily-visitors', loadChildren: '../daily-visitors/daily-visitors.module#DailyVisitorsPageModule' },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    IonicSelectableModule,
    WebcamModalModule
  ],
  declarations: [SecurityHomePage],
  entryComponents: [
    WebcamModalComponent,
  ],
})
export class SecurityHomePageModule {}
