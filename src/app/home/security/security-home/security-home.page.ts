import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { CacheService } from '../../services/cache.service';
import { Cache } from '../../services/cache';

@Component({
  selector: 'app-security-home',
  templateUrl: './security-home.page.html',
  styleUrls: ['./security-home.page.scss']
})
export class SecurityHomePage implements OnInit, AfterViewInit {

  cacheVisitorLog: Cache;
  cacheDeliveryLog: Cache;

  constructor(
    private router:Router, 
    private cacheService:CacheService,
    private workspaceToolbarService:WorkspaceToolbarService) 
  {
    this.workspaceToolbarService.hideBackButton();
  }

  async ngOnInit() {
    this.cacheService.initializeCache('VisitorLog','/GSServer/webapi/secured/Security/VisitorLog',false).then(cache=>{
      this.cacheVisitorLog=cache;
      this.cacheVisitorLog.load(1);
    });
    this.cacheService.initializeCache('DeliveryLog','/GSServer/webapi/secured/Security/DeliveryLog',false).then(cache=>{
      this.cacheDeliveryLog=cache;
      this.cacheDeliveryLog.load(1);
    });

  }

  ngAfterViewInit(): void {

  }

}
