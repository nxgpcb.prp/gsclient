import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SecurityPage } from './security.page';
import { PopoverMenuComponent } from '../popover-menu/popover-menu.component';
import { WorkspaceToolbarModule } from '../workspace-toolbar/workspace-toolbar.module';

const routes: Routes = [
  {
    path: '',
    component: SecurityPage,
    children:
    [
      { path: 'dashboard', loadChildren: './security-home/security-home.module#SecurityHomePageModule' },
      { path: 'add-daily-visitor', loadChildren: './add-daily-visitor/add-daily-visitor.module#AddDailyVisitorPageModule' },
      { path: 'add-visitor-log/:id', loadChildren: './add-visitor-log/add-visitor-log.module#AddVisitorLogPageModule' },
      { path: 'add-visitor-log', loadChildren: './add-visitor-log/add-visitor-log.module#AddVisitorLogPageModule' },
      { path: 'join-society', loadChildren: './join-society/join-society.module#JoinSocietyPageModule' },
    ]
  },
];

@NgModule({
  exports:[
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    WorkspaceToolbarModule
  ],
  declarations: [SecurityPage],
  entryComponents: [
    PopoverMenuComponent
  ],
})
export class SecurityPageModule {}
