import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { Security } from 'src/app/core/Security';
import { HomeService } from '../home.service';
import { SecurityService } from '../services/security.service';
import { LoginService } from 'src/app/login/login.service';


@Component({
  selector: 'app-security',
  templateUrl: './security.page.html',
  styleUrls: ['./security.page.scss'],
})
export class SecurityPage implements OnInit {

  images = [];
  visitorLogs: VisitorLog[] = [];
  selectedPath = '';
  user: Security;
  isFistTimeLoggedIn:boolean = false;

  constructor(private router: Router, private loginService: LoginService, public securityService:SecurityService) {
    if(this.router.url == "/security"){
      this.router.navigate(['/security/dashboard/visitor-logs']);
    }
  }

  async ngOnInit() {
    
    this.user = await this.loginService.getUser()
    if(this.user.name == null){
      this.isFistTimeLoggedIn = true;
    }
  }

}
