import { Component, OnInit, ViewChild, AfterViewInit, HostListener, OnChanges, DoCheck, NgZone, OnDestroy } from '@angular/core';
import { IonInfiniteScroll, PopoverController, Platform } from '@ionic/angular';
import { SecurityService } from '../../services/security.service';
import { Router, NavigationEnd } from '@angular/router';
import { DeliveryLog } from 'src/app/core/DeliveryLog';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Society } from 'src/app/core/Society';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from '../../ad/ad.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { CacheService } from '../../services/cache.service';
import { LoginService } from 'src/app/login/login.service';
import { Search } from 'src/app/filters/Search';

@Component({
  selector: 'app-delivery-logs',
  templateUrl: './delivery-logs.page.html',
  styleUrls: ['./delivery-logs.page.scss'],
})
@ListPageTrack(
  '/security/dashboard/delivery-logs',
  'DeliveryLog',
  '/GSServer/webapi/secured/Security/DeliveryLog',
  false
)
export class DeliveryLogsPage implements ListPageInterface {

  cache: Cache;
  search: Search = new Search("ISNULL", "deliveredTime")

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private cacheService: CacheService,
    public readonly loginService: LoginService,
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService: AdService,
    private securityService: SecurityService,
    private toaster:ToasterService,) 
  {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/security/join-society']);
    }  
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("TAB");
  }

  // ---------------------------------------------------------------------------------------
  
  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  logOut(deliveryLog) {
    deliveryLog.deliveredTime = new Date();
    this.cache.updateObject(deliveryLog);
  }

  ping(deliveryLog) {
    this.securityService.pingForVisitorLog(deliveryLog)
    .subscribe(
      (response) => {
        this.toaster.toast("DeliveryLog has been ping successfully.")
      },
      (error) => {
        console.log(error)
      }
    );
  }

}
