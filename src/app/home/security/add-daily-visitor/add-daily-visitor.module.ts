import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddDailyVisitorPage } from './add-daily-visitor.page';
import { WebcamModalModule } from 'src/app/camera/webcam-modal/webcam-modal.module';
import { WebcamModalComponent } from 'src/app/camera/webcam-modal/webcam-modal.component';
import { IonicSelectableModule } from 'ionic-selectable';

const routes: Routes = [
  {
    path: '',
    component: AddDailyVisitorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    IonicSelectableModule,
    WebcamModalModule
  ],
  declarations: [AddDailyVisitorPage],
  entryComponents: [
    WebcamModalComponent,
  ],
})
export class AddDailyVisitorPageModule {}
