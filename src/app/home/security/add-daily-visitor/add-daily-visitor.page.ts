import { Component, OnInit, ViewChild, OnDestroy, NgZone } from '@angular/core';
import { SecurityService } from '../../services/security.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { Platform, ModalController } from '@ionic/angular';
import { CameraService } from 'src/app/camera/camera.service';
import { WebcamModalComponent } from 'src/app/camera/webcam-modal/webcam-modal.component';
import { WebcamImage } from 'ngx-webcam';
import { ToasterService } from 'src/app/misc/toaster.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { DailyVisitor } from 'src/app/core/DailyVisitor';
import { Facility } from 'src/app/core/Facility';
import { IonicSelectableComponent } from 'ionic-selectable';
import { AdService } from '../../ad/ad.service';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Sort } from 'src/app/filters/Sort';
import { Search } from 'src/app/filters/Search';
import { Filter } from 'src/app/filters/Filter';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { AddPageTrack } from '../../arch/AddPageTrack';
import { AddPageInterface } from '../../arch/AddPageInterface';
import { CacheService } from '../../services/cache.service';
import { Apartment } from 'src/app/core/Apartment';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-add-daily-visitor',
  templateUrl: './add-daily-visitor.page.html',
  styleUrls: ['./add-daily-visitor.page.scss'],
})
@AddPageTrack(
  '/security/add-daily-visitor',
  'DailyVisitor',
  '/GSServer/webapi/secured/Security/DailyVisitor',
  false
)
export class AddDailyVisitorPage implements AddPageInterface {

  public webcamImage: WebcamImage = null;
  form: FormGroup;
  dailyVisitor: DailyVisitor = new DailyVisitor();
  cache: Cache;
  cacheObject: any;
  apartmentCache: Cache = null;
  apartments: Apartment[] = [];

  @ViewChild('selectComponent') selectComponent: IonicSelectableComponent;

  toggle = false;

  constructor(
    public securityService: SecurityService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private platform: Platform, 
    public toaster: ToasterService,
    private cameraService: CameraService, 
    private workspaceToolbarService: WorkspaceToolbarService,
    private modalController: ModalController, 
    private formBuilder: FormBuilder, 
    private loader:LoaderService, 
    private adService: AdService, 
    private ngZone: NgZone,
    private cacheService: CacheService,
    private loginService:LoginService
    ) {
      this.form = this.formBuilder.group({
        name: ['', [Validators.required]],
        phone: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
        address: ['', [Validators.required]],
        visitingTo: ['', [Validators.required]],
      });
     }

    ngOnInit() {
      if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
      {
        this.router.navigate(['/security/join-society']);
      } 

      this.takePicture();

      this.cacheService.initializeCache('Apartment', '/GSServer/webapi/secured/Security/Facilities/Apartment', true).then(cacheObject => {
        this.apartmentCache = cacheObject
        this.apartmentCache.load(CacheService.LOAD_ALL).then(
          res => {
            this.apartments = this.apartmentCache.objects
          }
        )
      });

      this.dailyVisitor = new DailyVisitor();
    }

    ngOnDestroy() {

    }

    onNavigationEnd(): void {
      this.workspaceToolbarService.showBackButton();
      this.adService.reSubscribeAds("NORMAL");
    }
  
   takePicture() {
    if ((this.platform.is('android') || this.platform.is('ios'))) 
    {
      this.cameraService.getPicture().then(
        (imageData) =>
        {
          this.dailyVisitor.profilePicture.content = 'data:image/jpeg;base64,' + imageData.base64String;
        }, 
        (err) =>
        {
          window.alert('Unable to open Camera : ' + err);
        }
      );
    } else {
      this.openWebcam();
    }
  }


  AddDailyVisitor() {
    this.dailyVisitor.name = this.form.get('name').value;
    this.dailyVisitor.phone = this.form.get('phone').value;
    this.dailyVisitor.address = this.form.get('address').value;
    this.dailyVisitor.visitingTo = this.form.get('visitingTo').value;

    this.loader.showLoader();
    this.cache.addObject(this.dailyVisitor)
      .then(
        (response:DailyVisitor) => {
          this.toaster.toast("Daily Visitor added successfully.")
          this.form.reset();
          this.router.navigate(['/security/dashboard/daily-visitors'])
          .finally(
            ()=>{
              this.loader.hideLoader();
            }
          )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader();
        }
      );

  }

  userChanged(event: { component: IonicSelectableComponent, value: any }) {
    console.log('Selected: ', event);
    let selectedFacility:Facility = event.value;
    this.form.get('visitingTo').setValue(selectedFacility);
  }

  onSearch(event: { component: IonicSelectableComponent, text: any }) {
    let text = event.text.trim();
    event.component.startSearch();
    event.component.items = this.filterSearch(text);
    event.component.endSearch();
  }

  onSearchFail(event: { component: IonicSelectableComponent, text: any }) {
    if (event.text != '') {
      this.selectComponent.searchFailText = "Result not found. Apartment with this name does not exist."
    } else {
      this.selectComponent.searchFailText = "Start typing apartment name."
    }
  }

  filterSearch(text: string){
    return this.apartments.filter(apartment => {
      return apartment.name.indexOf(text) !== -1;
    })
  }

  onOpen(){
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("TAB");
  }

  onClose(){
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("NORMAL");
  }

  openFromCode() {
    this.selectComponent.open();
  }

  clear() {
    this.selectComponent.clear();
    this.selectComponent.close();
  }

  toggleItems() {
    this.selectComponent.toggleItems(this.toggle);
    this.toggle = !this.toggle;
  }

  confirm() {
    this.selectComponent.confirm();
    this.selectComponent.close();
  }

  removePicture() {
    let temp:DailyVisitor = new DailyVisitor;
    this.dailyVisitor.profilePicture.content = temp.profilePicture.content;
  }

  async openWebcam() {
    const modal = await this.modalController.create({
      component: WebcamModalComponent,
      componentProps: {
        "modalController": this.modalController,
      },
    });
    modal.onDidDismiss()
      .then((data) => {
        if (data['data'] != null) {
          this.dailyVisitor.profilePicture.content = data['data']; // Here's your selected user!
        }
      });
    return await modal.present();

  }

  handleImage(webcamImage: WebcamImage) {
    this.webcamImage = webcamImage;
  }

}
