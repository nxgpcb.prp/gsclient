import { Component, OnInit, ViewChild, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Subscription } from 'rxjs';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoginService } from 'src/app/login/login.service';
import { SocietyService } from '../../services/society.service';

@Component({
  selector: 'app-complaints',
  templateUrl: './complaints.page.html',
  styleUrls: ['./complaints.page.scss'],
})
@ListPageTrack(
  '/society/complaints',
  'Complaint',
  '/GSServer/webapi/secured/Society/Complaint',
  false
)
export class ComplaintsPage implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService,
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService,
    private cacheService: CacheService,
    private adService: AdService,
    public readonly loginService: LoginService,
  ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addComplaint() {
    this.router.navigate(['/society/complaints/add-complaint'])
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  isExpired(date: Date) {
    return this.getLocalTime(date) < new Date();
  }

  resolve(complaint: any) {
    complaint.status = "Resolved";
    this.cache.updateObject(complaint)
      .then(response => {
        console.log(response);
      },
        error => {
          console.log(error);
        })
  }

  defer(complaint: any) {
    complaint.status = "Deferred";
    this.cache.updateObject(complaint)
      .then(response => {
        console.log(response);
      },
        error => {
          console.log(error);
        })
  }

}
