import { Component, OnInit, ViewChild, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Subscription } from 'rxjs';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoginService } from 'src/app/login/login.service';
import { SocietyService } from '../../services/society.service';

@Component({
  selector: 'app-notices',
  templateUrl: './notices.page.html',
  styleUrls: ['./notices.page.scss'],
})
@ListPageTrack(
  '/society/notices',
  'Notice',
  '/GSServer/webapi/secured/Society/Notice',
  false
)
export class NoticesPage implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
    ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addNotice() {
    this.router.navigate(['/society/notices/add-notice'])
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  isExpired(date: Date) {
    return this.getLocalTime(date) < new Date();
  }

}
