import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NoticesPage } from './notices.page';

const routes: Routes = [
  {
    path: '',
    component: NoticesPage
  },
  { path: 'add-notice', loadChildren: './add-notice/add-notice.module#AddNoticePageModule' },
  { path: 'update-notice/:id', loadChildren: './update-notice/update-notice.module#UpdateNoticePageModule' },

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [NoticesPage]
})
export class NoticesPageModule {}
