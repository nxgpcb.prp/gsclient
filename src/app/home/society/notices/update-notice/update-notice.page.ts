import { Component, OnInit } from '@angular/core';
import { Society } from 'src/app/core/Society';
import { Router, ActivatedRoute } from '@angular/router';
import { Notice } from 'src/app/core/Notice';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { LoginService } from 'src/app/login/login.service';
import { UpdatePageTrack } from 'src/app/home/arch/UpdatePageTrack';
import { UpdatePageInterface } from 'src/app/home/arch/UpdatePageInterface';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AdService } from 'src/app/home/ad/ad.service';

@Component({
  selector: 'app-update-notice',
  templateUrl: './update-notice.page.html',
  styleUrls: ['./update-notice.page.scss'],
})
@UpdatePageTrack(
  '/society/notices/update-notice',
  'Notice',
  '/GSServer/webapi/secured/Society/Notice',
  false
)
export class UpdateNoticePage implements UpdatePageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService:AdService,
    ) 
  {
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      validity: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.form.get('title').setValue(this.cacheObject.title);
    this.form.get('description').setValue(this.cacheObject.description);
    this.form.get('validity').setValue(new Date(this.cacheObject.validity.toString().replace("[UTC]", "")).toISOString());
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  updateNotice() {
    this.cacheObject.title = this.form.get('title').value;
    this.cacheObject.description = this.form.get('description').value;
    this.cacheObject.validity = new Date(this.form.get('validity').value);
    this.loader.showLoader();
    this.cache.updateObject(this.cacheObject).then(
      object=>{
        this.router.navigate(['/society/notices']).finally(
          () => {
            this.loader.hideLoader();
          }
        )
      },
      error=>{
        alert("Error occurred :"+error)
        this.loader.hideLoader()
      }
    );
  }

  removeNotice() {
    this.loader.showLoader();
    this.cache.deleteObject(this.cacheObject).then(
      object=>{
        this.router.navigate(['/society/notices'])
        this.loader.hideLoader()
      },
      error=>{
        alert("Error occurred :"+error)
        this.loader.hideLoader()
      }
    );
  }
}
