import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Notice } from 'src/app/core/Notice';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AddPageInterface } from 'src/app/home/arch/AddPageInterface';
import { AddPageTrack } from 'src/app/home/arch/AddPageTrack';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { SocietyService } from 'src/app/home/services/society.service';

@Component({
  selector: 'app-add-notice',
  templateUrl: './add-notice.page.html',
  styleUrls: ['./add-notice.page.scss'],
})
@AddPageTrack(
  '/society/notices/add-notice',
  'Notice',
  '/GSServer/webapi/secured/Society/Notice',
  false
)
export class AddNoticePage implements AddPageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    private societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      validity: ['', [Validators.required]],
    });
  }

  ngOnInit() {
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addNotice() {
    this.loader.showLoader()
    let notice: Notice = new Notice();
    notice.title = this.form.get('title').value;
    notice.description = this.form.get('description').value;
    notice.validity = new Date(this.form.get('validity').value);
    this.cache.addObject(notice).then(
      success=>
      {
        this.form.reset();
        this.router.navigate(['/society/notices'])
        .finally(
          ()=>this.loader.hideLoader());
      },
      error=>
      {
        console.log(error)
        this.loader.hideLoader();
      }
    );
  }
}
