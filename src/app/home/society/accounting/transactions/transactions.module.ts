import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TransactionsPage } from './transactions.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { UpdateUpiComponent } from './update-upi/update-upi.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionsPage
  },
  {
    path: 'add-transaction',
    loadChildren: './add-transaction/add-transaction.module#AddTransactionPageModule'
  },
  {
    path: 'update-transaction/:id',
    loadChildren: './update-transaction/update-transaction.module#UpdateTransactionPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TranslateModule,
  ],
  declarations: [
    TransactionsPage,
    UpdateUpiComponent
  ],
  entryComponents: [
    UpdateUpiComponent
  ],
})
export class TransactionsPageModule {}
