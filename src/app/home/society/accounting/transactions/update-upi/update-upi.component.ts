import { Component, OnInit, Input } from '@angular/core';
import { SocietyService } from 'src/app/home/services/society.service';
import { CacheService } from 'src/app/home/services/cache.service';
import { LoginService } from 'src/app/login/login.service';
import { PopoverController } from '@ionic/angular';
import { Cache } from '../../../../services/cache';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';

@Component({
  selector: 'app-update-upi',
  templateUrl: './update-upi.component.html',
  styleUrls: ['./update-upi.component.scss'],
})
export class UpdateUpiComponent implements OnInit {
  @Input()
  popoverController: PopoverController;
  accountCache: Cache = null;
  form: FormGroup;

  constructor(
    public societyService: SocietyService,
    private cacheService: CacheService,
    public readonly loginService: LoginService,
    private formBuilder:FormBuilder,
    private loader:LoaderService
  ) { 
    this.form = this.formBuilder.group({
      upiId: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.cacheService.initializeCache('accounts', '/GSServer/webapi/secured/Society/Accounting/Account', true).then(cacheObject => {
      this.accountCache = cacheObject
      this.accountCache.load(CacheService.DEFAULT_LOAD_COUNT)
    });
  }

  async updateUpi() {
    this.loader.showLoader();
    this.accountCache.objects[0].upiId = this.form.get('upiId').value;
    this.accountCache.update(this.accountCache.objects[0]).
      then(
        (response) => {
          console.log(response);
          this.popoverController.dismiss();
          this.loader.hideLoader();
        },
        (error) => {
          console.log(error);
          this.popoverController.dismiss();
          this.loader.hideLoader();
        }
      )
  }

}
