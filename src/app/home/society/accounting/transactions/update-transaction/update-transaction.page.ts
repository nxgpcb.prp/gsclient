import { Component, OnInit } from '@angular/core';
import { Society } from 'src/app/core/Society';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from 'src/app/home/home.service';
import { TransactionLog } from 'src/app/core/TransactionLog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { LoginService } from 'src/app/login/login.service';
import { UpdatePageTrack } from 'src/app/home/arch/UpdatePageTrack';
import { UpdatePageInterface } from 'src/app/home/arch/UpdatePageInterface';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AdService } from 'src/app/home/ad/ad.service';

@Component({
  selector: 'app-update-transaction',
  templateUrl: './update-transaction.page.html',
  styleUrls: ['./update-transaction.page.scss'],
})
@UpdatePageTrack(
  '/society/accounting/transactions/update-transaction',
  'TransactionLog',
  '/GSServer/webapi/secured/Society/Accounting/TransactionLog',
  false
)
export class UpdateTransactionPage implements UpdatePageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService:AdService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      transactionType: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.form.get('name').setValue(this.cacheObject.payment.name);
    this.form.get('transactionType').setValue(this.cacheObject.payment.transactionType);
    this.form.get('amount').setValue(this.cacheObject.payment.amount);
    this.form.get('description').setValue(this.cacheObject.payment.description);
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  updateTransactionLog() {
    this.changeToPositive();
    this.cacheObject.payment.name = this.form.get('name').value;
    this.cacheObject.payment.description = this.form.get('description').value;
    this.cacheObject.payment.transactionType = this.form.get('transactionType').value;
    this.cacheObject.payment.amount = this.form.get('amount').value;

    this.loader.showLoader();
    this.cache.updateObject(this.cacheObject)
      .then(
        (response) => {
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/accounting/transactions'])
          .finally(
            ()=>{
              this.loader.hideLoader();
            }
          )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader();
        }
      );
  }

  removeTransactionLog() {
    this.loader.showLoader()
    this.cache.deleteObject(this.cacheObject)
      .then(
        (response) => {
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/accounting/transactions'])
          .finally(
            ()=>{
              this.loader.hideLoader();
            }
          )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader();
        }
      );
  }

  changeToPositive(){
    let amount = this.form.get('amount').value;
    if(amount < 0){
      this.form.get('amount').setValue(amount*-1);
    }
  }
}
