import { Component, OnInit, ViewChild, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { TransactionLog } from 'src/app/core/TransactionLog';
import { Router, NavigationEnd } from '@angular/router';
import { IonInfiniteScroll, Platform, PopoverController } from '@ionic/angular';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Society } from 'src/app/core/Society';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { SocietyService } from 'src/app/home/services/society.service';
import { HomeService } from 'src/app/home/home.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../../services/cache';
import { environment } from 'src/environments/environment';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoginService } from 'src/app/login/login.service';
import { UpdateUpiComponent } from './update-upi/update-upi.component';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.page.html',
  animations: [
    trigger('readUnread', [
      state('read', style({
        backgroundColor: 'lightgray',
      })),
      state('unread', style({
        backgroundColor: 'white',
      })),
      transition('read=>unread', animate('900ms')),
      transition('unread=>read', animate('900ms')),
    ]),
    trigger('slidelefttitle', [
      transition('void => *', [
        style({ opacity: 0, transform: 'translateX(150%)' }),
        animate('900ms 300ms ease-out', style({ transform: 'translateX(0%)', opacity: 1 },))
      ])
    ])
  ],
  styleUrls: ['./transactions.page.scss'],
})
@ListPageTrack(
  '/society/accounting/transactions',
  'TransactionLog',
  '/GSServer/webapi/secured/Society/Accounting/TransactionLog',
  false
)
export class TransactionsPage implements ListPageInterface {

  upiIdInputChanged: boolean = false;
  updateUpiIdInProgress: boolean = false;
  cache: Cache
  accountCache: Cache = null;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService,
    private router: Router,
    private cacheService: CacheService,
    public readonly loginService: LoginService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService: AdService,
    private popoverController: PopoverController
  ) {
  }

  ngOnInit(): void {
    this.cacheService.initializeCache('accounts', '/GSServer/webapi/secured/Society/Accounting/Account', true).then(cacheObject => {
      this.accountCache = cacheObject
      this.accountCache.load(CacheService.DEFAULT_LOAD_COUNT);

    });

  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addTransactionLog() {
    this.router.navigate(['/society/accounting/transactions/add-transaction'])
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  upiIdChanged() {
    this.upiIdInputChanged = true;
  }

  async updateUpiId(ev: any) {
    const popover = await this.popoverController.create({
      component: UpdateUpiComponent,
      componentProps: {
        "popoverController": this.popoverController,
      },
      event: ev,
      translucent: true,
      backdropDismiss: true
    });

    return await popover.present();

  }
}
