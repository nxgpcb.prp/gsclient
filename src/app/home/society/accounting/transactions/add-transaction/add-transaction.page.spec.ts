import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { IonicModule } from '@ionic/angular';

import { AddTransactionPage } from './add-transaction.page';
import { HomeService } from 'src/app/home/home.service';
import { Society } from 'src/app/core/Society';

describe('AddTransactionPage', () => {
  let component: AddTransactionPage;
  let fixture: ComponentFixture<AddTransactionPage>;
  let de: DebugElement;
  let homeService: HomeService;
  let spy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddTransactionPage],
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        HttpClientModule,
        IonicModule.forRoot(),
      ],
      providers: [
        Firebase,
        AngularFireMessaging,
        HomeService
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AddTransactionPage);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    homeService = de.injector.get(HomeService);
    let user: Society = new Society();
    user.createdOn = new Date("2020-03-11T16:07:47Z");
    user.emailId = "todkarsayali@gmail.com"
    user.id = 5
    user.isActive = true
    user.lastModified = new Date("2020-07-03T17:25:27Z")
    user.name = "Royal Oak"
    user.phone = ""
    //user.profilePicture = { createdOn: "2020-03-30T16:35:56Z[UTC]", id: 113, lastModified: "2020-06-14T11:31:22Z[UTC]", type: "Image", content: "", … }
    user.status = "Happy"
    //user.subscriptionPlan = { createdOn: "2020-03-11T16:02:52Z[UTC]", id: 2, lastModified: "2020-06-14T11:31:22Z[UTC]", type: "SubscriptionPlan", duration: 365, … }
    user.type = "Society"
    spy = spyOn(homeService, 'getUser').and.returnValue(user);

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
