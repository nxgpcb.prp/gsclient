import { Component, OnInit, ViewChild } from '@angular/core';
import { SocietyService } from 'src/app/home/services/society.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Facility } from 'src/app/core/Facility';
import { Resident } from 'src/app/core/Resident';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Society } from 'src/app/core/Society';
import { HomeService } from 'src/app/home/home.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { Transaction } from 'src/app/core/Transaction';
import { TransactionLog } from 'src/app/core/TransactionLog';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { LoginService } from 'src/app/login/login.service';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AddPageInterface } from 'src/app/home/arch/AddPageInterface';
import { AddPageTrack } from 'src/app/home/arch/AddPageTrack';

@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.page.html',
  styleUrls: ['./add-transaction.page.scss'],
})
@AddPageTrack(
  '/society/accounting/transactions/add-transaction',
  'TransactionLog',
  '/GSServer/webapi/secured/Society/Accounting/TransactionLog',
  false
)
export class AddTransactionPage implements AddPageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;
  apartmentCache:Cache = null;

  @ViewChild('selectComponent') selectComponent: IonicSelectableComponent;
  toggle = false;
  selected = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    private societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      transactionType: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      description: ['', [Validators.required]],
    })
  }

  ngOnInit() {
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addTransactionLog() {
    this.changeToPositive();
    let transaction: Transaction = new Transaction();
    transaction.name = this.form.get('name').value;
    transaction.description = this.form.get('description').value;
    transaction.transactionType = this.form.get('transactionType').value;
    transaction.amount = this.form.get('amount').value;

    let transactionLog: TransactionLog = new TransactionLog();
    transactionLog.payment = transaction;

    this.cache.addObject(transactionLog)
      .then(
        (response) => {
          this.toaster.toast("Transaction added successfully.")
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/accounting/transactions'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          this.toaster.toast(error.error.errorMessage + '. Transaction not added.');
          console.log(error);
          this.loader.hideLoader();
        }
      );
  }

  userChanged(event: { component: IonicSelectableComponent, value: any }) {
    console.log('Selected: ', event);
    this.form.get('owner').setValue(event.value);
  }

  changeToPositive(){
    let amount = this.form.get('amount').value;
    if(amount < 0){
      this.form.get('amount').setValue(amount*-1);
    }
  }

  onOpen() {
    this.adService.reSubscribeAds("TAB");
  }

  onClose() {
    this.adService.reSubscribeAds("NORMAL");
  }

  openFromCode() {
    this.selectComponent.open();
  }

  clear() {
    this.selectComponent.clear();
    this.selectComponent.close();
  }

  toggleItems() {
    this.selectComponent.toggleItems(this.toggle);
    this.toggle = !this.toggle;
  }

  confirm() {
    this.selectComponent.confirm();
    this.selectComponent.close();
  }

}
