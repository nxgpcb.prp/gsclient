import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AccountingPage } from './accounting.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: AccountingPage,
    redirectTo: '/society/accounting/transactions',
    pathMatch: 'full'
  },
  {
    path: 'transactions',
    loadChildren: './transactions/transactions.module#TransactionsPageModule'
  },
  {
    path: 'demand',
    loadChildren: './demand/demand.module#DemandPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TranslateModule,
  ],
  declarations: [AccountingPage]
})
export class AccountingPageModule {}
