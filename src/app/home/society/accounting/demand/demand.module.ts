import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DemandPage } from './demand.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: DemandPage
  },
  {
    path: 'add-demand',
    loadChildren: './add-demand/add-demand.module#AddDemandPageModule'
  },
  {
    path: 'update-demand/:id',
    loadChildren: './update-demand/update-demand.module#UpdateDemandPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TranslateModule,
  ],
  declarations: [DemandPage]
})
export class DemandPageModule {}
