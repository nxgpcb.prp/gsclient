import { Component, OnInit } from '@angular/core';
import { Society } from 'src/app/core/Society';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from 'src/app/home/home.service';
import { DemandLog } from 'src/app/core/DemandLog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { LoginService } from 'src/app/login/login.service';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { UpdatePageTrack } from 'src/app/home/arch/UpdatePageTrack';
import { UpdatePageInterface } from 'src/app/home/arch/UpdatePageInterface';
import { AdService } from 'src/app/home/ad/ad.service';

@Component({
  selector: 'app-update-demand',
  templateUrl: './update-demand.page.html',
  styleUrls: ['./update-demand.page.scss'],
})
@UpdatePageTrack(
  '/society/accounting/demand/update-demand',
  'DemandLog',
  '/GSServer/webapi/secured/Society/Accounting/DemandLog',
  false
)
export class UpdateDemandPage implements UpdatePageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService:AdService,
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      dueDate: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.form.get('name').setValue(this.cacheObject.demand.name);
    this.form.get('dueDate').setValue(new Date(this.cacheObject.demand.dueDate.toString().replace("[UTC]", "")).toISOString());
    this.form.get('amount').setValue(this.cacheObject.demand.amount);
    this.form.get('description').setValue(this.cacheObject.demand.description);
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  updateDemandLog() {
    this.changeToPositive();
    this.cacheObject.demand.name = this.form.get('name').value;
    this.cacheObject.demand.description = this.form.get('description').value;
    this.cacheObject.demand.dueDate = this.form.get('dueDate').value;
    this.cacheObject.demand.amount = this.form.get('amount').value;

    this.loader.showLoader();
    this.cache.updateObject(this.cacheObject)
      .then(
        (response) => {
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/accounting/demand'])
          .finally(
            ()=>{
              this.loader.hideLoader();
            }
          )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader();
        }
      );
  }

  removeDemandLog() {
    this.loader.showLoader()
    this.cache.deleteObject(this.cacheObject)
      .then(
        (response) => {
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/accounting/demand'])
          .finally(
            ()=>{
              this.loader.hideLoader();
            }
          )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader();
        }
      );
  }

  changeToPositive(){
    let amount = this.form.get('amount').value;
    if(amount < 0){
      this.form.get('amount').setValue(amount*-1);
    }
  }

}
