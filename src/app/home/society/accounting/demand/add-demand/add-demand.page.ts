import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { SocietyService } from 'src/app/home/services/society.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Apartment } from 'src/app/core/Apartment';
import { Resident } from 'src/app/core/Resident';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Society } from 'src/app/core/Society';
import { HomeService } from 'src/app/home/home.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { Demand } from 'src/app/core/Demand';
import { DemandLog } from 'src/app/core/DemandLog';
import { Subscription } from 'rxjs/internal/Subscription';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Sort } from 'src/app/filters/Sort';
import { Search } from 'src/app/filters/Search';
import { Filter } from 'src/app/filters/Filter';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LoginService } from 'src/app/login/login.service';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AddPageInterface } from 'src/app/home/arch/AddPageInterface';
import { AddPageTrack } from 'src/app/home/arch/AddPageTrack';

@Component({
  selector: 'app-add-demand',
  templateUrl: './add-demand.page.html',
  styleUrls: ['./add-demand.page.scss'],
})
@AddPageTrack(
  '/society/accounting/demand/add-demand',
  'DemandLog',
  '/GSServer/webapi/secured/Society/Accounting/DemandLog',
  false
)
export class AddDemandPage implements AddPageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;
  apartmentCache:Cache = null;

  @ViewChild('selectComponent') selectComponent: IonicSelectableComponent;
  toggle = false;
  selected = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    public readonly societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      dueDate: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      description: ['', [Validators.required]],
      payees: ['', [Validators.required]],
    })
  }

  ngOnInit() {
    this.cacheService.initializeCache('apartments','/GSServer/webapi/secured/Society/Facilities/Apartment',true).then(cacheObject=>{
      this.apartmentCache=cacheObject
      this.apartmentCache.load(CacheService.DEFAULT_LOAD_COUNT);
  });
  }

  ngOnDestroy()
  {

  }
  
  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addDemandLog() {
    this.changeToPositive();
    let demand: Demand = new Demand();
    demand.name = this.form.get('name').value;
    demand.description = this.form.get('description').value;
    demand.dueDate = this.form.get('dueDate').value;
    demand.amount = this.form.get('amount').value;

    let payees = this.form.get('payees').value;
    console.log(payees)
    payees.forEach((payee:Apartment, idx) => {
      let demandLog: DemandLog = new DemandLog();
      demandLog.demand = demand;
      demandLog.payee = payee;

      this.cache.addObject(demandLog)
        .then(
          (response) => {
            this.toaster.toast("Demand added for "+ payee.name +" successfully.")
            console.log(response)
            this.form.reset();
            if (idx === payees.length - 1) {
              this.router.navigate(['/society/accounting/demand'])
                .finally(
                  () => {
                    this.loader.hideLoader();
                  }
                )
            }
          },
          (error) => {
            this.toaster.toast(error.error.errorMessage + '. Demand not added.');
            console.log(error);
            this.loader.hideLoader();
          }
        );
    });
  }

  changeToPositive(){
    let amount = this.form.get('amount').value;
    if(amount < 0){
      this.form.get('amount').setValue(amount*-1);
    }
  }


  userChanged(event: { component: IonicSelectableComponent, value: any }) {
    console.log('Selected: ', event);
    let selectedPayees: Apartment[] = event.value;
    this.form.get('payees').setValue(selectedPayees);
  }

  onSearch(event: { component: IonicSelectableComponent, text: any }) {
    let text = event.text.trim();
    event.component.startSearch();
    event.component.items = this.filterSearch(text);
    event.component.endSearch();
  }

  onSearchFail(event: { component: IonicSelectableComponent, text: any }) {
    if (event.text != '') {
      this.selectComponent.searchFailText = "Result not found. Apartment with this name does not exist."
    } else {
      this.selectComponent.searchFailText = "Start typing apartment name."
    }
  }

  filterSearch(text: string){
    return this.apartmentCache.objects.filter(facility => {
      return facility.name.indexOf(text) !== -1;
    })
  }

  onOpen() {
    this.adService.reSubscribeAds("TAB");
  }

  onClose() {
    this.adService.reSubscribeAds("NORMAL");
  }

  openFromCode() {
    this.selectComponent.open();
  }

  clear() {
    this.selectComponent.clear();
    this.selectComponent.close();
  }

  toggleItems() {
    this.selectComponent.toggleItems(this.toggle);
    this.toggle = !this.toggle;
  }

  confirm() {
    this.selectComponent.confirm();
    this.selectComponent.close();
  }

}
