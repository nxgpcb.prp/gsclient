import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.page.html',
  styleUrls: ['./accounting.page.scss'],
})
export class AccountingPage implements OnInit, OnDestroy {

  navigationSubscription: Subscription;

  constructor(private router:Router) {
    
   }

   ngOnInit() {
    this.init();
  }

  initDataService(){

  }

  ngOnDestroy() {
    this.unRegisterToEvents();
  }

  init() {
    this.navigate();
  }

  navigate() {
  }

  private unRegisterToEvents() {
    this.navigationSubscription.unsubscribe();
  }

}
