import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { Society } from 'src/app/core/Society';
import { HomeService } from '../../home.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';

@Component({
  selector: 'app-checkout-order',
  templateUrl: './checkout-order.page.html',
  styleUrls: ['./checkout-order.page.scss'],
})
export class CheckoutOrderPage implements OnInit {

  form: FormGroup;
  user: Society;
  email: any;
  phone: any;
  orderid: any;
  customerid: any;
  amount: any;
  paymentFormURL: String = environment.backend.baseURL + "/GSServer/pgRedirect.jsp"

  constructor(private router: Router, private route: ActivatedRoute,
    private formBuilder: FormBuilder, private platform: Platform, private loginService: LoginService, 
    private workspaceToolbarService: WorkspaceToolbarService) {
      this.workspaceToolbarService.hideBackButton();
    }

  ngOnInit() {
    this.form = this.formBuilder.group({
      ORDER_ID: ['', [Validators.required]],
      CUST_ID: ['', [Validators.required]],
      TXN_AMOUNT: ['', [Validators.required]],
      EMAIL: ['', [Validators.required]],
      MOBILE_NO: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
    });

    this.router.events.subscribe(async (value) => {
      if (value instanceof NavigationEnd && this.router.url.indexOf('/society/checkout-order') > -1) {
        console.log(value);
        
        this.user = await this.loginService.getUser()
        this.orderid = Math.floor(100000 + Math.random() * 900000);
        this.email = this.user.emailId;
        this.phone = this.user.phone;
        this.customerid = this.user.id;
        this.route.queryParams
          .subscribe(params => {
            console.log(params); 
            if(params.plan == "Basic"){
              this.amount = 36000
            }
            if(params.plan == "Standard"){
              this.amount = 80000
            }
            if(params.plan == "Premium"){
              this.amount = 150000
            }
            console.log(this.amount); 
          });
      }
    });
  }

  order(form: FormGroup) {

  }

}
