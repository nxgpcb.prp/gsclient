import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonInfiniteScroll } from '@ionic/angular';
import { LoginService } from 'src/app/login/login.service';
import { AdService } from '../../ad/ad.service';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { Cache } from '../../services/cache';
import { CacheService } from '../../services/cache.service';
import { SocietyService } from '../../services/society.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';

@Component({
  selector: 'app-committee',
  templateUrl: './committee.page.html',
  styleUrls: ['./committee.page.scss'],
})
@ListPageTrack(
  '/society/committee',
  'CommitteeMember',
  '/GSServer/webapi/secured/Society/CommitteeMember',
  false
)
export class CommitteePage implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addCommitteeMember() {
    this.router.navigate(['/society/committee/add-committee-member'])
  }

  removeCommitteeMember(Id) {
    this.societyService.setProgressing(true);
    this.cache.deleteObject(this.cache.objects[Id])
      .then(
        response => {
          if (response) {
          }
          this.societyService.setProgressing(false);
        },
        error => {
          console.log(error);
          this.societyService.setProgressing(false);
        }
      )
  }
}
