import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Notice } from 'src/app/core/Notice';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AddPageInterface } from 'src/app/home/arch/AddPageInterface';
import { AddPageTrack } from 'src/app/home/arch/AddPageTrack';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { CommitteeMember } from 'src/app/core/CommitteeMember';
import { Resident } from 'src/app/core/Resident';
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'app-add-committee-member',
  templateUrl: './add-committee-member.page.html',
  styleUrls: ['./add-committee-member.page.scss'],
})
@AddPageTrack(
  '/society/notices/add-committee-member',
  'CommitteeMember',
  '/GSServer/webapi/secured/Society/CommitteeMember',
  false
)
export class AddCommitteeMemberPage implements OnInit {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;
  residentCache: Cache = null;
  residents: Resident[] = [];
  toggle = false;

  @ViewChild('selectComponent') selectComponent: IonicSelectableComponent;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    private societyService:SocietyService
  ) { 
    this.form = this.formBuilder.group({
      resident: ['', [Validators.required]],
      role: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.cacheService.initializeCache('Resident', '/GSServer/webapi/secured/Society/Resident', true).then(cacheObject => {
      this.residentCache = cacheObject
      this.residentCache.load(CacheService.LOAD_ALL).then(
        res => {
          this.residents = this.residentCache.objects
        }
      )
    });
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addCommitteeMember() {
    this.loader.showLoader()
    let committeeMember: CommitteeMember = new CommitteeMember();
    committeeMember.resident = this.form.get('resident').value;
    committeeMember.role = this.form.get('role').value;
    this.cache.addObject(committeeMember).then(
      success=>
      {
        this.form.reset();
        this.router.navigate(['/society/committee'])
        .finally(
          ()=>this.loader.hideLoader());
      },
      error=>
      {
        console.log(error)
        this.loader.hideLoader();
      }
    );
  }

  onSearch(event: { component: IonicSelectableComponent, text: any }) {
    let text = event.text.trim();
    event.component.startSearch();
    event.component.items = this.filterSearch(text);
    event.component.endSearch();
  }

  onSearchFail(event: { component: IonicSelectableComponent, text: any }) {
    if (event.text != '') {
      this.selectComponent.searchFailText = "Result not found. Resident with this name does not exist."
    } else {
      this.selectComponent.searchFailText = "Start typing Resident name."
    }
  }

  filterSearch(text: string) {
    return this.residents.filter(resident => {
      return resident.name.indexOf(text) !== -1;
    })
  }

  onOpen() {
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("TAB");
  }

  onClose() {
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("NORMAL");
  }

  openFromCode() {
    this.selectComponent.open();
  }

  clear() {
    this.selectComponent.clear();
    this.selectComponent.close();
  }

  toggleItems() {
    this.selectComponent.toggleItems(this.toggle);
    this.toggle = !this.toggle;
  }

  confirm() {
    this.selectComponent.confirm();
    this.selectComponent.close();
  }

}
