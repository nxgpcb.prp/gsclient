import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router, NavigationEnd } from '@angular/router';
import { HomeService } from '../../home.service';
import { SocietyService } from '../../services/society.service';
import { SubscriptionPlanLog } from 'src/app/core/SubscriptionPlanLog';
import { messageService } from "src/messageService";
import { FcmService } from 'src/app/notifications/fcm.service';
import { ResidentService } from '../../services/resident.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { Society } from 'src/app/core/Society';
import { Storage } from 'src/app/core/Storage';
import { Subscription } from 'rxjs';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoginService } from 'src/app/login/login.service';
import { Cache } from '../../services/cache';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.page.html',

  styleUrls: ['./subscription.page.scss'],
})
@ListPageTrack(
  '/society/subscription',
  'subscriptionPlanLogs',
  '/GSServer/webapi/secured/Society/SubscriptionPlanLog',
  false
)
export class SubscriptionPage implements ListPageInterface {

  cache: Cache

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
    ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  goToPricingPage() {
    this.router.navigate(['/society/subscription/pricing']);
  }

  }
