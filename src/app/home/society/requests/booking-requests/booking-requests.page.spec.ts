import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookingRequestsPage } from './booking-requests.page';

describe('BookingRequestsPage', () => {
  let component: BookingRequestsPage;
  let fixture: ComponentFixture<BookingRequestsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingRequestsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookingRequestsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
