import { Component, OnInit, ViewChild, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { Search } from 'src/app/filters/Search';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../../services/cache';
import { environment } from 'src/environments/environment';
import { SocietyService } from 'src/app/home/services/society.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-booking-requests',
  templateUrl: './booking-requests.page.html',

  styleUrls: ['./booking-requests.page.scss'],
})
@ListPageTrack(
  '/society/requests/booking-requests',
  'BookingLog',
  '/GSServer/webapi/secured/Society/Facilities/BookingLog',
  false
)
export class BookingRequestsPage implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
    ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  approveBookingLog(bookingLog: any) {
    bookingLog.approvalStatus = "Approved";
    this.cache.updateObject(bookingLog)
      .then(response => {
        console.log(response);
      },
        error => {
          console.log(error);
        })
  }

  rejectBookingLog(bookingLog: any) {
    bookingLog.approvalStatus = "Rejected";
    this.cache.updateObject(bookingLog)
      .then(response => {
        console.log(response);
      },
        error => {
          console.log(error);
        })
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }
}
