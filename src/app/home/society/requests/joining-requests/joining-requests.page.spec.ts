import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JoiningRequestsPage } from './joining-requests.page';

describe('JoiningRequestsPage', () => {
  let component: JoiningRequestsPage;
  let fixture: ComponentFixture<JoiningRequestsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoiningRequestsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JoiningRequestsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
