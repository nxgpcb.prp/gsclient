import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RequestsPage } from './requests.page';

const routes: Routes = [
  {
    path: '',
    component: RequestsPage,
    redirectTo: '/society/requests/booking-requests',
    pathMatch: 'full'
  },
  {
    path: 'joining-requests', loadChildren: './joining-requests/joining-requests.module#JoiningRequestsPageModule'
  },
  {
    path: 'booking-requests', loadChildren: './booking-requests/booking-requests.module#BookingRequestsPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [RequestsPage]
})
export class RequestsPageModule {}
