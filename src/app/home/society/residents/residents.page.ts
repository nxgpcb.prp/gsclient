import { Component, OnInit, ViewChild, NgZone, OnDestroy } from '@angular/core';
import { SocietyService } from '../../services/society.service';
import { Router, NavigationEnd } from '@angular/router';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { Cache } from '../../services/cache';
import { LoginService } from 'src/app/login/login.service';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';

@Component({
  selector: 'app-residents',
  templateUrl: './residents.page.html',

  styleUrls: ['./residents.page.scss'],
})
@ListPageTrack(
  '/society/residents',
  'Resident',
  '/GSServer/webapi/secured/Society/Resident',
  false
)
export class ResidentsPage implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
    ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  removeResident(Id) {
    this.societyService.setProgressing(true);
    this.cache.deleteObject(this.cache.objects[Id])
      .then(
        response => {
          if (response) {
          }
          this.societyService.setProgressing(false);
        },
        error => {
          console.log(error);
          this.societyService.setProgressing(false);
        }
      )
  }
}
