import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { VisitorLogsPage } from './visitor-logs.page';

const routes: Routes = [
  {
    path: '',
    component: VisitorLogsPage,
    pathMatch: 'full'
  },
  {
    path: 'visitor-log-detail/:id',
    loadChildren: './visitor-log-detail/visitor-log-detail.module#VisitorLogDetailPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [VisitorLogsPage]
})
export class VisitorLogsPageModule { }
