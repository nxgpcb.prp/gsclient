import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { UpdatePageTrack } from '../../../arch/UpdatePageTrack';
import { UpdatePageInterface } from '../../../arch/UpdatePageInterface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { LoaderService } from 'src/app/misc/loader.service';
import { AdService } from 'src/app/home/ad/ad.service';


@Component({
  selector: 'app-visitor-log-detail',
  templateUrl: './visitor-log-detail.page.html',
  styleUrls: ['./visitor-log-detail.page.scss'],
})
@UpdatePageTrack(
  '/society/visitor-logs/visitor-log-detail',
  'VisitorLog',
  '/GSServer/webapi/secured/Society/VisitorLog',
  false
)
export class VisitorLogDetailPage implements UpdatePageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService:AdService,
  ) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

}

