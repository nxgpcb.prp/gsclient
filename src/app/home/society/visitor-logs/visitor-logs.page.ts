import { Component, OnInit, ViewChild, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { SocietyService } from '../../services/society.service';
import { Router, NavigationEnd } from '@angular/router';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { HomeService } from '../../home.service';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-visitor-logs',
  templateUrl: './visitor-logs.page.html',

  styleUrls: ['./visitor-logs.page.scss'],
})
@ListPageTrack(
  '/society/visitor-logs',
  'VisitorLog',
  '/GSServer/webapi/secured/Society/VisitorLog',
  false
)
export class VisitorLogsPage implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
    ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }
}
