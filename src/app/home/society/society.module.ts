import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SocietyPage } from './society.page';
import { PopoverMenuComponent } from '../popover-menu/popover-menu.component';
import { WorkspaceToolbarModule } from '../workspace-toolbar/workspace-toolbar.module';

const routes: Routes = [
  {
    path: '',
    component: SocietyPage,
    children: [
      { 
        path: '', loadChildren: './facilities/facilities.module#FacilitiesPageModule'
      },
      { path: 'visitor-logs', loadChildren: './visitor-logs/visitor-logs.module#VisitorLogsPageModule' },
      { path: 'requests', loadChildren: './requests/requests.module#RequestsPageModule' },
      { path: 'facilities', loadChildren: './facilities/facilities.module#FacilitiesPageModule' },
      { path: 'notices', loadChildren: './notices/notices.module#NoticesPageModule' },
      { path: 'securities', loadChildren: './securities/securities.module#SecuritiesPageModule' },
      { path: 'residents', loadChildren: './residents/residents.module#ResidentsPageModule' },
      { path: 'subscription', loadChildren: './subscription/subscription.module#SubscriptionPageModule' },
      { path: 'checkout-order', loadChildren: './checkout-order/checkout-order.module#CheckoutOrderPageModule' },
      { path: 'accounting', loadChildren: './accounting/accounting.module#AccountingPageModule' },
      { path: 'committee', loadChildren: './committee/committee.module#CommitteePageModule' },
      { path: 'complaints', loadChildren: './complaints/complaints.module#ComplaintsPageModule' },
    ]
  },  
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    WorkspaceToolbarModule
  ],
  declarations: [
    SocietyPage
  ],
  entryComponents: [
    PopoverMenuComponent
  ],
})
export class SocietyPageModule {}
