import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdateClubHousePage } from './update-club-house.page';

describe('UpdateClubHousePage', () => {
  let component: UpdateClubHousePage;
  let fixture: ComponentFixture<UpdateClubHousePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClubHousePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateClubHousePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
