import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddClubHousePage } from './add-club-house.page';

describe('AddClubHousePage', () => {
  let component: AddClubHousePage;
  let fixture: ComponentFixture<AddClubHousePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClubHousePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddClubHousePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
