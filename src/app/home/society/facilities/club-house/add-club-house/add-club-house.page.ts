import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AddPageInterface } from 'src/app/home/arch/AddPageInterface';
import { AddPageTrack } from 'src/app/home/arch/AddPageTrack';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { ClubHouse } from 'src/app/core/ClubHouse';

@Component({
  selector: 'app-add-club-house',
  templateUrl: './add-club-house.page.html',
  styleUrls: ['./add-club-house.page.scss'],
})
@AddPageTrack(
  '/society/facilities/club-house/add-club-house',
  'ClubHouse',
  '/GSServer/webapi/secured/Society/Facilities/ClubHouse',
  false
)
export class AddClubHousePage implements AddPageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cache: Cache;
  cacheObject: any;
  apartmentCache:Cache = null;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    public readonly societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
    })
  }

  ngOnInit() {
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addClubHouse() {
    this.loader.showLoader();
    let clubHouse: ClubHouse = new ClubHouse();
    clubHouse.name = this.form.get('name').value;
    this.cache.addObject(clubHouse)
      .then(
        (response) => {
          this.toaster.toast("ClubHouse added successfully.")
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/facilities/club-house'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          this.toaster.toast(error.error.errorMessage + '. ClubHouse not added.');
          console.log(error);
          this.loader.hideLoader();
        }
      );
  }
}
