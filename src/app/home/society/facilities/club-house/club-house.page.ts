import { Component, OnInit, ViewChild, NgZone, OnDestroy } from '@angular/core';
import { ClubHouse } from 'src/app/core/ClubHouse';
import { Router, NavigationEnd } from '@angular/router';
import { SocietyService } from '../../../services/society.service';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Society } from 'src/app/core/Society';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../../services/cache';
import { environment } from 'src/environments/environment';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-club-house',
  templateUrl: './club-house.page.html',

  styleUrls: ['./club-house.page.scss'],
})
@ListPageTrack(
  '/society/facilities/club-house',
  'ClubHouse',
  '/GSServer/webapi/secured/Society/Facilities/ClubHouse',
  false
)
export class ClubHousePage  implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
    ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addFacility() {
    this.router.navigate(['/society/facilities/club-house/add-club-house'])
  }
}
