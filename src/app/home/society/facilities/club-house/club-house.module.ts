import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ClubHousePage } from './club-house.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: ClubHousePage
  },
  {
    path: 'booking-log', loadChildren: '../booking-log/booking-log.module#BookingLogPageModule'
  },

  {
    path: 'update-club-house/:id',
    loadChildren: './update-club-house/update-club-house.module#UpdateClubHousePageModule'
  },
  {
    path: 'add-club-house',
    loadChildren: './add-club-house/add-club-house.module#AddClubHousePageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ClubHousePage]
})
export class ClubHousePageModule {}
