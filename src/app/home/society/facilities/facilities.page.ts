import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-facilities',
  templateUrl: './facilities.page.html',
  styleUrls: ['./facilities.page.scss'],
})
export class FacilitiesPage implements OnInit, OnDestroy {

  navigationSubscription: Subscription;

  constructor(private router:Router) {
    
   }

   ngOnInit() {
    this.init();
  }

  initDataService(){

  }

  ngOnDestroy() {
    this.unRegisterToEvents();
  }

  init() {
    this.navigate();
  }

  navigate() {
    }

  private unRegisterToEvents() {
    this.navigationSubscription.unsubscribe();
  }
}
