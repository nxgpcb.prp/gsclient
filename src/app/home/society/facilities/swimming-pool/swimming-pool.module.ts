import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SwimmingPoolPage } from './swimming-pool.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: SwimmingPoolPage
  },

  {
    path: 'update-swimming-pool/:id',
    loadChildren: './update-swimming-pool/update-swimming-pool.module#UpdateSwimmingPoolPageModule'
  },
  {
    path: 'add-swimming-pool',
    loadChildren: './add-swimming-pool/add-swimming-pool.module#AddSwimmingPoolPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [SwimmingPoolPage]
})
export class SwimmingPoolPageModule {}
