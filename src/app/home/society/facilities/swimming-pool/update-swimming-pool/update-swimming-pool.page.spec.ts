import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdateSwimmingPoolPage } from './update-swimming-pool.page';

describe('UpdateSwimmingPoolPage', () => {
  let component: UpdateSwimmingPoolPage;
  let fixture: ComponentFixture<UpdateSwimmingPoolPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSwimmingPoolPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateSwimmingPoolPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
