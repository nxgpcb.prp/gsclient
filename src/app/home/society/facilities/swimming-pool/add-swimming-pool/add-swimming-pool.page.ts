import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AddPageInterface } from 'src/app/home/arch/AddPageInterface';
import { AddPageTrack } from 'src/app/home/arch/AddPageTrack';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { SwimmingPool } from 'src/app/core/SwimmingPool';


@Component({
  selector: 'app-add-swimming-pool',
  templateUrl: './add-swimming-pool.page.html',
  styleUrls: ['./add-swimming-pool.page.scss'],
})
@AddPageTrack(
  '/society/facilities/swimming-pool/add-swimming-pool',
  'SwimmingPool',
  '/GSServer/webapi/secured/Society/Facilities/SwimmingPool',
  false
)
export class AddSwimmingPoolPage implements AddPageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cache: Cache;
  cacheObject: any;
  apartmentCache:Cache = null;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    public readonly societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]]
    })
  }

  ngOnInit() {
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addSwimmingPool() {
    this.loader.showLoader();
    let swimmingPool: SwimmingPool = new SwimmingPool();
    swimmingPool.name = this.form.get('name').value;
    this.cache.addObject(swimmingPool)
      .then(
        (response) => {
          this.toaster.toast("SwimmingPool added successfully.")
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/facilities/swimming-pool'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          this.toaster.toast(error.error.errorMessage + '. SwimmingPool not added.');
          console.log(error);
          this.loader.hideLoader();
        }
      );
  }

}
