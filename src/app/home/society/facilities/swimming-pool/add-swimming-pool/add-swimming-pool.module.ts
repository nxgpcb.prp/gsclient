import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicSelectableModule } from 'ionic-selectable';
import { AddSwimmingPoolPage } from './add-swimming-pool.page';

const routes: Routes = [
  {
    path: '',
    component: AddSwimmingPoolPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TranslateModule,
    IonicSelectableModule
  ],
  declarations: [AddSwimmingPoolPage]
})
export class AddSwimmingPoolPageModule {}
