import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ApartmentPage } from './apartment.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: ApartmentPage
  },

  { path: 'add-apartment', loadChildren: './add-apartment/add-apartment.module#AddApartmentPageModule' },
  {
    path: 'update-apartment/:id', loadChildren: './update-apartment/update-apartment.module#UpdateApartmentPageModule'
  },

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ApartmentPage]
})
export class ApartmentPageModule {}
