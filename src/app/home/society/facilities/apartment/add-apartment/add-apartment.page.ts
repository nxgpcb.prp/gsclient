import { Component, OnInit, ViewChild } from '@angular/core';
import { SocietyService } from 'src/app/home/services/society.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Apartment } from 'src/app/core/Apartment';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { LoaderService } from 'src/app/misc/loader.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { PopoverController } from '@ionic/angular';
import { IonicSelectableComponent } from 'ionic-selectable';
import { AdService } from '../../../../ad/ad.service';
import { BusinessPageTrack } from 'src/app/home/arch/BusinessPageTrack';
import { BusinessPageInterface } from 'src/app/home/arch/BusinessPageInterface';
import { Cache } from '../../../../services/cache';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FcmService } from 'src/app/notifications/fcm.service';
import { isNumber } from 'util';
import { AddPageTrack } from 'src/app/home/arch/AddPageTrack';
import { CacheService } from 'src/app/home/services/cache.service';
import { AddPageInterface } from 'src/app/home/arch/AddPageInterface';

@Component({
  selector: 'app-add-apartment',
  templateUrl: './add-apartment.page.html',
  styleUrls: ['./add-apartment.page.scss'],
})
@AddPageTrack(
  '/society/facilities/apartment/add-apartment',
  'Apartment',
  '/GSServer/webapi/secured/Society/Facilities/Apartment',
  false
)
export class AddApartmentPage implements AddPageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cache: Cache;
  cacheObject: any;
  popover:any;
  toggle = false;
  apartments: Apartment[] = [];
  isChecked: Boolean = true;
  isSubmit: Boolean = false;
  justLoaded: Boolean = true;

  @ViewChild('selectComponent') selectComponent: IonicSelectableComponent;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    public readonly societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      totalApartments: ['', Validators.pattern('[0-9]*')],
      totalFloors: ['', Validators.pattern('[0-9]*')],
      prefix: ['', [Validators.required]],
      postfix: ['', [Validators.required]],
      type: ['', [Validators.required]],
      separator: ['', [Validators.required]],
      apartmentConfirmation: ['', [Validators.required]],
    })
  }

  ngOnInit() {
    this.form.get('totalApartments').setValue(60);
    this.form.get('totalFloors').setValue(10);
    this.form.get('prefix').setValue('DE');
    this.form.get('postfix').setValue('S');
    this.form.get('type').setValue('101');
    this.form.get('separator').setValue('-');

    this.calculateApartmentNaming();
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  change() {
    this.apartments = [];
    if (this.isChecked) {
      this.form = this.formBuilder.group({
        totalApartments: ['', [Validators.pattern('[0-9]*'),Validators.required]],
        totalFloors: ['', [Validators.pattern('[0-9]*'),Validators.required]],
        prefix: ['', []],
        postfix: ['', []],
        type: ['', [Validators.required]],
        apartmentConfirmation: ['', [Validators.required]],
      })
      this.form.get('totalApartments').setValue(60);
      this.form.get('totalFloors').setValue(10);
      this.form.get('prefix').setValue('DE');
      this.form.get('postfix').setValue('S');
      this.form.get('type').setValue('101');

      this.calculateApartmentNaming();
    }
    else {
      this.form = this.formBuilder.group({
        name: ['', [Validators.required]],
      })
    }
  }

  addApartments(apartments:Apartment[]) {
    this.cache.addBulk(apartments).then(
      (response) => {
        this.toaster.toast("Apartments added successfully.")
        this.form.get('totalApartments').setValue(60);
        this.form.get('totalFloors').setValue(10);
        this.form.get('prefix').setValue('DE');
        this.form.get('postfix').setValue('S');
        this.form.get('type').setValue('101');
        this.router.navigate(['/society/facilities/apartment'])
        .finally(
          ()=>{
            this.loader.hideLoader();
          }
        )
      },
      (error) => {
        console.log(error)
        this.loader.hideLoader();
      }
    )
  }

  addApartment(apartmentName) {
    let apartment: Apartment=new Apartment();
    apartment.name = apartmentName;
    this.cache.addObject(apartment).then(
      (response) => {
        this.toaster.toast("Apartment added successfully.")
        this.form.reset();
        this.router.navigate(['/society/facilities/apartment'])
        .finally(
          ()=>{
            this.loader.hideLoader();
          }
        )
      },
      (error) => {
        console.log(error)
        this.loader.hideLoader();
      }
    )
  }

  submitApartments(){
    this.loader.showLoader();
    if(this.isChecked){
      this.addApartments(this.form.get('apartmentConfirmation').value);
    }else{
      this.addApartment(this.form.get('name').value);
    }
  }

  calculateApartmentNaming()
  {
    console.log('calculateApartmentNaming')
    let totalApartments = this.form.get('totalApartments').value;
    let totalFloors = this.form.get('totalFloors').value;
    let prefix = this.form.get('prefix').value;
    let postfix = this.form.get('postfix').value;
    let type = this.form.get('type').value;
    const totalFlatsPerFloor = totalApartments/totalFloors;
    console.log(totalFlatsPerFloor)
    if(isNaN(+totalFlatsPerFloor) || !isFinite(+totalFlatsPerFloor) || !isNumber(+totalFlatsPerFloor) || totalApartments % totalFloors !=0){
      alert("Apartments per floor is incorrect")
      this.form.get('totalApartments').setValue(60);
      this.form.get('totalFloors').setValue(10);
    }
    this.apartments = [];
    var curr = 0;
    let k = 1;
    for (let i = 1; i <= totalFloors; i++) {
      for (let j = 1; j <= totalFlatsPerFloor; j++) {
        let currentApartment: Apartment = new Apartment();
        if (type === '1') {
          currentApartment.name = prefix  + k+postfix;
          k++;
        }
        else {
          if (j >= 1 && j < 10)
            currentApartment.name = prefix  + i + 0 + j +postfix;
          else
            currentApartment.name = prefix  + i + j + postfix;
        }
        this.apartments.push(currentApartment);

        if (curr >= totalApartments)
          break;
        else
          curr++;
      }
    }    
  }

  addApartmentOnConsole(apartments: Apartment[]) {

    console.log(apartments)
  }

  onPreviewConfirm(event: { component: IonicSelectableComponent, value: any }) {
    console.log('Selected: ', event);
    let selectedApartments:Apartment[] = event.value;
    this.form.get('apartmentConfirmation').setValue(selectedApartments);
  }

  onSearch(event: { component: IonicSelectableComponent, text: any }) {
    let text = event.text.trim();
    event.component.startSearch();
    event.component.items = this.filterSearch(text);
    event.component.endSearch();
  }

  onSearchFail(event: { component: IonicSelectableComponent, text: any }) {
    if (event.text != '') {
      this.selectComponent.searchFailText = "Result not found. Apartment with this name does not exist."
    } else {
      this.selectComponent.searchFailText = "Start typing apartment name."
    }
  }

  filterSearch(text: string){
    return this.apartments.filter(apartment => {
      return apartment.name.indexOf(text) !== -1;
    })
  }

  onOpen(){
    this.calculateApartmentNaming()
    this.adService.reSubscribeAds("TAB");
  }

  onClose(){
    this.adService.reSubscribeAds("NORMAL");
  }

  openFromCode() {
    this.selectComponent.open();
  }

  clear() {
    this.selectComponent.clear();
    this.selectComponent.close();
  }

  selectAll() {
    this.selectComponent.toggleItems(true);
  }

  confirm() {
    this.selectComponent.confirm();
    this.selectComponent.close();
  }

}



