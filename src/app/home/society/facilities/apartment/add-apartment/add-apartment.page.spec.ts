import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddApartmentPage } from './add-apartment.page';

describe('AddApartmentPage', () => {
  let component: AddApartmentPage;
  let fixture: ComponentFixture<AddApartmentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddApartmentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddApartmentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
