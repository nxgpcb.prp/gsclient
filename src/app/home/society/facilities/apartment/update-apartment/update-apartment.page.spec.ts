import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdateApartmentPage } from './update-apartment.page';

describe('UpdateApartmentPage', () => {
  let component: UpdateApartmentPage;
  let fixture: ComponentFixture<UpdateApartmentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateApartmentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateApartmentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
