import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { GardenPage } from './garden.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: GardenPage
  },

  {
    path: 'update-garden/:id',
    loadChildren: './update-garden/update-garden.module#UpdateGardenPageModule'
  },
  {
    path: 'add-garden',
    loadChildren: './add-garden/add-garden.module#AddGardenPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [GardenPage]
})
export class GardenPageModule {}
