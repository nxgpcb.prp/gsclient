import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddGardenPage } from './add-garden.page';

describe('AddGardenPage', () => {
  let component: AddGardenPage;
  let fixture: ComponentFixture<AddGardenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGardenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddGardenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
