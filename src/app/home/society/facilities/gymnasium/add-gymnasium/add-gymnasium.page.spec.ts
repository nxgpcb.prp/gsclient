import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddGymnasiumPage } from './add-gymnasium.page';

describe('AddGymnasiumPage', () => {
  let component: AddGymnasiumPage;
  let fixture: ComponentFixture<AddGymnasiumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGymnasiumPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddGymnasiumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
