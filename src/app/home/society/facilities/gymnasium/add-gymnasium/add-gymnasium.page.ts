import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AddPageInterface } from 'src/app/home/arch/AddPageInterface';
import { AddPageTrack } from 'src/app/home/arch/AddPageTrack';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { Gymnasium } from 'src/app/core/Gymnasium';


@Component({
  selector: 'app-add-gymnasium',
  templateUrl: './add-gymnasium.page.html',
  styleUrls: ['./add-gymnasium.page.scss'],
})
@AddPageTrack(
  '/society/facilities/gymnasium/add-gymnasium',
  'Gymnasium',
  '/GSServer/webapi/secured/Society/Facilities/Gymnasium',
  false
)
export class AddGymnasiumPage implements AddPageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cache: Cache;
  cacheObject: any;
  apartmentCache:Cache = null;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    public readonly societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
    })
  }

  ngOnInit() {
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addGymnasium() {
    this.loader.showLoader();
    let gymnasium: Gymnasium = new Gymnasium();
    gymnasium.name = this.form.get('name').value;
    this.cache.addObject(gymnasium)
      .then(
        (response) => {
          this.toaster.toast("Gymnasium added successfully.")
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/facilities/gymnasium'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          this.toaster.toast(error.error.errorMessage + '. Gymnasium not added.');
          console.log(error);
          this.loader.hideLoader();
        }
      );
  }

}
