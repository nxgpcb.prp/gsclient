import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { UpdatePageInterface } from 'src/app/home/arch/UpdatePageInterface';
import { UpdatePageTrack } from 'src/app/home/arch/UpdatePageTrack';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { Gymnasium } from 'src/app/core/Gymnasium';
@Component({
  selector: 'app-update-gymnasium',
  templateUrl: './update-gymnasium.page.html',
  styleUrls: ['./update-gymnasium.page.scss'],
})
@UpdatePageTrack(
  '/society/facilities/apartment/update-gymnasium',
  'Gymnasium',
  '/GSServer/webapi/secured/Society/Facilities/Gymnasium',
  false
)
export class UpdateGymnasiumPage implements UpdatePageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cache: Cache;
  cacheObject: any;s
  apartmentCache:Cache = null;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    public readonly societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
    })
  }

  ngOnInit() {
    this.form.get('name').setValue(this.cacheObject.name);
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  updateGymnasium() {
    this.loader.showLoader();
    this.cacheObject.name = this.form.get('name').value;
    this.cache.updateObject(this.cacheObject)
      .then(
        (response) => {
          this.toaster.toast("Gymnasium updated successfully.")
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/facilities/gymnasium'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          this.toaster.toast(error.error.errorMessage + '. Gymnasium not updated.');
          console.log(error);
          this.loader.hideLoader();
        }
      );
  }

}

