import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { GymnasiumPage } from './gymnasium.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: GymnasiumPage
  },

  {
    path: 'update-gymnasium/:id',
    loadChildren: './update-gymnasium/update-gymnasium.module#UpdateGymnasiumPageModule'
  },
   {
    path: 'add-gymnasium',
    loadChildren: './add-gymnasium/add-gymnasium.module#AddGymnasiumPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [GymnasiumPage]
})
export class GymnasiumPageModule {}
