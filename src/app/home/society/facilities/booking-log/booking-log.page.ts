import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar/calendar';

@Component({
  selector: 'app-booking-log',
  templateUrl: './booking-log.page.html',
  styleUrls: ['./booking-log.page.scss'],
})
export class BookingLogPage implements OnInit {

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  }
  eventSource = [];
  startTime;
  endTime;

  @ViewChild(CalendarComponent) bookingCalendar:CalendarComponent;

  constructor() { }

  ngOnInit() {
  }

  onCurrentDateChanged(event){

  }

  onEventSelected(event){

  }

  onViewTitleChanged(event){

  }

  onTimeSelected(event){

  }

  reloadSource(startTime, endTime){
    
  }

}
