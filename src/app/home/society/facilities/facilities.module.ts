import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FacilitiesPage } from './facilities.page';

const routes: Routes = [
  {
    path: '',
    component: FacilitiesPage,
    redirectTo: '/society/facilities/apartment',
    pathMatch: 'full'
  },
  {
    path: 'apartment', loadChildren: './apartment/apartment.module#ApartmentPageModule'
  },
  {
    path: 'club-house', loadChildren: './club-house/club-house.module#ClubHousePageModule'
  },
  {
    path: 'garden', loadChildren: './garden/garden.module#GardenPageModule'
  },
  {
    path: 'gymnasium', loadChildren: './gymnasium/gymnasium.module#GymnasiumPageModule'
  },
  {
    path: 'parking', loadChildren: './parking/parking.module#ParkingPageModule'
  },
  {
    path: 'swimming-pool', loadChildren: './swimming-pool/swimming-pool.module#SwimmingPoolPageModule'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [FacilitiesPage]
})
export class FacilitiesPageModule {}
