import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ParkingPage } from './parking.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: ParkingPage
  },

  {
    path: 'update-parking/:id',
    loadChildren: './update-parking/update-parking.module#UpdateParkingPageModule'
  },
   {
    path: 'add-parking',
    loadChildren: './add-parking/add-parking.module#AddParkingPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ParkingPage]
})
export class ParkingPageModule {}
