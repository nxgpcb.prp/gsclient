import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { UpdatePageTrack } from 'src/app/home/arch/UpdatePageTrack';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { AdService } from 'src/app/home/ad/ad.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { Parking } from 'src/app/core/Parking';
import { UpdatePageInterface } from 'src/app/home/arch/UpdatePageInterface';

@Component({
  selector: 'app-update-parking',
  templateUrl: './update-parking.page.html',
  styleUrls: ['./update-parking.page.scss'],
})
@UpdatePageTrack(
  '/society/facilities/apartment/update-parking',
  'Parking',
  '/GSServer/webapi/secured/Society/Facilities/Parking',
  false
)
export class UpdateParkingPage implements UpdatePageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cache: Cache;
  cacheObject: any;
  apartmentCache:Cache = null;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    public readonly societyService:SocietyService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      parkingType: ['', [Validators.required]],
    })
  }

  ngOnInit() {
    this.form.get('name').setValue(this.cacheObject.name);
    this.form.get('parkingType').setValue(this.cacheObject.parkingType);
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  updateParking() {
    this.loader.showLoader();
    this.cacheObject.name = this.form.get('name').value;
    this.cacheObject.parkingType = this.form.get('parkingType').value;
    this.cache.updateObject(this.cacheObject)
      .then(
        (response) => {
          this.toaster.toast("Parking updated successfully.")
          console.log(response)
          this.form.reset();
          this.router.navigate(['/society/facilities/parking'])
            .finally(
              () => {
                this.loader.hideLoader();
              }
            )
        },
        (error) => {
          this.toaster.toast(error.error.errorMessage + '. Parking not updated.');
          console.log(error);
          this.loader.hideLoader();
        }
      );
  }

}

