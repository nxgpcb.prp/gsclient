import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { WorkspaceToolbarService } from '../workspace-toolbar/workspace-toolbar.service';
import { HomeService } from '../home.service';
import { SocietyService } from '../services/society.service';
import { NavController, Platform } from '@ionic/angular';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Society } from 'src/app/core/Society';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-society',
  templateUrl: './society.page.html',
  styleUrls: ['./society.page.scss'],
})
export class SocietyPage implements OnInit, AfterViewInit {

  isFistTimeLoggedIn: boolean = false;
  pages =
    [
      {
        title: 'Manage Facilities',
        showBadge: false,
        parentUrl: '/society/facilities',
        open: true,
        children: [
          {
            title: 'Apartment',
            url: '/society/facilities/apartment',
            showBadge: false,
          },
          {
            title: 'Club House',
            url: '/society/facilities/club-house',
            showBadge: false,
          },
          {
            title: 'Garden',
            url: '/society/facilities/garden',
            showBadge: false,   
          },
          {
            title: 'Gymnasium',
            url: '/society/facilities/gymnasium',
            showBadge: false,
          },
          {
            title: 'Parking',
            url: '/society/facilities/parking',
            showBadge: false,
          },
          {
            title: 'Swimming Pool',
            url: '/society/facilities/swimming-pool',
            showBadge: false,
          },
        ]
      },
      {
        title: 'Committee',
        url: '/society/committee',
        showBadge: false
      },
      {
        title: 'Complaints',
        url: '/society/complaints',
        showBadge: false
      },
      {
        title: 'Accounting & Billing',
        showBadge: false,
        parentUrl: '/society/accounting',
        open: false,
        children: [
          {
            title: 'Transactions',
            url: '/society/accounting/transactions',
            showBadge: false,
          },
          {
            title: 'Demand',
            url: '/society/accounting/demand',
            showBadge: false,
          },
        ]
      },
      {
        title: 'Visitor Logs',
        url: '/society/visitor-logs',
        showBadge: false
      },
      {
        title: 'Requests',
        showBadge: false,
        parentUrl: '/society/requests',
        open: false,
        children: [
          {
            title: 'Booking',
            url: '/society/requests/booking-requests',
            showBadge: false,
          },
          {
            title: 'Joining',
            url: '/society/requests/joining-requests',
            showBadge: false,
          },
        ]
      },
      {
        title: 'Residents',
        url: '/society/residents',
        showBadge: false
      },
      {
        title: 'Notices',
        url: '/society/notices',
        showBadge: false
      },
      {
        title: 'Securities',
        showBadge: false,
        parentUrl: '/society/securities',
        open: false,
        children: [
          {
            title: 'Accounts',
            url: '/society/securities/accounts',
            showBadge: false,
          },
          {
            title: 'Guards',
            url: '/society/securities/guards',
            showBadge: false,
          },
        ]
      },
      {
        title: 'Subscription',
        url: '/society/subscription',
        showBadge: false
      },
    ]
  selectedPath = '/society/facilities/apartment';
  society: Society = new Society();

  constructor(private platform: Platform, private fcm: FcmService, private router: Router, private loginService: LoginService,
    private workspaceToolbarService: WorkspaceToolbarService, private navController: NavController, public societyService: SocietyService) {
    this.workspaceToolbarService.hideBackButton();
    this.router.events.subscribe((event: RouterEvent) => {
      if(event.url != undefined){
        this.selectedPath = event.url;
      }
      this.pages.forEach(page => {
        if(page.open != null){
          page.open = false;
          if(this.selectedPath.includes(page.parentUrl)){
            page.open = true;
          }
        }
      });
    });
  }

  async ngOnInit() {
    
    this.society = await this.loginService.getUser()
    if (this.society.name == null) {
      this.isFistTimeLoggedIn = true;
    }
  }

  ngAfterViewInit(): void {

  }

  ngOnInAppUpdate(): void {

  }

  async refresh(): Promise<void> {
    
    this.society = await this.loginService.getUser()
  }

  expandMenu(p) {
    if (p.open == null) {
      this.pages.forEach(element => {
        if (element.open != null) {
          element.open = false;
        }
      });
    }
    if (!p.open) {
      p.open = !p.open
      this.pages.forEach(element => {
        if (element.title != p.title) {
          if (element.open != null) {
            element.open = false;
          }
        }
      });
    }
  }

}
