import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GuardsPage } from './guards.page';

describe('GuardsPage', () => {
  let component: GuardsPage;
  let fixture: ComponentFixture<GuardsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GuardsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
