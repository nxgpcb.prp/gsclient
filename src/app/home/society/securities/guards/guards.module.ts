import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { GuardsPage } from './guards.page';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: GuardsPage
  },
  { path: 'add-security-guard', loadChildren: './add-security-guard/add-security-guard.module#AddSecurityGuardPageModule' },
  { path: 'update-security-guard/:id', loadChildren: './update-security-guard/update-security-guard.module#UpdateSecurityGuardPageModule' },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
  ],
  declarations: [GuardsPage]
})
export class GuardsPageModule {}
