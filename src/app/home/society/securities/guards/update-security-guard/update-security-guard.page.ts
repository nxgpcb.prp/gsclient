import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { SecurityGuard } from 'src/app/core/SecurityGuard';
import { Platform, ModalController } from '@ionic/angular';
import { WebcamModalComponent } from 'src/app/camera/webcam-modal/webcam-modal.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebcamImage } from 'ngx-webcam';
import { ActivatedRoute, Router } from '@angular/router';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoaderService } from 'src/app/misc/loader.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { Cache } from '../../../../services/cache';
import { UpdatePageInterface } from 'src/app/home/arch/UpdatePageInterface';
import { CacheService } from 'src/app/home/services/cache.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { SocietyService } from 'src/app/home/services/society.service';
import { CameraService } from 'src/app/camera/camera.service';
import { UpdatePageTrack } from 'src/app/home/arch/UpdatePageTrack';

@Component({
  selector: 'app-update-security-guard',
  templateUrl: './update-security-guard.page.html',
  styleUrls: ['./update-security-guard.page.scss'],
})
@UpdatePageTrack(
  '/society/securities/guards/update-security-guard',
  'SecurityGuard',
  '/GSServer/webapi/secured/Society/SecurityGuard',
  false
)
export class UpdateSecurityGuardPage implements UpdatePageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;
  public webcamImage: WebcamImage = null;
  securityGuard: SecurityGuard = new SecurityGuard();

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private toaster:ToasterService,
    private adService:AdService,
    private societyService:SocietyService,
    private modalController:ModalController,
    private platform:Platform,
    private cameraService:CameraService
    ) 
  {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      pin: ['', [Validators.required, Validators.pattern('\\d{6}')]],
      phoneNumber: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
    });
  }

  ngOnInit() {
    this.form.get('name').setValue(this.cacheObject.name);
    this.form.get('pin').setValue(this.cacheObject.pin);
    this.form.get('phoneNumber').setValue(this.cacheObject.phoneNumber);
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  async openWebcam() {
    const modal = await this.modalController.create({
      component: WebcamModalComponent,
      componentProps: {
        "modalController": this.modalController,
      },
    });
    modal.onDidDismiss()
      .then((data) => {
        if (data['data'] != null) {
          this.securityGuard.profilePicture.content = data['data'];
        }
      });
    return await modal.present();

  }

  updateSecurityGuard() {
    this.cacheObject.name = this.form.get('name').value;
    this.cacheObject.pin = this.form.get('pin').value;
    this.cacheObject.phoneNumber = this.form.get('phoneNumber').value;
    this.loader.showLoader();
    this.cache.updateObject(this.cacheObject).then(
      (response) => {
        this.router.navigate(['/society/securities/guards'])
        .finally(
          () => {
            this.loader.hideLoader();
          }
        )
      },
      (error) => {
        console.log(error)
        this.loader.hideLoader()
      }
    )
  }

  removeSecurityGuard() {
    this.loader.showLoader();
    this.cache.deleteObject(this.cacheObject).then(
      object=>{
        this.router.navigate(['/society/securities/guards'])
        this.loader.hideLoader()
      },
      error=>{
        alert("Error occurred :"+error)
        this.loader.hideLoader()
      }
    );
  }


  takePicture() {
    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
      this.cameraService.getPicture().then(
        (imageData) => {
          this.securityGuard.profilePicture.content = 'data:image/jpeg;base64,' + imageData.base64String;
        },
        (err) => {
          window.alert('Unable to open Camera : ' + err);
        }
      );
    } else {
      this.openWebcam();
    }
  }

  removePicture() {
    let temp: SecurityGuard = new SecurityGuard;
    this.securityGuard.profilePicture.content = temp.profilePicture.content;
  }

  handleImage(webcamImage: WebcamImage) {
    this.webcamImage = webcamImage;
  }

}
