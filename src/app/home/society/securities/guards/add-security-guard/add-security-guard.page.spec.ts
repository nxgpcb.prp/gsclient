import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddSecurityGuardPage } from './add-security-guard.page';

describe('AddSecurityGuardPage', () => {
  let component: AddSecurityGuardPage;
  let fixture: ComponentFixture<AddSecurityGuardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSecurityGuardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddSecurityGuardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
