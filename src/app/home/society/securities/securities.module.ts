import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SecuritiesPage } from './securities.page';

const routes: Routes = [
  {
    path: '',
    component: SecuritiesPage,
    redirectTo: '/society/securities/accounts',
    pathMatch: 'full'
  },
  {
    path: 'accounts',
    loadChildren: './accounts/accounts.module#AccountsPageModule'
  },
  {
    path: 'guards',
    loadChildren: './guards/guards.module#GuardsPageModule'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [SecuritiesPage]
})
export class SecuritiesPageModule {}
