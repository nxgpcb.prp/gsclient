import { Component, OnInit, ViewChild, NgZone, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Security } from 'src/app/core/Security';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Image } from 'src/app/core/Image';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SocietyService } from 'src/app/home/services/society.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { Cache } from '../../../services/cache';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { CacheService } from 'src/app/home/services/cache.service';
import { ListPageInterface } from 'src/app/home/arch/ListPageInterface';
import { ListPageTrack } from 'src/app/home/arch/ListPageTrack';
import { AdService } from 'src/app/home/ad/ad.service';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.page.html',

  styleUrls: ['./accounts.page.scss'],
})
@ListPageTrack(
  '/society/securities/accounts',
  'Security',
  '/GSServer/webapi/secured/Society/Security',
  false
)
export class AccountsPage implements ListPageInterface {
  
  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
    ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addSecurity() {
    this.router.navigate(['/society/securities/accounts/add-security'])
  }


  removeSecurity(Id) {
    this.societyService.setProgressing(true);
    this.cache.deleteObject(this.cache.objects[Id])
      .then(
        response => {
          if (response) {
          }
          this.societyService.setProgressing(false);
        },
        error => {
          console.log(error);
          this.societyService.setProgressing(false);
        }
      )
  }

}
