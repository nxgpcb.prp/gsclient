import { Component, OnInit, ViewChild, NgZone, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-securities',
  templateUrl: './securities.page.html',
  styleUrls: ['./securities.page.scss'],
})
export class SecuritiesPage implements OnInit {

  navigationSubscription: Subscription;

  constructor(private router:Router) {
    
   }

   ngOnInit() {
    this.init();
  }

  initDataService(){

  }

  ngOnDestroy() {
    this.unRegisterToEvents();
  }

  init() {
    this.navigate();
  }

  navigate() {
  }

  private unRegisterToEvents() {
    this.navigationSubscription.unsubscribe();
  }

}
