import { Component, OnInit, Input } from '@angular/core';
import { NotificationsService } from 'src/app/notifications/notifications.service';

import { Router } from '@angular/router';
import { FcmService } from 'src/app/notifications/fcm.service';
import { PopoverController } from '@ionic/angular';
import { HomeService } from '../home.service';
import { SetStatusComponent } from '../set-status/set-status.component';
import { WorkspaceToolbarService } from '../workspace-toolbar/workspace-toolbar.service';
import { LoginService } from 'src/app/login/login.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AdService } from '../ad/ad.service';
import { DebugService } from 'src/app/for-developer/debug.service';
import { SecurityGuard } from 'src/app/core/SecurityGuard';
import { LanguageService } from 'src/app/language/language.service';
import { LanguagePopoverPage } from 'src/app/language/language-popover/language-popover.page';
import { Cache } from '../services/cache';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SecurityGuardLog } from 'src/app/core/SecurityGuardLog';
import { LoaderService } from 'src/app/misc/loader.service';


@Component({
  selector: 'app-popover-menu',
  templateUrl: './popover-menu.component.html',
  styleUrls: ['./popover-menu.component.scss']
})
export class PopoverMenuComponent implements OnInit {
  @Input()
  popoverController: PopoverController;

  @Input()
  securityGuard: SecurityGuard;

  @Input()
  securityGuardLog: SecurityGuardLog;

  user: any;
  securityGuardLogCache: Cache = null;

  p =
    {
      title: 'Settings',
      url: '/settings/profile',
      showBadge: false
    }

  constructor(private router: Router,
    private loginService: LoginService,
    private statusPopoverController: PopoverController, 
    private workspaceToolbarService: WorkspaceToolbarService,
    private loader:LoaderService,
    public readonly debugService: DebugService,
    private http: HttpClient,) { }

  async ngOnInit() {

    this.user = await this.loginService.getUser();

    this.workspaceToolbarService.getUpdateUserEmittedValue()
      .subscribe(item => {
        console.log("Emitted Value : " + item);
        this.user = item;
      }
    );

    this.workspaceToolbarService.getUpdateSecurityGuardEmittedValue()
      .subscribe(item => {
        console.log("Emitted Value : " + item);
        this.securityGuard = item;
      }
    );

    this.workspaceToolbarService.getUpdateSecurityGuardLogEmittedValue()
      .subscribe(item => {
        console.log("Emitted Value : " + item);
        this.securityGuardLog = item;
      }
    );

    this.securityGuardLogCache = new Cache(this.http,
      'securityGuardLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/SecurityGuardLog',
      Cache.type_REFERED);
  }

  async guardLogout() {
    this.securityGuardLog.timeOut = new Date();
    this.loader.showLoader();
    this.securityGuardLogCache.update(this.securityGuardLog).then(
      (response)=>{
        console.log(response)
        this.popoverController.dismiss();
        this.workspaceToolbarService.updateSecurityGuard(undefined);
        this.workspaceToolbarService.updateSecurityGuardLog(undefined);
        this.loader.hideLoader();
      },
      (error)=>{
        console.log(error)
        this.loader.hideLoader();
      }
    )
  }

  async logOut() {
    this.popoverController.dismiss();
    this.loginService.logOut();
  }

  forDeveloper() {
    this.popoverController.dismiss();
    this.router.navigate(['/for-developer']);
  }

  settings() {
    this.popoverController.dismiss();
  }

  async openStatusPopover(ev: any) {
    this.popoverController.dismiss();
    //Style defined in global.scss
    const popover = await this.statusPopoverController.create({
      component: SetStatusComponent,
      componentProps: {
        "popoverController": this.statusPopoverController,
      },
      event: ev,
      translucent: true,
      cssClass: 'custom-popover'
    });

    return await popover.present();
  }

  language(ev: any) {
    this.popoverController.dismiss();
    this.presentLanguagePopover(ev);
  }

  async presentLanguagePopover(ev: any) {
    //Style defined in global.scss
    const popover = await this.popoverController.create({
      component: LanguagePopoverPage,
      componentProps: {
        "popoverController": this.popoverController,
      },
      event: ev,
      translucent: true,
      cssClass: 'custom-popover'
    });

    return await popover.present();
  }

}
