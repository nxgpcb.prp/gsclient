import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Visitor } from 'src/app/core/Visitor';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { LoginService } from 'src/app/login/login.service';
import { LoaderService } from 'src/app/misc/loader.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { DailyVisitor } from 'src/app/core/DailyVisitor';
import { AdService } from '../ad/ad.service';
import { Cache } from './cache';
import { Paging } from 'src/app/filters/Paging';
import { Filter } from 'src/app/filters/Filter';
import { Sort } from 'src/app/filters/Sort';
import { DeliveryLog } from 'src/app/core/DeliveryLog';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Platform } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';
import { Society } from 'src/app/core/Society';

const { SplashScreen, StatusBar } = Plugins;

/** @deprecated since 1.1.7 */
@Injectable({
  providedIn: 'root'
})
export class SecurityService implements CanActivate {

  visitor: Visitor;
  visitorLog: VisitorLog;
  user: any;
  progressing: boolean = false;
  facilityCache: Cache = null;
  visitorLogCache: Cache = null;
  deliveryLogCache: Cache = null;
  societyCache: Cache = null;
  visitorCache: Cache = null;
  dailyVisitorCache: Cache = null;
  residentCache: Cache = null;
  parkingCache: Cache = null;

  constructor(private router: Router, private http: HttpClient, private platform: Platform,
    private loginService: LoginService, private loader: LoaderService, private fcm: FcmService,
    private afAuth: AngularFireAuth, private adService: AdService) {
    this.init();
  }

  private async init() {

    this.societyCache = await new Cache(this.http,
      'society',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/Society',
      Cache.type_REFERED);

    this.facilityCache = await new Cache(this.http,
      'facilities',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/Facility',
      Cache.type_NATIVE);

    this.visitorLogCache = await new Cache(this.http,
      'VisitorLog',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/VisitorLog',
      Cache.type_NATIVE);

    this.deliveryLogCache = await new Cache(this.http,
      'deliveryLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/DeliveryLog',
      Cache.type_NATIVE);

    this.visitorCache = await new Cache(this.http,
      'visitor',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/Visitor',
      Cache.type_NATIVE);

    this.dailyVisitorCache = await new Cache(this.http,
      'dailyVisitor',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/DailyVisitor',
      Cache.type_NATIVE);

    this.residentCache = await new Cache(this.http,
      'residents',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/Resident',
      Cache.type_NATIVE);

    this.parkingCache = new Cache(this.http,
      'parkings',
      environment.backend.baseURL + '/GSServer/webapi/secured/Security/Facilities/Parking',
      Cache.type_NATIVE);
  }

  /** @deprecated since 1.1.7 */
  addDailyVisitorLog(dailyVisitor: DailyVisitor) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.post<DailyVisitor>(environment.backend.baseURL + '/GSServer/webapi/secured/Security/DailyVisitorLog', dailyVisitor, { headers: headers })
  }

  /** @deprecated since 1.1.7 */
  pingForVisitorLog(visitorLog: VisitorLog) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<VisitorLog>(environment.backend.baseURL + '/GSServer/webapi/secured/Security/PingForVisitorLog', visitorLog, { headers: headers })
  }

  /** @deprecated since 1.1.7 */
  pingForDeliveryLog(deliveryLog: DeliveryLog) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<DeliveryLog>(environment.backend.baseURL + '/GSServer/webapi/secured/Security/PingForDeliveryLog', deliveryLog, { headers: headers })
  }

  /** @deprecated since 1.1.7 */
  searchVisitors(phone: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Visitor[]>(environment.backend.baseURL + '/GSServer/webapi/secured/Security/Visitors/phone/' + phone, { headers: headers })
  }


  canActivate(route: ActivatedRouteSnapshot): any {
    console.log("Security Service");
    this.loader.showLoader();
    return new Promise(res => {
      this.loginService.getUser()
        .then(
          (response: any) => {
            this.loader.hideLoader();
            console.log(response)
            if (response.type == "Security") {

              if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
                StatusBar.setStyle({
                  style: StatusBarStyle.Dark
                });
                SplashScreen.hide();
                StatusBar.show();
              }


              this.fcm.subscribeNotfication();

              //Subscribe to adds if plan is Free and if subscription is expired.
              if (response.subscriptionPlan != null) {
                if (response.isActive) {
                  console.log("Plan Active")
                } else {
                  console.log("Plan Expired")
                  this.adService.subscribeAds("NORMAL");
                }
              }
              res(true);
            } else {
              this.router.navigate(['/not-autherized']);
              res(false);
            }
          },
          (error) => {
            this.loader.hideLoader();
            this.router.navigate(['/login']);
            res(false);
          })
    })

  }

  /** @deprecated since 1.1.7 */
  isProgressing() {
    return this.progressing;
  }

  /** @deprecated since 1.1.7 */
  setProgressing(progress: boolean) {
    this.progressing = progress;
  }

  /** @deprecated since 1.1.7 */
  searchSocieties(searchTerm: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Society[]>(environment.backend.baseURL + '/GSServer/webapi/secured/Security/searchSocieties/' + searchTerm, { headers: headers })
  }

    /** @deprecated since 1.1.7 */
    searchVisitor(searchTerm: String) {
      let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));
  
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': "Basic " + idToken
      });
      return this.http.get<Visitor>(environment.backend.baseURL + '/GSServer/webapi/secured/Security/searchVisitor/' + searchTerm, { headers: headers })
    }
}
