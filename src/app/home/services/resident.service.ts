import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';
import { HomeService } from '../home.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Society } from 'src/app/core/Society';
import { LoaderService } from 'src/app/misc/loader.service';
import { JoiningRequest } from 'src/app/core/JoiningRequest';

import { environment } from 'src/environments/environment';
import { Facility } from 'src/app/core/Facility';
import { Notice } from 'src/app/core/Notice';
import { AdService } from '../ad/ad.service';
import { Cache } from './cache';
import { Paging } from 'src/app/filters/Paging';
import { Filter } from 'src/app/filters/Filter';
import { BookingLog } from 'src/app/core/BookingLog';
import { DeliveryLog } from 'src/app/core/DeliveryLog';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Plugins, StatusBarStyle } from '@capacitor/core';
import { Platform } from '@ionic/angular';

const { SplashScreen, StatusBar } = Plugins;
/** @deprecated since 1.1.7 */
@Injectable({
  providedIn: 'root'
})
export class ResidentService implements CanActivate {

  visitorLog: VisitorLog;
  user: any;
  public logs_notifications: number = 0;
  public requests_notifications: number = 0;
  public notices_notifications: number = 0;
  progressing: boolean = false;
  noticeCache: Cache = null;
  facilityCache: Cache = null;
  visitorLogCache: Cache = null;
  bookingLogCache: Cache = null;
  societyCache: Cache = null;
  joiningRequestCache: Cache = null;
  deliveryLogCache: Cache = null;
  subscriptionPlanLogCache: Cache = null;
  parkingCache: Cache = null;
  parkingLogCache: Cache = null;
  apartmentCache: Cache = null;

  constructor(private http: HttpClient, private router: Router, private loginService: LoginService,
    private afAuth: AngularFireAuth, private loader: LoaderService,
    private adService: AdService, private fcm:FcmService, private platform: Platform,) {
    this.init();
  }

  private async init() {

    this.societyCache = await new Cache(this.http,
      'societies',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/Society',
      Cache.type_REFERED);

    this.bookingLogCache = await new Cache(this.http,
      'bookingLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/BookingLog',
      Cache.type_NATIVE);

    this.joiningRequestCache = await new Cache(this.http,
      'joiningRequests',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/JoiningRequest',
      Cache.type_NATIVE);

    
    this.subscriptionPlanLogCache = new Cache(this.http,
      'subscriptionPlanLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/SubscriptionPlanLog',
      Cache.type_NATIVE);

    this.parkingCache = new Cache(this.http,
      'parkings',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/Facilities/Parking',
      Cache.type_NATIVE);

    this.parkingLogCache = new Cache(this.http,
      'parkingLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/ParkingLog',
      Cache.type_NATIVE);
  }

  canActivate(route: ActivatedRouteSnapshot): any {
    console.log("Resident Service ");
    this.loader.showLoader();
    return new Promise(res => {
      this.loginService.getUser()
        .then(
          (response: any) => {
            this.loader.hideLoader();
            console.log(response)
            if (response.type == "Resident") {

              if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
                StatusBar.setStyle({
                  style: StatusBarStyle.Dark
                });
                SplashScreen.hide();
                StatusBar.show();
              }

              
              
              this.fcm.subscribeNotfication();
              
              //Subscribe to adds if plan is Free and if subscription is expired.
              if (response.subscriptionPlan != null) {
                if (response.isActive) {
                  console.log("Plan Active")
                } else {
                  console.log("Plan Expired")
                  this.adService.subscribeAds("NORMAL");
                }
              }
              res(true);
            } else {
              this.router.navigate(['/not-autherized']);
              res(false);
            }
          },
          (error) => {
            this.loader.hideLoader();
            this.router.navigate(['/login']);
            res(false);
          }
        );
    })
  }

  /** @deprecated since 1.1.7 */
  addBookingLog(bookingLog: BookingLog) {
    return this.bookingLogCache.add(bookingLog);
  }

  /** @deprecated since 1.1.7 */
  searchSocieties(searchTerm: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Society[]>(environment.backend.baseURL + '/GSServer/webapi/secured/Resident/searchSocieties/' + searchTerm, { headers: headers })
  }

  /** @deprecated since 1.1.7 */
  getSociety() {
    return this.societyCache.getAll(JSON.parse('[]'));
  }

  /** @deprecated since 1.1.7 */
  getSocietyFacility(societyId) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<any>(environment.backend.baseURL + '/GSServer/webapi/secured/Resident/Society/' + societyId + '/Facility', { headers: headers })
  }

  /** @deprecated since 1.1.7 */
  setProgressing(progress: boolean) {
    this.progressing = progress;
  }

  /** @deprecated since 1.1.7 */
  isProgressing() {
    return this.progressing;
  }

  /** @deprecated since 1.1.7 */
  addSociety() {
    this.router.navigate(['/resident/add-society'])
      .then(
        Response => {
          this.setProgressing(false);
        }
      )
  }
}
