import { HttpClient, HttpParams } from "@angular/common/http";
import idb from './idb-promised.js';
import { QueryParam } from '../../core/QueryParam'
import { Paging } from 'src/app/filters/Paging.js';
import { Filter } from 'src/app/filters/Filter.js';
import { merge, fromEvent, Observable, Observer, Subject } from "rxjs";
import { map } from "rxjs/operators";
import { Sort } from "src/app/filters/Sort.js";
import { Search } from "src/app/filters/Search.js";
import { error } from "protractor";
import { openDB, deleteDB, wrap, unwrap } from 'idb';

export class Cache {

    constructor(private http: HttpClient, public objectType: string, private url: string, private type: string) {
        this.init(this.objectType, this.url);
    }

    static type_REFERED = 'refered';
    static type_NATIVE = 'native';
    static indexedDatabases: any[] = [];
    static dbVersion = 1;
    static OBJECT_STORE = 'object';
    static NEW_OBJECT_STORE = 'new_object';

    observer = new Subject<any>();;
    // lazyLoadProperties: any[];
    // lazyLoadPropertyObserver: any;
    // lazyLoadProperty: string;
    dbPromise: Promise<any>;
    objects: any=[];
    unReadCount: number=0;
    private cachedObjects: any=[];
    isLoading: boolean = true;
    isNextPageLoading: boolean = false;

    filter:Filter = new Filter();
    filterForServer:Filter = new Filter();
    

    public loadNext(pageSize: number)
    {
        return new Promise(res => {
            this.isNextPageLoading = true;
            this.filter.paging.size = pageSize;
            this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(
                newLocalObjects => {
                    this.getLocalObjects(Cache.OBJECT_STORE).then(
                        localObjects => {
                            this.unReadCount=0
                            localObjects.map(localObject=>{if(localObject.unRead==true) this.unReadCount++})
                            
                            this.cachedObjects = newLocalObjects.concat(localObjects)
                            if(this.filter.search != null){
                                this.cachedObjects = this.applySearch(this.cachedObjects, this.filter.search);
                            }
                            if(this.filter.sort != null){
                                this.cachedObjects = this.applySort(this.cachedObjects, this.filter.sort);
                            }
                            if(this.filter.paging != null){
                                let nextPageObjects=this.applyPaging(this.cachedObjects, this.filter.paging)
                                // nextPageObjects.length==0 means cache does not have more object: so ask server
                                //((this.filter.paging.start + this.filter.paging.size)>this.nextPageObjects.length)
                                if((this.filter.paging.start + this.filter.paging.size)>this.cachedObjects.length)
                                {
                                    if(nextPageObjects.length > 0 && this.filter.paging.start==0){
                                        this.objects = nextPageObjects;
                                    }else{
                                        this.objects.push(...nextPageObjects);
                                    }
                                    res(true);
                                    this.syncIDB(this.filter).then(
                                        nextPageObjectsFromServer=>{
                                            nextPageObjectsFromServer.map(
                                                nextPageObjectFromServer=>{
                                                    let nextPageObject=nextPageObjects.filter(nextPageObject=>nextPageObject.id==nextPageObjectFromServer.id)[0]
                                                    if(nextPageObject==undefined)
                                                    {
                                                        
                                                    }
                                                    else if( nextPageObject.unRead==true  || nextPageObjectFromServer.lastModified!=nextPageObject.lastModified)
                                                    {
                                                        nextPageObjectFromServer.unRead=true;
                                                        if(nextPageObject!=undefined && nextPageObject.unRead!=true)
                                                            this.unReadCount++;
                                                    }
                                                    this.updateCacheObject(nextPageObjectFromServer);

                                                })
                                            //this.objects.push(...nextPageObjectsFromServer);
                                            this.filter.paging.start=this.filter.paging.start+nextPageObjectsFromServer.length
                                            this.isNextPageLoading = false;
                                            console.log('Server : loadNext(' + pageSize + ') ' + this.objectType)
                                            this.saveObjectsLocally(nextPageObjectsFromServer, Cache.OBJECT_STORE);
                                            
                                        },
                                        error =>{
                                            this.objects.push(...nextPageObjects);
                                            this.filter.paging.start=this.filter.paging.start+nextPageObjects.length
                                            res(true)
                                        }
                                    );
                                }
                                // nextPageObjects.length!=0 means cache have some object: so add object to this.objects and do sync call
                                else
                                {
                                    this.objects.push(...nextPageObjects);
                                    res(true)
                                    this.syncIDB(this.filter).then(
                                        nextPageObjectsFromServer=>{
                                            nextPageObjectsFromServer.map(
                                                nextPageObjectFromServer=>
                                                {
                                                    let nextPageObject=nextPageObjects.filter(nextPageObject=>nextPageObject.id==nextPageObjectFromServer.id)[0]
                                                    if( nextPageObject==undefined || nextPageObject.unRead==true || nextPageObjectFromServer.lastModified!=nextPageObject.lastModified)
                                                    {
                                                        nextPageObjectFromServer.unRead=true;
                                                        if(nextPageObject!=undefined && nextPageObject.unRead!=true)
                                                            this.unReadCount++;
                                                    }
                                                    this.updateCacheObject(nextPageObjectFromServer);
                                                }
                                            );
                                            this.saveObjectsLocally(nextPageObjectsFromServer, Cache.OBJECT_STORE);
                                        },
                                        error =>{
                                        }
                                        );
                                    this.filter.paging.start=this.filter.paging.start+nextPageObjects.length
                                    this.isNextPageLoading = false;
                                    console.log('Cache : loadNext(' + pageSize + ') ' + this.objectType)
                                }
                            }
                            
                        }
                    );
                }
            );
        });
    }

    public load(pageSize: number)
    {
        return new Promise( (res,err) => {
            try{
                //console.log('Cache : load('+pageSize+') '+this.objectType)
                this.isLoading=true;
                this.filter.paging=new Paging();
                this.filter.paging.start=0;
                this.filter.paging.size=pageSize;
                this.objects=[];
                this.cachedObjects=[];
                //this.unReadCount=0;
                this.loadNext(pageSize).then(
                    result => {
                        this.isLoading=false;
                        res(true);
                    },
                    error=>{
                        this.isLoading=false;
                        err(error);
                    }
                )
                
            }
            catch(error)
            {
                err(error);
            }
        });
    }

    public  syncIDB(filterForServer:Filter )
    {
        return new Promise<any[]>(async (res,err) => {
            let queryParam: any = new QueryParam;
            queryParam.filter = JSON.stringify(filterForServer);
            this.http.get<any[]>(this.url, { params: queryParam }).subscribe(
                async (objects) => {
                    res(objects)
                },
                error=>{
                    err(error)
                }
            );
        });
        
    }


    
/* ---------------------------------------------------------------------------------------------------
 * do add/update/delete operation on this.objects
 * this.objects --->  rendered on UI
 * ____________
 * |         /_\
 * |           |
 * | object1   | <-------this.objects
 * | --------- |
 * | object2   |
 * | --------- |
 * | object3   |
 * |___________|
 * ---------------------------------------------------------------------------------------------------*/
    private async updateCacheObject(objectToUpdate) {
        let matchedWithExiting :Boolean=false;
        let objectToUpdates=this.applySearch([objectToUpdate],this.filter.search)
        if(objectToUpdates.length>0)
        {
            for (let i = 0; i < this.objects.length; i++) {
                if(this.objects[i].id==objectToUpdate.id)
                {
                    //this.setModifiedInBackground(this.objects[i],objectToUpdate)
                    this.objects[i]=objectToUpdate;
                    matchedWithExiting=true;
                    break;
                }
            }
        }
        else 
        {
            for (let i = 0; i < this.objects.length; i++) {
                if(this.objects[i].id==objectToUpdate.id)
                {
                     this.objects.splice(i, 1);
                     break;
                }
                
            }
 
        }
        if(!matchedWithExiting)
        {
            this.cachedObjects.push(objectToUpdate)
            if(this.filter.search != null){
                this.cachedObjects = this.applySearch(this.cachedObjects, this.filter.search);
            }
            if(this.filter.sort != null){
                this.cachedObjects = this.applySort(this.cachedObjects, this.filter.sort);
            }
            let indexToAdd=0;
            this.cachedObjects.map((cachedObject,i)=>{
                if(cachedObject.id==objectToUpdate.id)
                    indexToAdd=i
            });
            //this.setModifiedInBackground(null,objectToUpdate)
            this.objects.splice(indexToAdd,0,objectToUpdate);
        }
    }
    private async updateAddedCacheObject(objectToUpdate) {
        if(objectToUpdate._cache_pending_operation==undefined /*not addded*/)
        {
            for (let i = 0; i < this.objects.length; i++) {
                if(this.objects[i].id==objectToUpdate.addId)
                {
                    //this.setModifiedInBackground(this.objects[i],objectToUpdate)
                    this.objects[i]=objectToUpdate;
                }
                
            }
        }
        else
        {
            for (let i = 0; i < this.objects.length; i++) {
                if(this.objects[i].id==objectToUpdate.id)
                {
                    //this.setModifiedInBackground(this.objects[i],objectToUpdate)
                    this.objects[i]=objectToUpdate;
                }
                
            }
        }
    }
    private async deleteCacheObject(objectToDelete) {
        for (let i = 0; i < this.objects.length; i++) {
            if(this.objects[i].id==objectToDelete.id)
            {
                 this.objects.splice(i, 1);
                 this.loadNext(1)
                 break;
            }
            
        }
    }

    private async deleteAddedCacheObject(objectToDelete) {
        for (let i = 0; i < this.objects.length; i++) {
            if(this.objects[i].id==objectToDelete.id && this.objects[i]._cache_pending_operation == 'add')
            {
                this.objects.splice(i, 1);
                this.loadNext(1)
                break;
            }
            
        }
    }

    private async addCacheObject(objectToAdd) {
        this.objects.unshift(objectToAdd)
    }

    private async addCacheObjects(objectsToAdd) {
        objectsToAdd.forEach(element => {
            this.objects.unshift(element)
        });
    }

    /*private async setModifiedInBackground(objectOld,object) {
        if(objectOld==null || objectOld.lastModified!=object.lastModified)
            object.isModifiedInBackground=true
    }

    public async clearModifiedInBackground(object) {
        object.isModifiedInBackground=false
    }*/

/* ---------------------------------------------------------------------------------------------------
 * This method supposed to used by pages for add/update/delete/get objects from cache
 * syncing operation should be called by page using this.sync() method
 * ____________     ____________
 * |         /_\    |         /_\
 * |           |    |           |     this.getObject()
 * | object1  -|----|-->object1 |---->this.updateObject()
 * | --------- |    |           |     this.addObject()
 * | object2   |    | name:___  |     this.deleteObject
 * | --------- |    | date:___  |
 * | object3   |    |           |
 * |___________|    |___________|
 * ---------------------------------------------------------------------------------------------------*/
    public async updateObject(object: any) {

        return new Promise(async (res, err) => 
        {
            try{
                if(object._cache_pending_operation == 'add')
                {
                    await this.saveObjectsLocally([object], Cache.NEW_OBJECT_STORE);
                    await this.updateAddedCacheObject(object)
                    this.sync();
                    res(object);
                }
                else
                {
                    object._cache_pending_operation = 'update';
                    await this.saveObjectsLocally([object], Cache.OBJECT_STORE);
                    await this.updateCacheObject(object)
                    this.sync();
                    res(object);
                }
            }
            catch(error)
            {
                err(error);
            }
        });   
    }

    public async addObject(object: any) {

        return new Promise(async (res, err) => 
        {
            try{
                //let newObject = JSON.parse(JSON.stringify(object));
                object.createdOn=new Date();
                if (this.type == Cache.type_NATIVE) {
                    object.id = await this.getNextId();
                }
                object._cache_pending_operation = 'add';
                await this.saveObjectsLocally([object], Cache.NEW_OBJECT_STORE);
                await this.addCacheObject(object)
                this.sync();
                res(object);
            }
            catch(error)
            {
                err(error);
            }
        });   
    }

    public async addObjects(objects: any[]) {

        return new Promise(async (res, err) => 
        {
            try{
                let id = await this.getNextId();
                objects.forEach(async object => {
                    object.createdOn=new Date();
                    if (this.type == Cache.type_NATIVE) {
                        object.id = id++
                    }
                    object._cache_pending_operation = 'add';
                });   
                await this.saveObjectsLocally(objects, Cache.NEW_OBJECT_STORE);
                await this.addCacheObjects(objects)
                this.sync();             
                res(objects);
            }
            catch(error)
            {
                err(error);
            }
        });   
    }

    public async deleteObject(object: any) {

        return new Promise(async (res, err) => 
        {
            try{
                if(object._cache_pending_operation == 'add')
                {
                    await this.deleteLocalObjects([object], Cache.NEW_OBJECT_STORE);
                    await this.deleteAddedCacheObject(object)
                    //this.filter.paging.start--;
                    this.sync();
                    res(object);
                }
                else
                {
                    object._cache_pending_operation = 'delete';
                    await this.saveObjectsLocally([object], Cache.OBJECT_STORE);
                    await this.deleteCacheObject(object)
                    //this.filter.paging.start--;
                    this.sync();
                    res(object);
                }
            }
            catch(error)
            {
                err(error);
            }
        });   
    }

    public async getObject(id: number) {

        return new Promise<any>(async (res, err) => 
        {
            try{
                this.getLocalObject(id, Cache.OBJECT_STORE).then(
                    localObject => {
                        if(localObject==undefined)
                        {
                            this.getLocalObject(id, Cache.NEW_OBJECT_STORE).then(
                                localAddedObject =>{
                                    if(localAddedObject==undefined)
                                    {
                                        this.http.get<any>(this.url + '/' + id).subscribe(
                                            (object) => {
                                                this.saveObjectsLocally([object], Cache.OBJECT_STORE);
                                                this.updateCacheObject(object);
                                                res(object);
                                            },
                                            (error) => {
                                                //res(localObject);
                                                err('cannot find object')
                                                console.log('cannot find object '+this.objectType+' with id='+id)
                                            }
                                        );
                                        
                                    }
                                    else
                                    {
                                        this.updateCacheObject(localAddedObject);
                                        res(localAddedObject)
                                    }
                                }
                            );

                        }
                        else
                        {
                            if(localObject.unRead==true)
                                this.unReadCount--;
                            localObject.unRead=false;
                            this.saveObjectsLocally([localObject], Cache.OBJECT_STORE);
                            this.updateCacheObject(localObject);
                            res(localObject)
                        }
                    }
                );
            }
            catch(error)
            {
                err(error);
            }
        });   
    }

    public async refreshObject(id: number) {

        return new Promise<any>(async (res, err) => 
        {
            try{
                console.log(this.url + '/' + id);
                this.http.get<any>(this.url + '/' + id).subscribe(
                    (object) => {
                        console.log(object);
                        if(object==null)
                        {
                            this.deleteLocalObjects([{id:id}], Cache.OBJECT_STORE);
                            this.deleteCacheObject({id:id})
                            res(object);
                        }
                        else
                        {
                            object.unRead=true;
                            this.getLocalObject(id,Cache.OBJECT_STORE).then(
                                localObject=>{
                                    if(localObject==undefined || localObject.unRead!=true )
                                        this.unReadCount++;
                                }
                            )
                            
                            this.saveObjectsLocally([object], Cache.OBJECT_STORE);
                            this.updateCacheObject(object);
                            res(object);
                        } 
                        
                    },
                    (error) => {
                        //res(localObject);
                        err('cannot find object')
                        console.log('cannot find object '+this.objectType+' with id='+id)
                    }
                );                     
                                    
            }
            catch(error)
            {
                err(error);
            }
        });   

    }
/* ---------------------------------------------------------------------------------------------------
 * Notification Badge
 * unReadCount
 * ---------------------------------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------------------------------
 * ____________
 * |         /_\
 * |           |
 * |           |
 * |           |
 * |offline to |
 * |     online|-->this.sync()
 * |           |
 * |___________|
 * ---------------------------------------------------------------------------------------------------*/
    public sync()
    {
        console.log('Cache : sync() '+this.objectType)
        this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(
            newLocalObjects => {
                newLocalObjects.forEach(newLocalObject => {
                    // add ----------------------------------------------------
                    const id = newLocalObject.id;
                    if (this.type == Cache.type_NATIVE) {
                        delete newLocalObject.id;
                    }
                    
                    this.http.post(this.url, newLocalObject).toPromise().then(
                        async (newCreatedObject: any) => {
                            newLocalObject.id = id;
                            await this.deleteLocalObjects([newLocalObject], Cache.NEW_OBJECT_STORE);
                            await this.saveObjectsLocally([newCreatedObject], Cache.OBJECT_STORE);
                            newCreatedObject.addId = newLocalObject.id;
                            this.updateAddedCacheObject(newCreatedObject)
                        },
                        async (error) => {
                            if (!this.isNetworkFailed(error)) {
                                newLocalObject.id = id;
                                await this.deleteLocalObjects([newLocalObject], Cache.NEW_OBJECT_STORE);
                                newLocalObject._cache_sync_error = error.error.errorMessage;
                                this.updateCacheObject(newLocalObject)
                            }
                            else
                            {
                                newLocalObject._cache_sync_error = 'Offline';
                                this.updateCacheObject(newLocalObject)
                            }
                        }
                    );
                    // --------------------------------------------------------
                });
            }
        );

        this.getLocalObjects(Cache.OBJECT_STORE).then(
            updateOrDeleteObjects => {
                updateOrDeleteObjects.forEach(updateOrDeleteObject => {
                    if (updateOrDeleteObject._cache_pending_operation == 'update') {
                        // update -------------------------------------------------
                        this.http.patch(this.url, updateOrDeleteObject).subscribe(
                            async (updatedObject) => {
                                await this.saveObjectsLocally([updatedObject], Cache.OBJECT_STORE);
                                this.updateCacheObject(updatedObject)
                            },
                            async (error) => {
                                if (!this.isNetworkFailed(error)) 
                                {
                                    updateOrDeleteObject._cache_sync_error = error.error.errorMessage;
                                    this.updateCacheObject(updateOrDeleteObject)
                                }
                                else
                                {
                                    updateOrDeleteObject._cache_sync_error = 'Offline';
                                    this.updateCacheObject(updateOrDeleteObject)
                                }   
                            }
                        );
                        // --------------------------------------------------------
                    } else if (updateOrDeleteObject._cache_pending_operation == 'delete') {
                        // delete -------------------------------------------------
                        this.http.delete(this.url + '/' + updateOrDeleteObject.id).subscribe(
                            async (deletedStatus) => {
                                await this.deleteLocalObjects([updateOrDeleteObject], Cache.OBJECT_STORE);
                                this.deleteCacheObject(updateOrDeleteObject)
                            },
                            async (error) => {
                                if (!this.isNetworkFailed(error)) 
                                {
                                    updateOrDeleteObject._cache_sync_error = error.error.errorMessage;
                                    this.updateCacheObject(updateOrDeleteObject)
                                }
                                else
                                {
                                    updateOrDeleteObject._cache_sync_error = 'Offline';
                                    this.updateCacheObject(updateOrDeleteObject)
                                }    
                            }
                        );
                        // --------------------------------------------------------
                    }
                });
            }
        );
    }
/* ---------------------------------------------------------------------------------------------------
 * OLD CACHING FUNCTIONS
 * ---------------------------------------------------------------------------------------------------*/
    public static async purgeLocalCache() {
        if (!('indexedDB' in window)) {
            return null;
        }
        await Cache.indexedDatabases.forEach(async objectType => {
            await deleteDB(objectType, {
                
              });
        });
    }


    public async init(objectType: string, url: string) {
        if (!('indexedDB' in window)) {
            return null;
        }
        this.objectType = objectType;
        this.url = url;
        Cache.indexedDatabases.push(objectType);

        this.dbPromise = idb.open(objectType, Cache.dbVersion, db => {

            if (!db.objectStoreNames.contains(Cache.OBJECT_STORE)) {
                const eventsOS = db.createObjectStore(Cache.OBJECT_STORE, { keyPath: 'id' });
                const eventsOS1 = db.createObjectStore(Cache.NEW_OBJECT_STORE, { keyPath: 'id' });
            }
        });

    }

    /** @deprecated since 1.1.6 */
    isOnline() {
        return merge<boolean>(
            fromEvent(window, 'offline').pipe(map(() => false)),
            fromEvent(window, 'online').pipe(map(() => true)),
            new Observable((sub: Observer<boolean>) => {
                sub.next(navigator.onLine);
                sub.complete();
            }));
    }

    // public subscribe(observer) {
    //     this.observer = observer;
    // }

    /** @deprecated since 1.1.6 */
    get dataService() {
        return this.observer.asObservable();
      }

    /*public subscribe(observer, lazyLoadProperty?: string) {
        if (lazyLoadProperty == undefined) {
            this.observer = observer;
        } else {
            this.lazyLoadPropertyObserver = observer;
            this.lazyLoadProperty = lazyLoadProperty;
        }

    }*/

    // ---------------------------------------------------------------------------------------------------
    /** @deprecated since 1.1.6 */
    public updateMe(filter: Filter) {
        // Initialize query param if it is not defined by user
        if (filter == undefined) {
            filter = new Filter;
        }

        let queryParam: any = new QueryParam;
        queryParam.filter = JSON.stringify(filter);

        this.filter = filter;

        // this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(
        //     newLocalObjects => {
        //         this.getLocalObjects(Cache.OBJECT_STORE).then(
        //             localObjects => {
        //                 this.observer(newLocalObjects.concat(localObjects));
        //             }
        //         );
        //     }
        // );
        this.http.get<any[]>(this.url, { params: queryParam }).subscribe(
            (objects) => {
                this.saveObjectsLocally(objects, Cache.OBJECT_STORE);
                this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(
                    newLocalObjects => {
                        this.observer.next(newLocalObjects.concat(objects));
                    }
                );
            },
            (error) => {
                this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(
                    newLocalObjects => {
                        this.getLocalObjects(Cache.OBJECT_STORE).then(
                            localObjects => {
                                this.observer.next(this.applyFilter(newLocalObjects.concat(localObjects), filter));
                            }
                        );
                    }
                );
            }
        );
    }

    //---------------------------------------------------------------------------------------------------
    //Filters
    private applyFilter(objects:any[], filter?:Filter){
        if(filter != null){
            if(filter.search != null){
                objects = this.applySearch(objects, filter.search);
            }
            if(filter.sort != null){
                objects = this.applySort(objects, filter.sort);
            }
            if(filter.paging != null){
                objects = this.applyPaging(objects, filter.paging);
            }
        }
        return objects;
    }

    private applySearch(objects:any[], search:Search){
        if(search.method == "Like"){
            objects.find(x => x.name.contains(search.query))
        }
        else if(search.method == "Equal"){
            objects.find(x => x[''+search.parameter]==search.query)
        }else if(search.method == "ISNULL"){
            objects=objects.filter(x => x[''+search.parameter]==null);
        }
        return objects;
    }

    private applySort(objects:any[], sort:Sort){
        if(sort.parameter == "createdOn" && sort.method == "ASCENDING"){
            objects.sort(
                (a, b) => <any>new Date(a.createdOn) - <any>new Date(b.createdOn)
            )
        }
        if(sort.parameter == "createdOn" && sort.method == "DESCENDING"){
            objects.sort(
                (a, b) => <any>new Date(b.createdOn.toString().replace("[UTC]", "")) - <any>new Date(a.createdOn.toString().replace("[UTC]", ""))
            )
        }
        if(sort.parameter == "name" && sort.method == "ascending"){
            objects.sort(
                (a, b) => a.name.localCompare(b.createdOn)
            )
        }
        if(sort.parameter == "name" && sort.method == "descending"){
            objects.sort(
                (a, b) => a.name.localCompare(b.createdOn)
            )
        }
        return objects;
    }

    private applyPaging(objects:any[], paging:Paging){

        if((paging.start + paging.size)>this.cachedObjects.length)
        {
            //paging.start = this.cachedObjects.length-1;
            //if(paging.start<0) paging.start=0;
            objects = objects.slice(paging.start)
            return objects;
        }
        else
        {
            objects = objects.slice(paging.start, paging.start+paging.size)
            //paging.start=paging.start + paging.size;
            return objects;
        }
        
    }

    // ---------------------------------------------------------------------------------------------------
    /**
     *               _     ___   _  _
     *              | |   / _ \ | || |
     *   __ _   ___ | |_ / /_\ \| || |
     *  / _` | / _ \| __||  _  || || |
     * | (_| ||  __/| |_ | | | || || |
     *  \__, | \___| \__|\_| |_/|_||_|
     *   __/ |
     *  |___/
     *
     * @author Digvijay Gavas
     * @since 05-April-2020
     * @param filter
     */
    /** @deprecated since 1.1.6 */
    public getAll(filter?: Filter) {
        // Initialize query param if it is not defined by user
        if (filter == undefined) {
            filter = new Filter;
        }

        let queryParam: any = new QueryParam;
        queryParam.filter = JSON.stringify(filter);

        console.log('CACHE: -------');
        return this.syncPendingRequests().then(
            A => {
                return new Promise(res => {
                    this.http.get<any[]>(this.url, { params: queryParam }).subscribe(
                        (objects) => {
                            this.saveObjectsLocally(objects, Cache.OBJECT_STORE);
                            this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(
                                newLocalObjects => {
                                    res(newLocalObjects.concat(objects));
                                }
                            );
                        },
                        (error) => {
                            this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(
                                newLocalObjects => {
                                    this.getLocalObjects(Cache.OBJECT_STORE).then(
                                        localObjects => {
                                            res(newLocalObjects.concat(localObjects));
                                        }
                                    );
                                }
                            );
                        }
                    );
                }
                );
            }
        );
    }

    /**
     *               _
     *              | |
     *   __ _   ___ | |_
     *  / _` | / _ \| __|
     * | (_| ||  __/| |_
     *  \__, | \___| \__|
     *   __/ |
     *  |___/
     *
     * @author Digvijay Gavas
     * @since 05-April-2020
     * @param id
     * @param lightObject
     */
    /** @deprecated since 1.1.6 */
    public async get(id: number, lightObject?: any) {
        // this if block executed if second argument lightObject is passed to this method
        if (lightObject != undefined) {
            return new Promise<any>(res => {
                this.getLocalObject(id, Cache.OBJECT_STORE).then(
                    localObject => {
                        if (localObject == undefined) {
                            res(this.get(id));
                        } else {
                            if (lightObject.lastModified == localObject.lastModified) {
                                res(localObject);
                            } else {
                                res(this.get(id));
                                /*this.http.get<any[]>(this.url + '/' + id).subscribe(
                                    (object) => {
                                        this.saveObjectsLocally([object], Cache.OBJECT_STORE);
                                        res(object);
                                    },
                                    (error) => {
                                        res(localObject);
                                    }
                                );*/
                            }
                        }
                    }
                );
            });

        } else {
            await this.syncPendingRequests();
            return new Promise(res => {
                this.http.get<any[]>(this.url + '/' + id).subscribe(
                    (object) => {
                        this.saveObjectsLocally([object], Cache.OBJECT_STORE);
                        res(object);
                    },
                    (error) => {
                        this.getLocalObject(id, Cache.OBJECT_STORE).then(
                            localObject => {
                                res(localObject);
                            }

                        );
                    }
                );
            }
            );
        }

    }

    /**
     *             _      _
     *            | |    | |
     *   __ _   __| |  __| |
     *  / _` | / _` | / _` |
     * | (_| || (_| || (_| |
     *  \__,_| \__,_| \__,_|
     *
     * @author Digvijay Gavas
     * @since 05-April-2020
     * @param object
     */
    /** @deprecated since 1.1.6 */
    public async add(object: any) {
        await this.syncPendingRequests();
        let newObject = JSON.parse(JSON.stringify(object));
        newObject.createdOn=new Date();

        if (this.type == Cache.type_NATIVE) {
            newObject.id = await this.getNextId();
        }
        newObject._cache_pending_operation = 'add';
        return new Promise((res, err) => {
            this.http.post(this.url, object).subscribe(
                (newCreatedObject) => {
                    this.saveObjectsLocally([newCreatedObject], Cache.OBJECT_STORE);
                    res(newCreatedObject);
                },
                (error) => {
                    if (this.isNetworkFailed(error)) {
                        this.saveObjectsLocally([newObject], Cache.NEW_OBJECT_STORE);
                        res(newObject);
                    } else {
                        err(error);
                    }
                }
            );
        }
        );
    }
    /**
     *             _      _
     *            | |    | |
     *   __ _   __| |  __| |
     *  / _` | / _` | / _` |
     * | (_| || (_| || (_| |
     *  \__,_| \__,_| \__,_|
     *
     * @author Digvijay Gavas
     * @since 05-April-2020
     * @param object
     */
    /** @deprecated since 1.1.6 , the function need to be re-written in cache3.0*/
    public async addBulk(objects: any) {
        await this.syncPendingRequests();
        let newObjects = JSON.parse(JSON.stringify(objects));

        if (this.type == Cache.type_NATIVE) {
            newObjects.forEach(async newObject => {
                newObject.id = await this.getNextId();
                newObject._cache_pending_operation = 'add';
            }); 
        }
        
        return new Promise((res, err) => {
            this.http.put(this.url, objects).subscribe(
                (newCreatedObjects) => {
                    this.saveObjectsLocally(newCreatedObjects, Cache.OBJECT_STORE);
                    res(newCreatedObjects);
                },
                (error) => {
                    if (this.isNetworkFailed(error)) {
                        this.saveObjectsLocally(newObjects, Cache.NEW_OBJECT_STORE);
                        res(newObjects);
                    } else {
                        err(error);
                    }
                }
            );
        }
        );
    }
    
    /**
     *                    _         _
     *                   | |       | |
     *  _   _  _ __    __| |  __ _ | |_   ___
     * | | | || '_ \  / _` | / _` || __| / _ \
     * | |_| || |_) || (_| || (_| || |_ |  __/
     *  \__,_|| .__/  \__,_| \__,_| \__| \___|
     *        | |
     *        |_|
     *
     * @author Digvijay Gavas
     * @since 05-April-2020
     * @param object
     */
    /** @deprecated since 1.1.6 */
    public async update(object: any) {
        //await this.syncPendingRequests();
        //await this.sync();
        //object._cache_pending_operation = 'update';
        //await this.saveObjectsLocally([object], Cache.OBJECT_STORE);
        //await this.updateCacheObject(object,'update')
        //await this.sync();

        
        return new Promise((res, err) => {
            this.http.patch(this.url, object).subscribe(
                (updatedObject) => {
                    this.saveObjectsLocally([updatedObject], Cache.OBJECT_STORE);
                    res(updatedObject);
                },
                (error) => {
                    if (this.isNetworkFailed(error)) {
                        object._cache_pending_operation = 'update';
                        this.saveObjectsLocally([object], Cache.OBJECT_STORE);
                        res(object);
                    } else {
                        err(error);
                    }
                }
            );
        }
        );
    }

    /**
     * 	    _        _        _
     *     | |      | |      | |
     *   __| |  ___ | |  ___ | |_   ___
     *  / _` | / _ \| | / _ \| __| / _ \
     * | (_| ||  __/| ||  __/| |_ |  __/
     *  \__,_| \___||_| \___| \__| \___|
     *
     * @author Digvijay Gavas
     * @since 05-April-2020
     * @param object
     */
    /** @deprecated since 1.1.6 */
    public async delete(deleteObject: any) {
        await this.syncPendingRequests();
        return new Promise((res, err) => {
            this.http.delete(this.url + '/' + deleteObject.id).subscribe(
                (deletedStatus) => {
                    this.deleteLocalObjects([deleteObject], Cache.OBJECT_STORE);
                    res(deletedStatus);
                    this.notifyObservers();
                },
                (error) => {
                    const isNetworkFailedBool = this.isNetworkFailed(error);
                    if (isNetworkFailedBool) {
                        deleteObject._cache_pending_operation = 'delete';
                        this.saveObjectsLocally([deleteObject], Cache.OBJECT_STORE);
                        res(deleteObject);
                        this.objects.map((object,i)=>{
                            if(object.id==deleteObject.id && object._cache_pending_operation == 'delete')
                            {
                                object=deleteObject
                            }
                        })
                    } else {
                        err(error);
                    }
                }
            );
        }
        );
    }

    // ---------------------------------------------------------------------------------------------------

    private isNetworkFailed(error) {
        if ('status' in error && error.status != 0) {
            return false;
        } else {
            return true;
        }
    }

    private async getNextId() {
        return await this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(newLocalObjects => {
            let id = 0;
            newLocalObjects.forEach(newLocalObject => {
                if (newLocalObject.id > id) {
                    id = newLocalObject.id;
                }
            });
            return ++id;
        });
    }
    /** @deprecated since 1.1.6 */
    private notifyObservers() {
        this.updateMe(this.filter);
    }

    /** @deprecated since 1.1.6 */
    public syncPendingRequests() {
        return this.dbPromise.then(db => {
            this.getLocalObjects(Cache.NEW_OBJECT_STORE).then(
                newLocalObjects => {
                    newLocalObjects.forEach(newLocalObject => {
                        // add ----------------------------------------------------
                        const id = newLocalObject.id;
                        if (this.type == Cache.type_NATIVE) {
                            delete newLocalObject.id;
                        }
                        this.http.post(this.url, newLocalObject).toPromise().then(
                            async (newCreatedObject) => {
                                newLocalObject.id = id;
                                await this.deleteLocalObjects([newLocalObject], Cache.NEW_OBJECT_STORE);
                                await this.saveObjectsLocally([newCreatedObject], Cache.OBJECT_STORE);
                                this.notifyObservers();
                            },
                            async (error) => {
                                if (!this.isNetworkFailed(error)) {
                                    newLocalObject.id = id;
                                    await this.deleteLocalObjects([newLocalObject], Cache.NEW_OBJECT_STORE);
                                    this.notifyObservers();
                                }
                            }
                        );
                        // --------------------------------------------------------
                    });
                }
            );

            this.getLocalObjects(Cache.OBJECT_STORE).then(
                updateOrDeleteObjects => {
                    updateOrDeleteObjects.forEach(updateOrDeleteObject => {
                        if (updateOrDeleteObject._cache_pending_operation == 'update') {
                            // update -------------------------------------------------
                            this.http.patch(this.url, updateOrDeleteObject).subscribe(
                                async (updatedObject) => {
                                    await this.saveObjectsLocally([updatedObject], Cache.OBJECT_STORE);
                                    this.notifyObservers();
                                }
                            );
                            // --------------------------------------------------------
                        } else if (updateOrDeleteObject._cache_pending_operation == 'delete') {
                            // delete -------------------------------------------------
                            this.http.delete(this.url + '/' + updateOrDeleteObject.id).subscribe(
                                async (deletedStatus) => {
                                    await this.deleteLocalObjects([updateOrDeleteObject], Cache.OBJECT_STORE);
                                    this.notifyObservers();
                                }
                            );
                            // --------------------------------------------------------
                        }
                    });
                }
            );

        })


    }

    // ---------------------------------------------------------------------------------------------------


    private async saveObjectsLocally(objects, storeName: string) {
        return await this.dbPromise.then(async db => {
            const tx = db.transaction(storeName, 'readwrite');
            const store = tx.objectStore(storeName);
            await Promise.all([
                objects.map(async objectsData => {
                    store.put(objectsData);
                }),
                tx.done,
            ])
                .catch(() => {
                    tx.abort();
                    throw Error('Events were not added to the store');
                });
        });
    }

    private async getLocalObjects(storeName: string) {
        return await this.dbPromise.then(async db => {
            const newTx = db.transaction(storeName, 'readonly');
            const newStore = newTx.objectStore(storeName);
            let objects
            await Promise.all([
                objects = newStore.getAll(),
                newTx.done,
              ]);
            return objects;
        });
    }

    private async getLocalObject(id: number, storeName: string) {
        console.log('fetching...'+this.objectType+'('+id+') from '+storeName);
        return await this.dbPromise.then(async db => {
            const newTx = db.transaction(storeName, 'readonly');
            const newStore = newTx.objectStore(storeName);
            let objects
            await Promise.all([
                objects = newStore.get(id),
                newTx.done,
              ]);
            return objects;
        });
    }

    private async deleteLocalObjects(objects, storeName: string) {
        return await this.dbPromise.then(async db => {
            const newTx = db.transaction(storeName, 'readwrite');
            const newStore = newTx.objectStore(storeName);
            objects.forEach(async object => {
                let objects
                await Promise.all([
                    objects = newStore.delete(object.id),
                    newTx.done,
                ]);
                return objects;
            });
        });
    }

    private async clearAllLocalObjects(storeName: string) {
        return await this.dbPromise.then(async db => {
            await db.transaction(storeName, 'readwrite').objectStore(storeName).clear();
        });
    }
}
