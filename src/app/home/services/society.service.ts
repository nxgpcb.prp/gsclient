import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Facility } from 'src/app/core/Facility';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { Resident } from 'src/app/core/Resident';
import { Notice } from 'src/app/core/Notice';
import { LoginService } from 'src/app/login/login.service';
import { JoiningRequest } from 'src/app/core/JoiningRequest';
import { HomeService } from '../home.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Security } from 'src/app/core/Security';
import { LoaderService } from 'src/app/misc/loader.service';
import { environment } from 'src/environments/environment';
import { SubscriptionPlanLog } from 'src/app/core/SubscriptionPlanLog';
import { AdService } from '../ad/ad.service';
import { Cache } from './cache';
import { Paging } from 'src/app/filters/Paging';
import { Filter } from 'src/app/filters/Filter';
import { Apartment } from 'src/app/core/Apartment';
import { ClubHouse } from 'src/app/core/ClubHouse';
import { Garden } from 'src/app/core/Garden';
import { Gymnasium } from 'src/app/core/Gymnasium';
import { Parking } from 'src/app/core/Parking';
import { SwimmingPool } from 'src/app/core/SwimmingPool';
import { TransactionLog } from 'src/app/core/TransactionLog';
import { BookingLog } from 'src/app/core/BookingLog';
import { DemandLog } from 'src/app/core/DemandLog';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Platform } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';

const { SplashScreen, StatusBar } = Plugins;

/** @deprecated since 1.1.7 */
@Injectable({
  providedIn: 'root'
})
export class SocietyService {

  resident: Resident;
  security: Security;
  facility: Facility;
  notice: Notice;
  progressing: boolean = false;
  noticeCache: Cache = null;
  securityCache: Cache = null;
  facilityCache: Cache = null;
  apartmentCache: Cache = null;
  clubHouseCache: Cache = null;
  gardenCache: Cache = null;
  gymnasiumCache: Cache = null;
  parkingCache: Cache = null;
  swimmingPoolCache: Cache = null;
  residentCache: Cache = null;
  visitorLogCache: Cache = null;
  joiningRequestCache: Cache = null;
  bookingLogCache: Cache = null;
  subscriptionPlanLogCache: Cache = null;
  profilePictureCache: Cache = null;
  transactionLogCache: Cache = null;
  demandLogCache: Cache = null;
  accountCache: Cache = null;

  constructor(private router: Router, private http: HttpClient, private loginService: LoginService,
    private afAuth: AngularFireAuth, private loader: LoaderService,
    private adService: AdService, private fcm:FcmService, private platform: Platform,) {
    this.init();
  }

  private async init() {

    this.profilePictureCache = new Cache(this.http,
      'profilePicture',
      environment.backend.baseURL + '/GSServer/webapi/secured/File',
      Cache.type_NATIVE);

    this.noticeCache = new Cache(this.http,
      'notices',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Notice',
      Cache.type_NATIVE);

    this.securityCache = new Cache(this.http,
      'securities',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Security',
      Cache.type_REFERED);

    this.facilityCache = new Cache(this.http,
      'facilities',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Facility',
      Cache.type_NATIVE);

    this.apartmentCache = new Cache(this.http,
      'apartments',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Facilities/Apartment',
      Cache.type_NATIVE);

    this.clubHouseCache = new Cache(this.http,
      'clubhouses',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Facilities/ClubHouse',
      Cache.type_NATIVE);

    this.gardenCache = new Cache(this.http,
      'gardens',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Facilities/Garden',
      Cache.type_NATIVE);

    this.gymnasiumCache = new Cache(this.http,
      'gymnasiums',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Facilities/Gymnasium',
      Cache.type_NATIVE);

    this.parkingCache = new Cache(this.http,
      'parkings',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Facilities/Parking',
      Cache.type_NATIVE);

    this.swimmingPoolCache = new Cache(this.http,
      'swimmingpools',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Facilities/SwimmingPool',
      Cache.type_NATIVE);

    this.residentCache = new Cache(this.http,
      'residents',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Resident',
      Cache.type_REFERED);

    this.visitorLogCache = new Cache(this.http,
      'visitorLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/VisitorLog',
      Cache.type_NATIVE);

    this.joiningRequestCache = new Cache(this.http,
      'joiningRequests',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/JoiningRequest',
      Cache.type_NATIVE);

    this.bookingLogCache = new Cache(this.http,
      'bookingLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Facilities/BookingLog',
      Cache.type_NATIVE);

    this.subscriptionPlanLogCache = new Cache(this.http,
      'subscriptionPlanLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/SubscriptionPlanLog',
      Cache.type_NATIVE);

    this.transactionLogCache = new Cache(this.http,
      'transactionLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Accounting/TransactionLog',
      Cache.type_NATIVE);

    this.demandLogCache = new Cache(this.http,
      'demandLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Accounting/DemandLog',
      Cache.type_NATIVE);

    this.accountCache = new Cache(this.http,
      'accounts',
      environment.backend.baseURL + '/GSServer/webapi/secured/Society/Accounting/Account',
      Cache.type_NATIVE);
  }





  searchSecurities(searchTerm: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Security[]>(environment.backend.baseURL + '/GSServer/webapi/secured/Society/searchSecurities/' + searchTerm, { headers: headers })
  }

  /** @deprecated since 1.1.7 */
  setProgressing(progress: boolean) {
    this.progressing = progress;
  }

  canActivate(route: ActivatedRouteSnapshot): any {
    console.log("Society Service");
    this.loader.showLoader();
    return new Promise(res => {
      this.loginService.getUser()
        .then(
          (response: any) => {
            this.loader.hideLoader();
            console.log(response)
            if (response.type == "Society") {

              if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
                StatusBar.setStyle({
                  style: StatusBarStyle.Dark
                });
                SplashScreen.hide();
                StatusBar.show();
              }

              
              
              this.fcm.subscribeNotfication();
              
              //Subscribe to adds if plan is Free and if subscription is expired.
              if (response.subscriptionPlan != null) {
                if (response.isActive) {
                  console.log("Plan Active")
                } else {
                  console.log("Plan Expired")
                  this.adService.subscribeAds("NORMAL");
                }
                res(true);
              } else {
                this.router.navigate(['/not-autherized']);
                res(false);
              }
            } else {
              this.router.navigate(['/not-autherized']);
              res(false);
            }
          },
          (error) => {
            this.loader.hideLoader();
            this.router.navigate(['/login']);
            res(false);
          }
        );
    })
  }

}
