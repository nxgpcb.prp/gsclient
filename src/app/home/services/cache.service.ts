import { Injectable, NgZone } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Cache } from './cache';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  static LOAD_ALL=1000;
  static DEFAULT_LOAD_COUNT=10;
  static DEFAULT_LOAD_NEXT_COUNT=10;
  caches : Cache []=[];
  //Common
  userCache: Cache;

  constructor(private http: HttpClient,private fcm: FcmService,private readonly onlineOfflineService: OnlineOfflineService,private ngZone: NgZone) {
    this.init();
  }

  private async init() {

    this.fcm.currentMessage.subscribe((data) => {
      if(data!=undefined && data.data!=undefined)
      {
        this.getCache(data.data.objectType).then(cache=>{
          this.ngZone.run(() => {cache.refreshObject(Number(data.data.id))});
        });
      }
    });

    this.onlineOfflineService.connectionChanged.subscribe(online => {
      if (online) {
        this.ngZone.run(() => {this.caches.map(cache=>cache.sync())});
      }
    });

    this.userCache = await new Cache(this.http,'user',environment.backend.baseURL + '/GSServer/webapi/secured/User',Cache.type_NATIVE); 
  }

  public async initializeCache(objectType,RESTUrl,isReferred)
  {
    return new Promise<Cache>(res => {
      let matchedCaches=this.caches.filter(cache=>cache.objectType==objectType)
      if(matchedCaches.length>0)
      {
        res(matchedCaches[0])
      }
      else
      {
        let newCache=new Cache(this.http,objectType, environment.backend.baseURL + RESTUrl,isReferred?Cache.type_REFERED:Cache.type_NATIVE);
        this.caches.push(newCache);
        res(newCache)
      }
    });
  }

  public getCache(objectType)
  {
    return new Promise<Cache>((res,err) => {
      let matchedCaches=this.caches.filter(cache=>cache.objectType==objectType)
      if(matchedCaches.length>0)
      {
        res(matchedCaches[0])
      }
      else
      {
        err(null)
      }
    });
  }
}
