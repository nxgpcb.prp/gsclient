import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { HomeService } from '../home.service';
import { LoginService } from 'src/app/login/login.service';
import { LoaderService } from 'src/app/misc/loader.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {
  user: any;

  constructor(private router: Router, private loginService: LoginService,
    private loader: LoaderService) {

  }

  canActivate(route: ActivatedRouteSnapshot): any {
    console.log("Auth Service");
    this.loader.showLoader();
    return new Promise(res => {
      this.loginService.getUser()
        .then(
          (response: any) => {
            this.loader.hideLoader();
            console.log(response)
            
            res(true);
          },
          (error) => {
            this.loader.hideLoader();
            this.router.navigate(['/login']);
            res(false);
          }
        );
    })
  }
}
