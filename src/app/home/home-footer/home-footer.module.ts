import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { HomeFooterComponent } from './home-footer.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [HomeFooterComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule

  ],
  exports: [
    HomeFooterComponent
  ]
})
export class HomeFooterModule { }
