import { Injectable } from '@angular/core';
import { AdOptions, AdSize, AdPosition } from 'capacitor-admob';
import { environment } from 'src/environments/environment';
import { Plugins } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { LoaderService } from 'src/app/misc/loader.service';

const { AdMob } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class AdService {

  currentPageType: String;
  adsSubscribed: boolean = false;

  androidOptions: AdOptions = {
    //test add id. Remember to refer java code for methods.
    adId: environment.admob.android.bannerAddId,
    adSize: AdSize.FLUID,
    position: AdPosition.BOTTOM_CENTER,
    hasTabBar: false,
  };

  androidTabOptions: AdOptions = {
    //test add id. Remember to refer java code for methods.
    adId: environment.admob.android.bannerAddId,
    adSize: AdSize.FLUID,
    position: AdPosition.BOTTOM_CENTER,
    hasTabBar: true,
  };

  iosOptions: AdOptions = {
    //test add id. Remember to refer java code for methods.
    adId: environment.admob.ios.bannerAddId,
    adSize: AdSize.FLUID,
    position: AdPosition.BOTTOM_CENTER,
    hasTabBar: false,
  };

  iosTabOptions = {
    //test add id. Remember to refer java code for methods.
    adId: environment.admob.ios.bannerAddId,
    adSize: AdSize.FLUID,
    position: AdPosition.BOTTOM_CENTER,
    hasTabBar: true,
    //margin property not present in plugin so removed type of iosTabOptions
    margin: "56"
  };

  constructor(private platform: Platform, private loader: LoaderService,) { }

  subscribeAds(pageType: String) {
    console.log("Subscibe Adds:" + pageType)
    this.adsSubscribed = true;
    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
      let options: any;
      if (pageType == "NORMAL") {
        if (this.platform.is('android')){
          options = this.androidOptions;
        }
        if (this.platform.is('ios')){
          options = this.iosOptions;
        }
      }

      if (pageType == "TAB") {
        if (this.platform.is('android')){
          options = this.androidTabOptions;
        }
        if (this.platform.is('ios')){
          options = this.iosTabOptions;
        }
      }

      this.currentPageType = pageType;

      // Show Banner Ad
      AdMob.showBanner(options).then(
        value => {
          console.log(value); // true
          this.loader.hideLoader();
        },
        error => {
          console.error(error); // show error
          this.loader.hideLoader();
        }
      );
    }
  }

  reSubscribeAds(pageType: String) {
    console.log("Resubscription Called")
    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
      if (this.adsSubscribed) {
        console.log("Resubscription adsSubscribed")
        if (this.currentPageType !== pageType) {
          console.log("Resubscription Page Mismatch")
          AdMob.removeBanner().then(
            value => {
              console.log(value); // true
              this.subscribeAds(pageType);
            },
            error => {
              console.error(error); // show error
              this.subscribeAds(pageType);
            }
          );
        }
      }
    }
  }

  hideAds() {
    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
      AdMob.hideBanner().then(
        value => {
          console.log(value); // true
          this.loader.hideLoader();
        },
        error => {
          console.error(error); // show error
          this.loader.hideLoader();
        }
      );
    }
  }

  resumeAds() {
    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
      AdMob.resumeBanner().then(
        value => {
          console.log(value); // true
          this.loader.hideLoader();
        },
        error => {
          console.error(error); // show error
          this.loader.hideLoader();
        }
      );
    }
  }

  unSubscribeAds() {
    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
      // Show Banner Ad
      AdMob.removeBanner().then(
        value => {
          console.log(value); // true
          this.loader.hideLoader();
        },
        error => {
          console.error(error); // show error
          this.loader.hideLoader();
        }
      );
    }
  }
}
