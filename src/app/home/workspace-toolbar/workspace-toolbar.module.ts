import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { WorkspaceToolbarComponent } from './workspace-toolbar.component';
import { PopoverMenuComponent } from '../popover-menu/popover-menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SetStatusComponent } from '../set-status/set-status.component';
import { SetStatusModule } from '../set-status/set-status.module';
import { RouterModule, Routes } from '@angular/router';
import { SecurityGuardLoginComponent } from '../security/security-guard-login/security-guard-login.component';

const routes: Routes = [
  {
    path: '',
    component: WorkspaceToolbarComponent,
  },  
];

@NgModule({
  declarations: [WorkspaceToolbarComponent, PopoverMenuComponent,SecurityGuardLoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SetStatusModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    TranslateModule
  ],
  exports: [
    PopoverMenuComponent,
    WorkspaceToolbarComponent,
  ],
  entryComponents: [
    SetStatusComponent,
    SecurityGuardLoginComponent,
  ],
})
export class WorkspaceToolbarModule { }
