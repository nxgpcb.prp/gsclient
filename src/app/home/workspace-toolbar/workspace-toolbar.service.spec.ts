import { TestBed } from '@angular/core/testing';

import { WorkspaceToolbarService } from './workspace-toolbar.service';

describe('WorkspaceToolbarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkspaceToolbarService = TestBed.get(WorkspaceToolbarService);
    expect(service).toBeTruthy();
  });
});
