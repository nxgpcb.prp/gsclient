import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceToolbarService {

  @Output() backButtonFire: EventEmitter<any> = new EventEmitter();
  @Output() updateUserFire: EventEmitter<any> = new EventEmitter();
  @Output() updateSecurityGuardFire: EventEmitter<any> = new EventEmitter();
  @Output() updateSecurityGuardLogFire: EventEmitter<any> = new EventEmitter();

  constructor() {
    console.log('shared service started');
  }

  hideBackButton() {
    this.backButtonFire.emit(true);
  }

  showBackButton() {
    this.backButtonFire.emit(false);
  }

  getBackButtonEmittedValue() {
    return this.backButtonFire;
  }

  updateUser(user:any) {
    this.updateUserFire.emit(user);
  }

  getUpdateUserEmittedValue(){
    return this.updateUserFire;
  }

  updateSecurityGuard(securityGuard:any) {
    this.updateSecurityGuardFire.emit(securityGuard);
  }

  getUpdateSecurityGuardEmittedValue(){
    return this.updateSecurityGuardFire;
  }

  updateSecurityGuardLog(securityGuardLog:any) {
    this.updateSecurityGuardLogFire.emit(securityGuardLog);
  }

  getUpdateSecurityGuardLogEmittedValue(){
    return this.updateSecurityGuardLogFire;
  }

}
