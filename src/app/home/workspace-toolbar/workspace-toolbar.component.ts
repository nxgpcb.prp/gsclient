import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';
import { PopoverController, Platform } from '@ionic/angular';
import { PopoverMenuComponent } from '../popover-menu/popover-menu.component';
import { HomeService } from '../home.service';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { WorkspaceToolbarService } from './workspace-toolbar.service';
import { FcmService } from 'src/app/notifications/fcm.service';
import { User } from 'src/app/core/User';
import { SecurityGuardLoginComponent } from '../security/security-guard-login/security-guard-login.component';
import { SecurityGuard } from 'src/app/core/SecurityGuard';
import { SecurityGuardLog } from 'src/app/core/SecurityGuardLog';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';

@Component({
  selector: 'app-workspace-toolbar',
  templateUrl: './workspace-toolbar.component.html',
  styleUrls: ['./workspace-toolbar.component.scss']
})
export class WorkspaceToolbarComponent implements OnInit, OnInAppUpdateInterface, OnDestroy {

  user: any;
  loggedInUserName: String;
  hideBackButton: Boolean;
  securityGuard: SecurityGuard;
  securityGuardLog: SecurityGuardLog;

  constructor(
    private popoverController: PopoverController, 
    private loginService: LoginService,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private fcm: FcmService,
    public networkService:OnlineOfflineService
    ) {
    this.hideBackButton = false;
  }

  async ngOnInit() {

    this.ngOnInAppUpdate();
    this.workspaceToolbarService.getBackButtonEmittedValue()
      .subscribe(item => {
        console.log("Emitted Value : " + item);
        this.hideBackButton = item;
      }
      );

    this.workspaceToolbarService.getUpdateUserEmittedValue()
      .subscribe(item => {
        console.log(item);
        this.user = item;
        this.loggedInUserName = this.user.name;
      }
      );

    this.workspaceToolbarService.getUpdateSecurityGuardEmittedValue()
      .subscribe(item => {
        console.log(item);
        this.securityGuard = item;
        if (this.user.type == "Security" && this.securityGuard == undefined && this.user.society != undefined) {
          this.presentLoginPopover(null);
        }
      }
      );

      this.workspaceToolbarService.getUpdateSecurityGuardLogEmittedValue()
      .subscribe(item => {
        console.log(item);
        this.securityGuardLog = item;
      }
      );

    this.user = await this.loginService.getUser();
    console.log(this.user)
    this.loggedInUserName = this.user.name;
    if (this.user.type == "Security" && this.securityGuard == undefined && this.user.society != undefined) {
      this.presentLoginPopover(null);
    }
  }

  async presentLoginPopover(ev: any) {
    //Style defined in global.scss
    const popover = await this.popoverController.create({
      component: SecurityGuardLoginComponent,
      componentProps: {
        "popoverController": this.popoverController,
      },
      event: ev,
      translucent: true,
      backdropDismiss: false
    });

    popover.onDidDismiss().then(
      result => {
        if(this.securityGuard!=undefined){
          this.initTimer();
          this.startTimer();
        }
      }
    )

    return await popover.present();
  }

  async presentPopover(ev: any) {
    //Style defined in global.scss
    const popover = await this.popoverController.create({
      component: PopoverMenuComponent,
      componentProps: {
        "popoverController": this.popoverController,
        "securityGuard": this.securityGuard,
        "securityGuardLog": this.securityGuardLog,
      },
      event: ev,
      translucent: true,
      cssClass: 'custom-popover'
    });

    return await popover.present();  
  }

  ngOnInAppUpdate(): void {
    this.fcm.currentMessage.subscribe((data) => {
      if (data != null) {
        this.refresh(null);
      }
    });
  }

  refresh(event) {
    setTimeout(async () => {

      this.user = await this.loginService.getUser()
      this.loggedInUserName = this.user.name;
      if (event != null) {
        event.target.complete();
      }
    }, 2000);
  }

  initDataService() {

  }

  ngOnDestroy() {
    this.unRegisterToEvents();
  }

  private unRegisterToEvents() {

  }

  time;
  runTimer;
  hasStarted ;
  hasFinished;
  remainingTime;
  displayTime;

  initTimer() {
  
   let timeInSeconds = 3600; 
   
   this.time = timeInSeconds;
   this.runTimer = false;
   this.hasStarted = false;
   this.hasFinished = false;
   this.remainingTime = timeInSeconds;
   
   this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
 }
 
 startTimer() {
    this.runTimer = true;
    this.hasStarted = true;
    this.timerTick();
 }
  
 timerTick() {
   setTimeout(() => {
 
     if (!this.runTimer) { return; }
     this.remainingTime--;
     this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
     if (this.remainingTime > 0) {
       this.timerTick();
     }
     else {
       this.hasFinished = true;
       this.presentLoginPopover(null);
     }
   }, 1000);
 }
 
 getSecondsAsDigitalClock(inputSeconds: number) {
   var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
   var hours = Math.floor(sec_num / 3600);
   var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
   var seconds = sec_num - (hours * 3600) - (minutes * 60);
   var hoursString = '';
   var minutesString = '';
   var secondsString = '';
   hoursString = (hours < 10) ? "0" + hours : hours.toString();
   minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
   secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
   return hoursString + ':' + minutesString + ':' + secondsString;
 }
}
