import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/settings/settings.service';
import { Router } from '@angular/router';
import { HomeService } from '../home.service';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  
  user:any;
  name:string = '';
  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  constructor(private settingsService:SettingsService, private router:Router, private loginService: LoginService,) { }

  async ngOnInit() {
    
    this.user = await this.loginService.getUser()
  }

  submitContinue(){
    this.settingsService.updateName(this.name).
      subscribe(
        (response) => {
          console.log(response);
          this.router.navigate(['/'])
        },
        (error) => {
          console.log(error);
        }
      )
  }

}

