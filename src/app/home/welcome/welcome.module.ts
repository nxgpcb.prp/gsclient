import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WelcomePageRoutingModule } from './welcome-routing.module';

import { WelcomePage } from './welcome.page';
import { WorkspaceToolbarModule } from '../workspace-toolbar/workspace-toolbar.module';
import { TranslateModule } from '@ngx-translate/core';
import { PopoverMenuComponent } from '../popover-menu/popover-menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WelcomePageRoutingModule,
    TranslateModule,
    WorkspaceToolbarModule
  ],
  declarations: [WelcomePage],
  entryComponents: [
    PopoverMenuComponent,
  ],
})
export class WelcomePageModule {}
