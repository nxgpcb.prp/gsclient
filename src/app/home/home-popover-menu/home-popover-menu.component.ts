import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LanguageService } from 'src/app/language/language.service';
import { LanguagePopoverPage } from 'src/app/language/language-popover/language-popover.page';
import { DebugService } from 'src/app/for-developer/debug.service';

@Component({
  selector: 'app-home-popover-menu',
  templateUrl: './home-popover-menu.component.html',
  styleUrls: ['./home-popover-menu.component.scss']
})
export class HomePopoverMenuComponent implements OnInit {

  @Input()
  popoverController:PopoverController
  
  constructor(private router: Router, private languageService:LanguageService, public readonly debugService:DebugService) { }

  ngOnInit() {
  }

  forDeveloper(){
    this.popoverController.dismiss();
    this.router.navigate(['/for-developer']);
  }

  about(){
    this.popoverController.dismiss();
    this.router.navigate(['/about']);
  }

  pricing(){
    this.popoverController.dismiss();
    this.router.navigate(['/pricing']);
  }

  support(){
    this.popoverController.dismiss();
    this.router.navigate(['/support']);
  }

  login(){
    this.popoverController.dismiss();
    this.router.navigate(['/login']);
  }

  signUp(){
    this.popoverController.dismiss();
    this.router.navigate(['/register']);
  }

  language(ev:any){
    this.popoverController.dismiss();
    this.presentLanguagePopover(ev);
  }

  async presentLanguagePopover(ev: any) {
    //Style defined in global.scss
    const popover = await this.popoverController.create({
      component: LanguagePopoverPage,
      componentProps: {
        "popoverController" : this.popoverController,
      },
      event: ev,
      translucent: true,
      cssClass: 'custom-popover'
    });

    return await popover.present();
  }

}
