import { Injectable, NgZone } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginService } from '../login/login.service';
import { User } from '../core/User';
import { Resident } from '../core/Resident';
import { Society } from '../core/Society';
import { Security } from '../core/Security';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoaderService } from '../misc/loader.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService implements CanActivate {

  user: any;

  constructor(private http: HttpClient, private ngZone: NgZone) { }

  canActivate(route: ActivatedRouteSnapshot) {
    return true;
  }

  getResident() {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Resident>(environment.backend.baseURL + '/GSServer/webapi/secured/User', { headers: headers })
  }

  getSociety() {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Society>(environment.backend.baseURL + '/GSServer/webapi/secured/User', { headers: headers })
  }

  getSecurity() {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Security>(environment.backend.baseURL + '/GSServer/webapi/secured/User', { headers: headers })
  }

  // getUserUpdated() {
  //   let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

  //   let headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': "Basic " + idToken
  //   });
  //   return this.http.get<User>(environment.backend.baseURL + '/GSServer/webapi/secured/User', { headers: headers });
  // }

  // getUser(){
  //   return this.user;
  // }

  // getUserUpdatedAsync(): any | Observable<any> | Promise<any> {
  //   return new Promise<User>(
  //     (resolve, reject) => this.ngZone.run(() => {

  //       if(this.user){
  //         console.log("local user")
  //         resolve(this.user);
  //       }
  //       this.getUserUpdated().subscribe(
  //         response => {
  //           console.log("network user")
  //           this.user = response;
  //           resolve(response);
  //         },
  //         error => {
  //           reject(error);
  //         }

  //       )

  //     }))
  // }
}