import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdateComplaintPage } from './update-complaint.page';

describe('UpdateComplaintPage', () => {
  let component: UpdateComplaintPage;
  let fixture: ComponentFixture<UpdateComplaintPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateComplaintPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateComplaintPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
