import { Component, OnInit } from '@angular/core';
import { Society } from 'src/app/core/Society';
import { Router, ActivatedRoute } from '@angular/router';
import { Complaint } from 'src/app/core/Complaint';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/misc/loader.service';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { LoginService } from 'src/app/login/login.service';
import { UpdatePageTrack } from 'src/app/home/arch/UpdatePageTrack';
import { UpdatePageInterface } from 'src/app/home/arch/UpdatePageInterface';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';
import { AdService } from 'src/app/home/ad/ad.service';

@Component({
  selector: 'app-update-complaint',
  templateUrl: './update-complaint.page.html',
  styleUrls: ['./update-complaint.page.scss'],
})
@UpdatePageTrack(
  '/resident/resident-complaints/add-complaint',
  'Complaint',
  '/GSServer/webapi/secured/Resident/Complaint',
  false
)
export class UpdateComplaintPage implements UpdatePageInterface {

  minDate =new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  form: FormGroup;
  cacheObject: any;
  cache: Cache;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private formBuilder:FormBuilder,
    private loader:LoaderService,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService:AdService,
    ) 
  {
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      priority: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.form.get('title').setValue(this.cacheObject.title);
    this.form.get('description').setValue(this.cacheObject.description);
    this.form.get('priority').setValue(this.cacheObject.priority);
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  updateComplaint() {
    this.cacheObject.title = this.form.get('title').value;
    this.cacheObject.description = this.form.get('description').value;
    this.cacheObject.priority = this.form.get('priority').value;
    this.loader.showLoader();
    this.cache.updateObject(this.cacheObject).then(
      object=>{
        this.router.navigate(['/resident/resident-complaints']).finally(
          () => {
            this.loader.hideLoader();
          }
        )
      },
      error=>{
        alert("Error occurred :"+error)
        this.loader.hideLoader()
      }
    );
  }

  removeComplaint() {
    this.loader.showLoader();
    this.cache.deleteObject(this.cacheObject).then(
      object=>{
        this.router.navigate(['/resident/resident-complaints'])
        this.loader.hideLoader()
      },
      error=>{
        alert("Error occurred :"+error)
        this.loader.hideLoader()
      }
    );
  }

}
