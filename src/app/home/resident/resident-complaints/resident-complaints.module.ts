import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ResidentComplaintsPage } from './resident-complaints.page';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: ResidentComplaintsPage
  },
  {
    path: 'add-complaint', loadChildren: './add-complaint/add-complaint.module#AddComplaintPageModule'
  },
  {
    path: 'update-complaint/:id', loadChildren: './update-complaint/update-complaint.module#UpdateComplaintPageModule'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ResidentComplaintsPage]
})
export class ResidentComplaintsPageModule {}
