import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonInfiniteScroll } from '@ionic/angular';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { AdService } from '../../ad/ad.service';
import { Subscription } from 'rxjs';
import { Cache } from '../../services/cache';
import { LoginService } from 'src/app/login/login.service';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { CacheService } from '../../services/cache.service';

@Component({
  selector: 'app-resident-complaints',
  templateUrl: './resident-complaints.page.html',
  styleUrls: ['./resident-complaints.page.scss'],
})
@ListPageTrack(
  '/resident/home/resident-complaints',
  'Complaint',
  '/GSServer/webapi/secured/Resident/Complaint',
  false
)
export class ResidentComplaintsPage implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private cacheService: CacheService,
    private readonly loginService: LoginService,
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService: AdService
    ) 
  {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/resident/add-society']);
    } 
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  // ---------------------------------------------------------------------------------------

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  addComplaint() {
    this.router.navigate(['/resident/resident-complaints/add-complaint'])
  }

}
