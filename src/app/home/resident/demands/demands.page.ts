import { Component, OnInit, ViewChild, OnDestroy, NgZone } from '@angular/core';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ResidentService } from '../../services/resident.service';
import { HomeService } from '../../home.service';
import { Resident } from 'src/app/core/Resident';
import { Society } from 'src/app/core/Society';
import { FcmService } from 'src/app/notifications/fcm.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { AdService } from '../../ad/ad.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Subscription } from 'rxjs';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Cache } from '../../services/cache';
import { BusinessPageInterface } from '../../arch/BusinessPageInterface';
import { BusinessPageTrack } from '../../arch/BusinessPageTrack';
import { LoginService } from 'src/app/login/login.service';
import { DemandLog } from 'src/app/core/DemandLog';
import { ToasterService } from 'src/app/misc/toaster.service';
import { Plugins } from '@capacitor/core';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { CacheService } from '../../services/cache.service';

const { UPI } = Plugins;
@Component({
  selector: 'app-demands',
  templateUrl: './demands.page.html',
  styleUrls: ['./demands.page.scss'],
})
@ListPageTrack(
  '/resident/home/demands',
  'DemandLog',
  '/GSServer/webapi/secured/Resident/DemandLog',
  false
)
export class DemandsPage implements ListPageInterface {

  cache: Cache;
  user: Resident;
  society: any;
  demandLogs;
  accounts;
  message;
  onlinseOfflineSubscription: Subscription;
  dataServiceSubscription: Subscription;
  dataServiceSubscription1: Subscription;
  navigationSubscription: Subscription;
  filter: Filter;
  demandLogCache: Cache = null;
  accountCache: Cache = null;
  pageUrl: string = '/resident/home/demands';

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private cacheService: CacheService,
    public readonly loginService: LoginService,
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService: AdService,
    private platform: Platform
    ) 
  {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/resident/add-society']);
    }

  }
  
  ngOnInit() {
    this.cacheService.initializeCache('Account', '/GSServer/webapi/secured/Resident/Account', true).then(cacheObject => {
      this.accountCache = cacheObject
      this.accountCache.load(CacheService.DEFAULT_LOAD_COUNT);
    });
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.adService.reSubscribeAds("TAB");
    this.workspaceToolbarService.hideBackButton();
  }

  // ---------------------------------------------------------------------------------------

  pay(demandLog: DemandLog) {
    if (this.platform.is('ios') || this.platform.is('android')) {
      UPI.addListener("onTransactionSuccess", (info: any) => {
        alert("TransactionSuccess : " + info);
        demandLog.paymentStatus = "Paid"
        this.cache.updateObject(demandLog)
      });

      UPI.addListener("onTransactionCancelled", (info: any) => {
        alert("TransactionCancelled : " + info);
      });

      UPI.addListener("onTransactionFailed", (info: any) => {
        alert("TransactionFailed : " + info);
      });

      UPI.pay(
        {
          amount: demandLog.demand.amount,
          upiId: this.accountCache.objects[0].upiId,
          name: demandLog.demand.name,
          note: demandLog.demand.description,
        }
      );
    } else {
      alert("Install and use Dwarpal application for the transactions !!!")
      if (environment.debug) {
        demandLog.paymentStatus = "Paid"
        this.cache.updateObject(demandLog)
      }
    }
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  isExpired(date: Date) {
    return this.getLocalTime(date) < new Date();
  }

  isPending(approvalStatus: String) {
    if (approvalStatus === "Pending")
      return true;
    else
      return false;
  }
}

