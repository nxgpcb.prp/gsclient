import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Router} from '@angular/router';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { AdService } from '../../ad/ad.service';
import { Cache } from '../../services/cache';
import { LoginService } from 'src/app/login/login.service';
import { CacheService } from '../../services/cache.service';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-resident-delivery-logs',
  templateUrl: './resident-delivery-logs.page.html',
  styleUrls: ['./resident-delivery-logs.page.scss'],
})
@ListPageTrack(
  '/resident/home/resident-delivery-logs',
  'DeliveryLog',
  '/GSServer/webapi/secured/Resident/DeliveryLog',
  false
)
export class ResidentDeliveryLogsPage implements ListPageInterface {
  
  cache: Cache;
  
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private cacheService: CacheService,
    private readonly loginService: LoginService,
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService: AdService
    ) 
  {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/resident/add-society']);
    }
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("TAB");
  }

  // ---------------------------------------------------------------------------------------

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }
}

