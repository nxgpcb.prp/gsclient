import { Component, OnInit, ViewChild } from '@angular/core';
import { Resident } from 'src/app/core/Resident';
import { Facility } from 'src/app/core/Facility';
import { IonicSelectableComponent } from 'ionic-selectable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebcamImage } from 'ngx-webcam';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from 'src/app/misc/toaster.service';
import { LoaderService } from 'src/app/misc/loader.service';
import { AdService } from '../../ad/ad.service';
import { Visitor } from 'src/app/core/Visitor';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { ResidentService } from '../../services/resident.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { LoginService } from 'src/app/login/login.service';
import { AddPageTrack } from '../../arch/AddPageTrack';
import { AddPageInterface } from '../../arch/AddPageInterface';
import { Cache } from 'src/app/home/services/cache';
import { CacheService } from 'src/app/home/services/cache.service';

@Component({
  selector: 'app-visitor-booking',
  templateUrl: './visitor-booking.page.html',
  styleUrls: ['./visitor-booking.page.scss'],
})
@AddPageTrack(
  'resident/visitor-booking',
  'VisitorLog',
  '/GSServer/webapi/secured/Resident/VisitorLog',
  false
)
export class VisitorBookingPage implements AddPageInterface {
  minDate = new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();

  toggle = false;
  form: FormGroup;
  public webcamImage: WebcamImage = null;
  visitorId: any;
  visitor = new Visitor();
  cacheObject: any;
  cache: Cache;

  constructor(
    private residentService: ResidentService, 
    private router: Router,
    private loginService: LoginService, 
    public toaster: ToasterService, 
    private formBuilder: FormBuilder,
    private loader: LoaderService, 
    private adService: AdService, 
    private workspaceToolbarService: WorkspaceToolbarService,
    private cacheService:CacheService, 
    ) {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      address: ['', [Validators.required]],
      noOfAccompanyingVisitors: ['', [Validators.required, Validators.pattern('\\d{1}')]],
      vehicleNo: ['', [Validators.pattern('[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}')]],
      timeOfArrival: ['', [Validators.required]],
    });
  }

  async ngOnInit() {
    this.visitor = new Visitor();
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  addVisitorLog() {
    this.visitor.name = this.form.get('name').value;
    this.visitor.phone = this.form.get('phone').value;
    this.visitor.address = this.form.get('address').value;
    let visitorLog = new VisitorLog();
    visitorLog.visitor = this.visitor;
    visitorLog.noOfAccompanyingVisitors = this.form.get('noOfAccompanyingVisitors').value;
    visitorLog.vehicleNo = this.form.get('vehicleNo').value;
    visitorLog.timeIn = new Date(this.form.get('timeOfArrival').value);
    visitorLog.approvalStatus = "Pending";
    visitorLog.isBookedInAdvance = true;
    this.loader.showLoader();
    this.cache.addObject(visitorLog)
      .then(
        (response) => {
          console.log(response)
          this.form.reset();
          this.router.navigate(['/resident/visitor-booking'])
            .finally(
              () => {
                this.loader.hideLoader();
                this.toaster.toast("Visitor Booked Successfully. Check Logs tab in Home Menu.")
              }
            )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader()
        }
      );

  }

  onOpen() {
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("TAB");
  }

  onClose() {
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("NORMAL");
  }

  handleImage(webcamImage: WebcamImage) {
    this.webcamImage = webcamImage;
  }

}