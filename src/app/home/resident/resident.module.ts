import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ResidentPage } from './resident.page';
import { PopoverMenuComponent } from '../popover-menu/popover-menu.component';
import { WorkspaceToolbarModule } from '../workspace-toolbar/workspace-toolbar.module';
import { VisitorLogComponent } from './visitor-log/visitor-log.component';
import { IonicSelectableModule } from 'ionic-selectable';

const routes: Routes = [
  {
    path: '',
    component: ResidentPage,
    children: [
      { path: 'home', loadChildren: './resident-home/resident-home.module#ResidentHomePageModule'},
      { path: 'facility-booking', loadChildren: './facility-booking/facility-booking.module#FacilityBookingPageModule' },
      { path: 'visitor-booking', loadChildren: './visitor-booking/visitor-booking.module#VisitorBookingPageModule' },
      { path: 'parking-status', loadChildren: './parking-status/parking-status.module#ParkingStatusPageModule' },
      { path: 'subscription', loadChildren: './resident-subscription/resident-subscription.module#ResidentSubscriptionPageModule' },
      { path: 'my-society', loadChildren: './my-society/my-society.module#MySocietyPageModule' },   
      { path: 'resident-complaints', loadChildren: './resident-complaints/resident-complaints.module#ResidentComplaintsPageModule' },
    ]
  },
  { path: 'add-society', loadChildren:  './add-society/add-society.module#AddSocietyPageModule'},
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    WorkspaceToolbarModule,
    IonicSelectableModule
  ],
  declarations: [ResidentPage],
  entryComponents: [
    PopoverMenuComponent,
  ],
})
export class ResidentPageModule {}
