import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { ResidentService } from '../services/resident.service';
import { FcmService } from 'src/app/notifications/fcm.service';
import { WorkspaceToolbarService } from '../workspace-toolbar/workspace-toolbar.service';
import { Resident } from 'src/app/core/Resident';
import { HomeService } from '../home.service';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-resident',
  templateUrl: './resident.page.html',
  styleUrls: ['./resident.page.scss'],
})
export class ResidentPage implements OnInit, AfterViewInit {

  isFistTimeLoggedIn:boolean = false;
  resident:Resident = new Resident();
  pages = 
  [
    {
      title : 'Home',
      url : '/resident/home/logs',
      showBadge : false,
      //Trick to enable menu highlighting for child paths
      parentUrl: '/resident/home'
    },
    {
      title : 'Visitor Booking',
      url : '/resident/visitor-booking',
      //#Launch
      showBadge : false,
    },
    {
      title : 'Facility Booking',
      url : '/resident/facility-booking',
      showBadge : false,
    },
    {
      title : 'Complaint Box',
      url : '/resident/resident-complaints',
      //#Launch
      showBadge : false,
    },
    {
      title : 'Parking Status',
      url : '/resident/parking-status',
      //#Launch
      showBadge : false,
    },
    {
      title : 'My Society',
      url : '/resident/my-society',
      //#Launch
      showBadge : false,
    },
    {
      title : 'Subscription',
      url : '/resident/subscription',
      showBadge : false,
    },
  ]
  selectedPath = '/resident/home/logs';
  
  constructor(private router:Router, private loginService: LoginService, private fcm: FcmService,
    private workspaceToolbarService:WorkspaceToolbarService, public residentService:ResidentService) {
      this.workspaceToolbarService.hideBackButton();
      this.router.events.subscribe((event:RouterEvent) => {
        if(event.url != undefined){
          this.selectedPath = event.url;
        }
      });
   }

  async ngOnInit() {
    
    this.resident = await this.loginService.getUser();
    
    if(this.resident.name == null){
      this.isFistTimeLoggedIn = true;
    }
  }

  ngAfterViewInit(): void {

  }

  ngOnInAppUpdate(): void {

  }

  async refresh(): Promise<void> {
    
    this.resident = await this.loginService.getUser()
  }

}
