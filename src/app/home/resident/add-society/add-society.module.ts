import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { AddSocietyPage } from './add-society.page';
import { WorkspaceToolbarModule } from '../../workspace-toolbar/workspace-toolbar.module';
import { PopoverMenuComponent } from '../../popover-menu/popover-menu.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: AddSocietyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    TranslateModule,
    IonicSelectableModule,
  ],
  declarations: [AddSocietyPage],
})
export class AddSocietyPageModule {}
