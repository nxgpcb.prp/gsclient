import { Component, OnInit, ViewChild, OnDestroy, NgZone } from '@angular/core';
import { Society } from 'src/app/core/Society';
import { ResidentService } from '../../services/resident.service';
import { ToasterService } from 'src/app/misc/toaster.service';
import { IonicSelectableComponent } from 'ionic-selectable';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Resident } from 'src/app/core/Resident';
import { HomeService } from '../../home.service';
import { AdService } from '../../ad/ad.service';
import { Router, NavigationEnd } from '@angular/router';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Cache } from '../../services/cache';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Filter } from 'src/app/filters/Filter';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-add-society',
  templateUrl: './add-society.page.html',
  styleUrls: ['./add-society.page.scss'],
})
export class AddSocietyPage implements OnInit, OnInAppUpdateInterface, OnDestroy {

  searchedSocieties: Society[] = [];
  searchTerm: String;
  selectedSociety: Society = null;
  facilities: any[] = [];
  facility: any;
  toggle = false;
  residentType: string;
  form: FormGroup;
  user: Resident;
  society: Society;
  onlinseOfflineSubscription: Subscription;
  dataServiceSubscription: Subscription;
  navigationSubscription: Subscription;
  searchSocietiesSubscription: Subscription;
  societyCache: Cache = null;
  joiningRequestCache: Cache = null;
  filter: Filter;

  @ViewChild('selectComponent') selectComponent: IonicSelectableComponent;

  constructor(
    public residentService: ResidentService, 
    private toaster: ToasterService, 
    private fcm: FcmService,
    private formBuilder: FormBuilder, 
    private loginService: LoginService, 
    private adService: AdService, 
    private router: Router,
    private ngZone: NgZone, 
    private http: HttpClient, 
    private readonly onlineOfflineService: OnlineOfflineService
    ) {
    console.log("AddSocietyPage")
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      selectedSociety: ['', [Validators.required]],
      facility: ['', [Validators.required]],
      residentType: ['', [Validators.required]],
    })
    this.init();
  }

  initDataService() {
    this.societyCache = new Cache(this.http,
      'societies',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/Society',
      Cache.type_REFERED);

    this.joiningRequestCache = new Cache(this.http,
      'joiningRequests',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/JoiningRequest',
      Cache.type_NATIVE);
    this.registerToEvents();
  }

  ngOnDestroy() {
    this.unRegisterToEvents();
  }

  init() {
    this.initDataService();
    this.residentService.setProgressing(true);
    this.navigate();
    this.ngOnInAppUpdate();
  }

  refresh(event) {
    this.residentService.setProgressing(true);
    setTimeout(() => {
      this.ngZone.run(async () => {
        this.user = await this.loginService.getUser()
        this.getSociety();
        if (event != null) {
          event.target.complete();
        }
      })
    }, 2000);
  }

  ngOnInAppUpdate(): void {
    this.fcm.currentMessage.subscribe((data) => {
      if (this.router.url == '/resident/add-society' && data != null) {
        this.refresh(null);
      }
    });
  }

  navigate() {
    //Update list when you navigated on list page
    this.navigationSubscription = this.router.events.subscribe((value) => {
      if (value instanceof NavigationEnd && this.router.url == '/resident/add-society') {
        this.refresh(null);
        //this.adService.unSubscribeAds();
        this.adService.reSubscribeAds("NORMAL");
      }
    });
  }

  getSociety() {
    let filter = new Filter;
    this.filter = filter;
    this.societyCache.updateMe(this.filter);
  }

  getSocieties(evt) {
    this.searchTerm = evt.text;
    this.searchedSocieties = [];

    if (!this.searchTerm) {
      return;
    }

    if (this.searchTerm != '') {
      this.residentService.searchSocieties(this.searchTerm)
        .subscribe(
          (response) => {
            this.searchedSocieties = response;
          },
          (error) => {
            console.log(error)
          }
        );
    }
  }

  setSelectedSociety(id) {
    if (this.searchedSocieties != null) {
      this.selectedSociety = this.searchedSocieties[id];
    }
  }

  sendRequest() {
    this.residentType = this.form.get('residentType').value;
    let joiningRequest: any = {};
    joiningRequest.resident = this.user;
    joiningRequest.society = this.selectedSociety;
    joiningRequest.residentType = this.residentType;
    joiningRequest.requestedFacility = this.form.get('facility').value;

    console.log(joiningRequest);
    this.joiningRequestCache.add(joiningRequest)
      .then(
        (response) => {
          if (response) {
            this.toaster.toast("Request sent successfully.")
            this.form.reset();
          } else {
            this.toaster.toast("Your earlier request has not accepted yet. We have notified approvers.")
            this.form.reset();
          }
        },
        (error) => {
          console.log(error)
        }
      )
  }

  onSearch(event: { component: IonicSelectableComponent, text: any }) {
    let text = event.text.trim();
    event.component.startSearch();

    // Close any running subscription.
    if (this.searchSocietiesSubscription) {
      this.searchSocietiesSubscription.unsubscribe();
    }

    if (!text) {
      // Close any running subscription.
      if (this.searchSocietiesSubscription) {
        this.searchSocietiesSubscription.unsubscribe();
      }

      event.component.items = [];
      event.component.endSearch();
      return;
    }

    this.searchSocietiesSubscription = this.residentService.searchSocieties(event.text)
      .subscribe(
        (response) => {
          if (this.searchSocietiesSubscription.closed) {
            return;
          }
          event.component.items = response;
          event.component.endSearch();
        },
        (error) => {
          console.log(error)
          event.component.endSearch();
        }
      );

  }

  onSearchFail(event: { component: IonicSelectableComponent, text: any }) {
    if (event.text != '') {
      this.selectComponent.searchFailText = "Result not found. Society with this name does not exist."
    } else {
      this.selectComponent.searchFailText = "Start typing society name."
    }
  }


  userChanged(event: { component: IonicSelectableComponent, value: any }) {
    console.log('Selected: ', event.value);
    this.selectedSociety = event.value;
    console.log('Selected: ', this.selectedSociety);
    //Needs improvement
    this.residentService.getSocietyFacility(this.selectedSociety.id)
      .subscribe(
        response => {
          console.log(response)
          this.facilities = [];
          response.forEach(element => {
            if (element.type === "Apartment") {
              this.facilities.push(element);
            }
          });
        },
        error => {
          console.log(error);
        }
      )
  }

  onOpen() {
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("TAB");
  }

  onClose() {
    //this.adService.unSubscribeAds();
    this.adService.reSubscribeAds("NORMAL");
  }

  openFromCode() {
    this.selectComponent.open();
  }

  clear() {
    this.selectComponent.clear();
    this.selectComponent.close();
  }

  toggleItems() {
    this.selectComponent.toggleItems(this.toggle);
    this.toggle = !this.toggle;
  }

  confirm() {
    this.selectComponent.confirm();
    this.selectComponent.close();
  }

  private registerToEvents() {
    this.onlinseOfflineSubscription = this.onlineOfflineService.connectionChanged.subscribe(
      online => {
        if (online) {
          console.log('went online');
          console.log('sending all stored items');
          this.joiningRequestCache.syncPendingRequests().then(
            response => {
              console.log("sync complete");
            }
          )
        } else {
          console.log('went offline, storing in indexdb');
        }
      });

    this.dataServiceSubscription = this.societyCache.dataService.subscribe(
      societies => {
        console.log(societies);
        this.society = societies[0];
        if (this.society != null) {
          alert("You have already joined " + this.society.name + " society.")
          this.router.navigate(['/']);
        } else {
          alert("Your request is not approved yet or you have not sent any request.")
        }
        this.residentService.setProgressing(false);
      })
  }

  private unRegisterToEvents() {
    this.onlinseOfflineSubscription.unsubscribe();
    this.dataServiceSubscription.unsubscribe();
    this.navigationSubscription.unsubscribe();
  }
  async logOut() {
    this.loginService.logOut();
  }

}
