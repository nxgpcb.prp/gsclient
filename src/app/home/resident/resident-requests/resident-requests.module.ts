import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResidentRequestsPage } from './resident-requests.page';
import { AddSocietyPageModule } from '../add-society/add-society.module';
import { AddSocietyPage } from '../add-society/add-society.page';

const routes: Routes = [
  {
    path: '',
    component: ResidentRequestsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ResidentRequestsPage],
})
export class ResidentRequestsPageModule {}
