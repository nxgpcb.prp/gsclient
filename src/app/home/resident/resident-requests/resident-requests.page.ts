import { Component, OnInit, ViewChild, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { HomeService } from '../../home.service';
import { Router, NavigationEnd } from '@angular/router';
import { ResidentService } from '../../services/resident.service';
import { Resident } from 'src/app/core/Resident';
import { Society } from 'src/app/core/Society';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { FcmService } from 'src/app/notifications/fcm.service';
import { AdService } from '../../ad/ad.service';
import { VisitorLog } from 'src/app/core/VisitorLog';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Subscription } from 'rxjs';
import { Filter } from 'src/app/filters/Filter';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Search } from 'src/app/filters/Search';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { LoginService } from 'src/app/login/login.service';


@Component({
  selector: 'app-resident-requests',
  templateUrl: './resident-requests.page.html',
  styleUrls: ['./resident-requests.page.scss'],
})
export class ResidentRequestsPage implements OnInit, OnInAppUpdateInterface, OnDestroy {

  user: Resident;
  society: Society;
  visitorLogs: VisitorLog[];
  listStart = 0;
  listSize = 10;
  onlinseOfflineSubscription: Subscription;
  dataServiceSubscription: Subscription;
  navigationSubscription: Subscription;
  filter: Filter;
  visitorLogCache:Cache = null;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(private platform: Platform, private router: Router,
    private loginService: LoginService, public residentService: ResidentService,
    private workspaceToolbarService: WorkspaceToolbarService, private ngZone: NgZone, private fcm: FcmService,
    private adService:AdService, private readonly onlineOfflineService: OnlineOfflineService, private http:HttpClient,) {
    this.workspaceToolbarService.hideBackButton();
  }

  ngOnInit() {
    this.init();
  }

  initDataService(){
    this.visitorLogCache = new Cache(this.http,
      'visitorLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/VisitorLog',
      Cache.type_NATIVE);
      this.registerToEvents();
  }

  ngOnDestroy() {
    this.unRegisterToEvents();
  }

  init() {
    this.initDataService();
    this.residentService.setProgressing(true);
    this.navigate();
    this.ngOnInAppUpdate();
  }

  navigate() {
    //Update list when you navigated on list page
    this.navigationSubscription = this.router.events.subscribe((value) => {
      if (value instanceof NavigationEnd && this.router.url == '/resident/home/resident-requests') {
        this.visitorLogs = [];
        this.refresh(null);
        //this.adService.unSubscribeAds();
       this.adService.reSubscribeAds("TAB");
      }
    });
  }

  refresh(event) {
    this.residentService.setProgressing(true);
    setTimeout(() => {
      this.ngZone.run(async () => {
      this.user = await this.loginService.getUser()
      this.getSociety();
      if (event != null) {
        event.target.complete();
      }
    })
    }, 2000);
  }

  ngOnInAppUpdate(): void {
    this.fcm.currentMessage.subscribe((data) => {
      if (this.router.url == '/resident/home/resident-requests'&& data != null) {
        this.visitorLogs = [];
        this.refresh(null);
      }
    });
  }

  getSociety() {
    this.residentService.getSociety()
      .then(response => {
        console.log(response)
        this.society = response[0];
        if (this.society == null) {
          this.residentService.addSociety();
        } else {
          //This is extra call need to modify getSociety method
          this.getPendingVisitorLogs();
        }
      },
        error => {
          console.log(error)
        })
  }

  getPendingVisitorLogs() {
    let paging = new Paging;
    paging.start = 0;
    paging.size = 10;

    let sort = new Sort
    sort.method = "DESCENDING"
    sort.parameter = "createdOn"

    let search = new Search
    search.method = "Equal"
    search.parameter = "approvalStatus"
    search.query = "Pending"

    let filter = new Filter;
    filter.paging = paging;
    filter.sort = sort;
    filter.search = search;

    this.filter = filter;

    this.visitorLogCache.updateMe(this.filter);
  }

  loadData(event) {
    setTimeout(() => {
      if (this.visitorLogs.length < this.filter.paging.size) {
        this.residentService.setProgressing(false);
        event.target.complete();
      } else {
        this.filter.paging.start = this.filter.paging.start + 10;
        this.visitorLogCache.updateMe(this.filter);
        event.target.complete();
      }

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.visitorLogs.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  addLog(selectedVisitorLog) {
    this.router.navigate(['/resident/visitor-log', this.visitorLogs[selectedVisitorLog].id]);
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  private registerToEvents() {
    this.onlinseOfflineSubscription = this.onlineOfflineService.connectionChanged.subscribe(
      online => {
        if (online) {
          console.log('went online');
          console.log('sending all stored items');
          this.visitorLogs = [];
          this.visitorLogCache.syncPendingRequests().then(
            response => {
              console.log("sync complete");
            }
          )
        } else {
          console.log('went offline, storing in indexdb');
        }
    });

    this.dataServiceSubscription = this.visitorLogCache.dataService.subscribe(
      visitorLogs => {
        console.log(visitorLogs);
        this.visitorLogs = this.visitorLogs.concat(visitorLogs);
        this.residentService.setProgressing(false);
    })
  }

  private unRegisterToEvents() {
    this.onlinseOfflineSubscription.unsubscribe();
    this.dataServiceSubscription.unsubscribe();

    this.navigationSubscription.unsubscribe();
  }
}
