import { Component, OnInit, ViewChild, NgZone, OnDestroy, ElementRef } from '@angular/core';
import { ClubHouse } from 'src/app/core/ClubHouse';
import { Router, NavigationEnd } from '@angular/router';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Society } from 'src/app/core/Society';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { Subscription } from 'rxjs';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { ResidentService } from '../../services/resident.service';
import { BookingLog } from 'src/app/core/BookingLog';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Facility } from 'src/app/core/Facility';
import { Search } from 'src/app/filters/Search';
import { LoaderService } from 'src/app/misc/loader.service';
import { isDefined } from '@angular/compiler/src/util';
import { AdService } from '../../ad/ad.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { ToasterService } from 'src/app/misc/toaster.service';

@Component({
  selector: 'app-facility-booking',
  templateUrl: './facility-booking.page.html',
  styleUrls: ['./facility-booking.page.scss'],
})
export class FacilityBookingPage implements OnInit, OnDestroy {

  bookingLogs: BookingLog[];
  facilities: Facility[];
  user: Society;
  filter: Filter;
  onlinseOfflineSubscription: Subscription;
  dataServiceSubscription: Subscription;
  dataServiceSubscription1: Subscription;
  navigationSubscription: Subscription;
  form: FormGroup;
  minDate = new Date().toISOString();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  calendar = {
    mode: 'day',
    currentDate: new Date(),
  }
  calendarTitle: string;
  selectedFacility;
  eventSource = [];
  startTime;
  endTime;
  facilityCache: Cache = null;
  bookingLogCache: Cache = null;

  @ViewChild(CalendarComponent) bookingCalendar: CalendarComponent;

  constructor(private http: HttpClient, private platform: Platform, public residentService: ResidentService, private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, private ngZone: NgZone, private fcm: FcmService,
    private readonly onlineOfflineService: OnlineOfflineService, private formBuilder: FormBuilder, private loader: LoaderService,
    private adService: AdService, private toaster: ToasterService) {
    this.workspaceToolbarService.hideBackButton();
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      purpose: ['', [Validators.required]],
      description: ['', [Validators.required]],
      bookingFacility: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
    });
    this.init();
  }

  initDataService() {
    this.facilityCache = new Cache(this.http,
      'facilities',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/Facility',
      Cache.type_NATIVE);
    this.bookingLogCache = new Cache(this.http,
      'bookingLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/BookingLog',
      Cache.type_NATIVE);
    this.registerToEvents();
  }

  ngOnDestroy() {
    this.unRegisterToEvents();
  }

  init() {
    this.initDataService();
    this.residentService.setProgressing(true);
    this.navigate();
    this.ngOnInAppUpdate();
  }

  navigate() {
    // Update list when you navigated on list page
    this.navigationSubscription = this.router.events.subscribe((value) => {
      if (value instanceof NavigationEnd && this.router.url == '/resident/facility-booking') {
        this.bookingLogs = [];
        this.facilities = [];
        this.eventSource = [];
        this.refresh(null);
        this.adService.reSubscribeAds("NORMAL");
      }
    });
  }

  refresh(event) {
    this.residentService.setProgressing(true);
    setTimeout(() => {
      this.ngZone.run(() => {
        if (event != null) {
          this.bookingLogs = [];
          this.facilities = [];
          this.eventSource = [];
        }
        this.getBookingLogs();
        this.getBookableFacilities();
        if (event != null) {
          event.target.complete();
        }
      })
    }, 2000);
  }

  getBookingLogs() {
    let paging = new Paging;
    paging.start = 0;
    paging.size = 10;

    let sort = new Sort
    sort.method = "DESCENDING"
    sort.parameter = "createdOn"

    let filter = new Filter;
    filter.paging = paging;
    filter.sort = sort;

    this.filter = filter;

    this.bookingLogCache.updateMe(this.filter);
  }

  getBookableFacilities() {
    let search = new Search
    search.method = "Equal"
    search.parameter = "isBookable"
    search.query = true

    let sort = new Sort
    sort.method = "ASCENDING"
    sort.parameter = "name"

    let filter = new Filter;
    filter.sort = sort;
    //filter.search = search;

    this.filter = filter;

    this.facilityCache.updateMe(this.filter);
  }

  bookFacility() {
    let bookingLog: BookingLog = new BookingLog();
    bookingLog.purpose = this.form.get('purpose').value;
    bookingLog.description = this.form.get('description').value;
    bookingLog.bookingFacility = this.form.get('bookingFacility').value;
    bookingLog.startDate = new Date(this.form.get('startDate').value);
    bookingLog.endDate = new Date(this.form.get('endDate').value);
    bookingLog.approvalStatus = "Pending";

    console.log(bookingLog)
    this.loader.showLoader()
    this.residentService.addBookingLog(bookingLog)
      .then(
        (response) => {
          console.log(response)
          this.form.reset();
          this.router.navigate(['/resident/facility-booking'])
            .finally(
              () => {
                this.loader.hideLoader();
                this.toaster.toast("Booking Request Sent. Please wait for approval.")
              }
            )
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader();
        }
      );
  }

  loadEvents(event) {
    this.eventSource = [];
    this.bookingLogs.forEach(element => {
      let eventCopy = {
        title: element.purpose,
        startTime: new Date(element.startDate.toString().replace("[UTC]", "")),
        endTime: new Date(element.endDate.toString().replace("[UTC]", "")),
        description: element.description,
        approvalStatus: element.approvalStatus,
        bookingFacility: element.bookingFacility,
        allDay: false
      }
      if (event != null) {
        if (this.form.get('bookingFacility').value.type === element.bookingFacility.type) {
          this.eventSource.push(eventCopy);
        }
      }
    });

    if (this.bookingCalendar != null) {
      this.bookingCalendar.loadEvents();
    }
  }

  changeFacility(event) {
    console.log(event)
    this.selectedFacility = event.detail.value;
    this.loadEvents(event);
  }

  changeStartDate(event) {
    this.form.get('endDate').setValue(this.convertAndAdd(event.detail.value, 1));
    this.form.get('endDate').markAsTouched();
  }

  ngOnInAppUpdate(): void {
    this.fcm.currentMessage.subscribe((data) => {
      if (this.router.url == '/resident/facility-booking' && data != null) {
        this.bookingLogs = [];
        this.facilities = [];
        this.eventSource = [];
        this.refresh(null);
      }
    });
  }

  onCurrentDateChanged(event) {

  }

  onEventSelected(event) {

  }

  onViewTitleChanged(event) {
    console.log(event);
    this.calendarTitle = event;
  }

  onTimeSelected(event) {
    console.log('Selected time: ' + event.selectedTime + ', hasEvents: ' + (event.events !== undefined && event.events.length !== 0));
    if (event.events !== undefined && event.events.length !== 0) {
      //alert("Your booking time is confliting with another booking. Try choosing diferent time slot");
    } else {
      this.form.get('startDate').setValue(this.convertAndAdd(event.selectedTime));
      this.form.get('endDate').setValue(this.convertAndAdd(event.selectedTime, 1));
      this.form.get('endDate').markAsTouched();
    }
  }

  convertAndAdd(str, hour?, minute?) {
    let date = new Date(str);
    let month = ("0" + (date.getMonth() + 1)).slice(-2);
    let day = ("0" + date.getDate()).slice(-2);
    let hours = ("0" + date.getHours()).slice(-2);
    let minutes = ("0" + date.getMinutes()).slice(-2);
    let seconds = ("0" + date.getSeconds()).slice(-2);

    if (hour != null) {
      hours = parseInt(hours) + hour;
    }

    if (minute != null) {
      minutes = parseInt(minutes) + minute;
    }

    var mySQLDate = [date.getFullYear(), month, day].join("-");
    var mySQLTime = [hours, minutes, seconds].join(":");
    return [mySQLDate, mySQLTime].join(" ");
  }

  reloadSource(startTime, endTime) {

  }

  loadData(event) {
    setTimeout(() => {
      if (this.bookingLogs.length < this.filter.paging.size) {
        this.residentService.setProgressing(false);
        event.target.complete();
      } else {
        this.filter.paging.start = this.filter.paging.start + 10;
        this.bookingLogCache.updateMe(this.filter);
        event.target.complete();
      }

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.bookingLogs.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  private registerToEvents() {
    this.onlinseOfflineSubscription = this.onlineOfflineService.connectionChanged.subscribe(
      online => {
        if (online) {
          console.log('went online');
          console.log('sending all stored items');
          this.bookingLogs = [];
          this.bookingLogCache.syncPendingRequests().then(
            response => {
              console.log("sync complete");
            }
          )
        } else {
          console.log('went offline, storing in indexdb');
        }
      });

    this.dataServiceSubscription = this.bookingLogCache.dataService.subscribe(
      bookingLogs => {
        console.log(bookingLogs);
        this.bookingLogs = this.bookingLogs.concat(bookingLogs);
        this.residentService.setProgressing(false);
      })

    this.dataServiceSubscription1 = this.facilityCache.dataService.subscribe(
      facilities => {
        console.log(facilities);
        facilities.slice().reverse().forEach(function (item, index, object) {
          if (item.isBookable === false) {
            facilities.splice(object.length - 1 - index, 1);
          }
        });
        console.log(facilities);
        this.facilities = this.facilities.concat(facilities);
        this.residentService.setProgressing(false);
      })
  }

  private unRegisterToEvents() {
    this.navigationSubscription.unsubscribe();
    this.onlinseOfflineSubscription.unsubscribe();
    this.dataServiceSubscription.unsubscribe();

    this.dataServiceSubscription1.unsubscribe();
  }

}
