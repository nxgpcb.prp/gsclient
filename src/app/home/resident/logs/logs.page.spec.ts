import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { IonicModule } from '@ionic/angular';

import { LogsPage } from './logs.page';
import { ResidentService } from '../../services/resident.service';

describe('LogsPage', () => {
  let component: LogsPage;
  let fixture: ComponentFixture<LogsPage>;
  let de: DebugElement;
  let residentService: ResidentService;
  let spy: jasmine.Spy;

  beforeEach(async(() => {      
    TestBed.configureTestingModule({
      declarations: [ LogsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase), 
        AngularFirestoreModule,
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        HttpClientModule,
        IonicModule
      ],
      providers:[
        Firebase,
        AngularFireMessaging,
        ResidentService
      ]
    })
    .compileComponents();
  }));

  beforeEach(async() => {
    fixture = TestBed.createComponent(LogsPage);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    residentService = de.injector.get(ResidentService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
