import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { Cache } from '../../services/cache';
import { LoginService } from 'src/app/login/login.service';
import { CacheService } from '../../services/cache.service';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { AdService } from '../../ad/ad.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router } from '@angular/router';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.page.html',
  styleUrls: ['./logs.page.scss'],
})
@ListPageTrack(
  '/resident/home/logs',
  'VisitorLog',
  '/GSServer/webapi/secured/Resident/VisitorLog',
  false
)
export class LogsPage implements ListPageInterface {

  cache: Cache
  securityCache: Cache = null;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  constructor(
    private cacheService: CacheService,
    public readonly loginService: LoginService,
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService,
    private adService: AdService,
    private callNumber: CallNumber
  ) {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/resident/add-society']);
    }
  }
  ngOnInit() {
    this.cacheService.initializeCache('Security', '/GSServer/webapi/secured/Resident/Security', true).then(cacheObject => {
      this.securityCache = cacheObject
      this.securityCache.load(CacheService.DEFAULT_LOAD_COUNT);
    });
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/resident/add-society']);
    }
  }

  ngOnDestroy() {
  }

  onRefresh(event) {
    this.securityCache.load(CacheService.DEFAULT_LOAD_COUNT);
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.adService.reSubscribeAds("TAB");
    this.workspaceToolbarService.hideBackButton();
  }

  // ---------------------------------------------------------------------------------------

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  isExpired(date: Date) {
    return this.getLocalTime(date) < new Date();
  }

  isPending(approvalStatus: String) {
    if (approvalStatus === "Pending")
      return true;
    else
      return false;
  }

 callSecurity(number){
  this.callNumber.callNumber(number.toString(), true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));

 }
}
