import {
  Component,
  OnInit,
  ViewChild,
  NgZone,
  OnDestroy,
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { IonInfiniteScroll, Platform } from "@ionic/angular";
import { FcmService } from "src/app/notifications/fcm.service";
import { WorkspaceToolbarService } from "src/app/home/workspace-toolbar/workspace-toolbar.service";
import { ResidentService } from "src/app/home/services/resident.service";
import { AdService } from "src/app/home/ad/ad.service";
import { HttpClient } from "@angular/common/http";
import { Cache } from "../../services/cache";
import { LoginService } from "src/app/login/login.service";
import { ListPageTrack } from "../../arch/ListPageTrack";
import { ListPageInterface } from "../../arch/ListPageInterface";
import { CacheService } from "../../services/cache.service";
import { CommitteeMember } from "src/app/core/CommitteeMember";
import { CallNumber } from "@ionic-native/call-number/ngx";

@Component({
  selector: "app-my-society",
  templateUrl: "./my-society.page.html",
  styleUrls: ["./my-society.page.scss"],
})
@ListPageTrack(
  '/resident/my-society',
  'Apartment',
  '/GSServer/webapi/secured/Resident/Facilities/Apartment',
  false
)
export class MySocietyPage implements ListPageInterface {

  cache: Cache;
  committeeMemberCache: Cache = null;
  committeeMembers: CommitteeMember[] = [];
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private cacheService: CacheService,
    public residentService: ResidentService,
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService,
    private loginService: LoginService,
    private adService: AdService,
    private callNumber: CallNumber
  ) {

  }

  ngOnInit() {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/resident/add-society']);
    }

    this.cacheService.initializeCache('Resident', '/GSServer/webapi/secured/Resident/CommitteeMember', true).then(cacheObject => {
      this.committeeMemberCache = cacheObject
      this.committeeMemberCache.load(CacheService.LOAD_ALL).then(
        res => {
          this.committeeMembers = this.committeeMemberCache.objects
        }
      )
    });
  }

  
  ngOnDestroy() {
  }

  onRefresh(event: any): void {
  }
  
  onInAppMessage(data: any): void {
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  
  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  callCommitteeMember(number){
    this.callNumber.callNumber(number.toString(), true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  
   }
}
