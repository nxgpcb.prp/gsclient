import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResidentNoticesPage } from './resident-notices.page';
import { AddSocietyPageModule } from '../add-society/add-society.module';
import { AddSocietyPage } from '../add-society/add-society.page';

const routes: Routes = [
  {
    path: '',
    component: ResidentNoticesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ResidentNoticesPage],
})
export class ResidentNoticesPageModule {}
