import { Component } from '@angular/core';
import { DeliveryLog } from 'src/app/core/DeliveryLog';
import { Router, ActivatedRoute } from '@angular/router';
import { Resident } from 'src/app/core/Resident';
import { ResidentService } from '../../services/resident.service';
import { HomeService } from '../../home.service';
import { AdService } from '../../ad/ad.service';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { LoginService } from 'src/app/login/login.service';
import { UpdatePageTrack } from '../../arch/UpdatePageTrack';
import { UpdatePageInterface } from '../../arch/UpdatePageInterface';
import { CacheService } from '../../services/cache.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';

@Component({
  selector: 'app-delivery-log',
  templateUrl: './delivery-log.page.html',
  styleUrls: ['./delivery-log.page.scss'],
})
@UpdatePageTrack(
  '/resident/home/resident-delivery-logs/delivery-log',
  'DeliveryLog',
  '/GSServer/webapi/secured/Resident/DeliveryLog',
  false
)
export class DeliveryLogPage implements UpdatePageInterface {
  cacheObject: any;
  cache: Cache;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService,
    private adService:AdService,
    private workspaceToolbarService: WorkspaceToolbarService,
    ) 
  {
  }

  ngOnInit() {
    
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
  }

  received() {
    this.cacheObject.approvalStatus = 'Recieved'
    this.cache.updateObject(this.cacheObject).then(
      object=>this.router.navigate(['/resident/home/resident-delivery-logs']),
      error=>alert("Error occurred :"+error)
    );
  }

}