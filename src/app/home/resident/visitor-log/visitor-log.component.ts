import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Cache } from '../../services/cache';
import { UpdatePageTrack } from '../../arch/UpdatePageTrack';
import { UpdatePageInterface } from '../../arch/UpdatePageInterface';
import { CacheService } from '../../services/cache.service';
import { AdService } from '../../ad/ad.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';

@Component({
  selector: 'app-visitor-log',
  templateUrl: './visitor-log.component.html',
  styleUrls: ['./visitor-log.component.scss']
})
@UpdatePageTrack(
  '/resident/home/logs/visitor-log',
  'VisitorLog',
  '/GSServer/webapi/secured/Resident/VisitorLog',
  false
)
export class VisitorLogComponent implements UpdatePageInterface {

  cacheObject: any;
  cache: Cache;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cacheService:CacheService, 
    private adService:AdService,
    private workspaceToolbarService: WorkspaceToolbarService,
    ) 
  {
    
  }

  ngOnInit() 
  {
  }

  ngOnDestroy()
  {

  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.showBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  // ---------------------------------------------------------------------------------------
  accept() {
    this.cacheObject.approvalStatus='Approved';
    this.cache.updateObject(this.cacheObject).then(
      object=>this.router.navigate(['/resident/home/logs']),
      error=>alert("Error occurred :"+error)
    );
  }

  reject() {
    this.cacheObject.approvalStatus='Rejected';
    this.cache.updateObject(this.cacheObject).then(
      object=>this.router.navigate(['/resident/home/logs']),
      error=>alert("Error occurred :"+error)
    );
  }

  hold() {
    this.cacheObject.approvalStatus='Hold';
    this.cache.updateObject(this.cacheObject).then(
      object=>this.router.navigate(['/resident/home/logs']),
      error=>alert("Error occurred :"+error)
    );
  }
}

