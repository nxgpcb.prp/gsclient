import { Component, OnInit } from '@angular/core';
import { SocietyService } from '../../services/society.service';
import { Router } from '@angular/router';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { CacheService } from '../../services/cache.service';
import { AdService } from '../../ad/ad.service';
import { LoginService } from 'src/app/login/login.service';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { Cache } from '../../services/cache';

@Component({
  selector: 'app-resident-subscription',
  templateUrl: './resident-subscription.page.html',
  styleUrls: ['./resident-subscription.page.scss'],
})
@ListPageTrack(
  '/resident/subscription',
  'subscriptionPlanLogs',
  '/GSServer/webapi/secured/Society/SubscriptionPlanLog',
  false
)
export class ResidentSubscriptionPage implements ListPageInterface {

  cache: Cache
  societyCache: Cache = null;

  constructor(
    public societyService: SocietyService, 
    private router: Router,
    private workspaceToolbarService: WorkspaceToolbarService, 
    private cacheService:CacheService,
    private adService:AdService,
    public readonly loginService: LoginService, 
    ) {
  }

  ngOnInit(): void {
    this.cacheService.initializeCache('societies','/GSServer/webapi/secured/Resident/Society',true).then(cacheObject=>{
      this.societyCache=cacheObject
      this.societyCache.load(CacheService.DEFAULT_LOAD_COUNT);
  });
  }

  ngOnDestroy(): void {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

//------------------------------------------------------------------------

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }

  goToPricingPage() {
    this.router.navigate(['/society/subscription/pricing']);
  }

}
