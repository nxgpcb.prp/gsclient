import { Component, OnInit, ViewChild, OnDestroy, NgZone } from '@angular/core';
import { Parking } from 'src/app/core/Parking';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ResidentService } from '../../services/resident.service';
import { HomeService } from '../../home.service';
import { Resident } from 'src/app/core/Resident';
import { Society } from 'src/app/core/Society';
import { FcmService } from 'src/app/notifications/fcm.service';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { AdService } from '../../ad/ad.service';
import { OnlineOfflineService } from 'src/app/misc/OnlineOffline.service';
import { Subscription } from 'rxjs';
import { Paging } from 'src/app/filters/Paging';
import { Sort } from 'src/app/filters/Sort';
import { Filter } from 'src/app/filters/Filter';
import { HttpClient } from '@angular/common/http';
import { Cache } from '../../services/cache';
import { environment } from 'src/environments/environment';
import { LoginService } from 'src/app/login/login.service';
import { ListPageTrack } from '../../arch/ListPageTrack';
import { ListPageInterface } from '../../arch/ListPageInterface';
import { CacheService } from '../../services/cache.service';

@Component({
  selector: 'app-parking-status',
  templateUrl: './parking-status.page.html',
  styleUrls: ['./parking-status.page.scss'],
})
@ListPageTrack(
  '/resident/parking-status',
  'Parking',
  '/GSServer/webapi/secured/Resident/Facilities/Parking',
  false
)
export class ParkingStatusPage implements ListPageInterface {

  cache: Cache

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private cacheService: CacheService,
    private router: Router, 
    public residentService: ResidentService,
    private loginService: LoginService, 
    private workspaceToolbarService: WorkspaceToolbarService,
    private route: ActivatedRoute, 
    private adService: AdService, 
    ) {
  }

  ngOnInit() {
    if(this.loginService.userCache!=undefined && this.loginService.userCache.objects[0].subscriptionPlan==undefined)
    {
      this.router.navigate(['/resident/add-society']);
    }
  }


  ngOnDestroy() {
  }

  onRefresh(event) {
  }

  onInAppMessage(data): void {
    console.log(data)
  }

  onNavigationEnd(): void {
    this.workspaceToolbarService.hideBackButton();
    this.adService.reSubscribeAds("NORMAL");
  }

  getLocalTime(utcDate: Date) {
    if (utcDate != null) {
      return new Date(utcDate.toString().replace("[UTC]", ""));
    } else {
      return "";
    }
  }


}
