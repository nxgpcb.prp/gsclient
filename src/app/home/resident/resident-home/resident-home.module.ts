import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';

import { ResidentHomePage } from './resident-home.page';
import { VisitorLogComponent } from '../visitor-log/visitor-log.component';
import { PopoverMenuComponent } from '../../popover-menu/popover-menu.component';
import { WorkspaceToolbarModule } from '../../workspace-toolbar/workspace-toolbar.module';

const routes: Routes = [
  {
    path: '',
    component: ResidentHomePage,
    children: [
      { path: 'logs', loadChildren: '../logs/logs.module#LogsPageModule' },
      { path: 'resident-delivery-logs', loadChildren: '../resident-delivery-logs/resident-delivery-logs.module#ResidentDeliveryLogsPageModule' },
      { path: 'resident-requests', loadChildren: '../resident-requests/resident-requests.module#ResidentRequestsPageModule' },
      { path: 'resident-notices', loadChildren: '../resident-notices/resident-notices.module#ResidentNoticesPageModule' },
      { path: 'demands', loadChildren: '../demands/demands.module#DemandsPageModule' },
      { path: 'logs/visitor-log/:id', component: VisitorLogComponent },
      { path: 'resident-delivery-logs/delivery-log/:id', loadChildren: '../delivery-log/delivery-log.module#DeliveryLogPageModule' },
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    IonicSelectableModule,
    WorkspaceToolbarModule
  ],
  declarations: [ResidentHomePage, VisitorLogComponent],
  entryComponents: [
    PopoverMenuComponent,
  ],
})
export class ResidentHomePageModule {}
