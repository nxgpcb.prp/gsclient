import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResidentService } from '../../services/resident.service';
import { WorkspaceToolbarService } from '../../workspace-toolbar/workspace-toolbar.service';
import { Resident } from 'src/app/core/Resident';
import { AdService } from '../../ad/ad.service';
import { LoginService } from 'src/app/login/login.service';
import { CacheService } from '../../services/cache.service';
import { Cache } from '../../services/cache';

@Component({
  selector: 'app-resident-home',
  templateUrl: './resident-home.page.html',
  styleUrls: ['./resident-home.page.scss'],
})
export class ResidentHomePage implements OnInit, AfterViewInit {

  cacheVisitorLog: Cache;
  cacheDemandLog: Cache;
  cacheNotice: Cache;
  cacheDeliveryLog: Cache;

  constructor(
    private router:Router, 
    private cacheService:CacheService,
    private workspaceToolbarService:WorkspaceToolbarService,
    private adService: AdService) 
  {
    this.workspaceToolbarService.hideBackButton();
   }

  async ngOnInit() {
    this.cacheService.initializeCache('VisitorLog','/GSServer/webapi/secured/Resident/VisitorLog',false).then(cache=>{
      this.cacheVisitorLog=cache;
      this.cacheVisitorLog.load(1);
    });
    this.cacheService.initializeCache('DeliveryLog','/GSServer/webapi/secured/Resident/DeliveryLog',false).then(cache=>{
      this.cacheDeliveryLog=cache;
      this.cacheDeliveryLog.load(1);
    });
    this.cacheService.initializeCache('Notice','/GSServer/webapi/secured/Resident/Notice',false).then(cache=>{
      this.cacheNotice=cache;
      this.cacheNotice.load(1);
    });
    this.cacheService.initializeCache('DemandLog','/GSServer/webapi/secured/Resident/DemandLog',false).then(cache=>{
      this.cacheDemandLog=cache;
      this.cacheDemandLog.load(1);
    });

  }

  ngAfterViewInit(): void {

  }

}
