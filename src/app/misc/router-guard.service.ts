import { Injectable, NgZone } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../login/login.service';
import { User } from '../core/User';
import { AngularFireAuth } from '@angular/fire/auth';
import { HomeService } from '../home/home.service';

import { LoaderService } from './loader.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RouterGuardService implements CanActivate {

  user: any;

  constructor(private http: HttpClient, private router: Router,
    private loginService: LoginService, private afAuth: AngularFireAuth,
    private loader: LoaderService, private ngZone: NgZone) { }

  canActivate(route: ActivatedRouteSnapshot): any {
    return new Promise(res => {
      this.loginService.getUser()
        .then(
          (response: any) => {
            this.user = response;
            
            if (this.user.name != null) {
              if (this.user.type == "Resident") {
                this.loader.hideLoader();
                this.router.navigate(['/resident/home/logs'], { replaceUrl: true });
              }
              if (this.user.type == "Security") {
                this.loader.hideLoader();
                this.router.navigate(['/security/dashboard/visitor-logs'], { replaceUrl: true });
              }
              if (this.user.type == "Society") {
                this.loader.hideLoader();
                this.router.navigate(['/society/facilities/apartment'], { replaceUrl: true });
              }
            } else {
              this.loader.hideLoader();
              this.router.navigate(['/welcome'], { replaceUrl: true });
            }
            res(true);
          },
          (error) => {
            this.loader.hideLoader();
            this.router.navigate(['/login'], { replaceUrl: true });
            res(false);
          }
        );
    })
  }
}

