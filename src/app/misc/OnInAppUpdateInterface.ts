export declare interface OnInAppUpdateInterface {
    ngOnInAppUpdate(): void;
    refresh(event): void;
}