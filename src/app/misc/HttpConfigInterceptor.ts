//httpConfig.interceptor.ts
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError, from, Subject } from 'rxjs';
import { map, catchError, mergeMap, takeUntil } from 'rxjs/operators';
import { Injectable, NgZone } from '@angular/core';
import { LoadingController, Platform, PopoverController } from '@ionic/angular';
import { LoaderService } from './loader.service';
import { NetworkService } from '../network-error/network.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AppVersion } from '../core/AppVersion';
import { UpdatePopupComponent } from '../update-popup/update-popup.component';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  loaderToShow: any;
  appVersion : AppVersion =new AppVersion();
  popover:any;
  popOverPresented = false;
  constructor(
    public loadingController: LoadingController, public loader: LoaderService,
    private networkService: NetworkService, private router: Router, private afAuth: AngularFireAuth,
    private ngZone: NgZone, private popoverController: PopoverController,
    private platform: Platform
  ) { }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(request.url.search("secured") != -1){
    return from(this.getRefreshedToken()).pipe(mergeMap(
      data => {

        if (this.platform.is('android')) 
        {
          request = request.clone({
            setHeaders: {
              "Authorization": "Basic " + data,
              "AppVersion":""+this.appVersion.androidAppClient,
              "ClientType":"android"
            }
          });
        }
        else if(this.platform.is('ios'))
        {
          request = request.clone({
            setHeaders: {
              "Authorization": "Basic " + data,
              "AppVersion":""+this.appVersion.iOSAppClient,
              "ClientType":"ios"
            }
          });
        }
        else
        {
          request = request.clone({
            setHeaders: {
              "Authorization": "Basic " + data,
              "AppVersion":""+this.appVersion.webClient,
              "ClientType":"web"
            }
          });
        }
        return next.handle(request).pipe
        (
          map(
            (event: HttpEvent<any>) => {
              //this.loader.hideLoader();
              if (event instanceof HttpResponse) {
                //console.log('event--->>>', event);
              }
              return event;
            }),
          catchError
          (
            (error: HttpErrorResponse) => 
            {
              this.loader.hideLoader();
              //console.error('event--->>> ' + error);
              switch (error.status) 
              {
                case 401:
                  this.router.navigate(['/login'])
                  return throwError(error);
                case 404:
                  alert(error.error.errorMessage)
                  return throwError(error);
                case 406:
                case 409:
                  alert(error.error.errorMessage)
                  return throwError(error);
                case 410:
                  alert("Your session is expired for security reasons. Please login again.")
                  return throwError(error);
                case 504:
                  this.networkService.presentNetworkError();
                  return throwError(error);
                case 505:
                  console.log(error)
                  if(!this.popOverPresented){
                    this.popOverPresented = true;
                    this.presentPopover(null);
                  }
                  return throwError(error);
                default:
                  return throwError(error);
              }
            }
          )
        );
      }
    )
    )
    }else
    {
      if (this.platform.is('android')) {
        request = request.clone({
          setHeaders: {
            "AppVersion": "" + this.appVersion.androidAppClient,
            "ClientType": "android"
          }
        });
      }
      else if (this.platform.is('ios')) {
        request = request.clone({
          setHeaders: {
            "AppVersion": "" + this.appVersion.iOSAppClient,
            "ClientType": "ios"
          }
        });
      }
      else {
        request = request.clone({
          setHeaders: {
            "AppVersion": "" + this.appVersion.webClient,
            "ClientType": "web"
          }
        });
      }
      return next.handle(request).pipe
      (
        map(
          (event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
              //console.log('event--->>>', event);
            }
            return event;
          }),
        catchError(
          (error: HttpErrorResponse) => 
          {
            this.loader.hideLoader();
            //console.error('event--->>> ' + error);
            switch (error.status) 
            {
              case 401:
                this.router.navigate(['/login'])
                return throwError(error);
              case 406:
              case 409:
                alert(error.error.errorMessage)
                return throwError(error);
              case 410:
                alert(error.error.errorMessage)
                return throwError(error);
              case 504:
                this.networkService.presentNetworkError();
                return throwError(error);
              case 505:
                console.log(error)
                if(!this.popOverPresented){
                  this.popOverPresented = true;
                  this.presentPopover(null);
                }
                return throwError(error);
              default:
                return throwError(error);
            }
          }
        )
      );
    }
  }

  getRefreshedToken() {
    return new Promise(res => {
      this.afAuth.auth.onAuthStateChanged(
        user => {
          if (user) {
            res(user.getIdToken(false))
          }else{
            this.ngZone.run(() => {
            this.loader.hideLoader();
            this.router.navigate(['/login'])
            })
          }
        }
      )
    })
  }

  async presentPopover(ev: any) {
    //Style defined in global.scss
    this.popover = await this.popoverController.create({
      component: UpdatePopupComponent,
      componentProps: {
        "popoverController": this.popoverController,
      },
      event: ev,
      translucent: true,
      backdropDismiss: false
    });
    this.popover.present().then(
      response => {
        this.popOverPresented = true;
      }
    )

    this.popover.onDidDismiss().then(
      response => {
        this.popOverPresented = false;
      }
    )
  }
}
