import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { Cache } from '../home/services/cache';
import { LoaderService } from '../misc/loader.service';

const { Market } = Plugins;
@Component({
  selector: 'app-update-popup',
  templateUrl: './update-popup.component.html',
  styleUrls: ['./update-popup.component.scss'],
})
export class UpdatePopupComponent implements OnInit {

  constructor(
    private platform: Platform,
    private router: Router,
    private loader: LoaderService
  ) { }

  ngOnInit() { }

  update() {
    this.loader.showLoader()
    Cache.purgeLocalCache()
      .then(
        res => {
          this.loader.hideLoader()
          if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
            this.loader.hideLoader()
            if (this.platform.is('android')) {
              Market.open(
                {
                  appId: "com.Dwarpal Technologies.dwarpal"
                }
              );
            }
            if (this.platform.is('ios')) {
              Market.open(
                {
                  appId: "1528705828"
                }
              );
            }
          } else {
            location.reload(true);
          }
        })
  }
}
