import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(private router:Router,private http: HttpClient) { }

  subscribeToNotification(clientToken:String){
    console.log("called" + clientToken)
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));
    
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Boolean>(environment.backend.baseURL + '/GSServer/webapi/secured/subscribe/'+clientToken, {headers:headers})
  }

  unSubscribeToNotification(clientToken:String){
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));
    
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Boolean>(environment.backend.baseURL + '/GSServer/webapi/secured/unsubscribe/'+clientToken, {headers:headers})
  }

}
