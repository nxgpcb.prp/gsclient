import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase/ngx';
import { Platform, PopoverController } from '@ionic/angular';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NotificationsService } from './notifications.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { FCM } from '@ionic-native/fcm/ngx';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';
import { Router } from '@angular/router';
import { LocalNotificationsService } from './local-notifications.service';

const { DwarPushNotifications, PushNotifications } = Plugins;

export interface DwarPushNotification {
  title?: string;
  subtitle?: string;
  body?: string;
  id: string;
  badge?: number;
  notification?: any;
  data: any;
  click_action?: string;
  link?: string;
  /**
   * Android only: set the group identifier for notification grouping, like
   * threadIdentifier on iOS.
   */
  group?: string;
  /**
   * Android only: designate this notification as the summary for a group
   * (should be used with the `group` property).
   */
  groupSummary?: boolean;
}

export interface DwarFullNotificationActionPerformed {
  actionId: string;
  inputValue?: string;
  notification: DwarPushNotification;
}
export interface DwarPushNotificationActionPerformed {
  actionId: string;
  inputValue?: string;
  notification: DwarPushNotification;
}
export interface DwarPushNotificationToken {
  value: string;
}
export interface DwarPushNotificationDeliveredList {
  notifications: DwarPushNotification[];
}
export interface DwarPushNotificationChannel {
  id: string;
  name: string;
  description: string;
  sound: string;
  importance: 1 | 2 | 3 | 4 | 5;
  visibility?: -1 | 0 | 1;
}
export interface DwarPushNotificationChannelList {
  channels: DwarPushNotificationChannel[];
}

@Injectable({
  providedIn: 'root'
})
export class FcmService {

  currentMessage = new BehaviorSubject(null);
  popover: any;

  constructor(
    public firebase: Firebase,
    public afs: AngularFirestore,
    private platform: Platform,
    private afMessaging: AngularFireMessaging,
    private notificationsService: NotificationsService,
    private localNotificationsService: LocalNotificationsService,
    private router:Router
  ) {
    this.afMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )
  }

  token: string;
  // Get permission from the user
  async subscribeNotfication() {

    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {

      if(this.platform.is('android')){
        DwarPushNotifications.register();

        this.localNotificationsService.registerActionTypes();
  
        DwarPushNotifications.addListener('registration',
          (token: DwarPushNotificationToken) => {
            this.token = token.value;
            console.log('Push registration success, token: ' + token.value);
            if (localStorage.getItem('showNotifications') == undefined || this.getToken() != token.value) {
              this.notificationsService.subscribeToNotification(this.token)
                .subscribe(
                  (response: Boolean) => {
                    console.log(response)
                    localStorage.setItem('showNotifications', JSON.stringify(true));
                    localStorage.setItem('notificationsToken', JSON.stringify(this.token));
                  }
                )
            } else if (JSON.parse(localStorage.getItem('showNotifications')) == true) {
              this.notificationsService.subscribeToNotification(this.token)
                .subscribe(
                  (response: Boolean) => {
                    console.log(response)
                    localStorage.setItem('showNotifications', JSON.stringify(true));
                    localStorage.setItem('notificationsToken', JSON.stringify(this.token));
                  }
                )
            }
          }
        );
  
        DwarPushNotifications.addListener('registrationError',
          (error: any) => {
            console.log('Error on registration: ' + JSON.stringify(error));
          }
        );
  
        DwarPushNotifications.addListener('dwarPushNotificationReceived',
          (notification: DwarPushNotification) => {
            console.log('Push received: ' + JSON.stringify(notification));
            this.currentMessage.next(notification);
          }
        );
  
        DwarPushNotifications.addListener('dwarPushNotificationActionPerformed',
          (notification: DwarPushNotificationActionPerformed) => {
            console.log('Push action performed: ' + JSON.stringify(notification));
          }
        );
  
        DwarPushNotifications.addListener('fullScreenNotificationActionPerformed',
          (notification: DwarFullNotificationActionPerformed) => {
            console.log('Full Push action performed: ' + JSON.stringify(notification));
            this.localNotificationsService.performAction(notification).then(
              res=>{
                this.router.navigate(['/resident/home/logs'])
                this.currentMessage.next(notification.notification);
              },
              error =>{
                console.log(error)
              }
            )
          }
        );
      }

      if(this.platform.is('ios')){
        (PushNotifications as any).requestPermission().then(result => {
          PushNotifications.register();
        });

        this.localNotificationsService.registerActionTypes();
  
        PushNotifications.addListener('registration',
          (token: PushNotificationToken) => {
            this.token = token.value;
            console.log('Push registration success, token: ' + token.value);
            if (localStorage.getItem('showNotifications') == undefined || this.getToken() != token.value) {
              this.notificationsService.subscribeToNotification(this.token)
                .subscribe(
                  (response: Boolean) => {
                    console.log(response)
                    localStorage.setItem('showNotifications', JSON.stringify(true));
                    localStorage.setItem('notificationsToken', JSON.stringify(this.token));
                  }
                )
            }else if(JSON.parse(localStorage.getItem('showNotifications')) == true){
              this.notificationsService.subscribeToNotification(this.token)
              .subscribe(
                (response: Boolean) => {
                  console.log(response)
                  localStorage.setItem('showNotifications', JSON.stringify(true));
                  localStorage.setItem('notificationsToken', JSON.stringify(this.token));
                }
              )
            }
          }
        );
  
        PushNotifications.addListener('registrationError',
          (error: any) => {
            console.log('Error on registration: ' + JSON.stringify(error));
          }
        );
  
        PushNotifications.addListener('pushNotificationReceived',
          (notification: PushNotification) => {
            console.log('Push received: ' + JSON.stringify(notification));
            if(!notification.notification){
              this.localNotificationsService.publishLocalNotification(notification);
            }
            this.currentMessage.next(notification);
          }
        );
  
        PushNotifications.addListener('pushNotificationActionPerformed',
          (notification: PushNotificationActionPerformed) => {
            console.log('Push action performed: ' + JSON.stringify(notification));
            if(notification.notification){
              this.localNotificationsService.performPushAction(notification).then(
                res=>{
                  this.router.navigate(['/resident/home/logs'])
                  this.currentMessage.next(notification.notification);
                },
                error =>{
                  console.log(error)
                }
              )
            }
          }
        );
  
      }
      
    } else {
      console.log("web")
      this.afMessaging.requestToken.subscribe(
        (token) => {
          this.token = token;
          console.log(this.token)
          if (localStorage.getItem('showNotifications') == undefined || this.getToken() != token) {
            this.notificationsService.subscribeToNotification(token)
              .subscribe(
                (response: Boolean) => {
                  console.log(response)
                  localStorage.setItem('showNotifications', JSON.stringify(true));
                  localStorage.setItem('notificationsToken', JSON.stringify(this.token));
                }
              )
          } else if (JSON.parse(localStorage.getItem('showNotifications')) == true) {
            this.notificationsService.subscribeToNotification(token)
              .subscribe(
                (response: Boolean) => {
                  console.log(response)
                  localStorage.setItem('showNotifications', JSON.stringify(true));
                  localStorage.setItem('notificationsToken', JSON.stringify(this.token));
                }
              )
          }
        },
        (err) => {
          alert("This browser is not supported for push notifications")
        }
      );

      this.receiveMessage();
    }
  }

  receiveMessage() {
    this.afMessaging.messages.subscribe(
      (payload) => {
        console.log("new message received. ", payload);
        this.currentMessage.next(payload);
      })
  }

  getToken() {
    return JSON.parse(localStorage.getItem('notificationsToken'));
  }

}
