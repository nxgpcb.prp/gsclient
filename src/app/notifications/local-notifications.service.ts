import { Injectable } from '@angular/core';
import { Plugins, PushNotification, LocalNotificationActionPerformed } from '@capacitor/core';
import { Cache } from '../home/services/cache';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

const { LocalNotifications } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class LocalNotificationsService {

  visitorLogCache: Cache;
  deliveryLogCache: Cache;

  constructor(
    private http: HttpClient, 
    private router: Router) {
      
    this.visitorLogCache = new Cache(this.http,
      'visitorLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/VisitorLog',
      Cache.type_NATIVE);
    this.deliveryLogCache = new Cache(this.http,
      'deliveryLogs',
      environment.backend.baseURL + '/GSServer/webapi/secured/Resident/DeliveryLog',
      Cache.type_NATIVE);
  }

  registerActionTypes() {
    LocalNotifications.registerActionTypes({
      types: [
        {
          id: 'VISITOR_LOG',
          actions: [
            {
              id: 'APPROVE',
              title: 'Approve',
              foreground: false
            },
            {
              id: 'HOLD',
              title: 'Hold',
              foreground: false
            },
            {
              id: 'REJECT',
              title: 'Reject',
              foreground: false
            },
          ]
        },
        {
          id: 'DELIVERY_LOG',
          actions: [
            {
              id: 'RECIEVED',
              title: 'Recieved',
              foreground: false
            },
          ]
        }
      ]
    });

    this.registerListners();
  }

  private registerListners() {
    LocalNotifications.addListener('localNotificationActionPerformed',
      (notification: LocalNotificationActionPerformed) => {
        console.log('Local Notification action performed: ' + JSON.stringify(notification));
        if (notification.notification.extra.intent == "VISITORLOGPENDING") {
          if (notification.actionId == "APPROVE") {
            this.visitorLogCache.get(notification.notification.extra.id).then(
              (response) => {
                response.approvalStatus = "Approved"
                this.visitorLogCache.update(response)
                  .then((result) => {
                    console.log(result)
                   
                  },
                    error => {
                      console.log(error)
                      alert("Error occured while approving the visitor log")
                     
                    })
              },
              error => {
                console.log(error)
                alert("Error occured while approving the visitor log")
               
              })
          }
          if (notification.actionId == "HOLD") {
            this.visitorLogCache.get(notification.notification.extra.id).then(
              (response) => {
                response.approvalStatus = "Hold"
                this.visitorLogCache.update(response)
                  .then((result) => {
                    console.log(result)
                   
                  },
                    error => {
                      console.log(error)
                      alert("Error occured while approving the visitor log")
                     
                    })
              },
              error => {
                console.log(error)
                alert("Error occured while approving the visitor log")
               
              })
          }
          if (notification.actionId == "REJECT") {
            this.visitorLogCache.get(notification.notification.extra.id).then(
              (response) => {
                response.approvalStatus = "Rejected"
                this.visitorLogCache.update(response)
                  .then((result) => {
                    console.log(result)
                   
                  },
                    error => {
                      console.log(error)
                      alert("Error occured while approving the visitor log")
                     
                    })
              },
              error => {
                console.log(error)
                alert("Error occured while approving the visitor log")
               
              })
          }

          this.router.navigate(['/resident/home/logs'])

        }

        if (notification.notification.extra.intent == "DELIVERYLOGPENDING") {
          if (notification.actionId == "RECIEVED") {
            this.deliveryLogCache.get(notification.notification.extra.id).then(
              (response) => {
                response.approvalStatus = "Recieved"
                this.deliveryLogCache.update(response)
                  .then((result) => {
                    console.log(result)
                  },
                    error => {
                      console.log(error)
                      alert("Error occured while approving the visitor log")
                    })
              },
              error => {
                console.log(error)
                alert("Error occured while approving the visitor log")
              })

          }

          this.router.navigate(['/resident/home/resident-delivery-logs'])
        }
      }
    );

  }

  publishLocalNotification(pushNotification: PushNotification) {
    if (pushNotification.data.intent == 'VISITORLOGPENDING') {
      this.publishVisitorLogNotification(pushNotification);
    }

    if (pushNotification.data.intent == 'DELIVERYLOGPENDING') {
      this.publishDeliveryLogNotification(pushNotification);
    }
  }

  private publishVisitorLogNotification(pushNotification: PushNotification) {
    LocalNotifications.schedule({
      notifications: [
        {
          title: pushNotification.data.title,
          body: pushNotification.data.title,
          id: 1,
          actionTypeId: 'VISITOR_LOG',
          extra: pushNotification.data,

        }
      ]
    })
  }

  private publishDeliveryLogNotification(pushNotification: PushNotification) {
    LocalNotifications.schedule({
      notifications: [
        {
          title: pushNotification.data.title,
          body: pushNotification.data.title,
          id: 2,
          actionTypeId: 'DELIVERY_LOG',
          extra: pushNotification.data,
        }
      ]
    })
  }

  public performAction(notification){
    return new Promise<any>(async (res, err) => 
    {
      if (notification.actionId == "APPROVE") {
        this.visitorLogCache.get(notification.notification.data.id).then(
          (response) => {
            response.approvalStatus = "Approved"
            this.visitorLogCache.update(response)
              .then((result) => {
                console.log(result)
                res(true)
              },
                error => {
                  console.log(error)
                  alert("Error occured while approving the visitor log")
                  res(false)
                })
          },
          error => {
            console.log(error)
            alert("Error occured while approving the visitor log")
            res(false)
          })
      }
      if (notification.actionId == "HOLD") {
        this.visitorLogCache.get(notification.notification.data.id).then(
          (response) => {
            response.approvalStatus = "Hold"
            this.visitorLogCache.update(response)
              .then((result) => {
                console.log(result)
                res(true)
              },
                error => {
                  console.log(error)
                  alert("Error occured while approving the visitor log")
                  res(false)
                })
          },
          error => {
            console.log(error)
            alert("Error occured while approving the visitor log")
            res(false)
          })
      }
      if (notification.actionId == "REJECT") {
        this.visitorLogCache.get(notification.notification.data.id).then(
          (response) => {
            response.approvalStatus = "Rejected"
            this.visitorLogCache.update(response)
              .then((result) => {
                console.log(result)
                res(true)
              },
                error => {
                  console.log(error)
                  alert("Error occured while approving the visitor log")
                  res(false)
                })
          },
          error => {
            console.log(error)
            alert("Error occured while approving the visitor log")
            res(false)
          })
      }
    });
    
  }

  public performPushAction(notification){
    return new Promise<any>(async (res, err) => 
    {
      if (notification.actionId == "APPROVE") {
        this.visitorLogCache.get(notification.notification.data.id).then(
          (response) => {
            response.approvalStatus = "Approved"
            this.visitorLogCache.update(response)
              .then((result) => {
                console.log(result)
                res(true)
              },
                error => {
                  console.log(error)
                  alert("Error occured while approving the visitor log")
                  res(false)
                })
          },
          error => {
            console.log(error)
            alert("Error occured while approving the visitor log")
            res(false)
          })
      }
      if (notification.actionId == "HOLD") {
        this.visitorLogCache.get(notification.notification.data.id).then(
          (response) => {
            response.approvalStatus = "Hold"
            this.visitorLogCache.update(response)
              .then((result) => {
                console.log(result)
                res(true)
              },
                error => {
                  console.log(error)
                  alert("Error occured while approving the visitor log")
                  res(false)
                })
          },
          error => {
            console.log(error)
            alert("Error occured while approving the visitor log")
            res(false)
          })
      }
      if (notification.actionId == "REJECT") {
        this.visitorLogCache.get(notification.notification.data.id).then(
          (response) => {
            response.approvalStatus = "Rejected"
            this.visitorLogCache.update(response)
              .then((result) => {
                console.log(result)
                res(true)
              },
                error => {
                  console.log(error)
                  alert("Error occured while approving the visitor log")
                  res(false)
                })
          },
          error => {
            console.log(error)
            alert("Error occured while approving the visitor log")
            res(false)
          })
      }
    });
  }

}
