import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DebugService {

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    return this.isDebug();
  }

  getEnvironment(){
    if(environment.debug == true && environment.production == false){
      return "DEBUG";
    }
    if(environment.debug == true && environment.production == true){
      return "STAGING";
    }
    if(environment.debug == false && environment.production == true){
      return "PRODUCTION";
    }
  }

  isDebug(){
    return environment.debug;
  }

  isProduction(){
    return environment.production;
  }
}
