import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ForDeveloper } from './for-developer.page';
import { Routes, RouterModule } from '@angular/router';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';
import { HomeFooterModule } from '../home/home-footer/home-footer.module';

const routes: Routes = [
  {
    path: '',
    component: ForDeveloper
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeToolbarModule,
    HomeFooterModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ForDeveloper],
  entryComponents: [
    HomePopoverMenuComponent
  ],
})
export class ForDeveloperModule { }
