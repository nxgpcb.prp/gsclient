import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DebugService } from './debug.service';

@Component({
  selector: 'app-for-developer',
  templateUrl: './for-developer.page.html',
  styleUrls: ['./for-developer.page.scss'],
})
export class ForDeveloper implements OnInit {
  
  gsserverUrl = 'https://staging.dwarpal.co.in/downloads/gsserver.html';
  androidUrl = 'https://staging.dwarpal.co.in/downloads/android.html';
  constructor(private router: Router, private readonly debugService:DebugService) {

  }

  ngOnInit() {
    console.log(this.debugService.getEnvironment());
  }

}
