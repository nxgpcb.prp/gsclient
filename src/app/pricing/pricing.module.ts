import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { PricingPage } from './pricing.page';
import { Routes, RouterModule } from '@angular/router';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';
import { HomeFooterModule } from '../home/home-footer/home-footer.module';
import { FrequentlyAskedModule } from '../faq/frequently-asked/frequently-asked.module';

const routes: Routes = [
  {
    path: '',
    component: PricingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeToolbarModule,
    HomeFooterModule,
    FrequentlyAskedModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [PricingPage],
  entryComponents: [
    HomePopoverMenuComponent
  ],
})
export class PricingPageModule {}
