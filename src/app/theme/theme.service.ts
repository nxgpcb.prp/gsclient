import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import * as Color from 'color';
import { Storage } from '@ionic/storage';
import { Platform} from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private theme;
  private darkMode;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private storage: Storage,
    private platform:Platform
  ) {
    storage.get('themeName').then(themeName => {
      if(themeName == "dark"){
        this.setAppTheme(true);
      }else{
        this.setAppTheme(false);
      }
    });
  }

  // Override all global variables with a new theme
  setAppTheme(dark) {
    this.darkMode = dark;
    if(this.darkMode){
      document.body.classList.add("dark");
      this.storage.set('themeName', "dark");
    }else{
      document.body.classList.remove("dark");
      this.storage.set('themeName', "light");
    }    
  }

  getTheme(){
    return this.theme;
  }

  // Define a single CSS variable
  setVariable(name, value) {
    this.document.documentElement.style.setProperty(name, value);
  }

  getVariable(name) {
    return this.document.documentElement.style.getPropertyValue(name);
  }

  private setGlobalCSS(css: string) {
    this.document.documentElement.style.cssText = css;
  }

  get storedTheme() {
    return this.storage.get('themeName');
  }
}
