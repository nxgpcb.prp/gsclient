import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { IonicModule } from '@ionic/angular';

import { WhatIsDwarpalPage } from './what-is-dwarpal.page';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';
import { HomeFooterModule } from '../home/home-footer/home-footer.module';

describe('WhatIsDwarpalPage', () => {
  let component: WhatIsDwarpalPage;
  let fixture: ComponentFixture<WhatIsDwarpalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WhatIsDwarpalPage],
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        HttpClientModule,
        HomeToolbarModule,
        HomeFooterModule,
        IonicModule.forRoot(),
      ],
      providers: [
        Firebase,
        AngularFireMessaging,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(WhatIsDwarpalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
