import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { RouterGuardService } from './misc/router-guard.service';
import { SecurityService } from './home/services/security.service';
import { ResidentService } from './home/services/resident.service';
import { SocietyService } from './home/services/society.service';
import { SettingsService } from './settings/settings.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { NotAutherizedComponent } from './not-autherized/not-autherized.component';
import { DebugService } from './for-developer/debug.service';
import { AuthService } from './home/services/auth.service';

const routes: Routes = [
  { path: '', 
    canActivate: [RouterGuardService],
    loadChildren: './home/home.module#HomePageModule',
  },
  { 
    path: 'home', 
    loadChildren: './home/home.module#HomePageModule',
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { 
    path: 'settings', 
    loadChildren: './settings/settings.module#SettingsPageModule',
    canActivate: [SettingsService], 
  },
  { path: 'security', loadChildren: './home/security/security.module#SecurityPageModule', canActivate: [SecurityService] },
  { path: 'resident', loadChildren: './home/resident/resident.module#ResidentPageModule', canActivate: [ResidentService] },
  { path: 'society', loadChildren: './home/society/society.module#SocietyPageModule', canActivate: [SocietyService] },
  
  { path: 'verification-link', loadChildren: './verify/verify.module#VerifyPageModule' },
  { path: 'forgot-password', loadChildren: './forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'reset-password', loadChildren: './reset-password/reset-password.module#ResetPasswordPageModule' },   
  
  { path: 'for-developer', loadChildren: './for-developer/for-developer.module#ForDeveloperModule', canActivate: [DebugService] },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
  { path: 'support', loadChildren: './support/support.module#SupportPageModule' },
  { path: 'contact', loadChildren: './contactus/contactus.module#ContactusPageModule'  },
  { path: 'pricing', loadChildren: './pricing/pricing.module#PricingPageModule'  },
  { path: 'product', loadChildren: './product/product.module#ProductPageModule' },
  { path: 'sales',  loadChildren: './sales/sales.module#SalesPageModule' },
  { path: 'get-started', loadChildren: './get-started/get-started.module#GetStartedPageModule'  },
  { path: 'what-is-dwarpal', loadChildren: './what-is-dwarpal/what-is-dwarpal.module#WhatIsDwarpalPageModule'  },
  { path: 'jobs', loadChildren: './jobs/jobs.module#JobsPageModule' },
  { path: 'faq', loadChildren: './faq/faq.module#FaqPageModule'  },
  { path: 'tap_policy', loadChildren: './tap-policy/tap-policy.module#TapPolicyPageModule'  },
  { path: 'product_help', loadChildren: './product-help/product-help.module#ProductHelpPageModule' },
  { path: 'trademark', loadChildren: './trademark/trademark.module#TrademarkPageModule' },
  { path: 'welcome', loadChildren: './home/welcome/welcome.module#WelcomePageModule', canActivate: [AuthService]},
  { path: 'network-error', loadChildren: './network-error/network-error.module#NetworkErrorPageModule'  },
  { path: 'not-autherized', component: NotAutherizedComponent},
  //NotFoundComponent should be always last element
  { path: '**', component: NotFoundComponent},
     
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'ignore', enableTracing: false, useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
