import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Firebase } from '@ionic-native/firebase/ngx';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FcmService } from './notifications/fcm.service';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import {WebcamModule} from 'ngx-webcam';
import { HttpConfigInterceptor } from './misc/HttpConfigInterceptor';
import { FCM } from '@ionic-native/fcm/ngx';
import { NotFoundComponent } from './not-found/not-found.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HomeFooterComponent } from './home/home-footer/home-footer.component';
import { NotAutherizedComponent } from './not-autherized/not-autherized.component';
import {NgxImageCompressService} from 'ngx-image-compress';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { LanguagePopoverPage } from './language/language-popover/language-popover.page';
import { LanguagePopoverPageModule } from './language/language-popover/language-popover.module';
import { AdsenseModule } from 'ng2-adsense';
import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db';
import { NgCalendarModule } from 'ionic2-calendar';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { UpdatePopupComponent } from './update-popup/update-popup.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

export function createTranslateLoader(http:HttpClient){
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent, 
    NotFoundComponent,
    NotAutherizedComponent,
    UpdatePopupComponent
  ],
  entryComponents: [
    LanguagePopoverPage,
    UpdatePopupComponent
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  imports: [
    NgCalendarModule,
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule, 
    HttpClientModule, 
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase), 
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    WebcamModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader : {
        provide : TranslateLoader,
        useFactory : (createTranslateLoader),
        deps : [HttpClient]
      }
    }),
    LanguagePopoverPageModule,
    AdsenseModule.forRoot({
      adClient: 'ca-pub-3869986147080543',
      adSlot: 7259870550,
    }),
    // NgxIndexedDBModule.forRoot(dbConfig)
    BrowserAnimationsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Camera,
    File,
    FilePath,
    WebView,
    Firebase,
    FcmService,
    AngularFireMessaging,
    NgxImageCompressService,
    FCM,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    },
    AndroidPermissions,
    CallNumber,
    GooglePlus,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
