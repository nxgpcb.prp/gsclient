import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../core/User';
import { AngularFireAuth } from "@angular/fire/auth";
import { ToasterService } from '../misc/toaster.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient, private afAuth: AngularFireAuth,
    private toaster: ToasterService, private router: Router,) { }

  registerUser(user: any, userType: String, captchaResponse) {

    let headers = new HttpHeaders({
      'CaptchaResponse': captchaResponse,
    });

    if (userType == "Resident") {
      return this.http.post(environment.backend.baseURL + '/GSServer/webapi/Register/Resident', user, { headers: headers });
    } else
      if (userType == "Society") {
        return this.http.post(environment.backend.baseURL + '/GSServer/webapi/Register/Society', user, { headers: headers });
      } else
        if (userType == "Security") {
          return this.http.post(environment.backend.baseURL + '/GSServer/webapi/Register/Security', user, { headers: headers });
        } else {
          return null;
        }
  }

  registerUserWC(user: any, userType: String) {
    if (userType == "Resident") {
      return this.http.post(environment.backend.baseURL + '/GSServer/webapi/RegisterWC/Resident', user);
    } else
      if (userType == "Society") {
        return this.http.post(environment.backend.baseURL + '/GSServer/webapi/RegisterWC/Society', user);
      } else
        if (userType == "Security") {
          return this.http.post(environment.backend.baseURL + '/GSServer/webapi/RegisterWC/Security', user);
        } else {
          return null;
        }
  }

  markUserVerified(user: User) {

    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });

    this.getUser(user)
      .subscribe(
        (response: User) => {
          console.log(response)
          this.setOtpVerfied(response);

        },
        (error) => {
          console.log("error : " + error);
        }
      );


  }

  getUser(user: User) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get(environment.backend.baseURL + '/GSServer/webapi/secured/User/' + user.phone, { headers: headers })
  }

  setOtpVerfied(user: User) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });

    this.http.put(environment.backend.baseURL + '/GSServer/webapi/secured/User', user, { headers: headers })
      .subscribe(
        (response: User) => {
          console.log(response);
        },
        (error) => {
          console.log("error : " + error);
        }
      );
  }

  sendEmailVerificationLink(emailId: string) {
    const actionCodeSettings = {
      // Your redirect URL
      url: environment.domain + '/login',
      handleCodeInApp: true,
    }
    return this.afAuth.auth.currentUser.sendEmailVerification(actionCodeSettings);

  }
}
