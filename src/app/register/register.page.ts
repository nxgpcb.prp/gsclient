import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, NgZone } from '@angular/core';
import { RegisterService } from './register.service';
import { Resident } from '../core/Resident';
import { Society } from '../core/Society';
import { Security } from '../core/Security';
import { FormBuilder, FormGroup, FormControl, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { WindowService } from '../misc/window.service';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { ToasterService } from '../misc/toaster.service';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFireDatabase } from '@angular/fire/database';
import { MustMatch } from '../misc/must-match.validator';
import { LoaderService } from '../misc/loader.service';
import { LoginService } from '../login/login.service';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  userType: any;
  user: any;
  resident: Resident;
  society: Society;
  security: Security;
  form: FormGroup;
  windowRef: any;
  recaptchaVerified: boolean = false;
  formSubmitted: boolean = false;
  captchaResponse: any;

  constructor(
    private registerService: RegisterService,
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private win: WindowService,
    private router: Router,
    private toaster: ToasterService,
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private loader: LoaderService,
    private zone: NgZone,
    private platform: Platform,
    private googlePlus: GooglePlus
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")]],
      recaptcha: ['', Validators.required],
      //need to implement later
      retype_password: ['', Validators.required],
    },
      {
        validator: [MustMatch('password', 'retype_password')]
      });

    //Resident is the default selected user type
    this.userType = "Resident";
    this.resident = new Resident();
    this.user = this.resident;

    //windows reference
    this.windowRef = this.win.windowRef
  }

  resolved(captchaResponse: string) {
    this.zone.run(() => {
      console.log(`Resolved captcha with response: ${captchaResponse}`);
      this.captchaResponse = captchaResponse;
      this.recaptchaVerified = true;
    });
  }

  //This will called when user type changed.
  segmentChanged(ev: any) {
    this.userType = ev.detail.value;
    //set user type
    if (this.userType == "Resident") {
      this.resident = new Resident();
      this.user = this.resident;
    } else
      if (this.userType == "Society") {
        this.society = new Society();
        this.user = this.society;
      } else
        if (this.userType == "Security") {
          this.security = new Security();
          this.user = this.security;
        }
  }

  //to determine ui components only
  isResident() {
    return this.userType == "Resident"
  }

  //to determine ui components only
  isSociety() {
    return this.userType == "Society"
  }

  //to determine ui components only
  isSecurity() {
    return this.userType == "Security"
  }

  loginPage() {
    //Go to login page. Yet to implement....
    this.router.navigate(['/login']);
  }

  //Function for registering user
  register() {

    this.loader.showLoader();
    //arrangement for now
    this.user.date = new Date()

    this.form.disable();
    this.formSubmitted = true;
    if (!this.recaptchaVerified) {
      this.loader.hideLoader();
      return;
    }
    this.user.emailId = this.form.get('email').value;
    this.user.password = this.form.get('password').value;

    this.registerService.registerUser(this.user, this.userType, this.captchaResponse)
      .subscribe(
        (result) => {
          this.afAuth.auth.createUserWithEmailAndPassword(this.user.emailId, this.user.password)
            .then(result => {
              this.loader.hideLoader();
              this.registerService.sendEmailVerificationLink(this.user.emailId)
                .then(result => {
                  localStorage.setItem('currentUserForVerify', JSON.stringify(this.user.emailId));
                  this.router.navigate(['/verification-link']);
                },
                  (error) => {
                    console.log(error)
                    alert("Error occured while sending verification link");
                    this.router.navigate(['/verification-link']);
                  })
            })
            .catch(
              (error) => {
                console.log(error)
                this.loader.hideLoader();
                if (error.code === "auth/network-request-failed") {
                  window.alert('You do not have active internet connection!!!');
                } else if (error.code === "auth/email-already-in-use") {
                  window.alert('This email id is already registered');
                } else {
                  window.alert('Something went wrong');
                }
              }
            );
        },
        (error) => {
          console.log(error)
          this.loader.hideLoader();
        }
      );

    this.form.enable()

  }

  googleRegister() {
    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
      this.nativeGoogleRegister();
    } else {
      this.webGoogleRegister();
    }
  }

  webGoogleRegister() {
    try {
      this.loader.showLoader();
      const provider = new firebase.auth.GoogleAuthProvider();
      this.afAuth.auth.signInWithPopup(provider)
        .then(result => {
          result.user.getIdToken(false).then(
            idToken => {
              idToken = idToken;

              this.user.emailId = result.user.email;
              this.registerService.registerUserWC(this.user, this.userType)
                .subscribe(
                  (res) => {

                    if (result.user.emailVerified !== true) {
                      this.registerService.sendEmailVerificationLink(result.user.email);
                      this.loader.hideLoader()
                      window.alert('Please validate your email address. Kindly check your inbox.');
                    }
                    else {
                      this.loginService.loginToDb()
                        .subscribe(
                          (response: any) => {
                            this.user = response;
                            localStorage.setItem('currentUserId', this.user.id);
                            if (this.user.name != null) {
                              if (this.user.type == "Resident") {
                                this.loader.hideLoader();
                                this.router.navigate(['/resident/home/logs'], { replaceUrl: true });
                              }
                              if (this.user.type == "Security") {
                                this.loader.hideLoader();
                                this.router.navigate(['/security/dashboard/visitor-logs'], { replaceUrl: true });
                              }
                              if (this.user.type == "Society") {
                                this.loader.hideLoader();
                                this.router.navigate(['/society/facilities/apartment'], { replaceUrl: true });
                              }
                            } else {
                              this.loader.hideLoader();
                              this.router.navigate(['/welcome'], { replaceUrl: true });
                            }
                          },
                          (error) => {
                            this.loader.hideLoader();
                            this.router.navigate(['/login'], { replaceUrl: true });
                          }
                        );
                    }
                  });
            });
        })
        .catch(error => {
          this.loader.hideLoader()
          console.log(error);
          if (error.code === "auth/network-request-failed") {
            window.alert('You do not have active internet connection!!!');
          } else if (error.code === "auth/wrong-password" || error.code === "auth/user-not-found" || error.code === "auth/invalid-email") {
            window.alert('Username or Password is incorrect');
          } else if (error.code === "auth/user-disabled") {
            window.alert('Your account has been disabled');
          } else {
            window.alert('Something went wrong');
          }
        });
    } catch (error) {
      this.loader.hideLoader()
      console.log(error)
    };
  }

  async nativeGoogleRegister() {
    try {
      this.loader.showLoader();
      console.log("nativeGoogleLogin : ");
      const gplusUser = await this.googlePlus.login({
        'webClientId': environment.webClientId,
        'offline': true,
        'scopes': 'profile email'
      })
      this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken))
        .then(result => {
          result.user.getIdToken(false).then(
            idToken => {
              idToken = idToken;

              this.user.emailId = result.user.email;
              this.registerService.registerUserWC(this.user, this.userType)
                .subscribe(
                  (res) => {

                    if (result.user.emailVerified !== true) {
                      this.registerService.sendEmailVerificationLink(result.user.email);
                      this.loader.hideLoader()
                      window.alert('Please validate your email address. Kindly check your inbox.');
                    }
                    else {
                      this.loginService.loginToDb()
                        .subscribe(
                          (response: any) => {
                            this.user = response;
                            localStorage.setItem('currentUserId', this.user.id);
                            if (this.user.name != null) {
                              if (this.user.type == "Resident") {
                                this.loader.hideLoader();
                                this.router.navigate(['/resident/home/logs'], { replaceUrl: true });
                              }
                              if (this.user.type == "Security") {
                                this.loader.hideLoader();
                                this.router.navigate(['/security/dashboard/visitor-logs'], { replaceUrl: true });
                              }
                              if (this.user.type == "Society") {
                                this.loader.hideLoader();
                                this.router.navigate(['/society/facilities/apartment'], { replaceUrl: true });
                              }
                            } else {
                              this.loader.hideLoader();
                              this.router.navigate(['/welcome'], { replaceUrl: true });
                            }
                          },
                          (error) => {
                            this.loader.hideLoader();
                            this.router.navigate(['/login'], { replaceUrl: true });
                          }
                        );
                    }
                  });
            });
        })
        .catch(error => {
          this.loader.hideLoader()
          console.log(error);
          if (error.code === "auth/network-request-failed") {
            window.alert('You do not have active internet connection!!!');
          } else if (error.code === "auth/wrong-password" || error.code === "auth/user-not-found" || error.code === "auth/invalid-email") {
            window.alert('Username or Password is incorrect');
          } else if (error.code === "auth/user-disabled") {
            window.alert('Your account has been disabled');
          } else {
            window.alert('Something went wrong');
          }
        });
    } catch (error) {
      this.loader.hideLoader()
      console.log(error)
    };
  }

  resendLoginCode() {

    this.registerService.sendEmailVerificationLink(JSON.parse(localStorage.getItem('currentUserForVerify')));

  }

}
