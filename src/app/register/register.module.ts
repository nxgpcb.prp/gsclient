import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RegisterPage } from './register.page';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';
import { TranslateModule } from '@ngx-translate/core';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

const routes: Routes = [
  {
    path: '',
    component: RegisterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    HomeToolbarModule,
    RouterModule.forChild(routes),
    TranslateModule,
    RecaptchaModule,
    RecaptchaFormsModule,
  ],
  declarations: [RegisterPage],
  entryComponents: [
    HomePopoverMenuComponent
  ],
})
export class RegisterPageModule {}
