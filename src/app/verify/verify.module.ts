import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { VerifyPage } from './verify.page';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';

const routes: Routes = [
  {
    path: '',
    component: VerifyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeToolbarModule,
    RouterModule.forChild(routes),
    TranslateModule,
  ],
  declarations: [VerifyPage],
  entryComponents: [
    HomePopoverMenuComponent
  ],
})
export class VerifyPageModule {}
