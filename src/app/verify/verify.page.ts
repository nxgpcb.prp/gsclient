import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { RegisterService } from '../register/register.service';
import { ToasterService } from '../misc/toaster.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.scss'],
})
export class VerifyPage implements OnInit {

  constructor(private router:Router, private registerService: RegisterService,
    private toaster:ToasterService) { }

  ngOnInit() {
  }

  resend(){
    this.registerService.sendEmailVerificationLink(JSON.parse(localStorage.getItem('currentUserForVerify')))
    .then(result =>{
      this.toaster.toast("Verification link to sent to your email address.")
    },
    (error) => {
      console.log(error)
      alert("Error occured while sending verification link");
    })          
  }

  loginPage(){
    this.router.navigate(['/login']);
  }
}
