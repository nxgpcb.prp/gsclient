import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TapPolicyPage } from './tap-policy.page';
import { Routes, RouterModule } from '@angular/router';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';
import { HomeFooterModule } from '../home/home-footer/home-footer.module';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';

const routes: Routes = [
  {
    path: '',
    component: TapPolicyPage
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeToolbarModule,
    HomeFooterModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [TapPolicyPage],
  entryComponents: [
    HomePopoverMenuComponent
  ],
})
export class TapPolicyPageModule {}
