import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ForgotPasswordPage } from './forgot-password.page';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';

const routes: Routes = [
  {
    path: '',
    component: ForgotPasswordPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    HomeToolbarModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ForgotPasswordPage],
  entryComponents: [
    HomePopoverMenuComponent
  ],
})
export class ForgotPasswordPageModule {}
