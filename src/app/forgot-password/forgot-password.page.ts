import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { ToasterService } from '../misc/toaster.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  error:boolean = false;
  success:boolean = false;
  form: FormGroup;

  constructor(public toasterService: ToasterService, private router:Router,
    private loginService:LoginService, private formBuilder: FormBuilder, private afAuth:AngularFireAuth) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required]],
    });
  }

  sendForgotPassword(){
    if(!this.success){
      const actionCodeSettings = {
        url: environment.domain+"/login", 
        handleCodeInApp: true,
      };

      this.afAuth.auth.sendPasswordResetEmail(this.form.get('email').value, actionCodeSettings)
        .then(result => {
          alert('Reset password using email link. You will redirect to Login Page.');
          localStorage.removeItem('currentUserIdToken');
          //Remove the history after signout
          this.success = true;
          this.router.navigate(['/login'], { replaceUrl: true });
        })
        .catch(error => {
          alert(error);
          this.error = true;
        });
      }
  }

  loginPage() {
    //Go to login page. Yet to implement....
    this.router.navigate(['/login']);
  }

}
