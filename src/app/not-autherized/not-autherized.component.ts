import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-autherized',
  templateUrl: './not-autherized.component.html',
  styleUrls: ['./not-autherized.component.scss'],
})
export class NotAutherizedComponent implements OnInit {

  url: any;
  constructor(private router: Router) {
    this.url = this.router.url;
  }

  ngOnInit() { }

}
