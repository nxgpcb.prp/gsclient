import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FaqPage } from './faq.page';
import { RouterModule, Routes } from '@angular/router';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';
import { HomeFooterModule } from '../home/home-footer/home-footer.module';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';
import { FrequentlyAskedComponent } from './frequently-asked/frequently-asked.component';
import { FrequentlyAskedModule } from './frequently-asked/frequently-asked.module';

const routes: Routes = [
  {
    path: '',
    component: FaqPage
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeToolbarModule,
    HomeFooterModule,
    FrequentlyAskedModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [FaqPage],
  entryComponents: [
    HomePopoverMenuComponent
  ],
})
export class FaqPageModule {}
