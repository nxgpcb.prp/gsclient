import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FrequentlyAskedComponent } from './frequently-asked.component';
 

@NgModule({
  declarations: [FrequentlyAskedComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule

  ],
  exports: [
    FrequentlyAskedComponent
  ]
})
export class FrequentlyAskedModule { }
