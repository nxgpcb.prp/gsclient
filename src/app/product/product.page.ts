import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  constructor(private meta:Meta, private title:Title) {
    //For Seo
    title.setTitle('Product - Dwarpal - Society Security and Management App');
    
    meta.addTag({ name: 'keywords', 
    content: 'Dwarpal, My Gate, MyGate Security App, Apartment Security, Society Security, Society Management'
  + 'Android App , Security platform, dwarpal.co.in, Made in India, Dwarpal is a security management platform' + 
  ' Resident, Owner, Family Member, Safe, Visitor, Visitor Log, Delivery Management, Daily Visitors' + 
  ' Tenant, Society Notices, Society Collabration, Amenities Management, Amenities Booking, About, Web, WebApp'+
  ' Gated Security App, Pune, Startup, Free App, Ios app, Door Keeper, Protector'});

    meta.addTag({ name: 'description', content: 'Dwarpal is a Society, Security and Management Platform.'+
    'Dwarpal Technologies is the startup created by 3 idiots that wanted to start something on their own.' +
    ' Dwarpal was the idea generated from the need to manage own Society and protect the family from'+
    ' unwanted visitors and lazy security guards. Dwarpal is the platform created by Dwarpal Technologies Pvt Ltd.'+
    ' which was first designed to manage society\'s visitors and their logs but in the period of development'+
    ' and design, we decided to make Dwarpal as a platform to manage the entire society.'+
    ' In the Dwarpal Technologies, we believe in the world everything is open source and everyone can learn,'+
    ' contribute and gain from it but at the same time, we want to build our company and business.'+
    ' So the Dwarpal core server will be released as open-source and the Dwarpal dashboard is released'+
    ' with a paid subscription model.' });
   }

  ngOnInit() {
  }

}
