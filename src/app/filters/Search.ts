export class Search {

  method: String;
  parameter: String;
  query: any;

  constructor(...args: [method?:String, parameter?:String, query?:any]) {
    this.method = args[0];
    this.parameter = args[1];
    this.query = args[2];
  }
  
    fromJSON(json) {
      for (var propName in json)
          this[propName] = json[propName];
      return this;
  }
}