import { Sort } from "./Sort";
import { Paging } from "./Paging";
import { Search } from "./Search";

export class Filter {


  sort: Sort;
  paging: Paging;
  search: Search;

  constructor()
  {
    this.paging=new Paging();
    this.sort=new Sort();
    this.search=new Search();

    this.paging.start=0;
    this.paging.size=10;

    this.sort.parameter="createdOn";
    this.sort.method="DESCENDING";
  }

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}