export class Sort {

  method: String;
  parameter: String;

    fromJSON(json) {
      for (var propName in json)
          this[propName] = json[propName];
      return this;
  }
}