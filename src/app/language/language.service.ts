import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { LanguagePopoverPage } from './language-popover/language-popover.page';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

const LANG_KEY = 'SELECTED_LANGUAGE';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  
  selected = '';

  constructor(private translate:TranslateService, private storage:Storage,
    private router: Router, private popoverController: PopoverController) { }

  setInitialAppLanguage(){
    this.storage.get(LANG_KEY).then(val => {
      if(val){
        console.log(val)
        this.setLanguage(val);
        this.selected = val;
      }else{
        let language = this.translate.getBrowserLang();
        this.translate.setDefaultLang(language);
        console.log(language)
        this.selected = language;
      }
    });
  }

  getLanguages(){
    return [
      { text: 'English', value: 'en'},
      { text: 'Hindi', value: 'hi'},
      { text: 'Marathi', value: 'mr'}
    ]
  }

  setLanguage(lang){
    this.translate.use(lang.value);
    this.selected = lang;
    this.storage.set(LANG_KEY, lang)
  }

}
