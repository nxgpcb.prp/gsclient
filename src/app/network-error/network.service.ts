import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(private router:Router) { }

  checkIfOnline(){
    if(!navigator.onLine){
      this.router.navigateByUrl('/network-error',{skipLocationChange:true});
    }
  }

  presentNetworkError(){
    this.router.navigateByUrl('/network-error',{skipLocationChange:true});
  }
}
