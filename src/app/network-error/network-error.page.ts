import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoaderService } from '../misc/loader.service';

@Component({
  selector: 'app-network-error',
  templateUrl: './network-error.page.html',
  styleUrls: ['./network-error.page.scss'],
})
export class NetworkErrorPage implements OnInit {

  constructor(private loader:LoaderService) { }

  ngOnInit() {
    this.loader.hideLoader();
  }

  retry(){
    window.location.reload();
  }
}
