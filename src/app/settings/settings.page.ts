import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from '../home/home.service';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  user:any;
  type:String = ""
  
  constructor(private router: Router, private loginService:LoginService,
    private homeService:HomeService) {
   }

  async ngOnInit() {
    this.user = await this.loginService.getUser();
    this.type = this.user.type;
  }

}
