import { Component, OnInit } from '@angular/core';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit, OnInAppUpdateInterface {

  pages =
    [
      {
        title: 'FAQ',
        url: '/faq'
      },
      {
        title: 'Contact Us',
        url: '/support'
      },
      {
        title: 'Terms and Privacy Policy',
        url: '/tap_policy'
      },
      //#Launch
      // {
      //   title: 'Licenses',
      //   url: '/settings/help/licenses'
      // },
    ]

  constructor(private platform: Platform, ) { }

  ngOnInit() {
    this.ngOnInAppUpdate()
  }

  ngOnInAppUpdate(): void {

  }

  refresh(event) {
    setTimeout(() => {

      if(event != null){
        event.target.complete();
      }
    }, 2000);
  }
}
