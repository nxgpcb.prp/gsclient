import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SettingsPage } from './settings.page';
import { ResidentSettingsComponent } from '../settings/resident-settings/resident-settings.component';
import { SocietySettingsComponent } from '../settings/society-settings/society-settings.component';
import { SecuritySettingsComponent } from '../settings/security-settings/security-settings.component';
import { WorkspaceToolbarModule } from '../home/workspace-toolbar/workspace-toolbar.module';
import { PopoverMenuComponent } from '../home/popover-menu/popover-menu.component';


const routes: Routes = [
  {
    path: '',
    component: SettingsPage,
    children:
    [
      { 
        path: '', loadChildren: './profile/profile.module#ProfilePageModule'
      },
      { 
        path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule'
      },
      { 
        path: 'account', loadChildren: './account/account.module#AccountPageModule'
      },
      { 
        path: 'notifications', loadChildren: './notifications/notifications.module#NotificationsPageModule'
      },
      { 
        path: 'help', loadChildren: './help/help.module#HelpPageModule' 
      },
      { 
        path: 'invite-a-friend', loadChildren: './invite-a-friend/invite-a-friend.module#InviteAFriendPageModule' 
      },
      { 
        path: 'password', loadChildren: './password/password.module#PasswordPageModule' 
      },
      {
        path: 'themes', loadChildren: './themes/themes.module#ThemesPageModule'
      },
    ]
  },

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    WorkspaceToolbarModule
  ],
  declarations: [
    SettingsPage, 
    ResidentSettingsComponent, 
    SocietySettingsComponent,
    SecuritySettingsComponent,
  ],
  entryComponents: [
    PopoverMenuComponent
  ],
})
export class SettingsPageModule {}
