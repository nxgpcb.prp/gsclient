import { Component, OnInit } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { RouterEvent, Router } from '@angular/router';
import { SettingsService } from '../settings.service';
import { Society } from 'src/app/core/Society';
import { HomeService } from 'src/app/home/home.service';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-society-settings',
  templateUrl: './society-settings.component.html',
  styleUrls: ['./society-settings.component.scss']
})
export class SocietySettingsComponent implements OnInit, OnInAppUpdateInterface {
  pages =
    [
      {
        title: 'Profile',
        url: '/settings/profile'
      },
      {
        title: 'Account',
        url: '/settings/account'
      },
      {
        title : 'Themes',
        url : '/settings/themes'
      },
      {
        title: 'Password',
        url: '/settings/password'
      },
      {
        title: 'Notifications',
        url: '/settings/notifications'
      },
      {
        title: 'Help',
        url: '/settings/help'
      },
      //#Launch
      // {
      //   title: 'Invite A Friend',
      //   url: '/settings/invite-a-friend'
      // }
    ]
    selectedPath = '/settings/profile';
  society: Society;

  constructor(private platform: Platform, private router: Router, private menu: MenuController,
    private settingsService: SettingsService, private loginService: LoginService) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });

  }

  async ngOnInit() {
    this.ngOnInAppUpdate();
    
      this.society = await this.loginService.getUser();
    
  }

  ngOnInAppUpdate(): void {

  }

  refresh(event) {
    setTimeout(async () => {
      
        this.society = await this.loginService.getUser();
      
      if (event != null) {
        event.target.complete();
      }
    }, 2000);
  }

}
