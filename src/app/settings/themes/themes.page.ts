import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/theme/theme.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.page.html',
  styleUrls: ['./themes.page.scss'],
})
export class ThemesPage implements OnInit {

  isDark:any;

  constructor(
    private theme: ThemeService,
    private storage: Storage,
    ) {
      this.storage.get('themeName').then(themeName => {
        if(themeName == "dark"){
          this.isDark = true;
        }else{
          this.isDark = false;
        }
      });
    }

  changeTheme() {
    this.theme.setAppTheme(this.isDark);
  }

  ngOnInit() {
  }

}
