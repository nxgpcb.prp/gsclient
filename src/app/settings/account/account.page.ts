import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { SettingsService } from '../settings.service';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Platform } from '@ionic/angular';
import { HomeService } from 'src/app/home/home.service';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit, OnInAppUpdateInterface {

  user: any;
  deleteAccountInProgress: boolean = false;

  constructor(private platform: Platform, private afAuth: AngularFireAuth, private router: Router,
    private settingsService: SettingsService, private loginService: LoginService, ) { }

  async ngOnInit() {
    this.ngOnInAppUpdate()
    this.user = await this.loginService.getUser()
  }

  areYouSure(){
    this.deleteAccountInProgress = true
  }

  deleteAccountCancel(){
    this.deleteAccountInProgress = false
  }

  deleteAccount() {
    let type = this.user.type;
    if (type == "Resident") {
      this.settingsService.deleteResident(this.user.id).
        subscribe(
          response => {
            console.log(response)
            this.router.navigate(['/home'])
          },
          error => {
            console.log(error)
          }
        )
    } else
      if (type == "Security") {
        this.settingsService.deleteSecurity(this.user.id).
          subscribe(
            response => {
              console.log(response)
              this.router.navigate(['/home'])
            },
            error => {
              console.log(error)
            }
          )
      } else
        if (type == "Society") {
          this.settingsService.deleteSociety(this.user.id).
            subscribe(
              response => {
                console.log(response)
                this.router.navigate(['/home'])
              },
              error => {
                console.log(error)
              }
            )
        } else {
          this.router.navigate(['/login']);
        }

  }

  ngOnInAppUpdate(): void {

  }

  refresh(event) {
    setTimeout(() => {

      if (event != null) {
        event.target.complete();
      }
    }, 2000);
  }
}
