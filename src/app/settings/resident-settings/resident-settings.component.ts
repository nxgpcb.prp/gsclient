import { Component, OnInit } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { RouterEvent, Router } from '@angular/router';
import { SettingsService } from '../settings.service';
import { Resident } from 'src/app/core/Resident';
import { HomeService } from 'src/app/home/home.service';
import { ResidentService } from 'src/app/home/services/resident.service';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { LoginService } from 'src/app/login/login.service';


@Component({
  selector: 'app-resident-settings',
  templateUrl: './resident-settings.component.html',
  styleUrls: ['./resident-settings.component.scss']
})
export class ResidentSettingsComponent implements OnInit, OnInAppUpdateInterface {
  pages =
    [
      {
        title: 'Profile',
        url: '/settings/profile'
      },
      {
        title: 'Account',
        url: '/settings/account'
      },
      {
        title: 'Themes',
        url: '/settings/themes'
      },
      {
        title: 'Password',
        url: '/settings/password'
      },
      {
        title: 'Notifications',
        url: '/settings/notifications'
      },
      {
        title: 'Help',
        url: '/settings/help'
      },
      //#Launch
      // {
      //   title : 'Invite A Friend',
      //   url : '/settings/invite-a-friend'
      // }
    ]
  selectedPath = '/settings/profile';
  resident: Resident = new Resident();

  constructor(private platform: Platform, private router: Router, private menu: MenuController,
    private settingsService: SettingsService, private loginService: LoginService, private Service: ResidentService) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  async ngOnInit() {
    this.ngOnInAppUpdate();

    this.resident = await this.loginService.getUser();

  }

  ngOnInAppUpdate(): void {

  }

  async refresh(): Promise<void> {

    this.resident = await this.loginService.getUser();


  }
}
