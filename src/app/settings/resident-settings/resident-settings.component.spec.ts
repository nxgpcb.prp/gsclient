import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { IonicModule } from '@ionic/angular';

import { ResidentSettingsComponent } from './resident-settings.component';
import { HomeService } from 'src/app/home/home.service';
import { Resident } from 'src/app/core/Resident';

describe('ResidentSettingsComponent', () => {
  let component: ResidentSettingsComponent;
  let fixture: ComponentFixture<ResidentSettingsComponent>;
  let de: DebugElement;
  let homeService: HomeService;
  let spy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResidentSettingsComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase), 
        AngularFirestoreModule,
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        HttpClientModule,
        IonicModule
      ],
      providers:[
        Firebase,
        AngularFireMessaging,
        HomeService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentSettingsComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    homeService = de.injector.get(HomeService);
    let user: Resident = new Resident();
    user.type = "Resident"

    spy = spyOn(homeService, 'getUser').and.returnValue(user);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
