import { Component, OnInit } from '@angular/core';
import { Security } from 'src/app/core/Security';
import { Router, RouterEvent } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
import { HomeService } from 'src/app/home/home.service';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-security-settings',
  templateUrl: './security-settings.component.html',
  styleUrls: ['./security-settings.component.scss']
})
export class SecuritySettingsComponent implements OnInit, OnInAppUpdateInterface {

  pages =
    [
      {
        title: 'Profile',
        url: '/settings/profile'
      },
      {
        title: 'Account',
        url: '/settings/account'
      },
      {
        title : 'Themes',
        url : '/settings/themes'
      },
      {
        title: 'Password',
        url: '/settings/password'
      },
      {
        title: 'Notifications',
        url: '/settings/notifications'
      },
      {
        title: 'Help',
        url: '/settings/help'
      },
      //#Launch
      // {
      //   title: 'Invite A Friend',
      //   url: '/settings/invite-a-friend'
      // }
    ]
    selectedPath = '/settings/profile';
  security: Security;

  constructor(private platform: Platform, private router: Router, private menu: MenuController,
    private loginService: LoginService, ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  async ngOnInit() {
    this.ngOnInAppUpdate()
    
      this.security = await this.loginService.getUser();
  }

  ngOnInAppUpdate(): void {

  }

  refresh(event) {
    setTimeout(async () => {
      
        this.security = await this.loginService.getUser();

      if (event != null) {
        event.target.complete();
      }
    }, 2000);
  }

}
