import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Resident } from '../core/Resident';
import { Security } from '../core/Security';
import { Society } from '../core/Society';
import { User } from '../core/User';
import { HomeService } from '../home/home.service';
import { LoginService } from '../login/login.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoaderService } from '../misc/loader.service';
import { environment } from 'src/environments/environment';
import { Address } from '../core/Address';

@Injectable({
  providedIn: 'root'
})
export class SettingsService implements CanActivate {
  user: any;

  constructor(private router: Router, private http: HttpClient, private loginService: LoginService,
    private afAuth: AngularFireAuth, private loader: LoaderService) {

  }

  getResident() {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Resident>(environment.backend.baseURL + '/GSServer/webapi/secured/User', { headers: headers })
  }

  getSecurity() {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Security>(environment.backend.baseURL + '/GSServer/webapi/secured/User', { headers: headers })
  }

  getSociety() {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get<Society>(environment.backend.baseURL + '/GSServer/webapi/secured/User', { headers: headers })
  }

  updateResident(resident: Resident) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<Resident>(environment.backend.baseURL + '/GSServer/webapi/secured/Resident', resident, { headers: headers })
  }

  updateSecurity(secuirty: Security) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<Security>(environment.backend.baseURL + '/GSServer/webapi/secured/Security', secuirty, { headers: headers })
  }

  updateSociety(society: Society) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<Society>(environment.backend.baseURL + '/GSServer/webapi/secured/Society', society, { headers: headers })
  }

  deleteResident(id:number) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.delete<Boolean>(environment.backend.baseURL + '/GSServer/webapi/secured/Resident/'+id, { headers: headers })
  }

  deleteSecurity(id:number) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.delete<Boolean>(environment.backend.baseURL + '/GSServer/webapi/secured/Security/'+id, { headers: headers })
  }

  deleteSociety(id:number) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.delete<Boolean>(environment.backend.baseURL + '/GSServer/webapi/secured/Society/'+id, { headers: headers })
  }

  updateProfile(profilePic: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<User>(environment.backend.baseURL + '/GSServer/webapi/secured/User/ProfilePicture', profilePic, { headers: headers })
  }

  updateName(name: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<User>(environment.backend.baseURL + '/GSServer/webapi/secured/User/Name', name, { headers: headers })
  }

  addStatus(status: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.post<User>(environment.backend.baseURL + '/GSServer/webapi/secured/User/Status', status, { headers: headers })
  }

  updateStatus(status: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<User>(environment.backend.baseURL + '/GSServer/webapi/secured/User/Status', status, { headers: headers })
  }

  removeStatus() {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.delete<User>(environment.backend.baseURL + '/GSServer/webapi/secured/User/Status', { headers: headers })
  }

  updateAddress(address: Address) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put<User>(environment.backend.baseURL + '/GSServer/webapi/secured/Society/Address', address, { headers: headers })
  }

  updatePhoneNumber(phone: String) {
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.put(environment.backend.baseURL + '/GSServer/webapi/secured/User/PhoneNumber', phone, { headers: headers })
  }

  canActivate(route: ActivatedRouteSnapshot): any {
    console.log("Settings Service");
    this.loader.showLoader();
    return new Promise(res => {
      this.loginService.getUser()
        .then(
          (response: any) => {
            this.loader.hideLoader();
            console.log(response)
            
            res(true);
          },
          (error) => {
            this.loader.hideLoader();
            this.router.navigate(['/login']);
            res(false);
          }
        );
    })
  }

  isLoggedIn() {
    return this.afAuth.auth.onAuthStateChanged(function (user) {
      return user;
    });
  }
}
