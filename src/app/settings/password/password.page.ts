import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/misc/must-match.validator';
import { ToasterService } from 'src/app/misc/toaster.service';
import { SettingsService } from '../settings.service';
import { WindowService } from 'src/app/misc/window.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit, OnInAppUpdateInterface {

  form: FormGroup;

  constructor(private platform: Platform, private formBuilder: FormBuilder, private toaster: ToasterService,
    private settingsService: SettingsService, private win: WindowService, private router: Router,
    private afAuth: AngularFireAuth, ) { }

  ngOnInit() {
    this.ngOnInAppUpdate()
    this.form = this.formBuilder.group({
      current_password: ['', Validators.required],
      password: ['', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")]],
      retype_password: ['', Validators.required],
    },
      {
        validator: [MustMatch('password', 'retype_password')]
      });
  }

  changePassaword() {
    this.afAuth.auth.currentUser.updatePassword(this.form.get('password').value).
      then(
        result => {
            alert("Password Changed Successfully. You will redirect to Login Page.");
            localStorage.removeItem('currentUserIdToken');
            //Remove the history after signout
            this.router.navigate(['/login'], { replaceUrl: true });
        }
      ).catch(
        error => {
          alert(error);
          this.router.navigate(['/login']);
        }
      )


  }

  forgotPassword() {

  }

  ngOnInAppUpdate(): void {

  }

  refresh(event) {
    setTimeout(() => {

      if(event != null){
        event.target.complete();
      }
    }, 2000);
  }

}
