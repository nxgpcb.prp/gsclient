import { Component, OnInit, Input, OnChanges, SimpleChanges, DoCheck, NgZone, OnDestroy, ViewChild } from '@angular/core';
import { CameraService } from 'src/app/camera/camera.service';
import { SettingsService } from '../settings.service';
import { Platform, ModalController } from '@ionic/angular';
import { WebcamModalComponent } from 'src/app/camera/webcam-modal/webcam-modal.component';
import { WebcamImage } from 'ngx-webcam';
import { ToasterService } from 'src/app/misc/toaster.service';
import { WindowService } from 'src/app/misc/window.service';
import * as firebase from 'firebase';
import { HomeService } from 'src/app/home/home.service';
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { WorkspaceToolbarService } from 'src/app/home/workspace-toolbar/workspace-toolbar.service';
import { NgxImageCompressService } from 'ngx-image-compress';
import { AngularFireAuth } from '@angular/fire/auth';
import { Address } from 'src/app/core/Address';
import { FcmService } from 'src/app/notifications/fcm.service';
import { Router } from '@angular/router';
import { Resident } from 'src/app/core/Resident';
import { Security } from 'src/app/core/Security';
import { Society } from 'src/app/core/Society';
import { LoginService } from 'src/app/login/login.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit, OnInAppUpdateInterface, DoCheck, OnDestroy {

  user: any;
  phone: string;
  emailid: string;
  flat_no: string;
  currentImage: any = "https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y";
  public webcamImage: WebcamImage = null;
  verficationCodeSent: boolean = false;
  verificationCode: string;
  windowRef: any;
  verificationId: any;
  phoneInputChanged: boolean = false;
  nameInputChanged: boolean = false;
  statusInputChanged: boolean = false;
  profileImageUploadInprogress: boolean = false;
  updateNameInProgress: boolean = false;
  updateStatusInProgress: boolean = false;
  sendVerificationCodeInProgress: boolean = false;
  countryCode: string = "+91";
  isMobileDevice: boolean = false;
  areaChanged: boolean = false;
  cityChanged: boolean = false;
  stateChanged: boolean = false;
  pincodeChanged: boolean = false;

  constructor(private cameraService: CameraService,
    private settingsService: SettingsService, public toasterService: ToasterService, private ngZone: NgZone,
    public platform: Platform, private modalController: ModalController, private loginService: LoginService,
    private window: WindowService, private workspaceToolbarService: WorkspaceToolbarService,
    private imageCompress: NgxImageCompressService, private afAuth:AngularFireAuth,private fcm: FcmService,
    private router:Router) {
    if ((this.platform.is('android') || this.platform.is('ios'))) {
      this.isMobileDevice = true;
    }
  }

  async ngOnInit() {
    this.ngOnInAppUpdate();
    this.windowRef = this.window.windowRef
    this.user = await this.loginService.getUser()
  }

  ngDoCheck() {
    this.sendVerificationCodeInProgress;
    this.verficationCodeSent;
  }

  takePicture() {
    if (this.isMobileDevice) {
      this.cameraService.getPicture().then((imageData) => {
        console.log("Native Image : " + imageData)
        this.user.profilePicture.content = 'data:image/jpeg;base64,' + imageData.base64String;
        //not working yet...
        this.updateProfile(this.user.profilePicture.content);
      }, (err) => {
        // Handle error
        window.alert("Unable to open Camera :" + err);
      });
    } else {
      this.openWebcam();
    }
  }

  selectPicture(){
    if(this.isMobileDevice){
      this.selectPictureMobile();
    }else{
      this.selectPictureDesktop();
    }
  }

  selectPictureMobile() {
    this.cameraService.selectPicture().then((imageData) => {
      console.log("Native Image : " + imageData.base64String)

      this.user.profilePicture.content = 'data:image/jpeg;base64,' + imageData.base64String;

      let byteLength = imageData.base64String.length * 0.75;
      if (byteLength > 51200) {
        window.alert("Image size is greate than 50KB. Please select smaller image.");
        return
      }
      
      //not working yet...
      this.updateProfile(this.user.profilePicture.content);
    }, (err) => {
      // Handle error
      window.alert("Unable to select image :" + err);
    });
  }

  selectPictureDesktop() {

    this.imageCompress.uploadFile().then(({ image, orientation }) => {

      this.imageCompress.compressFile(image, orientation, 50, 50).then(
        result => {
          console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
          // after here 'file' can be accessed and used for further process
          // Abort uploading if file size is greater than 50KB
          if (this.imageCompress.byteCount(result) > 51200) {
            window.alert("Image size is greate than 50KB. Please select smaller image.");
            return
          }
          this.user.profilePicture.content = result;
          console.log(this.user.profilePicture.content);
          this.updateProfile(this.user.profilePicture.content)
        }
      );
    });
  }

  removePicture() {
    if(this.user.type === 'Resident'){
      let resident:Resident = new Resident()
      this.user.profilePicture.content = resident.profilePicture.content;
    }
    if(this.user.type === 'Security'){
      let security:Security = new Security()
      this.user.profilePicture.content = security.profilePicture.content;
    }
    if(this.user.type === 'Society'){
      let society:Society = new Society()
      this.user.profilePicture.content = society.profilePicture.content;
    }
    this.updateProfile(this.user.profilePicture.content.toString());
  }

  async openWebcam() {
    const modal = await this.modalController.create({
      component: WebcamModalComponent,
      componentProps: {
        "modalController": this.modalController,
      },
    });
    modal.onDidDismiss()
      .then((data) => {
        if (data['data'] != null) {
          this.user.profilePicture.content = data['data']; // Here's your selected user!
          this.updateProfile(this.user.profilePicture.content);
        }
      });
    return await modal.present();

  }

  handleImage(webcamImage: WebcamImage) {
    this.webcamImage = webcamImage;
  }

  updateProfile(profilePic: String) {
    this.profileImageUploadInprogress = true;
    this.settingsService.updateProfile(profilePic).
      subscribe(
        (response) => {
          this.workspaceToolbarService.updateUser(response);
          this.profileImageUploadInprogress = false;
          console.log(response);
        },
        (error) => {
          this.profileImageUploadInprogress = false;
          console.log(error);
        }
      )
  }

  sendVerificationCode() {
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sendVerificationCodeButton', {
      'size': 'invisible',
      'callback': function (response) {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        console.log(response)
      }
    });

    this.linkWithPhoneNumber();
  }

  linkWithPhoneNumber() {
    this.sendVerificationCodeInProgress = true;
    var phoneNumber = this.countryCode + this.user.phone;
    var appVerifier = this.windowRef.recaptchaVerifier;
    this.afAuth.auth.currentUser.linkWithPhoneNumber(phoneNumber, appVerifier)
      .then(confirmationResult => {
        this.windowRef.confirmationResult = confirmationResult;
        this.sendVerificationCodeInProgress = false;
        console.log("verficationCodeSent")
        this.verficationCodeSent = true;

        //Forcefully update the ui as it is not updating properly.
        this.ngZone.run(() => {
          this.verficationCodeSent = true;
        });
      }).catch(error => {
        this.sendVerificationCodeInProgress = false;
        console.log(error);
        if (error.code === "auth/invalid-phone-number") {
          window.alert('Add country code +91');
        }
        if (error.code == "auth/too-many-requests") {
          window.alert('You are blocked due to Too many rquests. Please try after some time.');
        }
      });
  }

  verify() {
    var code = this.verificationCode;
    this.windowRef.confirmationResult.confirm(code).then(
      result => {
        //For now we will update phone number withou country code. 
        //So that we will not have to do additional processing after fetching phone number from database.
        this.settingsService.updatePhoneNumber(this.user.phone)
          .subscribe(response => {
            console.log(response);
            this.phoneInputChanged = false;
          },
            error => {
              console.log(error);
            })
      }).catch(function (error) {
        console.log(error);
      });
  }

  phoneChanged() {
    this.phoneInputChanged = true;
  }

  nameChanged() {
    this.nameInputChanged = true;
  }

  updateName() {
    this.updateNameInProgress = true;
    this.settingsService.updateName(this.user.name).
      subscribe(
        (response) => {
          this.updateNameInProgress = false;
          console.log(response);
          this.nameInputChanged = false;
        },
        (error) => {
          this.updateNameInProgress = false;
          console.log(error);
        }
      )
  }

  statusChanged() {
    this.statusInputChanged = true;
  }

  updateStatus() {
    this.updateStatusInProgress = true;
    this.settingsService.updateStatus(this.user.status).
      subscribe(
        (response) => {
          this.updateStatusInProgress = false;
          console.log(response);
          this.statusInputChanged = false;
        },
        (error) => {
          this.updateStatusInProgress = false;
          console.log(error);
        }
      )
  }

  updateAddress(){
    let address: Address;
    address = this.user.address;
    this.settingsService.updateAddress(address).
      subscribe(
        (response) => {
          console.log(response);
          this.user.address = response;
          this.areaChanged = false;
          this.cityChanged = false;
          this.stateChanged = false;
          this.pincodeChanged = false;
        },
        (error) => {
          console.log(error);
        }
      )
  }

  ngOnInAppUpdate(): void {
    this.fcm.currentMessage.subscribe((data) => {
      if (this.router.url == '/settings/profile' && data != null) {
        this.refresh(null);
      }
    });
  }

  refresh(event) {
    setTimeout(async () => {
      this.user = await this.loginService.getUser()
      if (event != null) {
        event.target.complete();
      }
    }, 2000);
  }

  initDataService(){

  }

  ngOnDestroy() {
    this.unRegisterToEvents();
  }

  private unRegisterToEvents() {

  }
}
