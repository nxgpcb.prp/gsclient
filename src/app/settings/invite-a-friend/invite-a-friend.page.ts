import { Component, OnInit } from '@angular/core';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-invite-a-friend',
  templateUrl: './invite-a-friend.page.html',
  styleUrls: ['./invite-a-friend.page.scss'],
})
export class InviteAFriendPage implements OnInit, OnInAppUpdateInterface {

  constructor(private platform: Platform, ) { }

  ngOnInit() {
    this.ngOnInAppUpdate()
  }

  ngOnInAppUpdate(): void {

  }

  refresh(event) {
    setTimeout(() => {

      if(event != null){
        event.target.complete();
      }
    }, 2000);
  }
}
