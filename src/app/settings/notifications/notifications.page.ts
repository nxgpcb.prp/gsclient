import { Component, OnInit } from '@angular/core';
import { messageService } from "src/messageService";
import { OnInAppUpdateInterface } from 'src/app/misc/OnInAppUpdateInterface';
import { Platform } from '@ionic/angular';
import { NotificationsService } from 'src/app/notifications/notifications.service';
import { FcmService } from 'src/app/notifications/fcm.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit, OnInAppUpdateInterface {

  showNotifications: boolean;

  constructor(private platform: Platform, private notificationService: NotificationsService, private fcmService: FcmService) { }

  ngOnInit() {
    if (localStorage.getItem('showNotifications') == undefined) {
      this.showNotifications = true;
    } else {
      this.showNotifications = JSON.parse(localStorage.getItem('showNotifications'));
    }
    this.ngOnInAppUpdate();
  }

  ngOnInAppUpdate(): void {

  }

  refresh(event) {
    setTimeout(() => {

      if (event != null) {
        event.target.complete();
      }
    }, 2000);
  }

  async updateItem(toggleValue) {
    console.log(toggleValue)
    let token = await this.fcmService.getToken();
    if (token != null) {
      if (toggleValue.detail.checked == true) {
        this.notificationService.subscribeToNotification(token)
          .subscribe(
            (Response) => {
              console.log("Subscribed to notifications");
              localStorage.setItem('showNotifications', JSON.stringify(true));
            });
      } else {
        this.notificationService.unSubscribeToNotification(token)
          .subscribe(
            (Response) => {
              console.log("Unsubscribed from notifications");
              localStorage.setItem('showNotifications', JSON.stringify(false));
            });
      }
    }
  }
}
