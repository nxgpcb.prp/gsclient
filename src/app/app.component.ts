import { Component, ViewChildren, QueryList } from '@angular/core';
import { Platform, IonRouterOutlet, ModalController, MenuController, ActionSheetController, PopoverController } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';
import { Router } from '@angular/router';
import { NetworkService } from './network-error/network.service';
import { Meta, Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { ToasterService } from './misc/toaster.service';
import { LanguageService } from './language/language.service';
import { ThemeService } from './theme/theme.service';
import { HomeService } from './home/home.service';
import { LoginService } from './login/login.service';


const { SplashScreen, StatusBar, AdMob } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})

export class AppComponent {
  onlineOffline: any;

  // set up hardware back button event.
  lastTimeBackPress = 0;
  timePeriodToExit = 500;

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  constructor(
    private platform: Platform, private router: Router, private networkService: NetworkService,
    private meta: Meta, private title: Title, public modalCtrl: ModalController,
    private menu: MenuController,
    private actionSheetCtrl: ActionSheetController,
    private popoverCtrl: PopoverController,
    private toaster: ToasterService,
    private languageService: LanguageService,
    private theme: ThemeService,
    private loginService:LoginService
  ) {
    this.initializeApp();
    //For Seo
    title.setTitle('Dwarpal - Society Security and Management App');

    meta.addTag({
      name: 'keywords',
      content: 'Dwarpal, My Gate, MyGate Security App, Apartment Security, Society Security, Society Management'
        + 'Android App , Security platform, dwarpal.co.in, Made in India, Dwarpal is a security management platform' +
        ' Resident, Owner, Family Member, Safe, Visitor, Visitor Log, Delivery Management, Daily Visitors' +
        ' Tenant, Society Notices, Society Collabration, Amenities Management, Amenities Booking, About, Web, WebApp' +
        ' Gated Security App, Pune, Startup, Free App, Ios app, Door Keeper, Protector'
    });

    meta.addTag({
      name: 'description', content: 'Dwarpal is a Society, Security and Management Platform.' +
        'Dwarpal Technologies is the startup created by 3 idiots that wanted to start something on their own.' +
        ' Dwarpal was the idea generated from the need to manage own Society and protect the family from' +
        ' unwanted visitors and lazy security guards. Dwarpal is the platform created by Dwarpal Technologies Pvt Ltd.' +
        ' which was first designed to manage society\'s visitors and their logs but in the period of development' +
        ' and design, we decided to make Dwarpal as a platform to manage the entire society.' +
        ' In the Dwarpal Technologies, we believe in the world everything is open source and everyone can learn,' +
        ' contribute and gain from it but at the same time, we want to build our company and business.' +
        ' So the Dwarpal core server will be released as open-source and the Dwarpal dashboard is released' +
        ' with a paid subscription model.'
    });


  }

  initializeApp() {
    console.log(this.platform.platforms())
    this.platform.ready().then(() => {
      if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
        // StatusBar.setStyle({
        //   style: StatusBarStyle.Dark
        // });
        // StatusBar.show();
        //Initialize admob
        if(this.platform.is('android')){
          AdMob.initialize(environment.admob.android.appId);
        }
        if(this.platform.is('ios')){
          AdMob.initialize(environment.admob.ios.appId);
        }

        this.backButtonEvent();
      }
      this.languageService.setInitialAppLanguage();
      //SplashScreen.hide();
    });
  }

  // active hardware back button
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      // close action sheet
      try {
        const element = await this.actionSheetCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close popover
      try {
        const element = await this.popoverCtrl.getTop();
        if (element) {
          if(element.backdropDismiss){
            element.dismiss();
          }
          return;
        }
      } catch (error) {
      }

      // close modal
      try {
        const element = await this.modalCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }

      // close side menu
      try {
        const element = await this.menu.getOpen();
        if (element) {
          this.menu.close();
          return;

        }

      } catch (error) {

      }

      this.routerOutlets.forEach(async(outlet: IonRouterOutlet) => {
        console.log(this.router.url);
        if (this.router.url === '/home' || this.router.url === '/login' || this.router.url === '/register'
          || this.router.url === '/resident/home/logs'
          || this.router.url === '/security/dashboard/visitor-logs'
          || this.router.url === '/society/facilities/apartment'
          || this.router.url === '/society/facilities'
          || this.router.url === '/resident/add-society'
        ) {
          if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            navigator['app'].exitApp(); // work in ionic 4

          } else {
            this.toaster.toast("Press back again to exit App.");
            this.lastTimeBackPress = new Date().getTime();
          }
        }
        else {
          if (this.router.url.includes('/resident/')) {
            await this.router.navigate(['/resident/home/logs'], { replaceUrl: true })
          }
          else if (this.router.url.includes('/security/')) {
            await this.router.navigate(['/security/dashboard/visitor-logs'], { replaceUrl: true })
          }
          else if (this.router.url.includes('/society/')) {
            await this.router.navigate(['/society/facilities/apartment'], { replaceUrl: true })
          }
          else if (this.router.url.includes('/settings/')) {
            let user = await this.loginService.getUser();
            if(user.type == "Resident"){
              await this.router.navigate(['/resident/home/logs'], { replaceUrl: true })
            }else if (user.type == "Security") {
              await this.router.navigate(['/security/dashboard/visitor-logs'], { replaceUrl: true })
            }
            else if (user.type == "Society") {
              await this.router.navigate(['/society/facilities/apartment'], { replaceUrl: true })
            }
          }
        }
      });
    });
  }

  checkIfOnline() {
    this.networkService.checkIfOnline();
  }
}
