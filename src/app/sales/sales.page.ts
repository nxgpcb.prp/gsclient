import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnquiryForm } from '../core/EnquiryForm';
import { environment } from 'src/environments/environment';
import { Meta, Title } from '@angular/platform-browser';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.page.html',
  styleUrls: ['./sales.page.scss'],
})
export class SalesPage implements OnInit {

  form: FormGroup;
  
  constructor(private router: Router, 
    private http: HttpClient,
    private formBuilder: FormBuilder, 
    private platform: Platform,
    private meta:Meta, 
    private title:Title,
    private callNumber: CallNumber
    ) {
      //For Seo
    title.setTitle('Sales - Dwarpal - Society Security and Management App');
    
    meta.addTag({ name: 'keywords', 
    content: 'Dwarpal, My Gate, MyGate Security App, Apartment Security, Society Security, Society Management'
  + 'Android App , Security platform, dwarpal.co.in, Made in India, Dwarpal is a security management platform' + 
  ' Resident, Owner, Family Member, Safe, Visitor, Visitor Log, Delivery Management, Daily Visitors' + 
  ' Tenant, Society Notices, Society Collabration, Amenities Management, Amenities Booking, About, Web, WebApp'+
  ' Gated Security App, Pune, Startup, Free App, Ios app, Door Keeper, Protector'});

    meta.addTag({ name: 'description', content: 'Dwarpal is a Society, Security and Management Platform.'+
    'Dwarpal Technologies is the startup created by 3 idiots that wanted to start something on their own.' +
    ' Dwarpal was the idea generated from the need to manage own Society and protect the family from'+
    ' unwanted visitors and lazy security guards. Dwarpal is the platform created by Dwarpal Technologies Pvt Ltd.'+
    ' which was first designed to manage society\'s visitors and their logs but in the period of development'+
    ' and design, we decided to make Dwarpal as a platform to manage the entire society.'+
    ' In the Dwarpal Technologies, we believe in the world everything is open source and everyone can learn,'+
    ' contribute and gain from it but at the same time, we want to build our company and business.'+
    ' So the Dwarpal core server will be released as open-source and the Dwarpal dashboard is released'+
    ' with a paid subscription model.' });
     }

  ngOnInit() {
    this.form = this.formBuilder.group({
      firstName:   ['', [Validators.required]],
      lastName:    ['', [Validators.required]],
      emailAdress: ['', [Validators.required]],
      body:        ['', [Validators.required]],
      phoneNumber: [],
      adress:      [],
    });
  }

  contactSales(form: FormGroup) {
    this.form.disable()
    let enquiryForm:EnquiryForm = new EnquiryForm();
    enquiryForm.firstName = this.form.get("firstName").value
    enquiryForm.lastName = this.form.get("lastName").value
    enquiryForm.emailAdress = this.form.get("emailAdress").value
    enquiryForm.body = this.form.get("body").value
    enquiryForm.phoneNumber = this.form.get("phoneNumber").value
    enquiryForm.adress = this.form.get("adress").value
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    this.http.post(environment.backend.baseURL + '/GSServer/webapi/Sales/CreateInquiry', enquiryForm, { headers: headers })
      .subscribe(
        (response) => {
          window.alert("We got your form, we will get in touch with you soon.");
          this.form.reset();
        },
        (error) => {
          window.alert("Enable to send the form. Something went wrong.");
          this.form.reset();
        }
      )
    this.form.enable()
  }

  callSales(number){
    this.callNumber.callNumber(number.toString(), true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
   }

}
