import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ProductHelpPage } from './product-help.page';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';
import { HomeFooterModule } from '../home/home-footer/home-footer.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ProductHelpPage
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeToolbarModule,
    HomeFooterModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ProductHelpPage],
  entryComponents: [
    HomePopoverMenuComponent
  ],
})
export class ProductHelpPageModule {}
