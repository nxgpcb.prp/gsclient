import { BusinessObject } from "./BusinessObject";
import { Resident } from "./Resident";

export class CommitteeMember extends BusinessObject{

  resident: Resident;
  role: String;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}