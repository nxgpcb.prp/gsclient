import { Visitor } from "./Visitor";
import { Resident } from "./Resident";
import { Facility } from "./Facility";
import { Society } from "./Society";
import { Log } from "./Log";
import { Parking } from "./Parking";

export class VisitorLog extends Log{

  visitor: Visitor = new Visitor();
  timeIn: Date;
  timeOut: Date;
  visitingTo: Facility = new Facility();
  contactPerson: Resident = new Resident();
  vehicleNo: String;
  approvalStatus: string;
  society: Society = new Society();
  isBookedInAdvance:boolean = false;
  noOfAccompanyingVisitors: number;
  visitorParking:Parking;
  vehicleType: String;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }

  getLocalTimeIn() {
    return new Date(this.timeIn)
  }
}