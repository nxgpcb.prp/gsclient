import { BusinessObject } from "./BusinessObject";

export class Storage extends BusinessObject{

  memory: number;
  linkedAccounts: number;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}