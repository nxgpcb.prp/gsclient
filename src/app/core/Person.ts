import { BusinessObject } from "./BusinessObject";
import { Image } from "./Image";

export class Person extends BusinessObject {
    name: String;
    phone: String = "";
    profilePicture: Image = new Image;

    constructor()
    {
        super();
        this.profilePicture.content = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAIaSURBVDhPYxja4OCxk9H7jpx8svfw8X/7jpz4C8S3D5447QaVJg3sO3wy/vSla/8PHj/9H2ggGB86cfb/8bMX/+87etIZqow4sGrVFTaga14dOHYKbhgMHz559j9Q7jJUKXFgz8FjRkdPn8cwDIZB3j9y9qwUVDlhcODoKcMjhAw8QoKBZ86cYQVqenngGCL8oPjf4ZPnQAZegiolHuw7dib66OkLIM1wA/cfPQmOpP3HzztAlZEGFm/ctmjngcMQAw8d+79t36H/SzZubYdKEw/edajZ3ItjubWiKfV/64KV/3rmLfoPwiD2xqrg/w+T2M586tfWhCrHD17Vy0d8aFH+/7pO/v/1SOZ/20q9/i6Y3Pp34aSmv7sK7P7ejGb+965R8f+LeqX/z+vl8Xv9VY284dsmJbDiZ3VK/57VKv5/mMnz/3Yc6787QPwoh+//M6jc6wagujrFH6/rZXHH9st6xWPvm5X/P60FGlanCNL4/3mDMhSrAA1TBouB8NNaxX+fW5X/v6xTXAnVjgqe18vogAyDaSAGPwfTin9e1stLQI1BgFcNyp0fgWGHTSMuDPTJ/y+tKv9fNyr4Q41BgEeFYofftagieZEwfgbE39pV/z8pEU+AGoMAVwIZ0t/mC/5/Wiz+/2mRGFH4GVDty2z+/9cCGCyhxqCCq34MSZcCGI5eDmQ4QBAHMBwE4kNXfRkwvUs9wMAAAGv3+iKiJ3mUAAAAAElFTkSuQmCC';
    }

    // format phone numbers as E.164
    get e164() {
        const num = "91" + this.phone;
        return `+${num}`
    }


    fromJSON(json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    }
}