import { BusinessObject } from "./BusinessObject";
import { Resident } from "./Resident";

export class Complaint extends BusinessObject{

  title: String;
  description: String;
  priority: String;
  status: String;
  complainant: Resident;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}