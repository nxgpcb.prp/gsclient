import { Visitor } from "./Visitor";
import { Resident } from "./Resident";
import { Facility } from "./Facility";
import { Society } from "./Society";
import { Log } from "./Log";

export class BookingLog extends Log{

  purpose: string;
  description: string;
  startDate: Date;
  endDate: Date;
  bookingFacility: Facility = new Facility();
  contactPerson: Resident = new Resident();
  approvalStatus: string;
  society: Society = new Society();

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}