import { BusinessObject } from "./BusinessObject";

export class SubscriptionPlan extends BusinessObject{

  name: String;
  price: String;
  storageLimit: String;
  duration: number;
  residentAccountCount: number;
  securityAccountCount: number;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}