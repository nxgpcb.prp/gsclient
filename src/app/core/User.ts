import { SubscriptionPlan } from "./SubscriptionPlan";
import { Person } from "./Person";

export class User extends Person{

    date: Date;
    emailId: String;    
    status: String;
    subscriptionPlan: SubscriptionPlan;
    isActive: Boolean;

    fromJSON(json) {
      for (var propName in json)
          this[propName] = json[propName];
      return this;
  }
}