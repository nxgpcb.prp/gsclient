import { Facility } from "./Facility";

export class Parking extends Facility{
  
  parkingType: string;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}