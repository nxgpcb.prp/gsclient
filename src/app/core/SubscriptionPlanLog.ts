import { Society } from "./Society";
import { SubscriptionPlan } from "./SubscriptionPlan";
import { Log } from "./Log";

export class SubscriptionPlanLog extends Log{

  society: Society = new Society();
  subscriptionPlan: SubscriptionPlan = new SubscriptionPlan();
  startDate: Date;
  endDate: Date;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }

}