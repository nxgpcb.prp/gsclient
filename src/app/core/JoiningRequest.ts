import { Society } from "./Society";
import { Resident } from "./Resident";
import { Facility } from "./Facility";
import { Request } from "./Request";

export class JoiningRequest extends Request{

    resident: Resident;
    society: Society;
    approvalStatus: String;
    residentType: String;
    requestedFacility:Facility;

    fromJSON(json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    }
}