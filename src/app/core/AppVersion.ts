import { BusinessObject } from "./BusinessObject";

export class AppVersion extends BusinessObject {
    server: String;
    webClient: String;
    androidAppClient: String;
    iOSAppClient: String;
    isActive: boolean;

    constructor()
    {
        super();
        this.server="1.1.8";
        this.webClient="1.1.8";
        this.androidAppClient="1.1.8";
        this.iOSAppClient="1.1.8";
        this.isActive=true;
    }

    fromJSON(json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    }
}