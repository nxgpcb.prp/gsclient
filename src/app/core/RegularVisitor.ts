import { Visitor } from "./Visitor";

export class RegularVisitor extends Visitor{

    fromJSON(json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    }
}