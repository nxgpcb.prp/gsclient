import { BusinessObject } from "./BusinessObject";

export class File extends BusinessObject {
    content: string;
    contentSize: number;
}