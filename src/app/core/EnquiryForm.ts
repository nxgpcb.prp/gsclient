export class EnquiryForm {

    firstName: String
    lastName: String
    emailAdress: String
    body: String
    phoneNumber: String
    adress: String

    fromJSON(json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    }
}