export class BusinessObject {

  id: number;
  lastModified: Date;
  type: String;
  createdOn: Date;
  _cache_pending_operation: string;

    fromJSON(json) {
      for (var propName in json)
          this[propName] = json[propName];
      return this;
  }
}