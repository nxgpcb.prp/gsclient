export class Address {

	area: any;
	city: any;
	state: any;
	pincode: any;
	mapCordinates: any;
  
    fromJSON(json) {
      for (var propName in json)
          this[propName] = json[propName];
      return this;
  }
}