import { Facility } from "./Facility";

export class Garden extends Facility{

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}