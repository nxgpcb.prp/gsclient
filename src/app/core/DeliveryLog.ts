import { Visitor } from "./Visitor";
import { Resident } from "./Resident";
import { Facility } from "./Facility";
import { Society } from "./Society";
import { Log } from "./Log";

export class DeliveryLog extends Log{

  visitor: Visitor = new Visitor();
  deliveryInTime: Date;
  deliveredTime: Date;
  visitingTo: Facility = new Facility();
  contactPerson: Resident = new Resident();
  approvalStatus: string;
  society: Society = new Society();
  isBookedInAdvance:boolean = false;
  rackNumber: string;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }

}