import { Society } from "./Society";
import { Log } from "./Log";
import { Demand } from "./Demand";
import { User } from "./User";
import { Apartment } from "./Apartment";

export class DemandLog extends Log{

  demand: Demand = new Demand();
  society: Society = new Society();
  user: User;
  balance: number;
  payee:Apartment = new Apartment();
  paymentStatus: string;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}