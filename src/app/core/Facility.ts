import { Resident } from "./Resident";
import { BusinessObject } from "./BusinessObject";

export class Facility extends BusinessObject{

  name: String;
  facilityOwner: Resident;
  facilitySubscribers: Resident[] = [];

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}