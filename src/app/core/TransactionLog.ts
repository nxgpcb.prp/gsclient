import { Society } from "./Society";
import { Log } from "./Log";
import { Transaction } from "./Transaction";
import { User } from "./User";

export class TransactionLog extends Log{

  payment: Transaction = new Transaction();
  date: Date;
  society: Society = new Society();
  user: User;
  balance: number;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}