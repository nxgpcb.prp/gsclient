import { BusinessObject } from "./BusinessObject";

export class Notice extends BusinessObject{

  title: String;
  description: String;
  validity: Date;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}