import { Visitor } from "./Visitor";
import { Facility } from "./Facility";

export class DailyVisitor extends Visitor{

    visitingTo: Facility[] = [];
  
    fromJSON(json) {
      for (var propName in json)
          this[propName] = json[propName];
      return this;
  }
}