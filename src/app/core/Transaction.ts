import { BusinessObject } from "./BusinessObject";

export class Transaction extends BusinessObject{

  name: string;
  description: string;
  transactionType: string;
  amount: number;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}