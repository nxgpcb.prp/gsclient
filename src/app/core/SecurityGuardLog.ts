import { Visitor } from "./Visitor";
import { Resident } from "./Resident";
import { Facility } from "./Facility";
import { Society } from "./Society";
import { Log } from "./Log";
import { Parking } from "./Parking";
import { SecurityGuard } from "./SecurityGuard";
import { Security } from "./Security";

export class SecurityGuardLog extends Log{

  securityGuard: SecurityGuard = new SecurityGuard();
  securityAccount: Security = new Security();
  timeIn: Date;
  timeOut: Date;
  society: Society = new Society();

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }

  getLocalTimeIn() {
    return new Date(this.timeIn)
  }
}