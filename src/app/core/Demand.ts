import { BusinessObject } from "./BusinessObject";

export class Demand extends BusinessObject{

  name: string;
  description: string;
  amount: number;
  dueDate: Date;

  fromJSON(json) {
    for (var propName in json)
      this[propName] = json[propName];
    return this;
  }
}