import { BusinessObject } from "./BusinessObject";

export class Log extends BusinessObject{

    fromJSON(json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    }
}