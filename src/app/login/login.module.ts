import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';
import { HomePopoverMenuComponent } from '../home/home-popover-menu/home-popover-menu.component';
import { HomeToolbarModule } from '../home/home-toolbar/home-toolbar.module';
import { TranslateModule } from '@ngx-translate/core';
import { LanguagePopoverPage } from '../language/language-popover/language-popover.page';
import { LanguagePopoverPageModule } from '../language/language-popover/language-popover.module';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    HomeToolbarModule,
    LanguagePopoverPageModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [LoginPage],
  entryComponents: [
    HomePopoverMenuComponent,
    LanguagePopoverPage
  ],
})
export class LoginPageModule {}
