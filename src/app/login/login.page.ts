import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Plugins } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { Cache } from '../home/services/cache';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userType: any;
  username: string = "";
  password: string = "";
  verified: boolean = false;
  verificationFailed: boolean = false;
  keepMeLoggedIn = true;
  form: FormGroup;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private platform: Platform
  ) {

  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      keepMeLoggedIn: [],
      keepMeLoggedIn2: [],
    });
    //Set default value to true
    this.form.get('keepMeLoggedIn').setValue('true');
    this.form.get('keepMeLoggedIn2').setValue('true');
  }

  registerPage() {
    this.router.navigate(['/register']);
  }

  login(form: FormGroup) {
    this.form.disable()
    //Clear the cache before login
    Cache.purgeLocalCache()
      .then(
        res => {
          this.loginService.login(this.form.get('username').value, this.form.get('password').value, this.form.get('keepMeLoggedIn').value)
          this.form.enable()
        }
      )
  }

  loginWithGoogle() {
    Cache.purgeLocalCache()
      .then(
        res => {
          if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
            this.loginService.nativeGoogleLogin(this.form.get('keepMeLoggedIn2').value);
          } else {
            this.loginService.webGoogleLogin(this.form.get('keepMeLoggedIn2').value);
          }
        })
  }

}
