import { Injectable } from '@angular/core';

import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import { RegisterService } from '../register/register.service';
import { LoaderService } from '../misc/loader.service';
import { ToasterService } from '../misc/toaster.service';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';
import { NetworkService } from '../network-error/network.service';
import { AdOptions, AdSize, AdPosition } from 'capacitor-admob';
import { Plugins } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { Firebase } from '@ionic-native/firebase/ngx';
import { NotificationsService } from '../notifications/notifications.service';
import { FcmService } from '../notifications/fcm.service';
import { AdService } from '../home/ad/ad.service';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { Cache } from '../home/services/cache';
import idb from '../home/services/idb-promised.js';
import { CacheService } from '../home/services/cache.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  user: any = new Object();
  idTokenChangeListener: any;
  user$: Observable<any>;
  userCache: Cache = null;

  constructor(private afAuth: AngularFireAuth,
    private router: Router,
    private http: HttpClient,
    private registerService: RegisterService,
    private loader: LoaderService,
    private adService: AdService,
    private notificationService: NotificationsService,
    private fcmService: FcmService,
    private cacheService: CacheService,
    private googlePlus: GooglePlus,
    private platform: Platform
    ) {
    this.user$ = this.afAuth.authState;
    this.init();
  }

  private async init() {
    console.log('init()');
    this.userCache = await new Cache(this.http,
      'user',
      environment.backend.baseURL + '/GSServer/webapi/secured/User',
      Cache.type_NATIVE);
  }

  login(username: string, password: string, keepMeLoggedIn: boolean) {
    console.log('login()');
    this.loader.showLoader();

    this.afAuth.auth.setPersistence(this.getPesistanace(keepMeLoggedIn))
      .then(result => {
        this.afAuth.auth.signInWithEmailAndPassword(username, password)
          .then(result => {
            result.user.getIdToken(false).then(
              idToken => {
                idToken = idToken;

                if (result.user.emailVerified !== true) {
                  this.registerService.sendEmailVerificationLink(result.user.email);
                  this.loader.hideLoader()
                  window.alert('Please validate your email address. Kindly check your inbox.');
                }
                else {
                  this.loginToDb()
                    .subscribe(
                      (response: any) => {
                        this.user = response;
                        localStorage.setItem('currentUserId', this.user.id);
                        if (this.user.name != null) {
                          if (this.user.type == "Resident") {
                            this.loader.hideLoader();
                            this.router.navigate(['/resident/home/logs'], { replaceUrl: true });
                          }
                          if (this.user.type == "Security") {
                            this.loader.hideLoader();
                            this.router.navigate(['/security/dashboard/visitor-logs'], { replaceUrl: true });
                          }
                          if (this.user.type == "Society") {
                            this.loader.hideLoader();
                            this.router.navigate(['/society/facilities/apartment'], { replaceUrl: true });
                          }
                        } else {
                          this.loader.hideLoader();
                          this.router.navigate(['/welcome'], { replaceUrl: true });
                        }
                      },
                      (error) => {
                        this.loader.hideLoader();
                        this.router.navigate(['/login'], { replaceUrl: true });
                      }
                    );
                }
              });
          })
          .catch(error => {
            this.loader.hideLoader()
            console.log(error);
            if (error.code === "auth/network-request-failed") {
              window.alert('You do not have active internet connection!!!');
            } else if (error.code === "auth/wrong-password" || error.code === "auth/user-not-found" || error.code === "auth/invalid-email") {
              window.alert('Username or Password is incorrect');
            } else if (error.code === "auth/user-disabled") {
              window.alert('Your account has been disabled');
            } else {
              window.alert('Something went wrong');
            }
          });
      })
      .catch(function (error) {
        this.loader.hideLoader()
        console.log(error)
        console.log('Firebase remember me error ' + error.message);
      });
  }

  async nativeGoogleLogin(keepMeLoggedIn:boolean) {
    this.loader.showLoader();
    console.log("nativeGoogleLogin : " );
    const gplusUser = await this.googlePlus.login({
      'webClientId': environment.webClientId,
      'offline': true,
      'scopes': 'profile email'
    })

    console.log("signInWithCredential : " + gplusUser.idToken);
    this.afAuth.auth.setPersistence(this.getPesistanace(keepMeLoggedIn))
      .then(result => {
    this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken))
      .then(result => {
        console.log("result : " + result);
        result.user.getIdToken(false).then(
          idToken => {
            idToken = idToken;
            console.log("idToken : " + idToken);


            if (result.user.emailVerified !== true) {
              this.registerService.sendEmailVerificationLink(result.user.email);
              this.loader.hideLoader()
              window.alert('Please validate your email address. Kindly check your inbox.');
            }
            else {
              this.loginToDb()
                .subscribe(
                  (response: any) => {
                    this.user = response;
                    localStorage.setItem('currentUserId', this.user.id);
                    if (this.user.name != null) {
                      if (this.user.type == "Resident") {
                        this.loader.hideLoader();
                        this.router.navigate(['/resident/home/logs'], { replaceUrl: true });
                      }
                      if (this.user.type == "Security") {
                        this.loader.hideLoader();
                        this.router.navigate(['/security/dashboard/visitor-logs'], { replaceUrl: true });
                      }
                      if (this.user.type == "Society") {
                        this.loader.hideLoader();
                        this.router.navigate(['/society/facilities/apartment'], { replaceUrl: true });
                      }
                    } else {
                      this.loader.hideLoader();
                      this.router.navigate(['/welcome'], { replaceUrl: true });
                    }
                  },
                  (error) => {
                    this.loader.hideLoader();
                    this.router.navigate(['/login'], { replaceUrl: true });
                  }
                );
            }
          });
      })
      .catch(error => {
        this.loader.hideLoader()
        console.log(error);
        if (error.code === "auth/network-request-failed") {
          window.alert('You do not have active internet connection!!!');
        } else if (error.code === "auth/wrong-password" || error.code === "auth/user-not-found" || error.code === "auth/invalid-email") {
          window.alert('Username or Password is incorrect');
        } else if (error.code === "auth/user-disabled") {
          window.alert('Your account has been disabled');
        } else {
          window.alert('Something went wrong');
        }
      });
    });
  }

  webGoogleLogin(keepMeLoggedIn:boolean) {
    try {
      this.loader.showLoader();
      const provider = new firebase.auth.GoogleAuthProvider();

      this.afAuth.auth.setPersistence(this.getPesistanace(keepMeLoggedIn))
      .then(result => {
      this.afAuth.auth.signInWithPopup(provider)
        .then(result => {
          result.user.getIdToken(false).then(
            idToken => {
              idToken = idToken;

              if (result.user.emailVerified !== true) {
                this.registerService.sendEmailVerificationLink(result.user.email);
                this.loader.hideLoader()
                window.alert('Please validate your email address. Kindly check your inbox.');
              }
              else {
                this.loginToDb()
                  .subscribe(
                    (response: any) => {
                      this.user = response;
                      localStorage.setItem('currentUserId', this.user.id);
                      if (this.user.name != null) {
                        if (this.user.type == "Resident") {
                          this.loader.hideLoader();
                          this.router.navigate(['/resident/home/logs'], { replaceUrl: true });
                        }
                        if (this.user.type == "Security") {
                          this.loader.hideLoader();
                          this.router.navigate(['/security/dashboard/visitor-logs'], { replaceUrl: true });
                        }
                        if (this.user.type == "Society") {
                          this.loader.hideLoader();
                          this.router.navigate(['/society/facilities/apartment'], { replaceUrl: true });
                        }
                      } else {
                        this.loader.hideLoader();
                        this.router.navigate(['/welcome'], { replaceUrl: true });
                      }
                    },
                    (error) => {
                      this.loader.hideLoader();
                      this.router.navigate(['/login'], { replaceUrl: true });
                    }
                  );
              }
            });
        })
        .catch(error => {
          this.loader.hideLoader()
          console.log(error);
          if (error.code === "auth/network-request-failed") {
            window.alert('You do not have active internet connection!!!');
          } else if (error.code === "auth/wrong-password" || error.code === "auth/user-not-found" || error.code === "auth/invalid-email") {
            window.alert('Username or Password is incorrect');
          } else if (error.code === "auth/user-disabled") {
            window.alert('Your account has been disabled');
          } else {
            window.alert('Something went wrong');
          }
        });
      });
    } catch (error) {
      this.loader.hideLoader()
      console.log(error)
    };
  }

  async logOut() {
    this.loader.showLoader();
    if (localStorage.getItem('showNotifications') != null && JSON.parse(localStorage.getItem('showNotifications'))) {
      let token = this.fcmService.getToken();
      if (token != null) {
        this.notificationService.unSubscribeToNotification(token)
          .subscribe(
            (Response) => {
              console.log("Unsubscribed from notifications");
              this.afAuth.auth.signOut()
                .then(
                  (response) => {
                    console.log("User signed out successfully");
                    Cache.purgeLocalCache();
                    localStorage.removeItem('currentUserId');
                    localStorage.removeItem('currentUserIdToken');

                    // Unscubscribe admob
                    this.adService.unSubscribeAds();

                    //Remove the history after signout
                    this.router.navigate(['/login'], { replaceUrl: true })
                      .finally(
                        () => {
                          this.loader.hideLoader();
                        }
                      )
                  },
                  (error) => {
                    alert("Something went wrong : " + error);
                    this.loader.hideLoader();
                  }
                );
                if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
                  this.googlePlus.logout().then(
                    res => {
                      this.loader.hideLoader();
                    }
                  )
                }
            },
            (error) => {
              alert("Something went wrong : " + error);
              this.loader.hideLoader();
            }
          )
      } else {
        this.afAuth.auth.signOut()
          .then(
            (response) => {
              console.log("User signed out successfully");
              Cache.purgeLocalCache();
              localStorage.removeItem('currentUserId');
              localStorage.removeItem('currentUserIdToken');

              // Unscubscribe admob
              this.adService.unSubscribeAds();

              //Remove the history after signout
              this.router.navigate(['/login'], { replaceUrl: true })
                .finally(
                  () => {
                    this.loader.hideLoader();
                  }
                )
            },
            (error) => {
              alert("Something went wrong : " + error);
              this.loader.hideLoader();
            }
          );

          if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
            this.googlePlus.logout().then(
              res => {
                this.loader.hideLoader();
              }
            )
          }
      }
    } else {
      console.log("Unsubscribed from notifications");
      this.afAuth.auth.signOut()
        .then(
          (response) => {
            console.log("User signed out successfully");
            Cache.purgeLocalCache();
            localStorage.removeItem('currentUserId');
            localStorage.removeItem('currentUserIdToken');

            // Unscubscribe admob
            this.adService.unSubscribeAds();

            //Remove the history after signout
            this.router.navigate(['/login'], { replaceUrl: true })
              .finally(
                () => {
                  this.loader.hideLoader();
                }
              )
          },
          (error) => {
            alert("Something went wrong : " + error);
            this.loader.hideLoader();
          }
        );

        if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('mobileweb')) {
          this.googlePlus.logout().then(
            res => {
              this.loader.hideLoader();
            }
          )
        }
    }
  }

  homePage() {
    console.log("homePage")
    //Remove the history after login
    location.replace('/');
    this.router.navigate(['/'], { replaceUrl: true });

  }

  getPesistanace(keepMeLoggedIn: boolean) {
    console.log('getPesistanace()');
    if (keepMeLoggedIn) {
      return firebase.auth.Auth.Persistence.LOCAL;
    } else {
      return firebase.auth.Auth.Persistence.NONE;
    }
  }

  async getUser() {
    console.log('getUser()');
    if (this.userCache == undefined) {
      this.userCache = await new Cache(this.http, 'user', environment.backend.baseURL + '/GSServer/webapi/secured/User', Cache.type_NATIVE);
    }

    let id = JSON.parse(localStorage.getItem('currentUserId'));
    if (id != null) {
      await this.userCache.getObject(id);
      return await this.userCache.get(id);
    } else {
      console.log("null")
      return Promise.reject()
    }
  }

  loginToDb() {
    console.log('loginToDb()');
    let idToken = JSON.parse(localStorage.getItem('currentUserIdToken'));

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Basic " + idToken
    });
    return this.http.get(environment.backend.baseURL + '/GSServer/webapi/secured/User', { headers: headers })
  }

  registerForIdToken() {
    console.log('registerForIdToken()');
    this.afAuth.auth.onIdTokenChanged(
      user => {
        if (user) {
          user.getIdToken(true)
            .then(
              idToken => {
                idToken = idToken;
                localStorage.setItem('currentUserIdToken', JSON.stringify(idToken));
                console.log("Changed Token : " + idToken)
              })
        }
      });
  }

}
