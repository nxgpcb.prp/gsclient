//
//  Market.m
//  App
//
//  Created by Prasad on 02/09/20.
//

#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

// Define the plugin using the CAP_PLUGIN Macro, and
// each method the plugin supports using the CAP_PLUGIN_METHOD macro.
CAP_PLUGIN(Market, "Market",
                     
           CAP_PLUGIN_METHOD(open, CAPPluginReturnNone );

)
