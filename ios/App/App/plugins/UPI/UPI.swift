import Capacitor

@objc(UPI)
public class UPI: CAPPlugin {
  @objc func pay(_ call: CAPPluginCall) {
    let amount = call.getString("amount") ?? ""
    let upiId = call.getString("upiId") ?? ""
    let name = call.getString("name") ?? ""
    let note = call.getString("note") ?? ""

    if (UIApplication.shared.canOpenURL(URL(string:"upi://pay")!)) {
      UIApplication.shared.openURL(URL(string:
          "upi://pay?pa="+upiId+"&pn="+name+"&tn="+note+"&am="+amount+"&cu=INR")!)
    } else {
      NSLog("No UPI app found, please install one to continue");
    }
    call.resolve()
  }
}
