import Foundation
import Capacitor

@objc(Market)
public class Market: CAPPlugin {
  @objc func open(_ call: CAPPluginCall) {
    let appId = call.getString("appId") ?? ""

    if ((appId.elementsEqual("")) == false) {
        if (UIApplication.shared.canOpenURL(URL(string:"itms-apps://itunes.apple.com/app/id"+appId)!)) {
          UIApplication.shared.openURL(URL(string:
              "itms-apps://itunes.apple.com/app/id"+appId)!)
        } else {
          NSLog("Invalid application id");
        }
    } else {
      NSLog("Invalid application id");
    }
    call.resolve()
  }
}
